/**
 * @license AngularJS v1.4.8
 * (c) 2010-2015 Google, Inc. http://angularjs.org
 * License: MIT
 */
(function(window, document, undefined) {'use strict';

/**
 * @description
 *
 * This object provides a utility for producing rich Error messages within
 * Angular. It can be called as follows:
 *
 * var exampleMinErr = minErr('example');
 * throw exampleMinErr('one', 'This {0} is {1}', foo, bar);
 *
 * The above creates an instance of minErr in the example namespace. The
 * resulting error will have a namespaced error code of example.one.  The
 * resulting error will replace {0} with the value of foo, and {1} with the
 * value of bar. The object is not restricted in the number of arguments it can
 * take.
 *
 * If fewer arguments are specified than necessary for interpolation, the extra
 * interpolation markers will be preserved in the final string.
 *
 * Since data will be parsed statically during a build step, some restrictions
 * are applied with respect to how minErr instances are created and called.
 * Instances should have names of the form namespaceMinErr for a minErr created
 * using minErr('namespace') . Error codes, namespaces and template strings
 * should all be static strings, not variables or general expressions.
 *
 * @param {string} module The namespace to use for the new minErr instance.
 * @param {function} ErrorConstructor Custom error constructor to be instantiated when returning
 *   error from returned function, for cases when a particular type of error is useful.
 * @returns {function(code:string, template:string, ...templateArgs): Error} minErr instance
 */

function minErr(module, ErrorConstructor) {
  ErrorConstructor = ErrorConstructor || Error;
  return function() {
    var SKIP_INDEXES = 2;

    var templateArgs = arguments,
      code = templateArgs[0],
      message = '[' + (module ? module + ':' : '') + code + '] ',
      template = templateArgs[1],
      paramPrefix, i;

    message += template.replace(/\{\d+\}/g, function(match) {
      var index = +match.slice(1, -1),
        shiftedIndex = index + SKIP_INDEXES;

      if (shiftedIndex < templateArgs.length) {
        return toDebugString(templateArgs[shiftedIndex]);
      }

      return match;
    });

    message += '\nhttp://errors.angularjs.org/1.4.8/' +
      (module ? module + '/' : '') + code;

    for (i = SKIP_INDEXES, paramPrefix = '?'; i < templateArgs.length; i++, paramPrefix = '&') {
      message += paramPrefix + 'p' + (i - SKIP_INDEXES) + '=' +
        encodeURIComponent(toDebugString(templateArgs[i]));
    }

    return new ErrorConstructor(message);
  };
}

/* We need to tell jshint what variables are being exported */
/* global angular: true,
  msie: true,
  jqLite: true,
  jQuery: true,
  slice: true,
  splice: true,
  push: true,
  toString: true,
  ngMinErr: true,
  angularModule: true,
  uid: true,
  REGEX_STRING_REGEXP: true,
  VALIDITY_STATE_PROPERTY: true,

  lowercase: true,
  uppercase: true,
  manualLowercase: true,
  manualUppercase: true,
  nodeName_: true,
  isArrayLike: true,
  forEach: true,
  forEachSorted: true,
  reverseParams: true,
  nextUid: true,
  setHashKey: true,
  extend: true,
  toInt: true,
  inherit: true,
  merge: true,
  noop: true,
  identity: true,
  valueFn: true,
  isUndefined: true,
  isDefined: true,
  isObject: true,
  isBlankObject: true,
  isString: true,
  isNumber: true,
  isDate: true,
  isArray: true,
  isFunction: true,
  isRegExp: true,
  isWindow: true,
  isScope: true,
  isFile: true,
  isFormData: true,
  isBlob: true,
  isBoolean: true,
  isPromiseLike: true,
  trim: true,
  escapeForRegexp: true,
  isElement: true,
  makeMap: true,
  includes: true,
  arrayRemove: true,
  copy: true,
  shallowCopy: true,
  equals: true,
  csp: true,
  jq: true,
  concat: true,
  sliceArgs: true,
  bind: true,
  toJsonReplacer: true,
  toJson: true,
  fromJson: true,
  convertTimezoneToLocal: true,
  timezoneToOffset: true,
  startingTag: true,
  tryDecodeURIComponent: true,
  parseKeyValue: true,
  toKeyValue: true,
  encodeUriSegment: true,
  encodeUriQuery: true,
  angularInit: true,
  bootstrap: true,
  getTestability: true,
  snake_case: true,
  bindJQuery: true,
  assertArg: true,
  assertArgFn: true,
  assertNotHasOwnProperty: true,
  getter: true,
  getBlockNodes: true,
  hasOwnProperty: true,
  createMap: true,

  NODE_TYPE_ELEMENT: true,
  NODE_TYPE_ATTRIBUTE: true,
  NODE_TYPE_TEXT: true,
  NODE_TYPE_COMMENT: true,
  NODE_TYPE_DOCUMENT: true,
  NODE_TYPE_DOCUMENT_FRAGMENT: true,
*/

////////////////////////////////////

/**
 * @ngdoc module
 * @name ng
 * @module ng
 * @description
 *
 * # ng (core module)
 * The ng module is loaded by default when an AngularJS application is started. The module itself
 * contains the essential components for an AngularJS application to function. The table below
 * lists a high level breakdown of each of the services/factories, filters, directives and testing
 * components available within this core module.
 *
 * <div doc-module-components="ng"></div>
 */

var REGEX_STRING_REGEXP = /^\/(.+)\/([a-z]*)$/;

// The name of a form control's ValidityState property.
// This is used so that it's possible for internal tests to create mock ValidityStates.
var VALIDITY_STATE_PROPERTY = 'validity';

/**
 * @ngdoc function
 * @name angular.lowercase
 * @module ng
 * @kind function
 *
 * @description Converts the specified string to lowercase.
 * @param {string} string String to be converted to lowercase.
 * @returns {string} Lowercased string.
 */
var lowercase = function(string) {return isString(string) ? string.toLowerCase() : string;};
var hasOwnProperty = Object.prototype.hasOwnProperty;

/**
 * @ngdoc function
 * @name angular.uppercase
 * @module ng
 * @kind function
 *
 * @description Converts the specified string to uppercase.
 * @param {string} string String to be converted to uppercase.
 * @returns {string} Uppercased string.
 */
var uppercase = function(string) {return isString(string) ? string.toUpperCase() : string;};


var manualLowercase = function(s) {
  /* jshint bitwise: false */
  return isString(s)
      ? s.replace(/[A-Z]/g, function(ch) {return String.fromCharCode(ch.charCodeAt(0) | 32);})
      : s;
};
var manualUppercase = function(s) {
  /* jshint bitwise: false */
  return isString(s)
      ? s.replace(/[a-z]/g, function(ch) {return String.fromCharCode(ch.charCodeAt(0) & ~32);})
      : s;
};


// String#toLowerCase and String#toUpperCase don't produce correct results in browsers with Turkish
// locale, for this reason we need to detect this case and redefine lowercase/uppercase methods
// with correct but slower alternatives.
if ('i' !== 'I'.toLowerCase()) {
  lowercase = manualLowercase;
  uppercase = manualUppercase;
}


var
    msie,             // holds major version number for IE, or NaN if UA is not IE.
    jqLite,           // delay binding since jQuery could be loaded after us.
    jQuery,           // delay binding
    slice             = [].slice,
    splice            = [].splice,
    push              = [].push,
    toString          = Object.prototype.toString,
    getPrototypeOf    = Object.getPrototypeOf,
    ngMinErr          = minErr('ng'),

    /** @name angular */
    angular           = window.angular || (window.angular = {}),
    angularModule,
    uid               = 0;

/**
 * documentMode is an IE-only property
 * http://msdn.microsoft.com/en-us/library/ie/cc196988(v=vs.85).aspx
 */
msie = document.documentMode;


/**
 * @private
 * @param {*} obj
 * @return {boolean} Returns true if `obj` is an array or array-like object (NodeList, Arguments,
 *                   String ...)
 */
function isArrayLike(obj) {

  // `null`, `undefined` and `window` are not array-like
  if (obj == null || isWindow(obj)) return false;

  // arrays, strings and jQuery/jqLite objects are array like
  // * jqLite is either the jQuery or jqLite constructor function
  // * we have to check the existance of jqLite first as this method is called
  //   via the forEach method when constructing the jqLite object in the first place
  if (isArray(obj) || isString(obj) || (jqLite && obj instanceof jqLite)) return true;

  // Support: iOS 8.2 (not reproducible in simulator)
  // "length" in obj used to prevent JIT error (gh-11508)
  var length = "length" in Object(obj) && obj.length;

  // NodeList objects (with `item` method) and
  // other objects with suitable length characteristics are array-like
  return isNumber(length) &&
    (length >= 0 && (length - 1) in obj || typeof obj.item == 'function');
}

/**
 * @ngdoc function
 * @name angular.forEach
 * @module ng
 * @kind function
 *
 * @description
 * Invokes the `iterator` function once for each item in `obj` collection, which can be either an
 * object or an array. The `iterator` function is invoked with `iterator(value, key, obj)`, where `value`
 * is the value of an object property or an array element, `key` is the object property key or
 * array element index and obj is the `obj` itself. Specifying a `context` for the function is optional.
 *
 * It is worth noting that `.forEach` does not iterate over inherited properties because it filters
 * using the `hasOwnProperty` method.
 *
 * Unlike ES262's
 * [Array.prototype.forEach](http://www.ecma-international.org/ecma-262/5.1/#sec-15.4.4.18),
 * Providing 'undefined' or 'null' values for `obj` will not throw a TypeError, but rather just
 * return the value provided.
 *
   ```js
     var values = {name: 'misko', gender: 'male'};
     var log = [];
     angular.forEach(values, function(value, key) {
       this.push(key + ': ' + value);
     }, log);
     expect(log).toEqual(['name: misko', 'gender: male']);
   ```
 *
 * @param {Object|Array} obj Object to iterate over.
 * @param {Function} iterator Iterator function.
 * @param {Object=} context Object to become context (`this`) for the iterator function.
 * @returns {Object|Array} Reference to `obj`.
 */

function forEach(obj, iterator, context) {
  var key, length;
  if (obj) {
    if (isFunction(obj)) {
      for (key in obj) {
        // Need to check if hasOwnProperty exists,
        // as on IE8 the result of querySelectorAll is an object without a hasOwnProperty function
        if (key != 'prototype' && key != 'length' && key != 'name' && (!obj.hasOwnProperty || obj.hasOwnProperty(key))) {
          iterator.call(context, obj[key], key, obj);
        }
      }
    } else if (isArray(obj) || isArrayLike(obj)) {
      var isPrimitive = typeof obj !== 'object';
      for (key = 0, length = obj.length; key < length; key++) {
        if (isPrimitive || key in obj) {
          iterator.call(context, obj[key], key, obj);
        }
      }
    } else if (obj.forEach && obj.forEach !== forEach) {
        obj.forEach(iterator, context, obj);
    } else if (isBlankObject(obj)) {
      // createMap() fast path --- Safe to avoid hasOwnProperty check because prototype chain is empty
      for (key in obj) {
        iterator.call(context, obj[key], key, obj);
      }
    } else if (typeof obj.hasOwnProperty === 'function') {
      // Slow path for objects inheriting Object.prototype, hasOwnProperty check needed
      for (key in obj) {
        if (obj.hasOwnProperty(key)) {
          iterator.call(context, obj[key], key, obj);
        }
      }
    } else {
      // Slow path for objects which do not have a method `hasOwnProperty`
      for (key in obj) {
        if (hasOwnProperty.call(obj, key)) {
          iterator.call(context, obj[key], key, obj);
        }
      }
    }
  }
  return obj;
}

function forEachSorted(obj, iterator, context) {
  var keys = Object.keys(obj).sort();
  for (var i = 0; i < keys.length; i++) {
    iterator.call(context, obj[keys[i]], keys[i]);
  }
  return keys;
}


/**
 * when using forEach the params are value, key, but it is often useful to have key, value.
 * @param {function(string, *)} iteratorFn
 * @returns {function(*, string)}
 */
function reverseParams(iteratorFn) {
  return function(value, key) { iteratorFn(key, value); };
}

/**
 * A consistent way of creating unique IDs in angular.
 *
 * Using simple numbers allows us to generate 28.6 million unique ids per second for 10 years before
 * we hit number precision issues in JavaScript.
 *
 * Math.pow(2,53) / 60 / 60 / 24 / 365 / 10 = 28.6M
 *
 * @returns {number} an unique alpha-numeric string
 */
function nextUid() {
  return ++uid;
}


/**
 * Set or clear the hashkey for an object.
 * @param obj object
 * @param h the hashkey (!truthy to delete the hashkey)
 */
function setHashKey(obj, h) {
  if (h) {
    obj.$$hashKey = h;
  } else {
    delete obj.$$hashKey;
  }
}


function baseExtend(dst, objs, deep) {
  var h = dst.$$hashKey;

  for (var i = 0, ii = objs.length; i < ii; ++i) {
    var obj = objs[i];
    if (!isObject(obj) && !isFunction(obj)) continue;
    var keys = Object.keys(obj);
    for (var j = 0, jj = keys.length; j < jj; j++) {
      var key = keys[j];
      var src = obj[key];

      if (deep && isObject(src)) {
        if (isDate(src)) {
          dst[key] = new Date(src.valueOf());
        } else if (isRegExp(src)) {
          dst[key] = new RegExp(src);
        } else if (src.nodeName) {
          dst[key] = src.cloneNode(true);
        } else if (isElement(src)) {
          dst[key] = src.clone();
        } else {
          if (!isObject(dst[key])) dst[key] = isArray(src) ? [] : {};
          baseExtend(dst[key], [src], true);
        }
      } else {
        dst[key] = src;
      }
    }
  }

  setHashKey(dst, h);
  return dst;
}

/**
 * @ngdoc function
 * @name angular.extend
 * @module ng
 * @kind function
 *
 * @description
 * Extends the destination object `dst` by copying own enumerable properties from the `src` object(s)
 * to `dst`. You can specify multiple `src` objects. If you want to preserve original objects, you can do so
 * by passing an empty object as the target: `var object = angular.extend({}, object1, object2)`.
 *
 * **Note:** Keep in mind that `angular.extend` does not support recursive merge (deep copy). Use
 * {@link angular.merge} for this.
 *
 * @param {Object} dst Destination object.
 * @param {...Object} src Source object(s).
 * @returns {Object} Reference to `dst`.
 */
function extend(dst) {
  return baseExtend(dst, slice.call(arguments, 1), false);
}


/**
* @ngdoc function
* @name angular.merge
* @module ng
* @kind function
*
* @description
* Deeply extends the destination object `dst` by copying own enumerable properties from the `src` object(s)
* to `dst`. You can specify multiple `src` objects. If you want to preserve original objects, you can do so
* by passing an empty object as the target: `var object = angular.merge({}, object1, object2)`.
*
* Unlike {@link angular.extend extend()}, `merge()` recursively descends into object properties of source
* objects, performing a deep copy.
*
* @param {Object} dst Destination object.
* @param {...Object} src Source object(s).
* @returns {Object} Reference to `dst`.
*/
function merge(dst) {
  return baseExtend(dst, slice.call(arguments, 1), true);
}



function toInt(str) {
  return parseInt(str, 10);
}


function inherit(parent, extra) {
  return extend(Object.create(parent), extra);
}

/**
 * @ngdoc function
 * @name angular.noop
 * @module ng
 * @kind function
 *
 * @description
 * A function that performs no operations. This function can be useful when writing code in the
 * functional style.
   ```js
     function foo(callback) {
       var result = calculateResult();
       (callback || angular.noop)(result);
     }
   ```
 */
function noop() {}
noop.$inject = [];


/**
 * @ngdoc function
 * @name angular.identity
 * @module ng
 * @kind function
 *
 * @description
 * A function that returns its first argument. This function is useful when writing code in the
 * functional style.
 *
   ```js
     function transformer(transformationFn, value) {
       return (transformationFn || angular.identity)(value);
     };
   ```
  * @param {*} value to be returned.
  * @returns {*} the value passed in.
 */
function identity($) {return $;}
identity.$inject = [];


function valueFn(value) {return function() {return value;};}

function hasCustomToString(obj) {
  return isFunction(obj.toString) && obj.toString !== toString;
}


/**
 * @ngdoc function
 * @name angular.isUndefined
 * @module ng
 * @kind function
 *
 * @description
 * Determines if a reference is undefined.
 *
 * @param {*} value Reference to check.
 * @returns {boolean} True if `value` is undefined.
 */
function isUndefined(value) {return typeof value === 'undefined';}


/**
 * @ngdoc function
 * @name angular.isDefined
 * @module ng
 * @kind function
 *
 * @description
 * Determines if a reference is defined.
 *
 * @param {*} value Reference to check.
 * @returns {boolean} True if `value` is defined.
 */
function isDefined(value) {return typeof value !== 'undefined';}


/**
 * @ngdoc function
 * @name angular.isObject
 * @module ng
 * @kind function
 *
 * @description
 * Determines if a reference is an `Object`. Unlike `typeof` in JavaScript, `null`s are not
 * considered to be objects. Note that JavaScript arrays are objects.
 *
 * @param {*} value Reference to check.
 * @returns {boolean} True if `value` is an `Object` but not `null`.
 */
function isObject(value) {
  // http://jsperf.com/isobject4
  return value !== null && typeof value === 'object';
}


/**
 * Determine if a value is an object with a null prototype
 *
 * @returns {boolean} True if `value` is an `Object` with a null prototype
 */
function isBlankObject(value) {
  return value !== null && typeof value === 'object' && !getPrototypeOf(value);
}


/**
 * @ngdoc function
 * @name angular.isString
 * @module ng
 * @kind function
 *
 * @description
 * Determines if a reference is a `String`.
 *
 * @param {*} value Reference to check.
 * @returns {boolean} True if `value` is a `String`.
 */
function isString(value) {return typeof value === 'string';}


/**
 * @ngdoc function
 * @name angular.isNumber
 * @module ng
 * @kind function
 *
 * @description
 * Determines if a reference is a `Number`.
 *
 * This includes the "special" numbers `NaN`, `+Infinity` and `-Infinity`.
 *
 * If you wish to exclude these then you can use the native
 * [`isFinite'](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/isFinite)
 * method.
 *
 * @param {*} value Reference to check.
 * @returns {boolean} True if `value` is a `Number`.
 */
function isNumber(value) {return typeof value === 'number';}


/**
 * @ngdoc function
 * @name angular.isDate
 * @module ng
 * @kind function
 *
 * @description
 * Determines if a value is a date.
 *
 * @param {*} value Reference to check.
 * @returns {boolean} True if `value` is a `Date`.
 */
function isDate(value) {
  return toString.call(value) === '[object Date]';
}


/**
 * @ngdoc function
 * @name angular.isArray
 * @module ng
 * @kind function
 *
 * @description
 * Determines if a reference is an `Array`.
 *
 * @param {*} value Reference to check.
 * @returns {boolean} True if `value` is an `Array`.
 */
var isArray = Array.isArray;

/**
 * @ngdoc function
 * @name angular.isFunction
 * @module ng
 * @kind function
 *
 * @description
 * Determines if a reference is a `Function`.
 *
 * @param {*} value Reference to check.
 * @returns {boolean} True if `value` is a `Function`.
 */
function isFunction(value) {return typeof value === 'function';}


/**
 * Determines if a value is a regular expression object.
 *
 * @private
 * @param {*} value Reference to check.
 * @returns {boolean} True if `value` is a `RegExp`.
 */
function isRegExp(value) {
  return toString.call(value) === '[object RegExp]';
}


/**
 * Checks if `obj` is a window object.
 *
 * @private
 * @param {*} obj Object to check
 * @returns {boolean} True if `obj` is a window obj.
 */
function isWindow(obj) {
  return obj && obj.window === obj;
}


function isScope(obj) {
  return obj && obj.$evalAsync && obj.$watch;
}


function isFile(obj) {
  return toString.call(obj) === '[object File]';
}


function isFormData(obj) {
  return toString.call(obj) === '[object FormData]';
}


function isBlob(obj) {
  return toString.call(obj) === '[object Blob]';
}


function isBoolean(value) {
  return typeof value === 'boolean';
}


function isPromiseLike(obj) {
  return obj && isFunction(obj.then);
}


var TYPED_ARRAY_REGEXP = /^\[object (?:Uint8|Uint8Clamped|Uint16|Uint32|Int8|Int16|Int32|Float32|Float64)Array\]$/;
function isTypedArray(value) {
  return value && isNumber(value.length) && TYPED_ARRAY_REGEXP.test(toString.call(value));
}


var trim = function(value) {
  return isString(value) ? value.trim() : value;
};

// Copied from:
// http://docs.closure-library.googlecode.com/git/local_closure_goog_string_string.js.source.html#line1021
// Prereq: s is a string.
var escapeForRegexp = function(s) {
  return s.replace(/([-()\[\]{}+?*.$\^|,:#<!\\])/g, '\\$1').
           replace(/\x08/g, '\\x08');
};


/**
 * @ngdoc function
 * @name angular.isElement
 * @module ng
 * @kind function
 *
 * @description
 * Determines if a reference is a DOM element (or wrapped jQuery element).
 *
 * @param {*} value Reference to check.
 * @returns {boolean} True if `value` is a DOM element (or wrapped jQuery element).
 */
function isElement(node) {
  return !!(node &&
    (node.nodeName  // we are a direct element
    || (node.prop && node.attr && node.find)));  // we have an on and find method part of jQuery API
}

/**
 * @param str 'key1,key2,...'
 * @returns {object} in the form of {key1:true, key2:true, ...}
 */
function makeMap(str) {
  var obj = {}, items = str.split(","), i;
  for (i = 0; i < items.length; i++) {
    obj[items[i]] = true;
  }
  return obj;
}


function nodeName_(element) {
  return lowercase(element.nodeName || (element[0] && element[0].nodeName));
}

function includes(array, obj) {
  return Array.prototype.indexOf.call(array, obj) != -1;
}

function arrayRemove(array, value) {
  var index = array.indexOf(value);
  if (index >= 0) {
    array.splice(index, 1);
  }
  return index;
}

/**
 * @ngdoc function
 * @name angular.copy
 * @module ng
 * @kind function
 *
 * @description
 * Creates a deep copy of `source`, which should be an object or an array.
 *
 * * If no destination is supplied, a copy of the object or array is created.
 * * If a destination is provided, all of its elements (for arrays) or properties (for objects)
 *   are deleted and then all elements/properties from the source are copied to it.
 * * If `source` is not an object or array (inc. `null` and `undefined`), `source` is returned.
 * * If `source` is identical to 'destination' an exception will be thrown.
 *
 * @param {*} source The source that will be used to make a copy.
 *                   Can be any type, including primitives, `null`, and `undefined`.
 * @param {(Object|Array)=} destination Destination into which the source is copied. If
 *     provided, must be of the same type as `source`.
 * @returns {*} The copy or updated `destination`, if `destination` was specified.
 *
 * @example
 <example module="copyExample">
 <file name="index.html">
 <div ng-controller="ExampleController">
 <form novalidate class="simple-form">
 Name: <input type="text" ng-model="user.name" /><br />
 E-mail: <input type="email" ng-model="user.email" /><br />
 Gender: <input type="radio" ng-model="user.gender" value="male" />male
 <input type="radio" ng-model="user.gender" value="female" />female<br />
 <button ng-click="reset()">RESET</button>
 <button ng-click="update(user)">SAVE</button>
 </form>
 <pre>form = {{user | json}}</pre>
 <pre>master = {{master | json}}</pre>
 </div>

 <script>
  angular.module('copyExample', [])
    .controller('ExampleController', ['$scope', function($scope) {
      $scope.master= {};

      $scope.update = function(user) {
        // Example with 1 argument
        $scope.master= angular.copy(user);
      };

      $scope.reset = function() {
        // Example with 2 arguments
        angular.copy($scope.master, $scope.user);
      };

      $scope.reset();
    }]);
 </script>
 </file>
 </example>
 */
function copy(source, destination) {
  var stackSource = [];
  var stackDest = [];

  if (destination) {
    if (isTypedArray(destination)) {
      throw ngMinErr('cpta', "Can't copy! TypedArray destination cannot be mutated.");
    }
    if (source === destination) {
      throw ngMinErr('cpi', "Can't copy! Source and destination are identical.");
    }

    // Empty the destination object
    if (isArray(destination)) {
      destination.length = 0;
    } else {
      forEach(destination, function(value, key) {
        if (key !== '$$hashKey') {
          delete destination[key];
        }
      });
    }

    stackSource.push(source);
    stackDest.push(destination);
    return copyRecurse(source, destination);
  }

  return copyElement(source);

  function copyRecurse(source, destination) {
    var h = destination.$$hashKey;
    var result, key;
    if (isArray(source)) {
      for (var i = 0, ii = source.length; i < ii; i++) {
        destination.push(copyElement(source[i]));
      }
    } else if (isBlankObject(source)) {
      // createMap() fast path --- Safe to avoid hasOwnProperty check because prototype chain is empty
      for (key in source) {
        destination[key] = copyElement(source[key]);
      }
    } else if (source && typeof source.hasOwnProperty === 'function') {
      // Slow path, which must rely on hasOwnProperty
      for (key in source) {
        if (source.hasOwnProperty(key)) {
          destination[key] = copyElement(source[key]);
        }
      }
    } else {
      // Slowest path --- hasOwnProperty can't be called as a method
      for (key in source) {
        if (hasOwnProperty.call(source, key)) {
          destination[key] = copyElement(source[key]);
        }
      }
    }
    setHashKey(destination, h);
    return destination;
  }

  function copyElement(source) {
    // Simple values
    if (!isObject(source)) {
      return source;
    }

    // Already copied values
    var index = stackSource.indexOf(source);
    if (index !== -1) {
      return stackDest[index];
    }

    if (isWindow(source) || isScope(source)) {
      throw ngMinErr('cpws',
        "Can't copy! Making copies of Window or Scope instances is not supported.");
    }

    var needsRecurse = false;
    var destination;

    if (isArray(source)) {
      destination = [];
      needsRecurse = true;
    } else if (isTypedArray(source)) {
      destination = new source.constructor(source);
    } else if (isDate(source)) {
      destination = new Date(source.getTime());
    } else if (isRegExp(source)) {
      destination = new RegExp(source.source, source.toString().match(/[^\/]*$/)[0]);
      destination.lastIndex = source.lastIndex;
    } else if (isFunction(source.cloneNode)) {
        destination = source.cloneNode(true);
    } else {
      destination = Object.create(getPrototypeOf(source));
      needsRecurse = true;
    }

    stackSource.push(source);
    stackDest.push(destination);

    return needsRecurse
      ? copyRecurse(source, destination)
      : destination;
  }
}

/**
 * Creates a shallow copy of an object, an array or a primitive.
 *
 * Assumes that there are no proto properties for objects.
 */
function shallowCopy(src, dst) {
  if (isArray(src)) {
    dst = dst || [];

    for (var i = 0, ii = src.length; i < ii; i++) {
      dst[i] = src[i];
    }
  } else if (isObject(src)) {
    dst = dst || {};

    for (var key in src) {
      if (!(key.charAt(0) === '$' && key.charAt(1) === '$')) {
        dst[key] = src[key];
      }
    }
  }

  return dst || src;
}


/**
 * @ngdoc function
 * @name angular.equals
 * @module ng
 * @kind function
 *
 * @description
 * Determines if two objects or two values are equivalent. Supports value types, regular
 * expressions, arrays and objects.
 *
 * Two objects or values are considered equivalent if at least one of the following is true:
 *
 * * Both objects or values pass `===` comparison.
 * * Both objects or values are of the same type and all of their properties are equal by
 *   comparing them with `angular.equals`.
 * * Both values are NaN. (In JavaScript, NaN == NaN => false. But we consider two NaN as equal)
 * * Both values represent the same regular expression (In JavaScript,
 *   /abc/ == /abc/ => false. But we consider two regular expressions as equal when their textual
 *   representation matches).
 *
 * During a property comparison, properties of `function` type and properties with names
 * that begin with `$` are ignored.
 *
 * Scope and DOMWindow objects are being compared only by identify (`===`).
 *
 * @param {*} o1 Object or value to compare.
 * @param {*} o2 Object or value to compare.
 * @returns {boolean} True if arguments are equal.
 */
function equals(o1, o2) {
  if (o1 === o2) return true;
  if (o1 === null || o2 === null) return false;
  if (o1 !== o1 && o2 !== o2) return true; // NaN === NaN
  var t1 = typeof o1, t2 = typeof o2, length, key, keySet;
  if (t1 == t2) {
    if (t1 == 'object') {
      if (isArray(o1)) {
        if (!isArray(o2)) return false;
        if ((length = o1.length) == o2.length) {
          for (key = 0; key < length; key++) {
            if (!equals(o1[key], o2[key])) return false;
          }
          return true;
        }
      } else if (isDate(o1)) {
        if (!isDate(o2)) return false;
        return equals(o1.getTime(), o2.getTime());
      } else if (isRegExp(o1)) {
        return isRegExp(o2) ? o1.toString() == o2.toString() : false;
      } else {
        if (isScope(o1) || isScope(o2) || isWindow(o1) || isWindow(o2) ||
          isArray(o2) || isDate(o2) || isRegExp(o2)) return false;
        keySet = createMap();
        for (key in o1) {
          if (key.charAt(0) === '$' || isFunction(o1[key])) continue;
          if (!equals(o1[key], o2[key])) return false;
          keySet[key] = true;
        }
        for (key in o2) {
          if (!(key in keySet) &&
              key.charAt(0) !== '$' &&
              isDefined(o2[key]) &&
              !isFunction(o2[key])) return false;
        }
        return true;
      }
    }
  }
  return false;
}

var csp = function() {
  if (!isDefined(csp.rules)) {


    var ngCspElement = (document.querySelector('[ng-csp]') ||
                    document.querySelector('[data-ng-csp]'));

    if (ngCspElement) {
      var ngCspAttribute = ngCspElement.getAttribute('ng-csp') ||
                    ngCspElement.getAttribute('data-ng-csp');
      csp.rules = {
        noUnsafeEval: !ngCspAttribute || (ngCspAttribute.indexOf('no-unsafe-eval') !== -1),
        noInlineStyle: !ngCspAttribute || (ngCspAttribute.indexOf('no-inline-style') !== -1)
      };
    } else {
      csp.rules = {
        noUnsafeEval: noUnsafeEval(),
        noInlineStyle: false
      };
    }
  }

  return csp.rules;

  function noUnsafeEval() {
    try {
      /* jshint -W031, -W054 */
      new Function('');
      /* jshint +W031, +W054 */
      return false;
    } catch (e) {
      return true;
    }
  }
};

/**
 * @ngdoc directive
 * @module ng
 * @name ngJq
 *
 * @element ANY
 * @param {string=} ngJq the name of the library available under `window`
 * to be used for angular.element
 * @description
 * Use this directive to force the angular.element library.  This should be
 * used to force either jqLite by leaving ng-jq blank or setting the name of
 * the jquery variable under window (eg. jQuery).
 *
 * Since angular looks for this directive when it is loaded (doesn't wait for the
 * DOMContentLoaded event), it must be placed on an element that comes before the script
 * which loads angular. Also, only the first instance of `ng-jq` will be used and all
 * others ignored.
 *
 * @example
 * This example shows how to force jqLite using the `ngJq` directive to the `html` tag.
 ```html
 <!doctype html>
 <html ng-app ng-jq>
 ...
 ...
 </html>
 ```
 * @example
 * This example shows how to use a jQuery based library of a different name.
 * The library name must be available at the top most 'window'.
 ```html
 <!doctype html>
 <html ng-app ng-jq="jQueryLib">
 ...
 ...
 </html>
 ```
 */
var jq = function() {
  if (isDefined(jq.name_)) return jq.name_;
  var el;
  var i, ii = ngAttrPrefixes.length, prefix, name;
  for (i = 0; i < ii; ++i) {
    prefix = ngAttrPrefixes[i];
    if (el = document.querySelector('[' + prefix.replace(':', '\\:') + 'jq]')) {
      name = el.getAttribute(prefix + 'jq');
      break;
    }
  }

  return (jq.name_ = name);
};

function concat(array1, array2, index) {
  return array1.concat(slice.call(array2, index));
}

function sliceArgs(args, startIndex) {
  return slice.call(args, startIndex || 0);
}


/* jshint -W101 */
/**
 * @ngdoc function
 * @name angular.bind
 * @module ng
 * @kind function
 *
 * @description
 * Returns a function which calls function `fn` bound to `self` (`self` becomes the `this` for
 * `fn`). You can supply optional `args` that are prebound to the function. This feature is also
 * known as [partial application](http://en.wikipedia.org/wiki/Partial_application), as
 * distinguished from [function currying](http://en.wikipedia.org/wiki/Currying#Contrast_with_partial_function_application).
 *
 * @param {Object} self Context which `fn` should be evaluated in.
 * @param {function()} fn Function to be bound.
 * @param {...*} args Optional arguments to be prebound to the `fn` function call.
 * @returns {function()} Function that wraps the `fn` with all the specified bindings.
 */
/* jshint +W101 */
function bind(self, fn) {
  var curryArgs = arguments.length > 2 ? sliceArgs(arguments, 2) : [];
  if (isFunction(fn) && !(fn instanceof RegExp)) {
    return curryArgs.length
      ? function() {
          return arguments.length
            ? fn.apply(self, concat(curryArgs, arguments, 0))
            : fn.apply(self, curryArgs);
        }
      : function() {
          return arguments.length
            ? fn.apply(self, arguments)
            : fn.call(self);
        };
  } else {
    // in IE, native methods are not functions so they cannot be bound (note: they don't need to be)
    return fn;
  }
}


function toJsonReplacer(key, value) {
  var val = value;

  if (typeof key === 'string' && key.charAt(0) === '$' && key.charAt(1) === '$') {
    val = undefined;
  } else if (isWindow(value)) {
    val = '$WINDOW';
  } else if (value &&  document === value) {
    val = '$DOCUMENT';
  } else if (isScope(value)) {
    val = '$SCOPE';
  }

  return val;
}


/**
 * @ngdoc function
 * @name angular.toJson
 * @module ng
 * @kind function
 *
 * @description
 * Serializes input into a JSON-formatted string. Properties with leading $$ characters will be
 * stripped since angular uses this notation internally.
 *
 * @param {Object|Array|Date|string|number} obj Input to be serialized into JSON.
 * @param {boolean|number} [pretty=2] If set to true, the JSON output will contain newlines and whitespace.
 *    If set to an integer, the JSON output will contain that many spaces per indentation.
 * @returns {string|undefined} JSON-ified string representing `obj`.
 */
function toJson(obj, pretty) {
  if (typeof obj === 'undefined') return undefined;
  if (!isNumber(pretty)) {
    pretty = pretty ? 2 : null;
  }
  return JSON.stringify(obj, toJsonReplacer, pretty);
}


/**
 * @ngdoc function
 * @name angular.fromJson
 * @module ng
 * @kind function
 *
 * @description
 * Deserializes a JSON string.
 *
 * @param {string} json JSON string to deserialize.
 * @returns {Object|Array|string|number} Deserialized JSON string.
 */
function fromJson(json) {
  return isString(json)
      ? JSON.parse(json)
      : json;
}


function timezoneToOffset(timezone, fallback) {
  var requestedTimezoneOffset = Date.parse('Jan 01, 1970 00:00:00 ' + timezone) / 60000;
  return isNaN(requestedTimezoneOffset) ? fallback : requestedTimezoneOffset;
}


function addDateMinutes(date, minutes) {
  date = new Date(date.getTime());
  date.setMinutes(date.getMinutes() + minutes);
  return date;
}


function convertTimezoneToLocal(date, timezone, reverse) {
  reverse = reverse ? -1 : 1;
  var timezoneOffset = timezoneToOffset(timezone, date.getTimezoneOffset());
  return addDateMinutes(date, reverse * (timezoneOffset - date.getTimezoneOffset()));
}


/**
 * @returns {string} Returns the string representation of the element.
 */
function startingTag(element) {
  element = jqLite(element).clone();
  try {
    // turns out IE does not let you set .html() on elements which
    // are not allowed to have children. So we just ignore it.
    element.empty();
  } catch (e) {}
  var elemHtml = jqLite('<div>').append(element).html();
  try {
    return element[0].nodeType === NODE_TYPE_TEXT ? lowercase(elemHtml) :
        elemHtml.
          match(/^(<[^>]+>)/)[1].
          replace(/^<([\w\-]+)/, function(match, nodeName) { return '<' + lowercase(nodeName); });
  } catch (e) {
    return lowercase(elemHtml);
  }

}


/////////////////////////////////////////////////

/**
 * Tries to decode the URI component without throwing an exception.
 *
 * @private
 * @param str value potential URI component to check.
 * @returns {boolean} True if `value` can be decoded
 * with the decodeURIComponent function.
 */
function tryDecodeURIComponent(value) {
  try {
    return decodeURIComponent(value);
  } catch (e) {
    // Ignore any invalid uri component
  }
}


/**
 * Parses an escaped url query string into key-value pairs.
 * @returns {Object.<string,boolean|Array>}
 */
function parseKeyValue(/**string*/keyValue) {
  var obj = {};
  forEach((keyValue || "").split('&'), function(keyValue) {
    var splitPoint, key, val;
    if (keyValue) {
      key = keyValue = keyValue.replace(/\+/g,'%20');
      splitPoint = keyValue.indexOf('=');
      if (splitPoint !== -1) {
        key = keyValue.substring(0, splitPoint);
        val = keyValue.substring(splitPoint + 1);
      }
      key = tryDecodeURIComponent(key);
      if (isDefined(key)) {
        val = isDefined(val) ? tryDecodeURIComponent(val) : true;
        if (!hasOwnProperty.call(obj, key)) {
          obj[key] = val;
        } else if (isArray(obj[key])) {
          obj[key].push(val);
        } else {
          obj[key] = [obj[key],val];
        }
      }
    }
  });
  return obj;
}

function toKeyValue(obj) {
  var parts = [];
  forEach(obj, function(value, key) {
    if (isArray(value)) {
      forEach(value, function(arrayValue) {
        parts.push(encodeUriQuery(key, true) +
                   (arrayValue === true ? '' : '=' + encodeUriQuery(arrayValue, true)));
      });
    } else {
    parts.push(encodeUriQuery(key, true) +
               (value === true ? '' : '=' + encodeUriQuery(value, true)));
    }
  });
  return parts.length ? parts.join('&') : '';
}


/**
 * We need our custom method because encodeURIComponent is too aggressive and doesn't follow
 * http://www.ietf.org/rfc/rfc3986.txt with regards to the character set (pchar) allowed in path
 * segments:
 *    segment       = *pchar
 *    pchar         = unreserved / pct-encoded / sub-delims / ":" / "@"
 *    pct-encoded   = "%" HEXDIG HEXDIG
 *    unreserved    = ALPHA / DIGIT / "-" / "." / "_" / "~"
 *    sub-delims    = "!" / "$" / "&" / "'" / "(" / ")"
 *                     / "*" / "+" / "," / ";" / "="
 */
function encodeUriSegment(val) {
  return encodeUriQuery(val, true).
             replace(/%26/gi, '&').
             replace(/%3D/gi, '=').
             replace(/%2B/gi, '+');
}


/**
 * This method is intended for encoding *key* or *value* parts of query component. We need a custom
 * method because encodeURIComponent is too aggressive and encodes stuff that doesn't have to be
 * encoded per http://tools.ietf.org/html/rfc3986:
 *    query       = *( pchar / "/" / "?" )
 *    pchar         = unreserved / pct-encoded / sub-delims / ":" / "@"
 *    unreserved    = ALPHA / DIGIT / "-" / "." / "_" / "~"
 *    pct-encoded   = "%" HEXDIG HEXDIG
 *    sub-delims    = "!" / "$" / "&" / "'" / "(" / ")"
 *                     / "*" / "+" / "," / ";" / "="
 */
function encodeUriQuery(val, pctEncodeSpaces) {
  return encodeURIComponent(val).
             replace(/%40/gi, '@').
             replace(/%3A/gi, ':').
             replace(/%24/g, '$').
             replace(/%2C/gi, ',').
             replace(/%3B/gi, ';').
             replace(/%20/g, (pctEncodeSpaces ? '%20' : '+'));
}

var ngAttrPrefixes = ['ng-', 'data-ng-', 'ng:', 'x-ng-'];

function getNgAttribute(element, ngAttr) {
  var attr, i, ii = ngAttrPrefixes.length;
  for (i = 0; i < ii; ++i) {
    attr = ngAttrPrefixes[i] + ngAttr;
    if (isString(attr = element.getAttribute(attr))) {
      return attr;
    }
  }
  return null;
}

/**
 * @ngdoc directive
 * @name ngApp
 * @module ng
 *
 * @element ANY
 * @param {angular.Module} ngApp an optional application
 *   {@link angular.module module} name to load.
 * @param {boolean=} ngStrictDi if this attribute is present on the app element, the injector will be
 *   created in "strict-di" mode. This means that the application will fail to invoke functions which
 *   do not use explicit function annotation (and are thus unsuitable for minification), as described
 *   in {@link guide/di the Dependency Injection guide}, and useful debugging info will assist in
 *   tracking down the root of these bugs.
 *
 * @description
 *
 * Use this directive to **auto-bootstrap** an AngularJS application. The `ngApp` directive
 * designates the **root element** of the application and is typically placed near the root element
 * of the page - e.g. on the `<body>` or `<html>` tags.
 *
 * Only one AngularJS application can be auto-bootstrapped per HTML document. The first `ngApp`
 * found in the document will be used to define the root element to auto-bootstrap as an
 * application. To run multiple applications in an HTML document you must manually bootstrap them using
 * {@link angular.bootstrap} instead. AngularJS applications cannot be nested within each other.
 *
 * You can specify an **AngularJS module** to be used as the root module for the application.  This
 * module will be loaded into the {@link auto.$injector} when the application is bootstrapped. It
 * should contain the application code needed or have dependencies on other modules that will
 * contain the code. See {@link angular.module} for more information.
 *
 * In the example below if the `ngApp` directive were not placed on the `html` element then the
 * document would not be compiled, the `AppController` would not be instantiated and the `{{ a+b }}`
 * would not be resolved to `3`.
 *
 * `ngApp` is the easiest, and most common way to bootstrap an application.
 *
 <example module="ngAppDemo">
   <file name="index.html">
   <div ng-controller="ngAppDemoController">
     I can add: {{a}} + {{b}} =  {{ a+b }}
   </div>
   </file>
   <file name="script.js">
   angular.module('ngAppDemo', []).controller('ngAppDemoController', function($scope) {
     $scope.a = 1;
     $scope.b = 2;
   });
   </file>
 </example>
 *
 * Using `ngStrictDi`, you would see something like this:
 *
 <example ng-app-included="true">
   <file name="index.html">
   <div ng-app="ngAppStrictDemo" ng-strict-di>
       <div ng-controller="GoodController1">
           I can add: {{a}} + {{b}} =  {{ a+b }}

           <p>This renders because the controller does not fail to
              instantiate, by using explicit annotation style (see
              script.js for details)
           </p>
       </div>

       <div ng-controller="GoodController2">
           Name: <input ng-model="name"><br />
           Hello, {{name}}!

           <p>This renders because the controller does not fail to
              instantiate, by using explicit annotation style
              (see script.js for details)
           </p>
       </div>

       <div ng-controller="BadController">
           I can add: {{a}} + {{b}} =  {{ a+b }}

           <p>The controller could not be instantiated, due to relying
              on automatic function annotations (which are disabled in
              strict mode). As such, the content of this section is not
              interpolated, and there should be an error in your web console.
           </p>
       </div>
   </div>
   </file>
   <file name="script.js">
   angular.module('ngAppStrictDemo', [])
     // BadController will fail to instantiate, due to relying on automatic function annotation,
     // rather than an explicit annotation
     .controller('BadController', function($scope) {
       $scope.a = 1;
       $scope.b = 2;
     })
     // Unlike BadController, GoodController1 and GoodController2 will not fail to be instantiated,
     // due to using explicit annotations using the array style and $inject property, respectively.
     .controller('GoodController1', ['$scope', function($scope) {
       $scope.a = 1;
       $scope.b = 2;
     }])
     .controller('GoodController2', GoodController2);
     function GoodController2($scope) {
       $scope.name = "World";
     }
     GoodController2.$inject = ['$scope'];
   </file>
   <file name="style.css">
   div[ng-controller] {
       margin-bottom: 1em;
       -webkit-border-radius: 4px;
       border-radius: 4px;
       border: 1px solid;
       padding: .5em;
   }
   div[ng-controller^=Good] {
       border-color: #d6e9c6;
       background-color: #dff0d8;
       color: #3c763d;
   }
   div[ng-controller^=Bad] {
       border-color: #ebccd1;
       background-color: #f2dede;
       color: #a94442;
       margin-bottom: 0;
   }
   </file>
 </example>
 */
function angularInit(element, bootstrap) {
  var appElement,
      module,
      config = {};

  // The element `element` has priority over any other element
  forEach(ngAttrPrefixes, function(prefix) {
    var name = prefix + 'app';

    if (!appElement && element.hasAttribute && element.hasAttribute(name)) {
      appElement = element;
      module = element.getAttribute(name);
    }
  });
  forEach(ngAttrPrefixes, function(prefix) {
    var name = prefix + 'app';
    var candidate;

    if (!appElement && (candidate = element.querySelector('[' + name.replace(':', '\\:') + ']'))) {
      appElement = candidate;
      module = candidate.getAttribute(name);
    }
  });
  if (appElement) {
    config.strictDi = getNgAttribute(appElement, "strict-di") !== null;
    bootstrap(appElement, module ? [module] : [], config);
  }
}

/**
 * @ngdoc function
 * @name angular.bootstrap
 * @module ng
 * @description
 * Use this function to manually start up angular application.
 *
 * See: {@link guide/bootstrap Bootstrap}
 *
 * Note that Protractor based end-to-end tests cannot use this function to bootstrap manually.
 * They must use {@link ng.directive:ngApp ngApp}.
 *
 * Angular will detect if it has been loaded into the browser more than once and only allow the
 * first loaded script to be bootstrapped and will report a warning to the browser console for
 * each of the subsequent scripts. This prevents strange results in applications, where otherwise
 * multiple instances of Angular try to work on the DOM.
 *
 * ```html
 * <!doctype html>
 * <html>
 * <body>
 * <div ng-controller="WelcomeController">
 *   {{greeting}}
 * </div>
 *
 * <script src="angular.js"></script>
 * <script>
 *   var app = angular.module('demo', [])
 *   .controller('WelcomeController', function($scope) {
 *       $scope.greeting = 'Welcome!';
 *   });
 *   angular.bootstrap(document, ['demo']);
 * </script>
 * </body>
 * </html>
 * ```
 *
 * @param {DOMElement} element DOM element which is the root of angular application.
 * @param {Array<String|Function|Array>=} modules an array of modules to load into the application.
 *     Each item in the array should be the name of a predefined module or a (DI annotated)
 *     function that will be invoked by the injector as a `config` block.
 *     See: {@link angular.module modules}
 * @param {Object=} config an object for defining configuration options for the application. The
 *     following keys are supported:
 *
 * * `strictDi` - disable automatic function annotation for the application. This is meant to
 *   assist in finding bugs which break minified code. Defaults to `false`.
 *
 * @returns {auto.$injector} Returns the newly created injector for this app.
 */
function bootstrap(element, modules, config) {
  if (!isObject(config)) config = {};
  var defaultConfig = {
    strictDi: false
  };
  config = extend(defaultConfig, config);
  var doBootstrap = function() {
    element = jqLite(element);

    if (element.injector()) {
      var tag = (element[0] === document) ? 'document' : startingTag(element);
      //Encode angle brackets to prevent input from being sanitized to empty string #8683
      throw ngMinErr(
          'btstrpd',
          "App Already Bootstrapped with this Element '{0}'",
          tag.replace(/</,'&lt;').replace(/>/,'&gt;'));
    }

    modules = modules || [];
    modules.unshift(['$provide', function($provide) {
      $provide.value('$rootElement', element);
    }]);

    if (config.debugInfoEnabled) {
      // Pushing so that this overrides `debugInfoEnabled` setting defined in user's `modules`.
      modules.push(['$compileProvider', function($compileProvider) {
        $compileProvider.debugInfoEnabled(true);
      }]);
    }

    modules.unshift('ng');
    var injector = createInjector(modules, config.strictDi);
    injector.invoke(['$rootScope', '$rootElement', '$compile', '$injector',
       function bootstrapApply(scope, element, compile, injector) {
        scope.$apply(function() {
          element.data('$injector', injector);
          compile(element)(scope);
        });
      }]
    );
    return injector;
  };

  var NG_ENABLE_DEBUG_INFO = /^NG_ENABLE_DEBUG_INFO!/;
  var NG_DEFER_BOOTSTRAP = /^NG_DEFER_BOOTSTRAP!/;

  if (window && NG_ENABLE_DEBUG_INFO.test(window.name)) {
    config.debugInfoEnabled = true;
    window.name = window.name.replace(NG_ENABLE_DEBUG_INFO, '');
  }

  if (window && !NG_DEFER_BOOTSTRAP.test(window.name)) {
    return doBootstrap();
  }

  window.name = window.name.replace(NG_DEFER_BOOTSTRAP, '');
  angular.resumeBootstrap = function(extraModules) {
    forEach(extraModules, function(module) {
      modules.push(module);
    });
    return doBootstrap();
  };

  if (isFunction(angular.resumeDeferredBootstrap)) {
    angular.resumeDeferredBootstrap();
  }
}

/**
 * @ngdoc function
 * @name angular.reloadWithDebugInfo
 * @module ng
 * @description
 * Use this function to reload the current application with debug information turned on.
 * This takes precedence over a call to `$compileProvider.debugInfoEnabled(false)`.
 *
 * See {@link ng.$compileProvider#debugInfoEnabled} for more.
 */
function reloadWithDebugInfo() {
  window.name = 'NG_ENABLE_DEBUG_INFO!' + window.name;
  window.location.reload();
}

/**
 * @name angular.getTestability
 * @module ng
 * @description
 * Get the testability service for the instance of Angular on the given
 * element.
 * @param {DOMElement} element DOM element which is the root of angular application.
 */
function getTestability(rootElement) {
  var injector = angular.element(rootElement).injector();
  if (!injector) {
    throw ngMinErr('test',
      'no injector found for element argument to getTestability');
  }
  return injector.get('$$testability');
}

var SNAKE_CASE_REGEXP = /[A-Z]/g;
function snake_case(name, separator) {
  separator = separator || '_';
  return name.replace(SNAKE_CASE_REGEXP, function(letter, pos) {
    return (pos ? separator : '') + letter.toLowerCase();
  });
}

var bindJQueryFired = false;
var skipDestroyOnNextJQueryCleanData;
function bindJQuery() {
  var originalCleanData;

  if (bindJQueryFired) {
    return;
  }

  // bind to jQuery if present;
  var jqName = jq();
  jQuery = isUndefined(jqName) ? window.jQuery :   // use jQuery (if present)
           !jqName             ? undefined     :   // use jqLite
                                 window[jqName];   // use jQuery specified by `ngJq`

  // Use jQuery if it exists with proper functionality, otherwise default to us.
  // Angular 1.2+ requires jQuery 1.7+ for on()/off() support.
  // Angular 1.3+ technically requires at least jQuery 2.1+ but it may work with older
  // versions. It will not work for sure with jQuery <1.7, though.
  if (jQuery && jQuery.fn.on) {
    jqLite = jQuery;
    extend(jQuery.fn, {
      scope: JQLitePrototype.scope,
      isolateScope: JQLitePrototype.isolateScope,
      controller: JQLitePrototype.controller,
      injector: JQLitePrototype.injector,
      inheritedData: JQLitePrototype.inheritedData
    });

    // All nodes removed from the DOM via various jQuery APIs like .remove()
    // are passed through jQuery.cleanData. Monkey-patch this method to fire
    // the $destroy event on all removed nodes.
    originalCleanData = jQuery.cleanData;
    jQuery.cleanData = function(elems) {
      var events;
      if (!skipDestroyOnNextJQueryCleanData) {
        for (var i = 0, elem; (elem = elems[i]) != null; i++) {
          events = jQuery._data(elem, "events");
          if (events && events.$destroy) {
            jQuery(elem).triggerHandler('$destroy');
          }
        }
      } else {
        skipDestroyOnNextJQueryCleanData = false;
      }
      originalCleanData(elems);
    };
  } else {
    jqLite = JQLite;
  }

  angular.element = jqLite;

  // Prevent double-proxying.
  bindJQueryFired = true;
}

/**
 * throw error if the argument is falsy.
 */
function assertArg(arg, name, reason) {
  if (!arg) {
    throw ngMinErr('areq', "Argument '{0}' is {1}", (name || '?'), (reason || "required"));
  }
  return arg;
}

function assertArgFn(arg, name, acceptArrayAnnotation) {
  if (acceptArrayAnnotation && isArray(arg)) {
      arg = arg[arg.length - 1];
  }

  assertArg(isFunction(arg), name, 'not a function, got ' +
      (arg && typeof arg === 'object' ? arg.constructor.name || 'Object' : typeof arg));
  return arg;
}

/**
 * throw error if the name given is hasOwnProperty
 * @param  {String} name    the name to test
 * @param  {String} context the context in which the name is used, such as module or directive
 */
function assertNotHasOwnProperty(name, context) {
  if (name === 'hasOwnProperty') {
    throw ngMinErr('badname', "hasOwnProperty is not a valid {0} name", context);
  }
}

/**
 * Return the value accessible from the object by path. Any undefined traversals are ignored
 * @param {Object} obj starting object
 * @param {String} path path to traverse
 * @param {boolean} [bindFnToScope=true]
 * @returns {Object} value as accessible by path
 */
//TODO(misko): this function needs to be removed
function getter(obj, path, bindFnToScope) {
  if (!path) return obj;
  var keys = path.split('.');
  var key;
  var lastInstance = obj;
  var len = keys.length;

  for (var i = 0; i < len; i++) {
    key = keys[i];
    if (obj) {
      obj = (lastInstance = obj)[key];
    }
  }
  if (!bindFnToScope && isFunction(obj)) {
    return bind(lastInstance, obj);
  }
  return obj;
}

/**
 * Return the DOM siblings between the first and last node in the given array.
 * @param {Array} array like object
 * @returns {Array} the inputted object or a jqLite collection containing the nodes
 */
function getBlockNodes(nodes) {
  // TODO(perf): update `nodes` instead of creating a new object?
  var node = nodes[0];
  var endNode = nodes[nodes.length - 1];
  var blockNodes;

  for (var i = 1; node !== endNode && (node = node.nextSibling); i++) {
    if (blockNodes || nodes[i] !== node) {
      if (!blockNodes) {
        blockNodes = jqLite(slice.call(nodes, 0, i));
      }
      blockNodes.push(node);
    }
  }

  return blockNodes || nodes;
}


/**
 * Creates a new object without a prototype. This object is useful for lookup without having to
 * guard against prototypically inherited properties via hasOwnProperty.
 *
 * Related micro-benchmarks:
 * - http://jsperf.com/object-create2
 * - http://jsperf.com/proto-map-lookup/2
 * - http://jsperf.com/for-in-vs-object-keys2
 *
 * @returns {Object}
 */
function createMap() {
  return Object.create(null);
}

var NODE_TYPE_ELEMENT = 1;
var NODE_TYPE_ATTRIBUTE = 2;
var NODE_TYPE_TEXT = 3;
var NODE_TYPE_COMMENT = 8;
var NODE_TYPE_DOCUMENT = 9;
var NODE_TYPE_DOCUMENT_FRAGMENT = 11;

/**
 * @ngdoc type
 * @name angular.Module
 * @module ng
 * @description
 *
 * Interface for configuring angular {@link angular.module modules}.
 */

function setupModuleLoader(window) {

  var $injectorMinErr = minErr('$injector');
  var ngMinErr = minErr('ng');

  function ensure(obj, name, factory) {
    return obj[name] || (obj[name] = factory());
  }

  var angular = ensure(window, 'angular', Object);

  // We need to expose `angular.$$minErr` to modules such as `ngResource` that reference it during bootstrap
  angular.$$minErr = angular.$$minErr || minErr;

  return ensure(angular, 'module', function() {
    /** @type {Object.<string, angular.Module>} */
    var modules = {};

    /**
     * @ngdoc function
     * @name angular.module
     * @module ng
     * @description
     *
     * The `angular.module` is a global place for creating, registering and retrieving Angular
     * modules.
     * All modules (angular core or 3rd party) that should be available to an application must be
     * registered using this mechanism.
     *
     * Passing one argument retrieves an existing {@link angular.Module},
     * whereas passing more than one argument creates a new {@link angular.Module}
     *
     *
     * # Module
     *
     * A module is a collection of services, directives, controllers, filters, and configuration information.
     * `angular.module` is used to configure the {@link auto.$injector $injector}.
     *
     * ```js
     * // Create a new module
     * var myModule = angular.module('myModule', []);
     *
     * // register a new service
     * myModule.value('appName', 'MyCoolApp');
     *
     * // configure existing services inside initialization blocks.
     * myModule.config(['$locationProvider', function($locationProvider) {
     *   // Configure existing providers
     *   $locationProvider.hashPrefix('!');
     * }]);
     * ```
     *
     * Then you can create an injector and load your modules like this:
     *
     * ```js
     * var injector = angular.injector(['ng', 'myModule'])
     * ```
     *
     * However it's more likely that you'll just use
     * {@link ng.directive:ngApp ngApp} or
     * {@link angular.bootstrap} to simplify this process for you.
     *
     * @param {!string} name The name of the module to create or retrieve.
     * @param {!Array.<string>=} requires If specified then new module is being created. If
     *        unspecified then the module is being retrieved for further configuration.
     * @param {Function=} configFn Optional configuration function for the module. Same as
     *        {@link angular.Module#config Module#config()}.
     * @returns {module} new module with the {@link angular.Module} api.
     */
    return function module(name, requires, configFn) {
      var assertNotHasOwnProperty = function(name, context) {
        if (name === 'hasOwnProperty') {
          throw ngMinErr('badname', 'hasOwnProperty is not a valid {0} name', context);
        }
      };

      assertNotHasOwnProperty(name, 'module');
      if (requires && modules.hasOwnProperty(name)) {
        modules[name] = null;
      }
      return ensure(modules, name, function() {
        if (!requires) {
          throw $injectorMinErr('nomod', "Module '{0}' is not available! You either misspelled " +
             "the module name or forgot to load it. If registering a module ensure that you " +
             "specify the dependencies as the second argument.", name);
        }

        /** @type {!Array.<Array.<*>>} */
        var invokeQueue = [];

        /** @type {!Array.<Function>} */
        var configBlocks = [];

        /** @type {!Array.<Function>} */
        var runBlocks = [];

        var config = invokeLater('$injector', 'invoke', 'push', configBlocks);

        /** @type {angular.Module} */
        var moduleInstance = {
          // Private state
          _invokeQueue: invokeQueue,
          _configBlocks: configBlocks,
          _runBlocks: runBlocks,

          /**
           * @ngdoc property
           * @name angular.Module#requires
           * @module ng
           *
           * @description
           * Holds the list of modules which the injector will load before the current module is
           * loaded.
           */
          requires: requires,

          /**
           * @ngdoc property
           * @name angular.Module#name
           * @module ng
           *
           * @description
           * Name of the module.
           */
          name: name,


          /**
           * @ngdoc method
           * @name angular.Module#provider
           * @module ng
           * @param {string} name service name
           * @param {Function} providerType Construction function for creating new instance of the
           *                                service.
           * @description
           * See {@link auto.$provide#provider $provide.provider()}.
           */
          provider: invokeLaterAndSetModuleName('$provide', 'provider'),

          /**
           * @ngdoc method
           * @name angular.Module#factory
           * @module ng
           * @param {string} name service name
           * @param {Function} providerFunction Function for creating new instance of the service.
           * @description
           * See {@link auto.$provide#factory $provide.factory()}.
           */
          factory: invokeLaterAndSetModuleName('$provide', 'factory'),

          /**
           * @ngdoc method
           * @name angular.Module#service
           * @module ng
           * @param {string} name service name
           * @param {Function} constructor A constructor function that will be instantiated.
           * @description
           * See {@link auto.$provide#service $provide.service()}.
           */
          service: invokeLaterAndSetModuleName('$provide', 'service'),

          /**
           * @ngdoc method
           * @name angular.Module#value
           * @module ng
           * @param {string} name service name
           * @param {*} object Service instance object.
           * @description
           * See {@link auto.$provide#value $provide.value()}.
           */
          value: invokeLater('$provide', 'value'),

          /**
           * @ngdoc method
           * @name angular.Module#constant
           * @module ng
           * @param {string} name constant name
           * @param {*} object Constant value.
           * @description
           * Because the constants are fixed, they get applied before other provide methods.
           * See {@link auto.$provide#constant $provide.constant()}.
           */
          constant: invokeLater('$provide', 'constant', 'unshift'),

           /**
           * @ngdoc method
           * @name angular.Module#decorator
           * @module ng
           * @param {string} The name of the service to decorate.
           * @param {Function} This function will be invoked when the service needs to be
           *                                    instantiated and should return the decorated service instance.
           * @description
           * See {@link auto.$provide#decorator $provide.decorator()}.
           */
          decorator: invokeLaterAndSetModuleName('$provide', 'decorator'),

          /**
           * @ngdoc method
           * @name angular.Module#animation
           * @module ng
           * @param {string} name animation name
           * @param {Function} animationFactory Factory function for creating new instance of an
           *                                    animation.
           * @description
           *
           * **NOTE**: animations take effect only if the **ngAnimate** module is loaded.
           *
           *
           * Defines an animation hook that can be later used with
           * {@link $animate $animate} service and directives that use this service.
           *
           * ```js
           * module.animation('.animation-name', function($inject1, $inject2) {
           *   return {
           *     eventName : function(element, done) {
           *       //code to run the animation
           *       //once complete, then run done()
           *       return function cancellationFunction(element) {
           *         //code to cancel the animation
           *       }
           *     }
           *   }
           * })
           * ```
           *
           * See {@link ng.$animateProvider#register $animateProvider.register()} and
           * {@link ngAnimate ngAnimate module} for more information.
           */
          animation: invokeLaterAndSetModuleName('$animateProvider', 'register'),

          /**
           * @ngdoc method
           * @name angular.Module#filter
           * @module ng
           * @param {string} name Filter name - this must be a valid angular expression identifier
           * @param {Function} filterFactory Factory function for creating new instance of filter.
           * @description
           * See {@link ng.$filterProvider#register $filterProvider.register()}.
           *
           * <div class="alert alert-warning">
           * **Note:** Filter names must be valid angular {@link expression} identifiers, such as `uppercase` or `orderBy`.
           * Names with special characters, such as hyphens and dots, are not allowed. If you wish to namespace
           * your filters, then you can use capitalization (`myappSubsectionFilterx`) or underscores
           * (`myapp_subsection_filterx`).
           * </div>
           */
          filter: invokeLaterAndSetModuleName('$filterProvider', 'register'),

          /**
           * @ngdoc method
           * @name angular.Module#controller
           * @module ng
           * @param {string|Object} name Controller name, or an object map of controllers where the
           *    keys are the names and the values are the constructors.
           * @param {Function} constructor Controller constructor function.
           * @description
           * See {@link ng.$controllerProvider#register $controllerProvider.register()}.
           */
          controller: invokeLaterAndSetModuleName('$controllerProvider', 'register'),

          /**
           * @ngdoc method
           * @name angular.Module#directive
           * @module ng
           * @param {string|Object} name Directive name, or an object map of directives where the
           *    keys are the names and the values are the factories.
           * @param {Function} directiveFactory Factory function for creating new instance of
           * directives.
           * @description
           * See {@link ng.$compileProvider#directive $compileProvider.directive()}.
           */
          directive: invokeLaterAndSetModuleName('$compileProvider', 'directive'),

          /**
           * @ngdoc method
           * @name angular.Module#config
           * @module ng
           * @param {Function} configFn Execute this function on module load. Useful for service
           *    configuration.
           * @description
           * Use this method to register work which needs to be performed on module loading.
           * For more about how to configure services, see
           * {@link providers#provider-recipe Provider Recipe}.
           */
          config: config,

          /**
           * @ngdoc method
           * @name angular.Module#run
           * @module ng
           * @param {Function} initializationFn Execute this function after injector creation.
           *    Useful for application initialization.
           * @description
           * Use this method to register work which should be performed when the injector is done
           * loading all modules.
           */
          run: function(block) {
            runBlocks.push(block);
            return this;
          }
        };

        if (configFn) {
          config(configFn);
        }

        return moduleInstance;

        /**
         * @param {string} provider
         * @param {string} method
         * @param {String=} insertMethod
         * @returns {angular.Module}
         */
        function invokeLater(provider, method, insertMethod, queue) {
          if (!queue) queue = invokeQueue;
          return function() {
            queue[insertMethod || 'push']([provider, method, arguments]);
            return moduleInstance;
          };
        }

        /**
         * @param {string} provider
         * @param {string} method
         * @returns {angular.Module}
         */
        function invokeLaterAndSetModuleName(provider, method) {
          return function(recipeName, factoryFunction) {
            if (factoryFunction && isFunction(factoryFunction)) factoryFunction.$$moduleName = name;
            invokeQueue.push([provider, method, arguments]);
            return moduleInstance;
          };
        }
      });
    };
  });

}

/* global: toDebugString: true */

function serializeObject(obj) {
  var seen = [];

  return JSON.stringify(obj, function(key, val) {
    val = toJsonReplacer(key, val);
    if (isObject(val)) {

      if (seen.indexOf(val) >= 0) return '...';

      seen.push(val);
    }
    return val;
  });
}

function toDebugString(obj) {
  if (typeof obj === 'function') {
    return obj.toString().replace(/ \{[\s\S]*$/, '');
  } else if (isUndefined(obj)) {
    return 'undefined';
  } else if (typeof obj !== 'string') {
    return serializeObject(obj);
  }
  return obj;
}

/* global angularModule: true,
  version: true,

  $CompileProvider,

  htmlAnchorDirective,
  inputDirective,
  inputDirective,
  formDirective,
  scriptDirective,
  selectDirective,
  styleDirective,
  optionDirective,
  ngBindDirective,
  ngBindHtmlDirective,
  ngBindTemplateDirective,
  ngClassDirective,
  ngClassEvenDirective,
  ngClassOddDirective,
  ngCloakDirective,
  ngControllerDirective,
  ngFormDirective,
  ngHideDirective,
  ngIfDirective,
  ngIncludeDirective,
  ngIncludeFillContentDirective,
  ngInitDirective,
  ngNonBindableDirective,
  ngPluralizeDirective,
  ngRepeatDirective,
  ngShowDirective,
  ngStyleDirective,
  ngSwitchDirective,
  ngSwitchWhenDirective,
  ngSwitchDefaultDirective,
  ngOptionsDirective,
  ngTranscludeDirective,
  ngModelDirective,
  ngListDirective,
  ngChangeDirective,
  patternDirective,
  patternDirective,
  requiredDirective,
  requiredDirective,
  minlengthDirective,
  minlengthDirective,
  maxlengthDirective,
  maxlengthDirective,
  ngValueDirective,
  ngModelOptionsDirective,
  ngAttributeAliasDirectives,
  ngEventDirectives,

  $AnchorScrollProvider,
  $AnimateProvider,
  $CoreAnimateCssProvider,
  $$CoreAnimateQueueProvider,
  $$CoreAnimateRunnerProvider,
  $BrowserProvider,
  $CacheFactoryProvider,
  $ControllerProvider,
  $DocumentProvider,
  $ExceptionHandlerProvider,
  $FilterProvider,
  $$ForceReflowProvider,
  $InterpolateProvider,
  $IntervalProvider,
  $$HashMapProvider,
  $HttpProvider,
  $HttpParamSerializerProvider,
  $HttpParamSerializerJQLikeProvider,
  $HttpBackendProvider,
  $xhrFactoryProvider,
  $LocationProvider,
  $LogProvider,
  $ParseProvider,
  $RootScopeProvider,
  $QProvider,
  $$QProvider,
  $$SanitizeUriProvider,
  $SceProvider,
  $SceDelegateProvider,
  $SnifferProvider,
  $TemplateCacheProvider,
  $TemplateRequestProvider,
  $$TestabilityProvider,
  $TimeoutProvider,
  $$RAFProvider,
  $WindowProvider,
  $$jqLiteProvider,
  $$CookieReaderProvider
*/


/**
 * @ngdoc object
 * @name angular.version
 * @module ng
 * @description
 * An object that contains information about the current AngularJS version.
 *
 * This object has the following properties:
 *
 * - `full` – `{string}` – Full version string, such as "0.9.18".
 * - `major` – `{number}` – Major version number, such as "0".
 * - `minor` – `{number}` – Minor version number, such as "9".
 * - `dot` – `{number}` – Dot version number, such as "18".
 * - `codeName` – `{string}` – Code name of the release, such as "jiggling-armfat".
 */
var version = {
  full: '1.4.8',    // all of these placeholder strings will be replaced by grunt's
  major: 1,    // package task
  minor: 4,
  dot: 8,
  codeName: 'ice-manipulation'
};


function publishExternalAPI(angular) {
  extend(angular, {
    'bootstrap': bootstrap,
    'copy': copy,
    'extend': extend,
    'merge': merge,
    'equals': equals,
    'element': jqLite,
    'forEach': forEach,
    'injector': createInjector,
    'noop': noop,
    'bind': bind,
    'toJson': toJson,
    'fromJson': fromJson,
    'identity': identity,
    'isUndefined': isUndefined,
    'isDefined': isDefined,
    'isString': isString,
    'isFunction': isFunction,
    'isObject': isObject,
    'isNumber': isNumber,
    'isElement': isElement,
    'isArray': isArray,
    'version': version,
    'isDate': isDate,
    'lowercase': lowercase,
    'uppercase': uppercase,
    'callbacks': {counter: 0},
    'getTestability': getTestability,
    '$$minErr': minErr,
    '$$csp': csp,
    'reloadWithDebugInfo': reloadWithDebugInfo
  });

  angularModule = setupModuleLoader(window);

  angularModule('ng', ['ngLocale'], ['$provide',
    function ngModule($provide) {
      // $$sanitizeUriProvider needs to be before $compileProvider as it is used by it.
      $provide.provider({
        $$sanitizeUri: $$SanitizeUriProvider
      });
      $provide.provider('$compile', $CompileProvider).
        directive({
            a: htmlAnchorDirective,
            input: inputDirective,
            textarea: inputDirective,
            form: formDirective,
            script: scriptDirective,
            select: selectDirective,
            style: styleDirective,
            option: optionDirective,
            ngBind: ngBindDirective,
            ngBindHtml: ngBindHtmlDirective,
            ngBindTemplate: ngBindTemplateDirective,
            ngClass: ngClassDirective,
            ngClassEven: ngClassEvenDirective,
            ngClassOdd: ngClassOddDirective,
            ngCloak: ngCloakDirective,
            ngController: ngControllerDirective,
            ngForm: ngFormDirective,
            ngHide: ngHideDirective,
            ngIf: ngIfDirective,
            ngInclude: ngIncludeDirective,
            ngInit: ngInitDirective,
            ngNonBindable: ngNonBindableDirective,
            ngPluralize: ngPluralizeDirective,
            ngRepeat: ngRepeatDirective,
            ngShow: ngShowDirective,
            ngStyle: ngStyleDirective,
            ngSwitch: ngSwitchDirective,
            ngSwitchWhen: ngSwitchWhenDirective,
            ngSwitchDefault: ngSwitchDefaultDirective,
            ngOptions: ngOptionsDirective,
            ngTransclude: ngTranscludeDirective,
            ngModel: ngModelDirective,
            ngList: ngListDirective,
            ngChange: ngChangeDirective,
            pattern: patternDirective,
            ngPattern: patternDirective,
            required: requiredDirective,
            ngRequired: requiredDirective,
            minlength: minlengthDirective,
            ngMinlength: minlengthDirective,
            maxlength: maxlengthDirective,
            ngMaxlength: maxlengthDirective,
            ngValue: ngValueDirective,
            ngModelOptions: ngModelOptionsDirective
        }).
        directive({
          ngInclude: ngIncludeFillContentDirective
        }).
        directive(ngAttributeAliasDirectives).
        directive(ngEventDirectives);
      $provide.provider({
        $anchorScroll: $AnchorScrollProvider,
        $animate: $AnimateProvider,
        $animateCss: $CoreAnimateCssProvider,
        $$animateQueue: $$CoreAnimateQueueProvider,
        $$AnimateRunner: $$CoreAnimateRunnerProvider,
        $browser: $BrowserProvider,
        $cacheFactory: $CacheFactoryProvider,
        $controller: $ControllerProvider,
        $document: $DocumentProvider,
        $exceptionHandler: $ExceptionHandlerProvider,
        $filter: $FilterProvider,
        $$forceReflow: $$ForceReflowProvider,
        $interpolate: $InterpolateProvider,
        $interval: $IntervalProvider,
        $http: $HttpProvider,
        $httpParamSerializer: $HttpParamSerializerProvider,
        $httpParamSerializerJQLike: $HttpParamSerializerJQLikeProvider,
        $httpBackend: $HttpBackendProvider,
        $xhrFactory: $xhrFactoryProvider,
        $location: $LocationProvider,
        $log: $LogProvider,
        $parse: $ParseProvider,
        $rootScope: $RootScopeProvider,
        $q: $QProvider,
        $$q: $$QProvider,
        $sce: $SceProvider,
        $sceDelegate: $SceDelegateProvider,
        $sniffer: $SnifferProvider,
        $templateCache: $TemplateCacheProvider,
        $templateRequest: $TemplateRequestProvider,
        $$testability: $$TestabilityProvider,
        $timeout: $TimeoutProvider,
        $window: $WindowProvider,
        $$rAF: $$RAFProvider,
        $$jqLite: $$jqLiteProvider,
        $$HashMap: $$HashMapProvider,
        $$cookieReader: $$CookieReaderProvider
      });
    }
  ]);
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *     Any commits to this file should be reviewed with security in mind.  *
 *   Changes to this file can potentially create security vulnerabilities. *
 *          An approval from 2 Core members with history of modifying      *
 *                         this file is required.                          *
 *                                                                         *
 *  Does the change somehow allow for arbitrary javascript to be executed? *
 *    Or allows for someone to change the prototype of built-in objects?   *
 *     Or gives undesired access to variables likes document or window?    *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* global JQLitePrototype: true,
  addEventListenerFn: true,
  removeEventListenerFn: true,
  BOOLEAN_ATTR: true,
  ALIASED_ATTR: true,
*/

//////////////////////////////////
//JQLite
//////////////////////////////////

/**
 * @ngdoc function
 * @name angular.element
 * @module ng
 * @kind function
 *
 * @description
 * Wraps a raw DOM element or HTML string as a [jQuery](http://jquery.com) element.
 *
 * If jQuery is available, `angular.element` is an alias for the
 * [jQuery](http://api.jquery.com/jQuery/) function. If jQuery is not available, `angular.element`
 * delegates to Angular's built-in subset of jQuery, called "jQuery lite" or "jqLite."
 *
 * <div class="alert alert-success">jqLite is a tiny, API-compatible subset of jQuery that allows
 * Angular to manipulate the DOM in a cross-browser compatible way. **jqLite** implements only the most
 * commonly needed functionality with the goal of having a very small footprint.</div>
 *
 * To use `jQuery`, simply ensure it is loaded before the `angular.js` file.
 *
 * <div class="alert">**Note:** all element references in Angular are always wrapped with jQuery or
 * jqLite; they are never raw DOM references.</div>
 *
 * ## Angular's jqLite
 * jqLite provides only the following jQuery methods:
 *
 * - [`addClass()`](http://api.jquery.com/addClass/)
 * - [`after()`](http://api.jquery.com/after/)
 * - [`append()`](http://api.jquery.com/append/)
 * - [`attr()`](http://api.jquery.com/attr/) - Does not support functions as parameters
 * - [`bind()`](http://api.jquery.com/bind/) - Does not support namespaces, selectors or eventData
 * - [`children()`](http://api.jquery.com/children/) - Does not support selectors
 * - [`clone()`](http://api.jquery.com/clone/)
 * - [`contents()`](http://api.jquery.com/contents/)
 * - [`css()`](http://api.jquery.com/css/) - Only retrieves inline-styles, does not call `getComputedStyle()`. As a setter, does not convert numbers to strings or append 'px'.
 * - [`data()`](http://api.jquery.com/data/)
 * - [`detach()`](http://api.jquery.com/detach/)
 * - [`empty()`](http://api.jquery.com/empty/)
 * - [`eq()`](http://api.jquery.com/eq/)
 * - [`find()`](http://api.jquery.com/find/) - Limited to lookups by tag name
 * - [`hasClass()`](http://api.jquery.com/hasClass/)
 * - [`html()`](http://api.jquery.com/html/)
 * - [`next()`](http://api.jquery.com/next/) - Does not support selectors
 * - [`on()`](http://api.jquery.com/on/) - Does not support namespaces, selectors or eventData
 * - [`off()`](http://api.jquery.com/off/) - Does not support namespaces, selectors or event object as parameter
 * - [`one()`](http://api.jquery.com/one/) - Does not support namespaces or selectors
 * - [`parent()`](http://api.jquery.com/parent/) - Does not support selectors
 * - [`prepend()`](http://api.jquery.com/prepend/)
 * - [`prop()`](http://api.jquery.com/prop/)
 * - [`ready()`](http://api.jquery.com/ready/)
 * - [`remove()`](http://api.jquery.com/remove/)
 * - [`removeAttr()`](http://api.jquery.com/removeAttr/)
 * - [`removeClass()`](http://api.jquery.com/removeClass/)
 * - [`removeData()`](http://api.jquery.com/removeData/)
 * - [`replaceWith()`](http://api.jquery.com/replaceWith/)
 * - [`text()`](http://api.jquery.com/text/)
 * - [`toggleClass()`](http://api.jquery.com/toggleClass/)
 * - [`triggerHandler()`](http://api.jquery.com/triggerHandler/) - Passes a dummy event object to handlers.
 * - [`unbind()`](http://api.jquery.com/unbind/) - Does not support namespaces or event object as parameter
 * - [`val()`](http://api.jquery.com/val/)
 * - [`wrap()`](http://api.jquery.com/wrap/)
 *
 * ## jQuery/jqLite Extras
 * Angular also provides the following additional methods and events to both jQuery and jqLite:
 *
 * ### Events
 * - `$destroy` - AngularJS intercepts all jqLite/jQuery's DOM destruction apis and fires this event
 *    on all DOM nodes being removed.  This can be used to clean up any 3rd party bindings to the DOM
 *    element before it is removed.
 *
 * ### Methods
 * - `controller(name)` - retrieves the controller of the current element or its parent. By default
 *   retrieves controller associated with the `ngController` directive. If `name` is provided as
 *   camelCase directive name, then the controller for this directive will be retrieved (e.g.
 *   `'ngModel'`).
 * - `injector()` - retrieves the injector of the current element or its parent.
 * - `scope()` - retrieves the {@link ng.$rootScope.Scope scope} of the current
 *   element or its parent. Requires {@link guide/production#disabling-debug-data Debug Data} to
 *   be enabled.
 * - `isolateScope()` - retrieves an isolate {@link ng.$rootScope.Scope scope} if one is attached directly to the
 *   current element. This getter should be used only on elements that contain a directive which starts a new isolate
 *   scope. Calling `scope()` on this element always returns the original non-isolate scope.
 *   Requires {@link guide/production#disabling-debug-data Debug Data} to be enabled.
 * - `inheritedData()` - same as `data()`, but walks up the DOM until a value is found or the top
 *   parent element is reached.
 *
 * @param {string|DOMElement} element HTML string or DOMElement to be wrapped into jQuery.
 * @returns {Object} jQuery object.
 */

JQLite.expando = 'ng339';

var jqCache = JQLite.cache = {},
    jqId = 1,
    addEventListenerFn = function(element, type, fn) {
      element.addEventListener(type, fn, false);
    },
    removeEventListenerFn = function(element, type, fn) {
      element.removeEventListener(type, fn, false);
    };

/*
 * !!! This is an undocumented "private" function !!!
 */
JQLite._data = function(node) {
  //jQuery always returns an object on cache miss
  return this.cache[node[this.expando]] || {};
};

function jqNextId() { return ++jqId; }


var SPECIAL_CHARS_REGEXP = /([\:\-\_]+(.))/g;
var MOZ_HACK_REGEXP = /^moz([A-Z])/;
var MOUSE_EVENT_MAP= { mouseleave: "mouseout", mouseenter: "mouseover"};
var jqLiteMinErr = minErr('jqLite');

/**
 * Converts snake_case to camelCase.
 * Also there is special case for Moz prefix starting with upper case letter.
 * @param name Name to normalize
 */
function camelCase(name) {
  return name.
    replace(SPECIAL_CHARS_REGEXP, function(_, separator, letter, offset) {
      return offset ? letter.toUpperCase() : letter;
    }).
    replace(MOZ_HACK_REGEXP, 'Moz$1');
}

var SINGLE_TAG_REGEXP = /^<([\w-]+)\s*\/?>(?:<\/\1>|)$/;
var HTML_REGEXP = /<|&#?\w+;/;
var TAG_NAME_REGEXP = /<([\w:-]+)/;
var XHTML_TAG_REGEXP = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi;

var wrapMap = {
  'option': [1, '<select multiple="multiple">', '</select>'],

  'thead': [1, '<table>', '</table>'],
  'col': [2, '<table><colgroup>', '</colgroup></table>'],
  'tr': [2, '<table><tbody>', '</tbody></table>'],
  'td': [3, '<table><tbody><tr>', '</tr></tbody></table>'],
  '_default': [0, "", ""]
};

wrapMap.optgroup = wrapMap.option;
wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
wrapMap.th = wrapMap.td;


function jqLiteIsTextNode(html) {
  return !HTML_REGEXP.test(html);
}

function jqLiteAcceptsData(node) {
  // The window object can accept data but has no nodeType
  // Otherwise we are only interested in elements (1) and documents (9)
  var nodeType = node.nodeType;
  return nodeType === NODE_TYPE_ELEMENT || !nodeType || nodeType === NODE_TYPE_DOCUMENT;
}

function jqLiteHasData(node) {
  for (var key in jqCache[node.ng339]) {
    return true;
  }
  return false;
}

function jqLiteBuildFragment(html, context) {
  var tmp, tag, wrap,
      fragment = context.createDocumentFragment(),
      nodes = [], i;

  if (jqLiteIsTextNode(html)) {
    // Convert non-html into a text node
    nodes.push(context.createTextNode(html));
  } else {
    // Convert html into DOM nodes
    tmp = tmp || fragment.appendChild(context.createElement("div"));
    tag = (TAG_NAME_REGEXP.exec(html) || ["", ""])[1].toLowerCase();
    wrap = wrapMap[tag] || wrapMap._default;
    tmp.innerHTML = wrap[1] + html.replace(XHTML_TAG_REGEXP, "<$1></$2>") + wrap[2];

    // Descend through wrappers to the right content
    i = wrap[0];
    while (i--) {
      tmp = tmp.lastChild;
    }

    nodes = concat(nodes, tmp.childNodes);

    tmp = fragment.firstChild;
    tmp.textContent = "";
  }

  // Remove wrapper from fragment
  fragment.textContent = "";
  fragment.innerHTML = ""; // Clear inner HTML
  forEach(nodes, function(node) {
    fragment.appendChild(node);
  });

  return fragment;
}

function jqLiteParseHTML(html, context) {
  context = context || document;
  var parsed;

  if ((parsed = SINGLE_TAG_REGEXP.exec(html))) {
    return [context.createElement(parsed[1])];
  }

  if ((parsed = jqLiteBuildFragment(html, context))) {
    return parsed.childNodes;
  }

  return [];
}


// IE9-11 has no method "contains" in SVG element and in Node.prototype. Bug #10259.
var jqLiteContains = Node.prototype.contains || function(arg) {
  // jshint bitwise: false
  return !!(this.compareDocumentPosition(arg) & 16);
  // jshint bitwise: true
};

/////////////////////////////////////////////
function JQLite(element) {
  if (element instanceof JQLite) {
    return element;
  }

  var argIsString;

  if (isString(element)) {
    element = trim(element);
    argIsString = true;
  }
  if (!(this instanceof JQLite)) {
    if (argIsString && element.charAt(0) != '<') {
      throw jqLiteMinErr('nosel', 'Looking up elements via selectors is not supported by jqLite! See: http://docs.angularjs.org/api/angular.element');
    }
    return new JQLite(element);
  }

  if (argIsString) {
    jqLiteAddNodes(this, jqLiteParseHTML(element));
  } else {
    jqLiteAddNodes(this, element);
  }
}

function jqLiteClone(element) {
  return element.cloneNode(true);
}

function jqLiteDealoc(element, onlyDescendants) {
  if (!onlyDescendants) jqLiteRemoveData(element);

  if (element.querySelectorAll) {
    var descendants = element.querySelectorAll('*');
    for (var i = 0, l = descendants.length; i < l; i++) {
      jqLiteRemoveData(descendants[i]);
    }
  }
}

function jqLiteOff(element, type, fn, unsupported) {
  if (isDefined(unsupported)) throw jqLiteMinErr('offargs', 'jqLite#off() does not support the `selector` argument');

  var expandoStore = jqLiteExpandoStore(element);
  var events = expandoStore && expandoStore.events;
  var handle = expandoStore && expandoStore.handle;

  if (!handle) return; //no listeners registered

  if (!type) {
    for (type in events) {
      if (type !== '$destroy') {
        removeEventListenerFn(element, type, handle);
      }
      delete events[type];
    }
  } else {

    var removeHandler = function(type) {
      var listenerFns = events[type];
      if (isDefined(fn)) {
        arrayRemove(listenerFns || [], fn);
      }
      if (!(isDefined(fn) && listenerFns && listenerFns.length > 0)) {
        removeEventListenerFn(element, type, handle);
        delete events[type];
      }
    };

    forEach(type.split(' '), function(type) {
      removeHandler(type);
      if (MOUSE_EVENT_MAP[type]) {
        removeHandler(MOUSE_EVENT_MAP[type]);
      }
    });
  }
}

function jqLiteRemoveData(element, name) {
  var expandoId = element.ng339;
  var expandoStore = expandoId && jqCache[expandoId];

  if (expandoStore) {
    if (name) {
      delete expandoStore.data[name];
      return;
    }

    if (expandoStore.handle) {
      if (expandoStore.events.$destroy) {
        expandoStore.handle({}, '$destroy');
      }
      jqLiteOff(element);
    }
    delete jqCache[expandoId];
    element.ng339 = undefined; // don't delete DOM expandos. IE and Chrome don't like it
  }
}


function jqLiteExpandoStore(element, createIfNecessary) {
  var expandoId = element.ng339,
      expandoStore = expandoId && jqCache[expandoId];

  if (createIfNecessary && !expandoStore) {
    element.ng339 = expandoId = jqNextId();
    expandoStore = jqCache[expandoId] = {events: {}, data: {}, handle: undefined};
  }

  return expandoStore;
}


function jqLiteData(element, key, value) {
  if (jqLiteAcceptsData(element)) {

    var isSimpleSetter = isDefined(value);
    var isSimpleGetter = !isSimpleSetter && key && !isObject(key);
    var massGetter = !key;
    var expandoStore = jqLiteExpandoStore(element, !isSimpleGetter);
    var data = expandoStore && expandoStore.data;

    if (isSimpleSetter) { // data('key', value)
      data[key] = value;
    } else {
      if (massGetter) {  // data()
        return data;
      } else {
        if (isSimpleGetter) { // data('key')
          // don't force creation of expandoStore if it doesn't exist yet
          return data && data[key];
        } else { // mass-setter: data({key1: val1, key2: val2})
          extend(data, key);
        }
      }
    }
  }
}

function jqLiteHasClass(element, selector) {
  if (!element.getAttribute) return false;
  return ((" " + (element.getAttribute('class') || '') + " ").replace(/[\n\t]/g, " ").
      indexOf(" " + selector + " ") > -1);
}

function jqLiteRemoveClass(element, cssClasses) {
  if (cssClasses && element.setAttribute) {
    forEach(cssClasses.split(' '), function(cssClass) {
      element.setAttribute('class', trim(
          (" " + (element.getAttribute('class') || '') + " ")
          .replace(/[\n\t]/g, " ")
          .replace(" " + trim(cssClass) + " ", " "))
      );
    });
  }
}

function jqLiteAddClass(element, cssClasses) {
  if (cssClasses && element.setAttribute) {
    var existingClasses = (' ' + (element.getAttribute('class') || '') + ' ')
                            .replace(/[\n\t]/g, " ");

    forEach(cssClasses.split(' '), function(cssClass) {
      cssClass = trim(cssClass);
      if (existingClasses.indexOf(' ' + cssClass + ' ') === -1) {
        existingClasses += cssClass + ' ';
      }
    });

    element.setAttribute('class', trim(existingClasses));
  }
}


function jqLiteAddNodes(root, elements) {
  // THIS CODE IS VERY HOT. Don't make changes without benchmarking.

  if (elements) {

    // if a Node (the most common case)
    if (elements.nodeType) {
      root[root.length++] = elements;
    } else {
      var length = elements.length;

      // if an Array or NodeList and not a Window
      if (typeof length === 'number' && elements.window !== elements) {
        if (length) {
          for (var i = 0; i < length; i++) {
            root[root.length++] = elements[i];
          }
        }
      } else {
        root[root.length++] = elements;
      }
    }
  }
}


function jqLiteController(element, name) {
  return jqLiteInheritedData(element, '$' + (name || 'ngController') + 'Controller');
}

function jqLiteInheritedData(element, name, value) {
  // if element is the document object work with the html element instead
  // this makes $(document).scope() possible
  if (element.nodeType == NODE_TYPE_DOCUMENT) {
    element = element.documentElement;
  }
  var names = isArray(name) ? name : [name];

  while (element) {
    for (var i = 0, ii = names.length; i < ii; i++) {
      if (isDefined(value = jqLite.data(element, names[i]))) return value;
    }

    // If dealing with a document fragment node with a host element, and no parent, use the host
    // element as the parent. This enables directives within a Shadow DOM or polyfilled Shadow DOM
    // to lookup parent controllers.
    element = element.parentNode || (element.nodeType === NODE_TYPE_DOCUMENT_FRAGMENT && element.host);
  }
}

function jqLiteEmpty(element) {
  jqLiteDealoc(element, true);
  while (element.firstChild) {
    element.removeChild(element.firstChild);
  }
}

function jqLiteRemove(element, keepData) {
  if (!keepData) jqLiteDealoc(element);
  var parent = element.parentNode;
  if (parent) parent.removeChild(element);
}


function jqLiteDocumentLoaded(action, win) {
  win = win || window;
  if (win.document.readyState === 'complete') {
    // Force the action to be run async for consistent behaviour
    // from the action's point of view
    // i.e. it will definitely not be in a $apply
    win.setTimeout(action);
  } else {
    // No need to unbind this handler as load is only ever called once
    jqLite(win).on('load', action);
  }
}

//////////////////////////////////////////
// Functions which are declared directly.
//////////////////////////////////////////
var JQLitePrototype = JQLite.prototype = {
  ready: function(fn) {
    var fired = false;

    function trigger() {
      if (fired) return;
      fired = true;
      fn();
    }

    // check if document is already loaded
    if (document.readyState === 'complete') {
      setTimeout(trigger);
    } else {
      this.on('DOMContentLoaded', trigger); // works for modern browsers and IE9
      // we can not use jqLite since we are not done loading and jQuery could be loaded later.
      // jshint -W064
      JQLite(window).on('load', trigger); // fallback to window.onload for others
      // jshint +W064
    }
  },
  toString: function() {
    var value = [];
    forEach(this, function(e) { value.push('' + e);});
    return '[' + value.join(', ') + ']';
  },

  eq: function(index) {
      return (index >= 0) ? jqLite(this[index]) : jqLite(this[this.length + index]);
  },

  length: 0,
  push: push,
  sort: [].sort,
  splice: [].splice
};

//////////////////////////////////////////
// Functions iterating getter/setters.
// these functions return self on setter and
// value on get.
//////////////////////////////////////////
var BOOLEAN_ATTR = {};
forEach('multiple,selected,checked,disabled,readOnly,required,open'.split(','), function(value) {
  BOOLEAN_ATTR[lowercase(value)] = value;
});
var BOOLEAN_ELEMENTS = {};
forEach('input,select,option,textarea,button,form,details'.split(','), function(value) {
  BOOLEAN_ELEMENTS[value] = true;
});
var ALIASED_ATTR = {
  'ngMinlength': 'minlength',
  'ngMaxlength': 'maxlength',
  'ngMin': 'min',
  'ngMax': 'max',
  'ngPattern': 'pattern'
};

function getBooleanAttrName(element, name) {
  // check dom last since we will most likely fail on name
  var booleanAttr = BOOLEAN_ATTR[name.toLowerCase()];

  // booleanAttr is here twice to minimize DOM access
  return booleanAttr && BOOLEAN_ELEMENTS[nodeName_(element)] && booleanAttr;
}

function getAliasedAttrName(name) {
  return ALIASED_ATTR[name];
}

forEach({
  data: jqLiteData,
  removeData: jqLiteRemoveData,
  hasData: jqLiteHasData
}, function(fn, name) {
  JQLite[name] = fn;
});

forEach({
  data: jqLiteData,
  inheritedData: jqLiteInheritedData,

  scope: function(element) {
    // Can't use jqLiteData here directly so we stay compatible with jQuery!
    return jqLite.data(element, '$scope') || jqLiteInheritedData(element.parentNode || element, ['$isolateScope', '$scope']);
  },

  isolateScope: function(element) {
    // Can't use jqLiteData here directly so we stay compatible with jQuery!
    return jqLite.data(element, '$isolateScope') || jqLite.data(element, '$isolateScopeNoTemplate');
  },

  controller: jqLiteController,

  injector: function(element) {
    return jqLiteInheritedData(element, '$injector');
  },

  removeAttr: function(element, name) {
    element.removeAttribute(name);
  },

  hasClass: jqLiteHasClass,

  css: function(element, name, value) {
    name = camelCase(name);

    if (isDefined(value)) {
      element.style[name] = value;
    } else {
      return element.style[name];
    }
  },

  attr: function(element, name, value) {
    var nodeType = element.nodeType;
    if (nodeType === NODE_TYPE_TEXT || nodeType === NODE_TYPE_ATTRIBUTE || nodeType === NODE_TYPE_COMMENT) {
      return;
    }
    var lowercasedName = lowercase(name);
    if (BOOLEAN_ATTR[lowercasedName]) {
      if (isDefined(value)) {
        if (!!value) {
          element[name] = true;
          element.setAttribute(name, lowercasedName);
        } else {
          element[name] = false;
          element.removeAttribute(lowercasedName);
        }
      } else {
        return (element[name] ||
                 (element.attributes.getNamedItem(name) || noop).specified)
               ? lowercasedName
               : undefined;
      }
    } else if (isDefined(value)) {
      element.setAttribute(name, value);
    } else if (element.getAttribute) {
      // the extra argument "2" is to get the right thing for a.href in IE, see jQuery code
      // some elements (e.g. Document) don't have get attribute, so return undefined
      var ret = element.getAttribute(name, 2);
      // normalize non-existing attributes to undefined (as jQuery)
      return ret === null ? undefined : ret;
    }
  },

  prop: function(element, name, value) {
    if (isDefined(value)) {
      element[name] = value;
    } else {
      return element[name];
    }
  },

  text: (function() {
    getText.$dv = '';
    return getText;

    function getText(element, value) {
      if (isUndefined(value)) {
        var nodeType = element.nodeType;
        return (nodeType === NODE_TYPE_ELEMENT || nodeType === NODE_TYPE_TEXT) ? element.textContent : '';
      }
      element.textContent = value;
    }
  })(),

  val: function(element, value) {
    if (isUndefined(value)) {
      if (element.multiple && nodeName_(element) === 'select') {
        var result = [];
        forEach(element.options, function(option) {
          if (option.selected) {
            result.push(option.value || option.text);
          }
        });
        return result.length === 0 ? null : result;
      }
      return element.value;
    }
    element.value = value;
  },

  html: function(element, value) {
    if (isUndefined(value)) {
      return element.innerHTML;
    }
    jqLiteDealoc(element, true);
    element.innerHTML = value;
  },

  empty: jqLiteEmpty
}, function(fn, name) {
  /**
   * Properties: writes return selection, reads return first value
   */
  JQLite.prototype[name] = function(arg1, arg2) {
    var i, key;
    var nodeCount = this.length;

    // jqLiteHasClass has only two arguments, but is a getter-only fn, so we need to special-case it
    // in a way that survives minification.
    // jqLiteEmpty takes no arguments but is a setter.
    if (fn !== jqLiteEmpty &&
        (isUndefined((fn.length == 2 && (fn !== jqLiteHasClass && fn !== jqLiteController)) ? arg1 : arg2))) {
      if (isObject(arg1)) {

        // we are a write, but the object properties are the key/values
        for (i = 0; i < nodeCount; i++) {
          if (fn === jqLiteData) {
            // data() takes the whole object in jQuery
            fn(this[i], arg1);
          } else {
            for (key in arg1) {
              fn(this[i], key, arg1[key]);
            }
          }
        }
        // return self for chaining
        return this;
      } else {
        // we are a read, so read the first child.
        // TODO: do we still need this?
        var value = fn.$dv;
        // Only if we have $dv do we iterate over all, otherwise it is just the first element.
        var jj = (isUndefined(value)) ? Math.min(nodeCount, 1) : nodeCount;
        for (var j = 0; j < jj; j++) {
          var nodeValue = fn(this[j], arg1, arg2);
          value = value ? value + nodeValue : nodeValue;
        }
        return value;
      }
    } else {
      // we are a write, so apply to all children
      for (i = 0; i < nodeCount; i++) {
        fn(this[i], arg1, arg2);
      }
      // return self for chaining
      return this;
    }
  };
});

function createEventHandler(element, events) {
  var eventHandler = function(event, type) {
    // jQuery specific api
    event.isDefaultPrevented = function() {
      return event.defaultPrevented;
    };

    var eventFns = events[type || event.type];
    var eventFnsLength = eventFns ? eventFns.length : 0;

    if (!eventFnsLength) return;

    if (isUndefined(event.immediatePropagationStopped)) {
      var originalStopImmediatePropagation = event.stopImmediatePropagation;
      event.stopImmediatePropagation = function() {
        event.immediatePropagationStopped = true;

        if (event.stopPropagation) {
          event.stopPropagation();
        }

        if (originalStopImmediatePropagation) {
          originalStopImmediatePropagation.call(event);
        }
      };
    }

    event.isImmediatePropagationStopped = function() {
      return event.immediatePropagationStopped === true;
    };

    // Some events have special handlers that wrap the real handler
    var handlerWrapper = eventFns.specialHandlerWrapper || defaultHandlerWrapper;

    // Copy event handlers in case event handlers array is modified during execution.
    if ((eventFnsLength > 1)) {
      eventFns = shallowCopy(eventFns);
    }

    for (var i = 0; i < eventFnsLength; i++) {
      if (!event.isImmediatePropagationStopped()) {
        handlerWrapper(element, event, eventFns[i]);
      }
    }
  };

  // TODO: this is a hack for angularMocks/clearDataCache that makes it possible to deregister all
  //       events on `element`
  eventHandler.elem = element;
  return eventHandler;
}

function defaultHandlerWrapper(element, event, handler) {
  handler.call(element, event);
}

function specialMouseHandlerWrapper(target, event, handler) {
  // Refer to jQuery's implementation of mouseenter & mouseleave
  // Read about mouseenter and mouseleave:
  // http://www.quirksmode.org/js/events_mouse.html#link8
  var related = event.relatedTarget;
  // For mousenter/leave call the handler if related is outside the target.
  // NB: No relatedTarget if the mouse left/entered the browser window
  if (!related || (related !== target && !jqLiteContains.call(target, related))) {
    handler.call(target, event);
  }
}

//////////////////////////////////////////
// Functions iterating traversal.
// These functions chain results into a single
// selector.
//////////////////////////////////////////
forEach({
  removeData: jqLiteRemoveData,

  on: function jqLiteOn(element, type, fn, unsupported) {
    if (isDefined(unsupported)) throw jqLiteMinErr('onargs', 'jqLite#on() does not support the `selector` or `eventData` parameters');

    // Do not add event handlers to non-elements because they will not be cleaned up.
    if (!jqLiteAcceptsData(element)) {
      return;
    }

    var expandoStore = jqLiteExpandoStore(element, true);
    var events = expandoStore.events;
    var handle = expandoStore.handle;

    if (!handle) {
      handle = expandoStore.handle = createEventHandler(element, events);
    }

    // http://jsperf.com/string-indexof-vs-split
    var types = type.indexOf(' ') >= 0 ? type.split(' ') : [type];
    var i = types.length;

    var addHandler = function(type, specialHandlerWrapper, noEventListener) {
      var eventFns = events[type];

      if (!eventFns) {
        eventFns = events[type] = [];
        eventFns.specialHandlerWrapper = specialHandlerWrapper;
        if (type !== '$destroy' && !noEventListener) {
          addEventListenerFn(element, type, handle);
        }
      }

      eventFns.push(fn);
    };

    while (i--) {
      type = types[i];
      if (MOUSE_EVENT_MAP[type]) {
        addHandler(MOUSE_EVENT_MAP[type], specialMouseHandlerWrapper);
        addHandler(type, undefined, true);
      } else {
        addHandler(type);
      }
    }
  },

  off: jqLiteOff,

  one: function(element, type, fn) {
    element = jqLite(element);

    //add the listener twice so that when it is called
    //you can remove the original function and still be
    //able to call element.off(ev, fn) normally
    element.on(type, function onFn() {
      element.off(type, fn);
      element.off(type, onFn);
    });
    element.on(type, fn);
  },

  replaceWith: function(element, replaceNode) {
    var index, parent = element.parentNode;
    jqLiteDealoc(element);
    forEach(new JQLite(replaceNode), function(node) {
      if (index) {
        parent.insertBefore(node, index.nextSibling);
      } else {
        parent.replaceChild(node, element);
      }
      index = node;
    });
  },

  children: function(element) {
    var children = [];
    forEach(element.childNodes, function(element) {
      if (element.nodeType === NODE_TYPE_ELEMENT) {
        children.push(element);
      }
    });
    return children;
  },

  contents: function(element) {
    return element.contentDocument || element.childNodes || [];
  },

  append: function(element, node) {
    var nodeType = element.nodeType;
    if (nodeType !== NODE_TYPE_ELEMENT && nodeType !== NODE_TYPE_DOCUMENT_FRAGMENT) return;

    node = new JQLite(node);

    for (var i = 0, ii = node.length; i < ii; i++) {
      var child = node[i];
      element.appendChild(child);
    }
  },

  prepend: function(element, node) {
    if (element.nodeType === NODE_TYPE_ELEMENT) {
      var index = element.firstChild;
      forEach(new JQLite(node), function(child) {
        element.insertBefore(child, index);
      });
    }
  },

  wrap: function(element, wrapNode) {
    wrapNode = jqLite(wrapNode).eq(0).clone()[0];
    var parent = element.parentNode;
    if (parent) {
      parent.replaceChild(wrapNode, element);
    }
    wrapNode.appendChild(element);
  },

  remove: jqLiteRemove,

  detach: function(element) {
    jqLiteRemove(element, true);
  },

  after: function(element, newElement) {
    var index = element, parent = element.parentNode;
    newElement = new JQLite(newElement);

    for (var i = 0, ii = newElement.length; i < ii; i++) {
      var node = newElement[i];
      parent.insertBefore(node, index.nextSibling);
      index = node;
    }
  },

  addClass: jqLiteAddClass,
  removeClass: jqLiteRemoveClass,

  toggleClass: function(element, selector, condition) {
    if (selector) {
      forEach(selector.split(' '), function(className) {
        var classCondition = condition;
        if (isUndefined(classCondition)) {
          classCondition = !jqLiteHasClass(element, className);
        }
        (classCondition ? jqLiteAddClass : jqLiteRemoveClass)(element, className);
      });
    }
  },

  parent: function(element) {
    var parent = element.parentNode;
    return parent && parent.nodeType !== NODE_TYPE_DOCUMENT_FRAGMENT ? parent : null;
  },

  next: function(element) {
    return element.nextElementSibling;
  },

  find: function(element, selector) {
    if (element.getElementsByTagName) {
      return element.getElementsByTagName(selector);
    } else {
      return [];
    }
  },

  clone: jqLiteClone,

  triggerHandler: function(element, event, extraParameters) {

    var dummyEvent, eventFnsCopy, handlerArgs;
    var eventName = event.type || event;
    var expandoStore = jqLiteExpandoStore(element);
    var events = expandoStore && expandoStore.events;
    var eventFns = events && events[eventName];

    if (eventFns) {
      // Create a dummy event to pass to the handlers
      dummyEvent = {
        preventDefault: function() { this.defaultPrevented = true; },
        isDefaultPrevented: function() { return this.defaultPrevented === true; },
        stopImmediatePropagation: function() { this.immediatePropagationStopped = true; },
        isImmediatePropagationStopped: function() { return this.immediatePropagationStopped === true; },
        stopPropagation: noop,
        type: eventName,
        target: element
      };

      // If a custom event was provided then extend our dummy event with it
      if (event.type) {
        dummyEvent = extend(dummyEvent, event);
      }

      // Copy event handlers in case event handlers array is modified during execution.
      eventFnsCopy = shallowCopy(eventFns);
      handlerArgs = extraParameters ? [dummyEvent].concat(extraParameters) : [dummyEvent];

      forEach(eventFnsCopy, function(fn) {
        if (!dummyEvent.isImmediatePropagationStopped()) {
          fn.apply(element, handlerArgs);
        }
      });
    }
  }
}, function(fn, name) {
  /**
   * chaining functions
   */
  JQLite.prototype[name] = function(arg1, arg2, arg3) {
    var value;

    for (var i = 0, ii = this.length; i < ii; i++) {
      if (isUndefined(value)) {
        value = fn(this[i], arg1, arg2, arg3);
        if (isDefined(value)) {
          // any function which returns a value needs to be wrapped
          value = jqLite(value);
        }
      } else {
        jqLiteAddNodes(value, fn(this[i], arg1, arg2, arg3));
      }
    }
    return isDefined(value) ? value : this;
  };

  // bind legacy bind/unbind to on/off
  JQLite.prototype.bind = JQLite.prototype.on;
  JQLite.prototype.unbind = JQLite.prototype.off;
});


// Provider for private $$jqLite service
function $$jqLiteProvider() {
  this.$get = function $$jqLite() {
    return extend(JQLite, {
      hasClass: function(node, classes) {
        if (node.attr) node = node[0];
        return jqLiteHasClass(node, classes);
      },
      addClass: function(node, classes) {
        if (node.attr) node = node[0];
        return jqLiteAddClass(node, classes);
      },
      removeClass: function(node, classes) {
        if (node.attr) node = node[0];
        return jqLiteRemoveClass(node, classes);
      }
    });
  };
}

/**
 * Computes a hash of an 'obj'.
 * Hash of a:
 *  string is string
 *  number is number as string
 *  object is either result of calling $$hashKey function on the object or uniquely generated id,
 *         that is also assigned to the $$hashKey property of the object.
 *
 * @param obj
 * @returns {string} hash string such that the same input will have the same hash string.
 *         The resulting string key is in 'type:hashKey' format.
 */
function hashKey(obj, nextUidFn) {
  var key = obj && obj.$$hashKey;

  if (key) {
    if (typeof key === 'function') {
      key = obj.$$hashKey();
    }
    return key;
  }

  var objType = typeof obj;
  if (objType == 'function' || (objType == 'object' && obj !== null)) {
    key = obj.$$hashKey = objType + ':' + (nextUidFn || nextUid)();
  } else {
    key = objType + ':' + obj;
  }

  return key;
}

/**
 * HashMap which can use objects as keys
 */
function HashMap(array, isolatedUid) {
  if (isolatedUid) {
    var uid = 0;
    this.nextUid = function() {
      return ++uid;
    };
  }
  forEach(array, this.put, this);
}
HashMap.prototype = {
  /**
   * Store key value pair
   * @param key key to store can be any type
   * @param value value to store can be any type
   */
  put: function(key, value) {
    this[hashKey(key, this.nextUid)] = value;
  },

  /**
   * @param key
   * @returns {Object} the value for the key
   */
  get: function(key) {
    return this[hashKey(key, this.nextUid)];
  },

  /**
   * Remove the key/value pair
   * @param key
   */
  remove: function(key) {
    var value = this[key = hashKey(key, this.nextUid)];
    delete this[key];
    return value;
  }
};

var $$HashMapProvider = [function() {
  this.$get = [function() {
    return HashMap;
  }];
}];

/**
 * @ngdoc function
 * @module ng
 * @name angular.injector
 * @kind function
 *
 * @description
 * Creates an injector object that can be used for retrieving services as well as for
 * dependency injection (see {@link guide/di dependency injection}).
 *
 * @param {Array.<string|Function>} modules A list of module functions or their aliases. See
 *     {@link angular.module}. The `ng` module must be explicitly added.
 * @param {boolean=} [strictDi=false] Whether the injector should be in strict mode, which
 *     disallows argument name annotation inference.
 * @returns {injector} Injector object. See {@link auto.$injector $injector}.
 *
 * @example
 * Typical usage
 * ```js
 *   // create an injector
 *   var $injector = angular.injector(['ng']);
 *
 *   // use the injector to kick off your application
 *   // use the type inference to auto inject arguments, or use implicit injection
 *   $injector.invoke(function($rootScope, $compile, $document) {
 *     $compile($document)($rootScope);
 *     $rootScope.$digest();
 *   });
 * ```
 *
 * Sometimes you want to get access to the injector of a currently running Angular app
 * from outside Angular. Perhaps, you want to inject and compile some markup after the
 * application has been bootstrapped. You can do this using the extra `injector()` added
 * to JQuery/jqLite elements. See {@link angular.element}.
 *
 * *This is fairly rare but could be the case if a third party library is injecting the
 * markup.*
 *
 * In the following example a new block of HTML containing a `ng-controller`
 * directive is added to the end of the document body by JQuery. We then compile and link
 * it into the current AngularJS scope.
 *
 * ```js
 * var $div = $('<div ng-controller="MyCtrl">{{content.label}}</div>');
 * $(document.body).append($div);
 *
 * angular.element(document).injector().invoke(function($compile) {
 *   var scope = angular.element($div).scope();
 *   $compile($div)(scope);
 * });
 * ```
 */


/**
 * @ngdoc module
 * @name auto
 * @description
 *
 * Implicit module which gets automatically added to each {@link auto.$injector $injector}.
 */

var FN_ARGS = /^[^\(]*\(\s*([^\)]*)\)/m;
var FN_ARG_SPLIT = /,/;
var FN_ARG = /^\s*(_?)(\S+?)\1\s*$/;
var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
var $injectorMinErr = minErr('$injector');

function anonFn(fn) {
  // For anonymous functions, showing at the very least the function signature can help in
  // debugging.
  var fnText = fn.toString().replace(STRIP_COMMENTS, ''),
      args = fnText.match(FN_ARGS);
  if (args) {
    return 'function(' + (args[1] || '').replace(/[\s\r\n]+/, ' ') + ')';
  }
  return 'fn';
}

function annotate(fn, strictDi, name) {
  var $inject,
      fnText,
      argDecl,
      last;

  if (typeof fn === 'function') {
    if (!($inject = fn.$inject)) {
      $inject = [];
      if (fn.length) {
        if (strictDi) {
          if (!isString(name) || !name) {
            name = fn.name || anonFn(fn);
          }
          throw $injectorMinErr('strictdi',
            '{0} is not using explicit annotation and cannot be invoked in strict mode', name);
        }
        fnText = fn.toString().replace(STRIP_COMMENTS, '');
        argDecl = fnText.match(FN_ARGS);
        forEach(argDecl[1].split(FN_ARG_SPLIT), function(arg) {
          arg.replace(FN_ARG, function(all, underscore, name) {
            $inject.push(name);
          });
        });
      }
      fn.$inject = $inject;
    }
  } else if (isArray(fn)) {
    last = fn.length - 1;
    assertArgFn(fn[last], 'fn');
    $inject = fn.slice(0, last);
  } else {
    assertArgFn(fn, 'fn', true);
  }
  return $inject;
}

///////////////////////////////////////

/**
 * @ngdoc service
 * @name $injector
 *
 * @description
 *
 * `$injector` is used to retrieve object instances as defined by
 * {@link auto.$provide provider}, instantiate types, invoke methods,
 * and load modules.
 *
 * The following always holds true:
 *
 * ```js
 *   var $injector = angular.injector();
 *   expect($injector.get('$injector')).toBe($injector);
 *   expect($injector.invoke(function($injector) {
 *     return $injector;
 *   })).toBe($injector);
 * ```
 *
 * # Injection Function Annotation
 *
 * JavaScript does not have annotations, and annotations are needed for dependency injection. The
 * following are all valid ways of annotating function with injection arguments and are equivalent.
 *
 * ```js
 *   // inferred (only works if code not minified/obfuscated)
 *   $injector.invoke(function(serviceA){});
 *
 *   // annotated
 *   function explicit(serviceA) {};
 *   explicit.$inject = ['serviceA'];
 *   $injector.invoke(explicit);
 *
 *   // inline
 *   $injector.invoke(['serviceA', function(serviceA){}]);
 * ```
 *
 * ## Inference
 *
 * In JavaScript calling `toString()` on a function returns the function definition. The definition
 * can then be parsed and the function arguments can be extracted. This method of discovering
 * annotations is disallowed when the injector is in strict mode.
 * *NOTE:* This does not work with minification, and obfuscation tools since these tools change the
 * argument names.
 *
 * ## `$inject` Annotation
 * By adding an `$inject` property onto a function the injection parameters can be specified.
 *
 * ## Inline
 * As an array of injection names, where the last item in the array is the function to call.
 */

/**
 * @ngdoc method
 * @name $injector#get
 *
 * @description
 * Return an instance of the service.
 *
 * @param {string} name The name of the instance to retrieve.
 * @param {string=} caller An optional string to provide the origin of the function call for error messages.
 * @return {*} The instance.
 */

/**
 * @ngdoc method
 * @name $injector#invoke
 *
 * @description
 * Invoke the method and supply the method arguments from the `$injector`.
 *
 * @param {Function|Array.<string|Function>} fn The injectable function to invoke. Function parameters are
 *   injected according to the {@link guide/di $inject Annotation} rules.
 * @param {Object=} self The `this` for the invoked method.
 * @param {Object=} locals Optional object. If preset then any argument names are read from this
 *                         object first, before the `$injector` is consulted.
 * @returns {*} the value returned by the invoked `fn` function.
 */

/**
 * @ngdoc method
 * @name $injector#has
 *
 * @description
 * Allows the user to query if the particular service exists.
 *
 * @param {string} name Name of the service to query.
 * @returns {boolean} `true` if injector has given service.
 */

/**
 * @ngdoc method
 * @name $injector#instantiate
 * @description
 * Create a new instance of JS type. The method takes a constructor function, invokes the new
 * operator, and supplies all of the arguments to the constructor function as specified by the
 * constructor annotation.
 *
 * @param {Function} Type Annotated constructor function.
 * @param {Object=} locals Optional object. If preset then any argument names are read from this
 * object first, before the `$injector` is consulted.
 * @returns {Object} new instance of `Type`.
 */

/**
 * @ngdoc method
 * @name $injector#annotate
 *
 * @description
 * Returns an array of service names which the function is requesting for injection. This API is
 * used by the injector to determine which services need to be injected into the function when the
 * function is invoked. There are three ways in which the function can be annotated with the needed
 * dependencies.
 *
 * # Argument names
 *
 * The simplest form is to extract the dependencies from the arguments of the function. This is done
 * by converting the function into a string using `toString()` method and extracting the argument
 * names.
 * ```js
 *   // Given
 *   function MyController($scope, $route) {
 *     // ...
 *   }
 *
 *   // Then
 *   expect(injector.annotate(MyController)).toEqual(['$scope', '$route']);
 * ```
 *
 * You can disallow this method by using strict injection mode.
 *
 * This method does not work with code minification / obfuscation. For this reason the following
 * annotation strategies are supported.
 *
 * # The `$inject` property
 *
 * If a function has an `$inject` property and its value is an array of strings, then the strings
 * represent names of services to be injected into the function.
 * ```js
 *   // Given
 *   var MyController = function(obfuscatedScope, obfuscatedRoute) {
 *     // ...
 *   }
 *   // Define function dependencies
 *   MyController['$inject'] = ['$scope', '$route'];
 *
 *   // Then
 *   expect(injector.annotate(MyController)).toEqual(['$scope', '$route']);
 * ```
 *
 * # The array notation
 *
 * It is often desirable to inline Injected functions and that's when setting the `$inject` property
 * is very inconvenient. In these situations using the array notation to specify the dependencies in
 * a way that survives minification is a better choice:
 *
 * ```js
 *   // We wish to write this (not minification / obfuscation safe)
 *   injector.invoke(function($compile, $rootScope) {
 *     // ...
 *   });
 *
 *   // We are forced to write break inlining
 *   var tmpFn = function(obfuscatedCompile, obfuscatedRootScope) {
 *     // ...
 *   };
 *   tmpFn.$inject = ['$compile', '$rootScope'];
 *   injector.invoke(tmpFn);
 *
 *   // To better support inline function the inline annotation is supported
 *   injector.invoke(['$compile', '$rootScope', function(obfCompile, obfRootScope) {
 *     // ...
 *   }]);
 *
 *   // Therefore
 *   expect(injector.annotate(
 *      ['$compile', '$rootScope', function(obfus_$compile, obfus_$rootScope) {}])
 *    ).toEqual(['$compile', '$rootScope']);
 * ```
 *
 * @param {Function|Array.<string|Function>} fn Function for which dependent service names need to
 * be retrieved as described above.
 *
 * @param {boolean=} [strictDi=false] Disallow argument name annotation inference.
 *
 * @returns {Array.<string>} The names of the services which the function requires.
 */




/**
 * @ngdoc service
 * @name $provide
 *
 * @description
 *
 * The {@link auto.$provide $provide} service has a number of methods for registering components
 * with the {@link auto.$injector $injector}. Many of these functions are also exposed on
 * {@link angular.Module}.
 *
 * An Angular **service** is a singleton object created by a **service factory**.  These **service
 * factories** are functions which, in turn, are created by a **service provider**.
 * The **service providers** are constructor functions. When instantiated they must contain a
 * property called `$get`, which holds the **service factory** function.
 *
 * When you request a service, the {@link auto.$injector $injector} is responsible for finding the
 * correct **service provider**, instantiating it and then calling its `$get` **service factory**
 * function to get the instance of the **service**.
 *
 * Often services have no configuration options and there is no need to add methods to the service
 * provider.  The provider will be no more than a constructor function with a `$get` property. For
 * these cases the {@link auto.$provide $provide} service has additional helper methods to register
 * services without specifying a provider.
 *
 * * {@link auto.$provide#provider provider(provider)} - registers a **service provider** with the
 *     {@link auto.$injector $injector}
 * * {@link auto.$provide#constant constant(obj)} - registers a value/object that can be accessed by
 *     providers and services.
 * * {@link auto.$provide#value value(obj)} - registers a value/object that can only be accessed by
 *     services, not providers.
 * * {@link auto.$provide#factory factory(fn)} - registers a service **factory function**, `fn`,
 *     that will be wrapped in a **service provider** object, whose `$get` property will contain the
 *     given factory function.
 * * {@link auto.$provide#service service(class)} - registers a **constructor function**, `class`
 *     that will be wrapped in a **service provider** object, whose `$get` property will instantiate
 *      a new object using the given constructor function.
 *
 * See the individual methods for more information and examples.
 */

/**
 * @ngdoc method
 * @name $provide#provider
 * @description
 *
 * Register a **provider function** with the {@link auto.$injector $injector}. Provider functions
 * are constructor functions, whose instances are responsible for "providing" a factory for a
 * service.
 *
 * Service provider names start with the name of the service they provide followed by `Provider`.
 * For example, the {@link ng.$log $log} service has a provider called
 * {@link ng.$logProvider $logProvider}.
 *
 * Service provider objects can have additional methods which allow configuration of the provider
 * and its service. Importantly, you can configure what kind of service is created by the `$get`
 * method, or how that service will act. For example, the {@link ng.$logProvider $logProvider} has a
 * method {@link ng.$logProvider#debugEnabled debugEnabled}
 * which lets you specify whether the {@link ng.$log $log} service will log debug messages to the
 * console or not.
 *
 * @param {string} name The name of the instance. NOTE: the provider will be available under `name +
                        'Provider'` key.
 * @param {(Object|function())} provider If the provider is:
 *
 *   - `Object`: then it should have a `$get` method. The `$get` method will be invoked using
 *     {@link auto.$injector#invoke $injector.invoke()} when an instance needs to be created.
 *   - `Constructor`: a new instance of the provider will be created using
 *     {@link auto.$injector#instantiate $injector.instantiate()}, then treated as `object`.
 *
 * @returns {Object} registered provider instance

 * @example
 *
 * The following example shows how to create a simple event tracking service and register it using
 * {@link auto.$provide#provider $provide.provider()}.
 *
 * ```js
 *  // Define the eventTracker provider
 *  function EventTrackerProvider() {
 *    var trackingUrl = '/track';
 *
 *    // A provider method for configuring where the tracked events should been saved
 *    this.setTrackingUrl = function(url) {
 *      trackingUrl = url;
 *    };
 *
 *    // The service factory function
 *    this.$get = ['$http', function($http) {
 *      var trackedEvents = {};
 *      return {
 *        // Call this to track an event
 *        event: function(event) {
 *          var count = trackedEvents[event] || 0;
 *          count += 1;
 *          trackedEvents[event] = count;
 *          return count;
 *        },
 *        // Call this to save the tracked events to the trackingUrl
 *        save: function() {
 *          $http.post(trackingUrl, trackedEvents);
 *        }
 *      };
 *    }];
 *  }
 *
 *  describe('eventTracker', function() {
 *    var postSpy;
 *
 *    beforeEach(module(function($provide) {
 *      // Register the eventTracker provider
 *      $provide.provider('eventTracker', EventTrackerProvider);
 *    }));
 *
 *    beforeEach(module(function(eventTrackerProvider) {
 *      // Configure eventTracker provider
 *      eventTrackerProvider.setTrackingUrl('/custom-track');
 *    }));
 *
 *    it('tracks events', inject(function(eventTracker) {
 *      expect(eventTracker.event('login')).toEqual(1);
 *      expect(eventTracker.event('login')).toEqual(2);
 *    }));
 *
 *    it('saves to the tracking url', inject(function(eventTracker, $http) {
 *      postSpy = spyOn($http, 'post');
 *      eventTracker.event('login');
 *      eventTracker.save();
 *      expect(postSpy).toHaveBeenCalled();
 *      expect(postSpy.mostRecentCall.args[0]).not.toEqual('/track');
 *      expect(postSpy.mostRecentCall.args[0]).toEqual('/custom-track');
 *      expect(postSpy.mostRecentCall.args[1]).toEqual({ 'login': 1 });
 *    }));
 *  });
 * ```
 */

/**
 * @ngdoc method
 * @name $provide#factory
 * @description
 *
 * Register a **service factory**, which will be called to return the service instance.
 * This is short for registering a service where its provider consists of only a `$get` property,
 * which is the given service factory function.
 * You should use {@link auto.$provide#factory $provide.factory(getFn)} if you do not need to
 * configure your service in a provider.
 *
 * @param {string} name The name of the instance.
 * @param {Function|Array.<string|Function>} $getFn The injectable $getFn for the instance creation.
 *                      Internally this is a short hand for `$provide.provider(name, {$get: $getFn})`.
 * @returns {Object} registered provider instance
 *
 * @example
 * Here is an example of registering a service
 * ```js
 *   $provide.factory('ping', ['$http', function($http) {
 *     return function ping() {
 *       return $http.send('/ping');
 *     };
 *   }]);
 * ```
 * You would then inject and use this service like this:
 * ```js
 *   someModule.controller('Ctrl', ['ping', function(ping) {
 *     ping();
 *   }]);
 * ```
 */


/**
 * @ngdoc method
 * @name $provide#service
 * @description
 *
 * Register a **service constructor**, which will be invoked with `new` to create the service
 * instance.
 * This is short for registering a service where its provider's `$get` property is the service
 * constructor function that will be used to instantiate the service instance.
 *
 * You should use {@link auto.$provide#service $provide.service(class)} if you define your service
 * as a type/class.
 *
 * @param {string} name The name of the instance.
 * @param {Function|Array.<string|Function>} constructor An injectable class (constructor function)
 *     that will be instantiated.
 * @returns {Object} registered provider instance
 *
 * @example
 * Here is an example of registering a service using
 * {@link auto.$provide#service $provide.service(class)}.
 * ```js
 *   var Ping = function($http) {
 *     this.$http = $http;
 *   };
 *
 *   Ping.$inject = ['$http'];
 *
 *   Ping.prototype.send = function() {
 *     return this.$http.get('/ping');
 *   };
 *   $provide.service('ping', Ping);
 * ```
 * You would then inject and use this service like this:
 * ```js
 *   someModule.controller('Ctrl', ['ping', function(ping) {
 *     ping.send();
 *   }]);
 * ```
 */


/**
 * @ngdoc method
 * @name $provide#value
 * @description
 *
 * Register a **value service** with the {@link auto.$injector $injector}, such as a string, a
 * number, an array, an object or a function.  This is short for registering a service where its
 * provider's `$get` property is a factory function that takes no arguments and returns the **value
 * service**.
 *
 * Value services are similar to constant services, except that they cannot be injected into a
 * module configuration function (see {@link angular.Module#config}) but they can be overridden by
 * an Angular
 * {@link auto.$provide#decorator decorator}.
 *
 * @param {string} name The name of the instance.
 * @param {*} value The value.
 * @returns {Object} registered provider instance
 *
 * @example
 * Here are some examples of creating value services.
 * ```js
 *   $provide.value('ADMIN_USER', 'admin');
 *
 *   $provide.value('RoleLookup', { admin: 0, writer: 1, reader: 2 });
 *
 *   $provide.value('halfOf', function(value) {
 *     return value / 2;
 *   });
 * ```
 */


/**
 * @ngdoc method
 * @name $provide#constant
 * @description
 *
 * Register a **constant service**, such as a string, a number, an array, an object or a function,
 * with the {@link auto.$injector $injector}. Unlike {@link auto.$provide#value value} it can be
 * injected into a module configuration function (see {@link angular.Module#config}) and it cannot
 * be overridden by an Angular {@link auto.$provide#decorator decorator}.
 *
 * @param {string} name The name of the constant.
 * @param {*} value The constant value.
 * @returns {Object} registered instance
 *
 * @example
 * Here a some examples of creating constants:
 * ```js
 *   $provide.constant('SHARD_HEIGHT', 306);
 *
 *   $provide.constant('MY_COLOURS', ['red', 'blue', 'grey']);
 *
 *   $provide.constant('double', function(value) {
 *     return value * 2;
 *   });
 * ```
 */


/**
 * @ngdoc method
 * @name $provide#decorator
 * @description
 *
 * Register a **service decorator** with the {@link auto.$injector $injector}. A service decorator
 * intercepts the creation of a service, allowing it to override or modify the behaviour of the
 * service. The object returned by the decorator may be the original service, or a new service
 * object which replaces or wraps and delegates to the original service.
 *
 * @param {string} name The name of the service to decorate.
 * @param {Function|Array.<string|Function>} decorator This function will be invoked when the service needs to be
 *    instantiated and should return the decorated service instance. The function is called using
 *    the {@link auto.$injector#invoke injector.invoke} method and is therefore fully injectable.
 *    Local injection arguments:
 *
 *    * `$delegate` - The original service instance, which can be monkey patched, configured,
 *      decorated or delegated to.
 *
 * @example
 * Here we decorate the {@link ng.$log $log} service to convert warnings to errors by intercepting
 * calls to {@link ng.$log#error $log.warn()}.
 * ```js
 *   $provide.decorator('$log', ['$delegate', function($delegate) {
 *     $delegate.warn = $delegate.error;
 *     return $delegate;
 *   }]);
 * ```
 */


function createInjector(modulesToLoad, strictDi) {
  strictDi = (strictDi === true);
  var INSTANTIATING = {},
      providerSuffix = 'Provider',
      path = [],
      loadedModules = new HashMap([], true),
      providerCache = {
        $provide: {
            provider: supportObject(provider),
            factory: supportObject(factory),
            service: supportObject(service),
            value: supportObject(value),
            constant: supportObject(constant),
            decorator: decorator
          }
      },
      providerInjector = (providerCache.$injector =
          createInternalInjector(providerCache, function(serviceName, caller) {
            if (angular.isString(caller)) {
              path.push(caller);
            }
            throw $injectorMinErr('unpr', "Unknown provider: {0}", path.join(' <- '));
          })),
      instanceCache = {},
      instanceInjector = (instanceCache.$injector =
          createInternalInjector(instanceCache, function(serviceName, caller) {
            var provider = providerInjector.get(serviceName + providerSuffix, caller);
            return instanceInjector.invoke(provider.$get, provider, undefined, serviceName);
          }));


  forEach(loadModules(modulesToLoad), function(fn) { if (fn) instanceInjector.invoke(fn); });

  return instanceInjector;

  ////////////////////////////////////
  // $provider
  ////////////////////////////////////

  function supportObject(delegate) {
    return function(key, value) {
      if (isObject(key)) {
        forEach(key, reverseParams(delegate));
      } else {
        return delegate(key, value);
      }
    };
  }

  function provider(name, provider_) {
    assertNotHasOwnProperty(name, 'service');
    if (isFunction(provider_) || isArray(provider_)) {
      provider_ = providerInjector.instantiate(provider_);
    }
    if (!provider_.$get) {
      throw $injectorMinErr('pget', "Provider '{0}' must define $get factory method.", name);
    }
    return providerCache[name + providerSuffix] = provider_;
  }

  function enforceReturnValue(name, factory) {
    return function enforcedReturnValue() {
      var result = instanceInjector.invoke(factory, this);
      if (isUndefined(result)) {
        throw $injectorMinErr('undef', "Provider '{0}' must return a value from $get factory method.", name);
      }
      return result;
    };
  }

  function factory(name, factoryFn, enforce) {
    return provider(name, {
      $get: enforce !== false ? enforceReturnValue(name, factoryFn) : factoryFn
    });
  }

  function service(name, constructor) {
    return factory(name, ['$injector', function($injector) {
      return $injector.instantiate(constructor);
    }]);
  }

  function value(name, val) { return factory(name, valueFn(val), false); }

  function constant(name, value) {
    assertNotHasOwnProperty(name, 'constant');
    providerCache[name] = value;
    instanceCache[name] = value;
  }

  function decorator(serviceName, decorFn) {
    var origProvider = providerInjector.get(serviceName + providerSuffix),
        orig$get = origProvider.$get;

    origProvider.$get = function() {
      var origInstance = instanceInjector.invoke(orig$get, origProvider);
      return instanceInjector.invoke(decorFn, null, {$delegate: origInstance});
    };
  }

  ////////////////////////////////////
  // Module Loading
  ////////////////////////////////////
  function loadModules(modulesToLoad) {
    assertArg(isUndefined(modulesToLoad) || isArray(modulesToLoad), 'modulesToLoad', 'not an array');
    var runBlocks = [], moduleFn;
    forEach(modulesToLoad, function(module) {
      if (loadedModules.get(module)) return;
      loadedModules.put(module, true);

      function runInvokeQueue(queue) {
        var i, ii;
        for (i = 0, ii = queue.length; i < ii; i++) {
          var invokeArgs = queue[i],
              provider = providerInjector.get(invokeArgs[0]);

          provider[invokeArgs[1]].apply(provider, invokeArgs[2]);
        }
      }

      try {
        if (isString(module)) {
          moduleFn = angularModule(module);
          runBlocks = runBlocks.concat(loadModules(moduleFn.requires)).concat(moduleFn._runBlocks);
          runInvokeQueue(moduleFn._invokeQueue);
          runInvokeQueue(moduleFn._configBlocks);
        } else if (isFunction(module)) {
            runBlocks.push(providerInjector.invoke(module));
        } else if (isArray(module)) {
            runBlocks.push(providerInjector.invoke(module));
        } else {
          assertArgFn(module, 'module');
        }
      } catch (e) {
        if (isArray(module)) {
          module = module[module.length - 1];
        }
        if (e.message && e.stack && e.stack.indexOf(e.message) == -1) {
          // Safari & FF's stack traces don't contain error.message content
          // unlike those of Chrome and IE
          // So if stack doesn't contain message, we create a new string that contains both.
          // Since error.stack is read-only in Safari, I'm overriding e and not e.stack here.
          /* jshint -W022 */
          e = e.message + '\n' + e.stack;
        }
        throw $injectorMinErr('modulerr', "Failed to instantiate module {0} due to:\n{1}",
                  module, e.stack || e.message || e);
      }
    });
    return runBlocks;
  }

  ////////////////////////////////////
  // internal Injector
  ////////////////////////////////////

  function createInternalInjector(cache, factory) {

    function getService(serviceName, caller) {
      if (cache.hasOwnProperty(serviceName)) {
        if (cache[serviceName] === INSTANTIATING) {
          throw $injectorMinErr('cdep', 'Circular dependency found: {0}',
                    serviceName + ' <- ' + path.join(' <- '));
        }
        return cache[serviceName];
      } else {
        try {
          path.unshift(serviceName);
          cache[serviceName] = INSTANTIATING;
          return cache[serviceName] = factory(serviceName, caller);
        } catch (err) {
          if (cache[serviceName] === INSTANTIATING) {
            delete cache[serviceName];
          }
          throw err;
        } finally {
          path.shift();
        }
      }
    }

    function invoke(fn, self, locals, serviceName) {
      if (typeof locals === 'string') {
        serviceName = locals;
        locals = null;
      }

      var args = [],
          $inject = createInjector.$$annotate(fn, strictDi, serviceName),
          length, i,
          key;

      for (i = 0, length = $inject.length; i < length; i++) {
        key = $inject[i];
        if (typeof key !== 'string') {
          throw $injectorMinErr('itkn',
                  'Incorrect injection token! Expected service name as string, got {0}', key);
        }
        args.push(
          locals && locals.hasOwnProperty(key)
          ? locals[key]
          : getService(key, serviceName)
        );
      }
      if (isArray(fn)) {
        fn = fn[length];
      }

      // http://jsperf.com/angularjs-invoke-apply-vs-switch
      // #5388
      return fn.apply(self, args);
    }

    function instantiate(Type, locals, serviceName) {
      // Check if Type is annotated and use just the given function at n-1 as parameter
      // e.g. someModule.factory('greeter', ['$window', function(renamed$window) {}]);
      // Object creation: http://jsperf.com/create-constructor/2
      var instance = Object.create((isArray(Type) ? Type[Type.length - 1] : Type).prototype || null);
      var returnedValue = invoke(Type, instance, locals, serviceName);

      return isObject(returnedValue) || isFunction(returnedValue) ? returnedValue : instance;
    }

    return {
      invoke: invoke,
      instantiate: instantiate,
      get: getService,
      annotate: createInjector.$$annotate,
      has: function(name) {
        return providerCache.hasOwnProperty(name + providerSuffix) || cache.hasOwnProperty(name);
      }
    };
  }
}

createInjector.$$annotate = annotate;

/**
 * @ngdoc provider
 * @name $anchorScrollProvider
 *
 * @description
 * Use `$anchorScrollProvider` to disable automatic scrolling whenever
 * {@link ng.$location#hash $location.hash()} changes.
 */
function $AnchorScrollProvider() {

  var autoScrollingEnabled = true;

  /**
   * @ngdoc method
   * @name $anchorScrollProvider#disableAutoScrolling
   *
   * @description
   * By default, {@link ng.$anchorScroll $anchorScroll()} will automatically detect changes to
   * {@link ng.$location#hash $location.hash()} and scroll to the element matching the new hash.<br />
   * Use this method to disable automatic scrolling.
   *
   * If automatic scrolling is disabled, one must explicitly call
   * {@link ng.$anchorScroll $anchorScroll()} in order to scroll to the element related to the
   * current hash.
   */
  this.disableAutoScrolling = function() {
    autoScrollingEnabled = false;
  };

  /**
   * @ngdoc service
   * @name $anchorScroll
   * @kind function
   * @requires $window
   * @requires $location
   * @requires $rootScope
   *
   * @description
   * When called, it scrolls to the element related to the specified `hash` or (if omitted) to the
   * current value of {@link ng.$location#hash $location.hash()}, according to the rules specified
   * in the
   * [HTML5 spec](http://www.w3.org/html/wg/drafts/html/master/browsers.html#the-indicated-part-of-the-document).
   *
   * It also watches the {@link ng.$location#hash $location.hash()} and automatically scrolls to
   * match any anchor whenever it changes. This can be disabled by calling
   * {@link ng.$anchorScrollProvider#disableAutoScrolling $anchorScrollProvider.disableAutoScrolling()}.
   *
   * Additionally, you can use its {@link ng.$anchorScroll#yOffset yOffset} property to specify a
   * vertical scroll-offset (either fixed or dynamic).
   *
   * @param {string=} hash The hash specifying the element to scroll to. If omitted, the value of
   *                       {@link ng.$location#hash $location.hash()} will be used.
   *
   * @property {(number|function|jqLite)} yOffset
   * If set, specifies a vertical scroll-offset. This is often useful when there are fixed
   * positioned elements at the top of the page, such as navbars, headers etc.
   *
   * `yOffset` can be specified in various ways:
   * - **number**: A fixed number of pixels to be used as offset.<br /><br />
   * - **function**: A getter function called everytime `$anchorScroll()` is executed. Must return
   *   a number representing the offset (in pixels).<br /><br />
   * - **jqLite**: A jqLite/jQuery element to be used for specifying the offset. The distance from
   *   the top of the page to the element's bottom will be used as offset.<br />
   *   **Note**: The element will be taken into account only as long as its `position` is set to
   *   `fixed`. This option is useful, when dealing with responsive navbars/headers that adjust
   *   their height and/or positioning according to the viewport's size.
   *
   * <br />
   * <div class="alert alert-warning">
   * In order for `yOffset` to work properly, scrolling should take place on the document's root and
   * not some child element.
   * </div>
   *
   * @example
     <example module="anchorScrollExample">
       <file name="index.html">
         <div id="scrollArea" ng-controller="ScrollController">
           <a ng-click="gotoBottom()">Go to bottom</a>
           <a id="bottom"></a> You're at the bottom!
         </div>
       </file>
       <file name="script.js">
         angular.module('anchorScrollExample', [])
           .controller('ScrollController', ['$scope', '$location', '$anchorScroll',
             function ($scope, $location, $anchorScroll) {
               $scope.gotoBottom = function() {
                 // set the location.hash to the id of
                 // the element you wish to scroll to.
                 $location.hash('bottom');

                 // call $anchorScroll()
                 $anchorScroll();
               };
             }]);
       </file>
       <file name="style.css">
         #scrollArea {
           height: 280px;
           overflow: auto;
         }

         #bottom {
           display: block;
           margin-top: 2000px;
         }
       </file>
     </example>
   *
   * <hr />
   * The example below illustrates the use of a vertical scroll-offset (specified as a fixed value).
   * See {@link ng.$anchorScroll#yOffset $anchorScroll.yOffset} for more details.
   *
   * @example
     <example module="anchorScrollOffsetExample">
       <file name="index.html">
         <div class="fixed-header" ng-controller="headerCtrl">
           <a href="" ng-click="gotoAnchor(x)" ng-repeat="x in [1,2,3,4,5]">
             Go to anchor {{x}}
           </a>
         </div>
         <div id="anchor{{x}}" class="anchor" ng-repeat="x in [1,2,3,4,5]">
           Anchor {{x}} of 5
         </div>
       </file>
       <file name="script.js">
         angular.module('anchorScrollOffsetExample', [])
           .run(['$anchorScroll', function($anchorScroll) {
             $anchorScroll.yOffset = 50;   // always scroll by 50 extra pixels
           }])
           .controller('headerCtrl', ['$anchorScroll', '$location', '$scope',
             function ($anchorScroll, $location, $scope) {
               $scope.gotoAnchor = function(x) {
                 var newHash = 'anchor' + x;
                 if ($location.hash() !== newHash) {
                   // set the $location.hash to `newHash` and
                   // $anchorScroll will automatically scroll to it
                   $location.hash('anchor' + x);
                 } else {
                   // call $anchorScroll() explicitly,
                   // since $location.hash hasn't changed
                   $anchorScroll();
                 }
               };
             }
           ]);
       </file>
       <file name="style.css">
         body {
           padding-top: 50px;
         }

         .anchor {
           border: 2px dashed DarkOrchid;
           padding: 10px 10px 200px 10px;
         }

         .fixed-header {
           background-color: rgba(0, 0, 0, 0.2);
           height: 50px;
           position: fixed;
           top: 0; left: 0; right: 0;
         }

         .fixed-header > a {
           display: inline-block;
           margin: 5px 15px;
         }
       </file>
     </example>
   */
  this.$get = ['$window', '$location', '$rootScope', function($window, $location, $rootScope) {
    var document = $window.document;

    // Helper function to get first anchor from a NodeList
    // (using `Array#some()` instead of `angular#forEach()` since it's more performant
    //  and working in all supported browsers.)
    function getFirstAnchor(list) {
      var result = null;
      Array.prototype.some.call(list, function(element) {
        if (nodeName_(element) === 'a') {
          result = element;
          return true;
        }
      });
      return result;
    }

    function getYOffset() {

      var offset = scroll.yOffset;

      if (isFunction(offset)) {
        offset = offset();
      } else if (isElement(offset)) {
        var elem = offset[0];
        var style = $window.getComputedStyle(elem);
        if (style.position !== 'fixed') {
          offset = 0;
        } else {
          offset = elem.getBoundingClientRect().bottom;
        }
      } else if (!isNumber(offset)) {
        offset = 0;
      }

      return offset;
    }

    function scrollTo(elem) {
      if (elem) {
        elem.scrollIntoView();

        var offset = getYOffset();

        if (offset) {
          // `offset` is the number of pixels we should scroll UP in order to align `elem` properly.
          // This is true ONLY if the call to `elem.scrollIntoView()` initially aligns `elem` at the
          // top of the viewport.
          //
          // IF the number of pixels from the top of `elem` to the end of the page's content is less
          // than the height of the viewport, then `elem.scrollIntoView()` will align the `elem` some
          // way down the page.
          //
          // This is often the case for elements near the bottom of the page.
          //
          // In such cases we do not need to scroll the whole `offset` up, just the difference between
          // the top of the element and the offset, which is enough to align the top of `elem` at the
          // desired position.
          var elemTop = elem.getBoundingClientRect().top;
          $window.scrollBy(0, elemTop - offset);
        }
      } else {
        $window.scrollTo(0, 0);
      }
    }

    function scroll(hash) {
      hash = isString(hash) ? hash : $location.hash();
      var elm;

      // empty hash, scroll to the top of the page
      if (!hash) scrollTo(null);

      // element with given id
      else if ((elm = document.getElementById(hash))) scrollTo(elm);

      // first anchor with given name :-D
      else if ((elm = getFirstAnchor(document.getElementsByName(hash)))) scrollTo(elm);

      // no element and hash == 'top', scroll to the top of the page
      else if (hash === 'top') scrollTo(null);
    }

    // does not scroll when user clicks on anchor link that is currently on
    // (no url change, no $location.hash() change), browser native does scroll
    if (autoScrollingEnabled) {
      $rootScope.$watch(function autoScrollWatch() {return $location.hash();},
        function autoScrollWatchAction(newVal, oldVal) {
          // skip the initial scroll if $location.hash is empty
          if (newVal === oldVal && newVal === '') return;

          jqLiteDocumentLoaded(function() {
            $rootScope.$evalAsync(scroll);
          });
        });
    }

    return scroll;
  }];
}

var $animateMinErr = minErr('$animate');
var ELEMENT_NODE = 1;
var NG_ANIMATE_CLASSNAME = 'ng-animate';

function mergeClasses(a,b) {
  if (!a && !b) return '';
  if (!a) return b;
  if (!b) return a;
  if (isArray(a)) a = a.join(' ');
  if (isArray(b)) b = b.join(' ');
  return a + ' ' + b;
}

function extractElementNode(element) {
  for (var i = 0; i < element.length; i++) {
    var elm = element[i];
    if (elm.nodeType === ELEMENT_NODE) {
      return elm;
    }
  }
}

function splitClasses(classes) {
  if (isString(classes)) {
    classes = classes.split(' ');
  }

  // Use createMap() to prevent class assumptions involving property names in
  // Object.prototype
  var obj = createMap();
  forEach(classes, function(klass) {
    // sometimes the split leaves empty string values
    // incase extra spaces were applied to the options
    if (klass.length) {
      obj[klass] = true;
    }
  });
  return obj;
}

// if any other type of options value besides an Object value is
// passed into the $animate.method() animation then this helper code
// will be run which will ignore it. While this patch is not the
// greatest solution to this, a lot of existing plugins depend on
// $animate to either call the callback (< 1.2) or return a promise
// that can be changed. This helper function ensures that the options
// are wiped clean incase a callback function is provided.
function prepareAnimateOptions(options) {
  return isObject(options)
      ? options
      : {};
}

var $$CoreAnimateRunnerProvider = function() {
  this.$get = ['$q', '$$rAF', function($q, $$rAF) {
    function AnimateRunner() {}
    AnimateRunner.all = noop;
    AnimateRunner.chain = noop;
    AnimateRunner.prototype = {
      end: noop,
      cancel: noop,
      resume: noop,
      pause: noop,
      complete: noop,
      then: function(pass, fail) {
        return $q(function(resolve) {
          $$rAF(function() {
            resolve();
          });
        }).then(pass, fail);
      }
    };
    return AnimateRunner;
  }];
};

// this is prefixed with Core since it conflicts with
// the animateQueueProvider defined in ngAnimate/animateQueue.js
var $$CoreAnimateQueueProvider = function() {
  var postDigestQueue = new HashMap();
  var postDigestElements = [];

  this.$get = ['$$AnimateRunner', '$rootScope',
       function($$AnimateRunner,   $rootScope) {
    return {
      enabled: noop,
      on: noop,
      off: noop,
      pin: noop,

      push: function(element, event, options, domOperation) {
        domOperation        && domOperation();

        options = options || {};
        options.from        && element.css(options.from);
        options.to          && element.css(options.to);

        if (options.addClass || options.removeClass) {
          addRemoveClassesPostDigest(element, options.addClass, options.removeClass);
        }

        return new $$AnimateRunner(); // jshint ignore:line
      }
    };


    function updateData(data, classes, value) {
      var changed = false;
      if (classes) {
        classes = isString(classes) ? classes.split(' ') :
                  isArray(classes) ? classes : [];
        forEach(classes, function(className) {
          if (className) {
            changed = true;
            data[className] = value;
          }
        });
      }
      return changed;
    }

    function handleCSSClassChanges() {
      forEach(postDigestElements, function(element) {
        var data = postDigestQueue.get(element);
        if (data) {
          var existing = splitClasses(element.attr('class'));
          var toAdd = '';
          var toRemove = '';
          forEach(data, function(status, className) {
            var hasClass = !!existing[className];
            if (status !== hasClass) {
              if (status) {
                toAdd += (toAdd.length ? ' ' : '') + className;
              } else {
                toRemove += (toRemove.length ? ' ' : '') + className;
              }
            }
          });

          forEach(element, function(elm) {
            toAdd    && jqLiteAddClass(elm, toAdd);
            toRemove && jqLiteRemoveClass(elm, toRemove);
          });
          postDigestQueue.remove(element);
        }
      });
      postDigestElements.length = 0;
    }


    function addRemoveClassesPostDigest(element, add, remove) {
      var data = postDigestQueue.get(element) || {};

      var classesAdded = updateData(data, add, true);
      var classesRemoved = updateData(data, remove, false);

      if (classesAdded || classesRemoved) {

        postDigestQueue.put(element, data);
        postDigestElements.push(element);

        if (postDigestElements.length === 1) {
          $rootScope.$$postDigest(handleCSSClassChanges);
        }
      }
    }
  }];
};

/**
 * @ngdoc provider
 * @name $animateProvider
 *
 * @description
 * Default implementation of $animate that doesn't perform any animations, instead just
 * synchronously performs DOM updates and resolves the returned runner promise.
 *
 * In order to enable animations the `ngAnimate` module has to be loaded.
 *
 * To see the functional implementation check out `src/ngAnimate/animate.js`.
 */
var $AnimateProvider = ['$provide', function($provide) {
  var provider = this;

  this.$$registeredAnimations = Object.create(null);

   /**
   * @ngdoc method
   * @name $animateProvider#register
   *
   * @description
   * Registers a new injectable animation factory function. The factory function produces the
   * animation object which contains callback functions for each event that is expected to be
   * animated.
   *
   *   * `eventFn`: `function(element, ... , doneFunction, options)`
   *   The element to animate, the `doneFunction` and the options fed into the animation. Depending
   *   on the type of animation additional arguments will be injected into the animation function. The
   *   list below explains the function signatures for the different animation methods:
   *
   *   - setClass: function(element, addedClasses, removedClasses, doneFunction, options)
   *   - addClass: function(element, addedClasses, doneFunction, options)
   *   - removeClass: function(element, removedClasses, doneFunction, options)
   *   - enter, leave, move: function(element, doneFunction, options)
   *   - animate: function(element, fromStyles, toStyles, doneFunction, options)
   *
   *   Make sure to trigger the `doneFunction` once the animation is fully complete.
   *
   * ```js
   *   return {
   *     //enter, leave, move signature
   *     eventFn : function(element, done, options) {
   *       //code to run the animation
   *       //once complete, then run done()
   *       return function endFunction(wasCancelled) {
   *         //code to cancel the animation
   *       }
   *     }
   *   }
   * ```
   *
   * @param {string} name The name of the animation (this is what the class-based CSS value will be compared to).
   * @param {Function} factory The factory function that will be executed to return the animation
   *                           object.
   */
  this.register = function(name, factory) {
    if (name && name.charAt(0) !== '.') {
      throw $animateMinErr('notcsel', "Expecting class selector starting with '.' got '{0}'.", name);
    }

    var key = name + '-animation';
    provider.$$registeredAnimations[name.substr(1)] = key;
    $provide.factory(key, factory);
  };

  /**
   * @ngdoc method
   * @name $animateProvider#classNameFilter
   *
   * @description
   * Sets and/or returns the CSS class regular expression that is checked when performing
   * an animation. Upon bootstrap the classNameFilter value is not set at all and will
   * therefore enable $animate to attempt to perform an animation on any element that is triggered.
   * When setting the `classNameFilter` value, animations will only be performed on elements
   * that successfully match the filter expression. This in turn can boost performance
   * for low-powered devices as well as applications containing a lot of structural operations.
   * @param {RegExp=} expression The className expression which will be checked against all animations
   * @return {RegExp} The current CSS className expression value. If null then there is no expression value
   */
  this.classNameFilter = function(expression) {
    if (arguments.length === 1) {
      this.$$classNameFilter = (expression instanceof RegExp) ? expression : null;
      if (this.$$classNameFilter) {
        var reservedRegex = new RegExp("(\\s+|\\/)" + NG_ANIMATE_CLASSNAME + "(\\s+|\\/)");
        if (reservedRegex.test(this.$$classNameFilter.toString())) {
          throw $animateMinErr('nongcls','$animateProvider.classNameFilter(regex) prohibits accepting a regex value which matches/contains the "{0}" CSS class.', NG_ANIMATE_CLASSNAME);

        }
      }
    }
    return this.$$classNameFilter;
  };

  this.$get = ['$$animateQueue', function($$animateQueue) {
    function domInsert(element, parentElement, afterElement) {
      // if for some reason the previous element was removed
      // from the dom sometime before this code runs then let's
      // just stick to using the parent element as the anchor
      if (afterElement) {
        var afterNode = extractElementNode(afterElement);
        if (afterNode && !afterNode.parentNode && !afterNode.previousElementSibling) {
          afterElement = null;
        }
      }
      afterElement ? afterElement.after(element) : parentElement.prepend(element);
    }

    /**
     * @ngdoc service
     * @name $animate
     * @description The $animate service exposes a series of DOM utility methods that provide support
     * for animation hooks. The default behavior is the application of DOM operations, however,
     * when an animation is detected (and animations are enabled), $animate will do the heavy lifting
     * to ensure that animation runs with the triggered DOM operation.
     *
     * By default $animate doesn't trigger any animations. This is because the `ngAnimate` module isn't
     * included and only when it is active then the animation hooks that `$animate` triggers will be
     * functional. Once active then all structural `ng-` directives will trigger animations as they perform
     * their DOM-related operations (enter, leave and move). Other directives such as `ngClass`,
     * `ngShow`, `ngHide` and `ngMessages` also provide support for animations.
     *
     * It is recommended that the`$animate` service is always used when executing DOM-related procedures within directives.
     *
     * To learn more about enabling animation support, click here to visit the
     * {@link ngAnimate ngAnimate module page}.
     */
    return {
      // we don't call it directly since non-existant arguments may
      // be interpreted as null within the sub enabled function

      /**
       *
       * @ngdoc method
       * @name $animate#on
       * @kind function
       * @description Sets up an event listener to fire whenever the animation event (enter, leave, move, etc...)
       *    has fired on the given element or among any of its children. Once the listener is fired, the provided callback
       *    is fired with the following params:
       *
       * ```js
       * $animate.on('enter', container,
       *    function callback(element, phase) {
       *      // cool we detected an enter animation within the container
       *    }
       * );
       * ```
       *
       * @param {string} event the animation event that will be captured (e.g. enter, leave, move, addClass, removeClass, etc...)
       * @param {DOMElement} container the container element that will capture each of the animation events that are fired on itself
       *     as well as among its children
       * @param {Function} callback the callback function that will be fired when the listener is triggered
       *
       * The arguments present in the callback function are:
       * * `element` - The captured DOM element that the animation was fired on.
       * * `phase` - The phase of the animation. The two possible phases are **start** (when the animation starts) and **close** (when it ends).
       */
      on: $$animateQueue.on,

      /**
       *
       * @ngdoc method
       * @name $animate#off
       * @kind function
       * @description Deregisters an event listener based on the event which has been associated with the provided element. This method
       * can be used in three different ways depending on the arguments:
       *
       * ```js
       * // remove all the animation event listeners listening for `enter`
       * $animate.off('enter');
       *
       * // remove all the animation event listeners listening for `enter` on the given element and its children
       * $animate.off('enter', container);
       *
       * // remove the event listener function provided by `listenerFn` that is set
       * // to listen for `enter` on the given `element` as well as its children
       * $animate.off('enter', container, callback);
       * ```
       *
       * @param {string} event the animation event (e.g. enter, leave, move, addClass, removeClass, etc...)
       * @param {DOMElement=} container the container element the event listener was placed on
       * @param {Function=} callback the callback function that was registered as the listener
       */
      off: $$animateQueue.off,

      /**
       * @ngdoc method
       * @name $animate#pin
       * @kind function
       * @description Associates the provided element with a host parent element to allow the element to be animated even if it exists
       *    outside of the DOM structure of the Angular application. By doing so, any animation triggered via `$animate` can be issued on the
       *    element despite being outside the realm of the application or within another application. Say for example if the application
       *    was bootstrapped on an element that is somewhere inside of the `<body>` tag, but we wanted to allow for an element to be situated
       *    as a direct child of `document.body`, then this can be achieved by pinning the element via `$animate.pin(element)`. Keep in mind
       *    that calling `$animate.pin(element, parentElement)` will not actually insert into the DOM anywhere; it will just create the association.
       *
       *    Note that this feature is only active when the `ngAnimate` module is used.
       *
       * @param {DOMElement} element the external element that will be pinned
       * @param {DOMElement} parentElement the host parent element that will be associated with the external element
       */
      pin: $$animateQueue.pin,

      /**
       *
       * @ngdoc method
       * @name $animate#enabled
       * @kind function
       * @description Used to get and set whether animations are enabled or not on the entire application or on an element and its children. This
       * function can be called in four ways:
       *
       * ```js
       * // returns true or false
       * $animate.enabled();
       *
       * // changes the enabled state for all animations
       * $animate.enabled(false);
       * $animate.enabled(true);
       *
       * // returns true or false if animations are enabled for an element
       * $animate.enabled(element);
       *
       * // changes the enabled state for an element and its children
       * $animate.enabled(element, true);
       * $animate.enabled(element, false);
       * ```
       *
       * @param {DOMElement=} element the element that will be considered for checking/setting the enabled state
       * @param {boolean=} enabled whether or not the animations will be enabled for the element
       *
       * @return {boolean} whether or not animations are enabled
       */
      enabled: $$animateQueue.enabled,

      /**
       * @ngdoc method
       * @name $animate#cancel
       * @kind function
       * @description Cancels the provided animation.
       *
       * @param {Promise} animationPromise The animation promise that is returned when an animation is started.
       */
      cancel: function(runner) {
        runner.end && runner.end();
      },

      /**
       *
       * @ngdoc method
       * @name $animate#enter
       * @kind function
       * @description Inserts the element into the DOM either after the `after` element (if provided) or
       *   as the first child within the `parent` element and then triggers an animation.
       *   A promise is returned that will be resolved during the next digest once the animation
       *   has completed.
       *
       * @param {DOMElement} element the element which will be inserted into the DOM
       * @param {DOMElement} parent the parent element which will append the element as
       *   a child (so long as the after element is not present)
       * @param {DOMElement=} after the sibling element after which the element will be appended
       * @param {object=} options an optional collection of options/styles that will be applied to the element
       *
       * @return {Promise} the animation callback promise
       */
      enter: function(element, parent, after, options) {
        parent = parent && jqLite(parent);
        after = after && jqLite(after);
        parent = parent || after.parent();
        domInsert(element, parent, after);
        return $$animateQueue.push(element, 'enter', prepareAnimateOptions(options));
      },

      /**
       *
       * @ngdoc method
       * @name $animate#move
       * @kind function
       * @description Inserts (moves) the element into its new position in the DOM either after
       *   the `after` element (if provided) or as the first child within the `parent` element
       *   and then triggers an animation. A promise is returned that will be resolved
       *   during the next digest once the animation has completed.
       *
       * @param {DOMElement} element the element which will be moved into the new DOM position
       * @param {DOMElement} parent the parent element which will append the element as
       *   a child (so long as the after element is not present)
       * @param {DOMElement=} after the sibling element after which the element will be appended
       * @param {object=} options an optional collection of options/styles that will be applied to the element
       *
       * @return {Promise} the animation callback promise
       */
      move: function(element, parent, after, options) {
        parent = parent && jqLite(parent);
        after = after && jqLite(after);
        parent = parent || after.parent();
        domInsert(element, parent, after);
        return $$animateQueue.push(element, 'move', prepareAnimateOptions(options));
      },

      /**
       * @ngdoc method
       * @name $animate#leave
       * @kind function
       * @description Triggers an animation and then removes the element from the DOM.
       * When the function is called a promise is returned that will be resolved during the next
       * digest once the animation has completed.
       *
       * @param {DOMElement} element the element which will be removed from the DOM
       * @param {object=} options an optional collection of options/styles that will be applied to the element
       *
       * @return {Promise} the animation callback promise
       */
      leave: function(element, options) {
        return $$animateQueue.push(element, 'leave', prepareAnimateOptions(options), function() {
          element.remove();
        });
      },

      /**
       * @ngdoc method
       * @name $animate#addClass
       * @kind function
       *
       * @description Triggers an addClass animation surrounding the addition of the provided CSS class(es). Upon
       *   execution, the addClass operation will only be handled after the next digest and it will not trigger an
       *   animation if element already contains the CSS class or if the class is removed at a later step.
       *   Note that class-based animations are treated differently compared to structural animations
       *   (like enter, move and leave) since the CSS classes may be added/removed at different points
       *   depending if CSS or JavaScript animations are used.
       *
       * @param {DOMElement} element the element which the CSS classes will be applied to
       * @param {string} className the CSS class(es) that will be added (multiple classes are separated via spaces)
       * @param {object=} options an optional collection of options/styles that will be applied to the element
       *
       * @return {Promise} the animation callback promise
       */
      addClass: function(element, className, options) {
        options = prepareAnimateOptions(options);
        options.addClass = mergeClasses(options.addclass, className);
        return $$animateQueue.push(element, 'addClass', options);
      },

      /**
       * @ngdoc method
       * @name $animate#removeClass
       * @kind function
       *
       * @description Triggers a removeClass animation surrounding the removal of the provided CSS class(es). Upon
       *   execution, the removeClass operation will only be handled after the next digest and it will not trigger an
       *   animation if element does not contain the CSS class or if the class is added at a later step.
       *   Note that class-based animations are treated differently compared to structural animations
       *   (like enter, move and leave) since the CSS classes may be added/removed at different points
       *   depending if CSS or JavaScript animations are used.
       *
       * @param {DOMElement} element the element which the CSS classes will be applied to
       * @param {string} className the CSS class(es) that will be removed (multiple classes are separated via spaces)
       * @param {object=} options an optional collection of options/styles that will be applied to the element
       *
       * @return {Promise} the animation callback promise
       */
      removeClass: function(element, className, options) {
        options = prepareAnimateOptions(options);
        options.removeClass = mergeClasses(options.removeClass, className);
        return $$animateQueue.push(element, 'removeClass', options);
      },

      /**
       * @ngdoc method
       * @name $animate#setClass
       * @kind function
       *
       * @description Performs both the addition and removal of a CSS classes on an element and (during the process)
       *    triggers an animation surrounding the class addition/removal. Much like `$animate.addClass` and
       *    `$animate.removeClass`, `setClass` will only evaluate the classes being added/removed once a digest has
       *    passed. Note that class-based animations are treated differently compared to structural animations
       *    (like enter, move and leave) since the CSS classes may be added/removed at different points
       *    depending if CSS or JavaScript animations are used.
       *
       * @param {DOMElement} element the element which the CSS classes will be applied to
       * @param {string} add the CSS class(es) that will be added (multiple classes are separated via spaces)
       * @param {string} remove the CSS class(es) that will be removed (multiple classes are separated via spaces)
       * @param {object=} options an optional collection of options/styles that will be applied to the element
       *
       * @return {Promise} the animation callback promise
       */
      setClass: function(element, add, remove, options) {
        options = prepareAnimateOptions(options);
        options.addClass = mergeClasses(options.addClass, add);
        options.removeClass = mergeClasses(options.removeClass, remove);
        return $$animateQueue.push(element, 'setClass', options);
      },

      /**
       * @ngdoc method
       * @name $animate#animate
       * @kind function
       *
       * @description Performs an inline animation on the element which applies the provided to and from CSS styles to the element.
       * If any detected CSS transition, keyframe or JavaScript matches the provided className value then the animation will take
       * on the provided styles. For example, if a transition animation is set for the given className then the provided from and
       * to styles will be applied alongside the given transition. If a JavaScript animation is detected then the provided styles
       * will be given in as function paramters into the `animate` method (or as apart of the `options` parameter).
       *
       * @param {DOMElement} element the element which the CSS styles will be applied to
       * @param {object} from the from (starting) CSS styles that will be applied to the element and across the animation.
       * @param {object} to the to (destination) CSS styles that will be applied to the element and across the animation.
       * @param {string=} className an optional CSS class that will be applied to the element for the duration of the animation. If
       *    this value is left as empty then a CSS class of `ng-inline-animate` will be applied to the element.
       *    (Note that if no animation is detected then this value will not be appplied to the element.)
       * @param {object=} options an optional collection of options/styles that will be applied to the element
       *
       * @return {Promise} the animation callback promise
       */
      animate: function(element, from, to, className, options) {
        options = prepareAnimateOptions(options);
        options.from = options.from ? extend(options.from, from) : from;
        options.to   = options.to   ? extend(options.to, to)     : to;

        className = className || 'ng-inline-animate';
        options.tempClasses = mergeClasses(options.tempClasses, className);
        return $$animateQueue.push(element, 'animate', options);
      }
    };
  }];
}];

/**
 * @ngdoc service
 * @name $animateCss
 * @kind object
 *
 * @description
 * This is the core version of `$animateCss`. By default, only when the `ngAnimate` is included,
 * then the `$animateCss` service will actually perform animations.
 *
 * Click here {@link ngAnimate.$animateCss to read the documentation for $animateCss}.
 */
var $CoreAnimateCssProvider = function() {
  this.$get = ['$$rAF', '$q', function($$rAF, $q) {

    var RAFPromise = function() {};
    RAFPromise.prototype = {
      done: function(cancel) {
        this.defer && this.defer[cancel === true ? 'reject' : 'resolve']();
      },
      end: function() {
        this.done();
      },
      cancel: function() {
        this.done(true);
      },
      getPromise: function() {
        if (!this.defer) {
          this.defer = $q.defer();
        }
        return this.defer.promise;
      },
      then: function(f1,f2) {
        return this.getPromise().then(f1,f2);
      },
      'catch': function(f1) {
        return this.getPromise()['catch'](f1);
      },
      'finally': function(f1) {
        return this.getPromise()['finally'](f1);
      }
    };

    return function(element, options) {
      // there is no point in applying the styles since
      // there is no animation that goes on at all in
      // this version of $animateCss.
      if (options.cleanupStyles) {
        options.from = options.to = null;
      }

      if (options.from) {
        element.css(options.from);
        options.from = null;
      }

      var closed, runner = new RAFPromise();
      return {
        start: run,
        end: run
      };

      function run() {
        $$rAF(function() {
          close();
          if (!closed) {
            runner.done();
          }
          closed = true;
        });
        return runner;
      }

      function close() {
        if (options.addClass) {
          element.addClass(options.addClass);
          options.addClass = null;
        }
        if (options.removeClass) {
          element.removeClass(options.removeClass);
          options.removeClass = null;
        }
        if (options.to) {
          element.css(options.to);
          options.to = null;
        }
      }
    };
  }];
};

/* global stripHash: true */

/**
 * ! This is a private undocumented service !
 *
 * @name $browser
 * @requires $log
 * @description
 * This object has two goals:
 *
 * - hide all the global state in the browser caused by the window object
 * - abstract away all the browser specific features and inconsistencies
 *
 * For tests we provide {@link ngMock.$browser mock implementation} of the `$browser`
 * service, which can be used for convenient testing of the application without the interaction with
 * the real browser apis.
 */
/**
 * @param {object} window The global window object.
 * @param {object} document jQuery wrapped document.
 * @param {object} $log window.console or an object with the same interface.
 * @param {object} $sniffer $sniffer service
 */
function Browser(window, document, $log, $sniffer) {
  var self = this,
      rawDocument = document[0],
      location = window.location,
      history = window.history,
      setTimeout = window.setTimeout,
      clearTimeout = window.clearTimeout,
      pendingDeferIds = {};

  self.isMock = false;

  var outstandingRequestCount = 0;
  var outstandingRequestCallbacks = [];

  // TODO(vojta): remove this temporary api
  self.$$completeOutstandingRequest = completeOutstandingRequest;
  self.$$incOutstandingRequestCount = function() { outstandingRequestCount++; };

  /**
   * Executes the `fn` function(supports currying) and decrements the `outstandingRequestCallbacks`
   * counter. If the counter reaches 0, all the `outstandingRequestCallbacks` are executed.
   */
  function completeOutstandingRequest(fn) {
    try {
      fn.apply(null, sliceArgs(arguments, 1));
    } finally {
      outstandingRequestCount--;
      if (outstandingRequestCount === 0) {
        while (outstandingRequestCallbacks.length) {
          try {
            outstandingRequestCallbacks.pop()();
          } catch (e) {
            $log.error(e);
          }
        }
      }
    }
  }

  function getHash(url) {
    var index = url.indexOf('#');
    return index === -1 ? '' : url.substr(index);
  }

  /**
   * @private
   * Note: this method is used only by scenario runner
   * TODO(vojta): prefix this method with $$ ?
   * @param {function()} callback Function that will be called when no outstanding request
   */
  self.notifyWhenNoOutstandingRequests = function(callback) {
    if (outstandingRequestCount === 0) {
      callback();
    } else {
      outstandingRequestCallbacks.push(callback);
    }
  };

  //////////////////////////////////////////////////////////////
  // URL API
  //////////////////////////////////////////////////////////////

  var cachedState, lastHistoryState,
      lastBrowserUrl = location.href,
      baseElement = document.find('base'),
      pendingLocation = null;

  cacheState();
  lastHistoryState = cachedState;

  /**
   * @name $browser#url
   *
   * @description
   * GETTER:
   * Without any argument, this method just returns current value of location.href.
   *
   * SETTER:
   * With at least one argument, this method sets url to new value.
   * If html5 history api supported, pushState/replaceState is used, otherwise
   * location.href/location.replace is used.
   * Returns its own instance to allow chaining
   *
   * NOTE: this api is intended for use only by the $location service. Please use the
   * {@link ng.$location $location service} to change url.
   *
   * @param {string} url New url (when used as setter)
   * @param {boolean=} replace Should new url replace current history record?
   * @param {object=} state object to use with pushState/replaceState
   */
  self.url = function(url, replace, state) {
    // In modern browsers `history.state` is `null` by default; treating it separately
    // from `undefined` would cause `$browser.url('/foo')` to change `history.state`
    // to undefined via `pushState`. Instead, let's change `undefined` to `null` here.
    if (isUndefined(state)) {
      state = null;
    }

    // Android Browser BFCache causes location, history reference to become stale.
    if (location !== window.location) location = window.location;
    if (history !== window.history) history = window.history;

    // setter
    if (url) {
      var sameState = lastHistoryState === state;

      // Don't change anything if previous and current URLs and states match. This also prevents
      // IE<10 from getting into redirect loop when in LocationHashbangInHtml5Url mode.
      // See https://github.com/angular/angular.js/commit/ffb2701
      if (lastBrowserUrl === url && (!$sniffer.history || sameState)) {
        return self;
      }
      var sameBase = lastBrowserUrl && stripHash(lastBrowserUrl) === stripHash(url);
      lastBrowserUrl = url;
      lastHistoryState = state;
      // Don't use history API if only the hash changed
      // due to a bug in IE10/IE11 which leads
      // to not firing a `hashchange` nor `popstate` event
      // in some cases (see #9143).
      if ($sniffer.history && (!sameBase || !sameState)) {
        history[replace ? 'replaceState' : 'pushState'](state, '', url);
        cacheState();
        // Do the assignment again so that those two variables are referentially identical.
        lastHistoryState = cachedState;
      } else {
        if (!sameBase || pendingLocation) {
          pendingLocation = url;
        }
        if (replace) {
          location.replace(url);
        } else if (!sameBase) {
          location.href = url;
        } else {
          location.hash = getHash(url);
        }
        if (location.href !== url) {
          pendingLocation = url;
        }
      }
      return self;
    // getter
    } else {
      // - pendingLocation is needed as browsers don't allow to read out
      //   the new location.href if a reload happened or if there is a bug like in iOS 9 (see
      //   https://openradar.appspot.com/22186109).
      // - the replacement is a workaround for https://bugzilla.mozilla.org/show_bug.cgi?id=407172
      return pendingLocation || location.href.replace(/%27/g,"'");
    }
  };

  /**
   * @name $browser#state
   *
   * @description
   * This method is a getter.
   *
   * Return history.state or null if history.state is undefined.
   *
   * @returns {object} state
   */
  self.state = function() {
    return cachedState;
  };

  var urlChangeListeners = [],
      urlChangeInit = false;

  function cacheStateAndFireUrlChange() {
    pendingLocation = null;
    cacheState();
    fireUrlChange();
  }

  function getCurrentState() {
    try {
      return history.state;
    } catch (e) {
      // MSIE can reportedly throw when there is no state (UNCONFIRMED).
    }
  }

  // This variable should be used *only* inside the cacheState function.
  var lastCachedState = null;
  function cacheState() {
    // This should be the only place in $browser where `history.state` is read.
    cachedState = getCurrentState();
    cachedState = isUndefined(cachedState) ? null : cachedState;

    // Prevent callbacks fo fire twice if both hashchange & popstate were fired.
    if (equals(cachedState, lastCachedState)) {
      cachedState = lastCachedState;
    }
    lastCachedState = cachedState;
  }

  function fireUrlChange() {
    if (lastBrowserUrl === self.url() && lastHistoryState === cachedState) {
      return;
    }

    lastBrowserUrl = self.url();
    lastHistoryState = cachedState;
    forEach(urlChangeListeners, function(listener) {
      listener(self.url(), cachedState);
    });
  }

  /**
   * @name $browser#onUrlChange
   *
   * @description
   * Register callback function that will be called, when url changes.
   *
   * It's only called when the url is changed from outside of angular:
   * - user types different url into address bar
   * - user clicks on history (forward/back) button
   * - user clicks on a link
   *
   * It's not called when url is changed by $browser.url() method
   *
   * The listener gets called with new url as parameter.
   *
   * NOTE: this api is intended for use only by the $location service. Please use the
   * {@link ng.$location $location service} to monitor url changes in angular apps.
   *
   * @param {function(string)} listener Listener function to be called when url changes.
   * @return {function(string)} Returns the registered listener fn - handy if the fn is anonymous.
   */
  self.onUrlChange = function(callback) {
    // TODO(vojta): refactor to use node's syntax for events
    if (!urlChangeInit) {
      // We listen on both (hashchange/popstate) when available, as some browsers (e.g. Opera)
      // don't fire popstate when user change the address bar and don't fire hashchange when url
      // changed by push/replaceState

      // html5 history api - popstate event
      if ($sniffer.history) jqLite(window).on('popstate', cacheStateAndFireUrlChange);
      // hashchange event
      jqLite(window).on('hashchange', cacheStateAndFireUrlChange);

      urlChangeInit = true;
    }

    urlChangeListeners.push(callback);
    return callback;
  };

  /**
   * @private
   * Remove popstate and hashchange handler from window.
   *
   * NOTE: this api is intended for use only by $rootScope.
   */
  self.$$applicationDestroyed = function() {
    jqLite(window).off('hashchange popstate', cacheStateAndFireUrlChange);
  };

  /**
   * Checks whether the url has changed outside of Angular.
   * Needs to be exported to be able to check for changes that have been done in sync,
   * as hashchange/popstate events fire in async.
   */
  self.$$checkUrlChange = fireUrlChange;

  //////////////////////////////////////////////////////////////
  // Misc API
  //////////////////////////////////////////////////////////////

  /**
   * @name $browser#baseHref
   *
   * @description
   * Returns current <base href>
   * (always relative - without domain)
   *
   * @returns {string} The current base href
   */
  self.baseHref = function() {
    var href = baseElement.attr('href');
    return href ? href.replace(/^(https?\:)?\/\/[^\/]*/, '') : '';
  };

  /**
   * @name $browser#defer
   * @param {function()} fn A function, who's execution should be deferred.
   * @param {number=} [delay=0] of milliseconds to defer the function execution.
   * @returns {*} DeferId that can be used to cancel the task via `$browser.defer.cancel()`.
   *
   * @description
   * Executes a fn asynchronously via `setTimeout(fn, delay)`.
   *
   * Unlike when calling `setTimeout` directly, in test this function is mocked and instead of using
   * `setTimeout` in tests, the fns are queued in an array, which can be programmatically flushed
   * via `$browser.defer.flush()`.
   *
   */
  self.defer = function(fn, delay) {
    var timeoutId;
    outstandingRequestCount++;
    timeoutId = setTimeout(function() {
      delete pendingDeferIds[timeoutId];
      completeOutstandingRequest(fn);
    }, delay || 0);
    pendingDeferIds[timeoutId] = true;
    return timeoutId;
  };


  /**
   * @name $browser#defer.cancel
   *
   * @description
   * Cancels a deferred task identified with `deferId`.
   *
   * @param {*} deferId Token returned by the `$browser.defer` function.
   * @returns {boolean} Returns `true` if the task hasn't executed yet and was successfully
   *                    canceled.
   */
  self.defer.cancel = function(deferId) {
    if (pendingDeferIds[deferId]) {
      delete pendingDeferIds[deferId];
      clearTimeout(deferId);
      completeOutstandingRequest(noop);
      return true;
    }
    return false;
  };

}

function $BrowserProvider() {
  this.$get = ['$window', '$log', '$sniffer', '$document',
      function($window, $log, $sniffer, $document) {
        return new Browser($window, $document, $log, $sniffer);
      }];
}

/**
 * @ngdoc service
 * @name $cacheFactory
 *
 * @description
 * Factory that constructs {@link $cacheFactory.Cache Cache} objects and gives access to
 * them.
 *
 * ```js
 *
 *  var cache = $cacheFactory('cacheId');
 *  expect($cacheFactory.get('cacheId')).toBe(cache);
 *  expect($cacheFactory.get('noSuchCacheId')).not.toBeDefined();
 *
 *  cache.put("key", "value");
 *  cache.put("another key", "another value");
 *
 *  // We've specified no options on creation
 *  expect(cache.info()).toEqual({id: 'cacheId', size: 2});
 *
 * ```
 *
 *
 * @param {string} cacheId Name or id of the newly created cache.
 * @param {object=} options Options object that specifies the cache behavior. Properties:
 *
 *   - `{number=}` `capacity` — turns the cache into LRU cache.
 *
 * @returns {object} Newly created cache object with the following set of methods:
 *
 * - `{object}` `info()` — Returns id, size, and options of cache.
 * - `{{*}}` `put({string} key, {*} value)` — Puts a new key-value pair into the cache and returns
 *   it.
 * - `{{*}}` `get({string} key)` — Returns cached value for `key` or undefined for cache miss.
 * - `{void}` `remove({string} key)` — Removes a key-value pair from the cache.
 * - `{void}` `removeAll()` — Removes all cached values.
 * - `{void}` `destroy()` — Removes references to this cache from $cacheFactory.
 *
 * @example
   <example module="cacheExampleApp">
     <file name="index.html">
       <div ng-controller="CacheController">
         <input ng-model="newCacheKey" placeholder="Key">
         <input ng-model="newCacheValue" placeholder="Value">
         <button ng-click="put(newCacheKey, newCacheValue)">Cache</button>

         <p ng-if="keys.length">Cached Values</p>
         <div ng-repeat="key in keys">
           <span ng-bind="key"></span>
           <span>: </span>
           <b ng-bind="cache.get(key)"></b>
         </div>

         <p>Cache Info</p>
         <div ng-repeat="(key, value) in cache.info()">
           <span ng-bind="key"></span>
           <span>: </span>
           <b ng-bind="value"></b>
         </div>
       </div>
     </file>
     <file name="script.js">
       angular.module('cacheExampleApp', []).
         controller('CacheController', ['$scope', '$cacheFactory', function($scope, $cacheFactory) {
           $scope.keys = [];
           $scope.cache = $cacheFactory('cacheId');
           $scope.put = function(key, value) {
             if (angular.isUndefined($scope.cache.get(key))) {
               $scope.keys.push(key);
             }
             $scope.cache.put(key, angular.isUndefined(value) ? null : value);
           };
         }]);
     </file>
     <file name="style.css">
       p {
         margin: 10px 0 3px;
       }
     </file>
   </example>
 */
function $CacheFactoryProvider() {

  this.$get = function() {
    var caches = {};

    function cacheFactory(cacheId, options) {
      if (cacheId in caches) {
        throw minErr('$cacheFactory')('iid', "CacheId '{0}' is already taken!", cacheId);
      }

      var size = 0,
          stats = extend({}, options, {id: cacheId}),
          data = createMap(),
          capacity = (options && options.capacity) || Number.MAX_VALUE,
          lruHash = createMap(),
          freshEnd = null,
          staleEnd = null;

      /**
       * @ngdoc type
       * @name $cacheFactory.Cache
       *
       * @description
       * A cache object used to store and retrieve data, primarily used by
       * {@link $http $http} and the {@link ng.directive:script script} directive to cache
       * templates and other data.
       *
       * ```js
       *  angular.module('superCache')
       *    .factory('superCache', ['$cacheFactory', function($cacheFactory) {
       *      return $cacheFactory('super-cache');
       *    }]);
       * ```
       *
       * Example test:
       *
       * ```js
       *  it('should behave like a cache', inject(function(superCache) {
       *    superCache.put('key', 'value');
       *    superCache.put('another key', 'another value');
       *
       *    expect(superCache.info()).toEqual({
       *      id: 'super-cache',
       *      size: 2
       *    });
       *
       *    superCache.remove('another key');
       *    expect(superCache.get('another key')).toBeUndefined();
       *
       *    superCache.removeAll();
       *    expect(superCache.info()).toEqual({
       *      id: 'super-cache',
       *      size: 0
       *    });
       *  }));
       * ```
       */
      return caches[cacheId] = {

        /**
         * @ngdoc method
         * @name $cacheFactory.Cache#put
         * @kind function
         *
         * @description
         * Inserts a named entry into the {@link $cacheFactory.Cache Cache} object to be
         * retrieved later, and incrementing the size of the cache if the key was not already
         * present in the cache. If behaving like an LRU cache, it will also remove stale
         * entries from the set.
         *
         * It will not insert undefined values into the cache.
         *
         * @param {string} key the key under which the cached data is stored.
         * @param {*} value the value to store alongside the key. If it is undefined, the key
         *    will not be stored.
         * @returns {*} the value stored.
         */
        put: function(key, value) {
          if (isUndefined(value)) return;
          if (capacity < Number.MAX_VALUE) {
            var lruEntry = lruHash[key] || (lruHash[key] = {key: key});

            refresh(lruEntry);
          }

          if (!(key in data)) size++;
          data[key] = value;

          if (size > capacity) {
            this.remove(staleEnd.key);
          }

          return value;
        },

        /**
         * @ngdoc method
         * @name $cacheFactory.Cache#get
         * @kind function
         *
         * @description
         * Retrieves named data stored in the {@link $cacheFactory.Cache Cache} object.
         *
         * @param {string} key the key of the data to be retrieved
         * @returns {*} the value stored.
         */
        get: function(key) {
          if (capacity < Number.MAX_VALUE) {
            var lruEntry = lruHash[key];

            if (!lruEntry) return;

            refresh(lruEntry);
          }

          return data[key];
        },


        /**
         * @ngdoc method
         * @name $cacheFactory.Cache#remove
         * @kind function
         *
         * @description
         * Removes an entry from the {@link $cacheFactory.Cache Cache} object.
         *
         * @param {string} key the key of the entry to be removed
         */
        remove: function(key) {
          if (capacity < Number.MAX_VALUE) {
            var lruEntry = lruHash[key];

            if (!lruEntry) return;

            if (lruEntry == freshEnd) freshEnd = lruEntry.p;
            if (lruEntry == staleEnd) staleEnd = lruEntry.n;
            link(lruEntry.n,lruEntry.p);

            delete lruHash[key];
          }

          if (!(key in data)) return;

          delete data[key];
          size--;
        },


        /**
         * @ngdoc method
         * @name $cacheFactory.Cache#removeAll
         * @kind function
         *
         * @description
         * Clears the cache object of any entries.
         */
        removeAll: function() {
          data = createMap();
          size = 0;
          lruHash = createMap();
          freshEnd = staleEnd = null;
        },


        /**
         * @ngdoc method
         * @name $cacheFactory.Cache#destroy
         * @kind function
         *
         * @description
         * Destroys the {@link $cacheFactory.Cache Cache} object entirely,
         * removing it from the {@link $cacheFactory $cacheFactory} set.
         */
        destroy: function() {
          data = null;
          stats = null;
          lruHash = null;
          delete caches[cacheId];
        },


        /**
         * @ngdoc method
         * @name $cacheFactory.Cache#info
         * @kind function
         *
         * @description
         * Retrieve information regarding a particular {@link $cacheFactory.Cache Cache}.
         *
         * @returns {object} an object with the following properties:
         *   <ul>
         *     <li>**id**: the id of the cache instance</li>
         *     <li>**size**: the number of entries kept in the cache instance</li>
         *     <li>**...**: any additional properties from the options object when creating the
         *       cache.</li>
         *   </ul>
         */
        info: function() {
          return extend({}, stats, {size: size});
        }
      };


      /**
       * makes the `entry` the freshEnd of the LRU linked list
       */
      function refresh(entry) {
        if (entry != freshEnd) {
          if (!staleEnd) {
            staleEnd = entry;
          } else if (staleEnd == entry) {
            staleEnd = entry.n;
          }

          link(entry.n, entry.p);
          link(entry, freshEnd);
          freshEnd = entry;
          freshEnd.n = null;
        }
      }


      /**
       * bidirectionally links two entries of the LRU linked list
       */
      function link(nextEntry, prevEntry) {
        if (nextEntry != prevEntry) {
          if (nextEntry) nextEntry.p = prevEntry; //p stands for previous, 'prev' didn't minify
          if (prevEntry) prevEntry.n = nextEntry; //n stands for next, 'next' didn't minify
        }
      }
    }


  /**
   * @ngdoc method
   * @name $cacheFactory#info
   *
   * @description
   * Get information about all the caches that have been created
   *
   * @returns {Object} - key-value map of `cacheId` to the result of calling `cache#info`
   */
    cacheFactory.info = function() {
      var info = {};
      forEach(caches, function(cache, cacheId) {
        info[cacheId] = cache.info();
      });
      return info;
    };


  /**
   * @ngdoc method
   * @name $cacheFactory#get
   *
   * @description
   * Get access to a cache object by the `cacheId` used when it was created.
   *
   * @param {string} cacheId Name or id of a cache to access.
   * @returns {object} Cache object identified by the cacheId or undefined if no such cache.
   */
    cacheFactory.get = function(cacheId) {
      return caches[cacheId];
    };


    return cacheFactory;
  };
}

/**
 * @ngdoc service
 * @name $templateCache
 *
 * @description
 * The first time a template is used, it is loaded in the template cache for quick retrieval. You
 * can load templates directly into the cache in a `script` tag, or by consuming the
 * `$templateCache` service directly.
 *
 * Adding via the `script` tag:
 *
 * ```html
 *   <script type="text/ng-template" id="templateId.html">
 *     <p>This is the content of the template</p>
 *   </script>
 * ```
 *
 * **Note:** the `script` tag containing the template does not need to be included in the `head` of
 * the document, but it must be a descendent of the {@link ng.$rootElement $rootElement} (IE,
 * element with ng-app attribute), otherwise the template will be ignored.
 *
 * Adding via the `$templateCache` service:
 *
 * ```js
 * var myApp = angular.module('myApp', []);
 * myApp.run(function($templateCache) {
 *   $templateCache.put('templateId.html', 'This is the content of the template');
 * });
 * ```
 *
 * To retrieve the template later, simply use it in your HTML:
 * ```html
 * <div ng-include=" 'templateId.html' "></div>
 * ```
 *
 * or get it via Javascript:
 * ```js
 * $templateCache.get('templateId.html')
 * ```
 *
 * See {@link ng.$cacheFactory $cacheFactory}.
 *
 */
function $TemplateCacheProvider() {
  this.$get = ['$cacheFactory', function($cacheFactory) {
    return $cacheFactory('templates');
  }];
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *     Any commits to this file should be reviewed with security in mind.  *
 *   Changes to this file can potentially create security vulnerabilities. *
 *          An approval from 2 Core members with history of modifying      *
 *                         this file is required.                          *
 *                                                                         *
 *  Does the change somehow allow for arbitrary javascript to be executed? *
 *    Or allows for someone to change the prototype of built-in objects?   *
 *     Or gives undesired access to variables likes document or window?    *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* ! VARIABLE/FUNCTION NAMING CONVENTIONS THAT APPLY TO THIS FILE!
 *
 * DOM-related variables:
 *
 * - "node" - DOM Node
 * - "element" - DOM Element or Node
 * - "$node" or "$element" - jqLite-wrapped node or element
 *
 *
 * Compiler related stuff:
 *
 * - "linkFn" - linking fn of a single directive
 * - "nodeLinkFn" - function that aggregates all linking fns for a particular node
 * - "childLinkFn" -  function that aggregates all linking fns for child nodes of a particular node
 * - "compositeLinkFn" - function that aggregates all linking fns for a compilation root (nodeList)
 */


/**
 * @ngdoc service
 * @name $compile
 * @kind function
 *
 * @description
 * Compiles an HTML string or DOM into a template and produces a template function, which
 * can then be used to link {@link ng.$rootScope.Scope `scope`} and the template together.
 *
 * The compilation is a process of walking the DOM tree and matching DOM elements to
 * {@link ng.$compileProvider#directive directives}.
 *
 * <div class="alert alert-warning">
 * **Note:** This document is an in-depth reference of all directive options.
 * For a gentle introduction to directives with examples of common use cases,
 * see the {@link guide/directive directive guide}.
 * </div>
 *
 * ## Comprehensive Directive API
 *
 * There are many different options for a directive.
 *
 * The difference resides in the return value of the factory function.
 * You can either return a "Directive Definition Object" (see below) that defines the directive properties,
 * or just the `postLink` function (all other properties will have the default values).
 *
 * <div class="alert alert-success">
 * **Best Practice:** It's recommended to use the "directive definition object" form.
 * </div>
 *
 * Here's an example directive declared with a Directive Definition Object:
 *
 * ```js
 *   var myModule = angular.module(...);
 *
 *   myModule.directive('directiveName', function factory(injectables) {
 *     var directiveDefinitionObject = {
 *       priority: 0,
 *       template: '<div></div>', // or // function(tElement, tAttrs) { ... },
 *       // or
 *       // templateUrl: 'directive.html', // or // function(tElement, tAttrs) { ... },
 *       transclude: false,
 *       restrict: 'A',
 *       templateNamespace: 'html',
 *       scope: false,
 *       controller: function($scope, $element, $attrs, $transclude, otherInjectables) { ... },
 *       controllerAs: 'stringIdentifier',
 *       bindToController: false,
 *       require: 'siblingDirectiveName', // or // ['^parentDirectiveName', '?optionalDirectiveName', '?^optionalParent'],
 *       compile: function compile(tElement, tAttrs, transclude) {
 *         return {
 *           pre: function preLink(scope, iElement, iAttrs, controller) { ... },
 *           post: function postLink(scope, iElement, iAttrs, controller) { ... }
 *         }
 *         // or
 *         // return function postLink( ... ) { ... }
 *       },
 *       // or
 *       // link: {
 *       //  pre: function preLink(scope, iElement, iAttrs, controller) { ... },
 *       //  post: function postLink(scope, iElement, iAttrs, controller) { ... }
 *       // }
 *       // or
 *       // link: function postLink( ... ) { ... }
 *     };
 *     return directiveDefinitionObject;
 *   });
 * ```
 *
 * <div class="alert alert-warning">
 * **Note:** Any unspecified options will use the default value. You can see the default values below.
 * </div>
 *
 * Therefore the above can be simplified as:
 *
 * ```js
 *   var myModule = angular.module(...);
 *
 *   myModule.directive('directiveName', function factory(injectables) {
 *     var directiveDefinitionObject = {
 *       link: function postLink(scope, iElement, iAttrs) { ... }
 *     };
 *     return directiveDefinitionObject;
 *     // or
 *     // return function postLink(scope, iElement, iAttrs) { ... }
 *   });
 * ```
 *
 *
 *
 * ### Directive Definition Object
 *
 * The directive definition object provides instructions to the {@link ng.$compile
 * compiler}. The attributes are:
 *
 * #### `multiElement`
 * When this property is set to true, the HTML compiler will collect DOM nodes between
 * nodes with the attributes `directive-name-start` and `directive-name-end`, and group them
 * together as the directive elements. It is recommended that this feature be used on directives
 * which are not strictly behavioural (such as {@link ngClick}), and which
 * do not manipulate or replace child nodes (such as {@link ngInclude}).
 *
 * #### `priority`
 * When there are multiple directives defined on a single DOM element, sometimes it
 * is necessary to specify the order in which the directives are applied. The `priority` is used
 * to sort the directives before their `compile` functions get called. Priority is defined as a
 * number. Directives with greater numerical `priority` are compiled first. Pre-link functions
 * are also run in priority order, but post-link functions are run in reverse order. The order
 * of directives with the same priority is undefined. The default priority is `0`.
 *
 * #### `terminal`
 * If set to true then the current `priority` will be the last set of directives
 * which will execute (any directives at the current priority will still execute
 * as the order of execution on same `priority` is undefined). Note that expressions
 * and other directives used in the directive's template will also be excluded from execution.
 *
 * #### `scope`
 * The scope property can be `true`, an object or a falsy value:
 *
 * * **falsy:** No scope will be created for the directive. The directive will use its parent's scope.
 *
 * * **`true`:** A new child scope that prototypically inherits from its parent will be created for
 * the directive's element. If multiple directives on the same element request a new scope,
 * only one new scope is created. The new scope rule does not apply for the root of the template
 * since the root of the template always gets a new scope.
 *
 * * **`{...}` (an object hash):** A new "isolate" scope is created for the directive's element. The
 * 'isolate' scope differs from normal scope in that it does not prototypically inherit from its parent
 * scope. This is useful when creating reusable components, which should not accidentally read or modify
 * data in the parent scope.
 *
 * The 'isolate' scope object hash defines a set of local scope properties derived from attributes on the
 * directive's element. These local properties are useful for aliasing values for templates. The keys in
 * the object hash map to the name of the property on the isolate scope; the values define how the property
 * is bound to the parent scope, via matching attributes on the directive's element:
 *
 * * `@` or `@attr` - bind a local scope property to the value of DOM attribute. The result is
 *   always a string since DOM attributes are strings. If no `attr` name is specified  then the
 *   attribute name is assumed to be the same as the local name.
 *   Given `<widget my-attr="hello {{name}}">` and widget definition
 *   of `scope: { localName:'@myAttr' }`, then widget scope property `localName` will reflect
 *   the interpolated value of `hello {{name}}`. As the `name` attribute changes so will the
 *   `localName` property on the widget scope. The `name` is read from the parent scope (not
 *   component scope).
 *
 * * `=` or `=attr` - set up bi-directional binding between a local scope property and the
 *   parent scope property of name defined via the value of the `attr` attribute. If no `attr`
 *   name is specified then the attribute name is assumed to be the same as the local name.
 *   Given `<widget my-attr="parentModel">` and widget definition of
 *   `scope: { localModel:'=myAttr' }`, then widget scope property `localModel` will reflect the
 *   value of `parentModel` on the parent scope. Any changes to `parentModel` will be reflected
 *   in `localModel` and any changes in `localModel` will reflect in `parentModel`. If the parent
 *   scope property doesn't exist, it will throw a NON_ASSIGNABLE_MODEL_EXPRESSION exception. You
 *   can avoid this behavior using `=?` or `=?attr` in order to flag the property as optional. If
 *   you want to shallow watch for changes (i.e. $watchCollection instead of $watch) you can use
 *   `=*` or `=*attr` (`=*?` or `=*?attr` if the property is optional).
 *
 * * `&` or `&attr` - provides a way to execute an expression in the context of the parent scope.
 *   If no `attr` name is specified then the attribute name is assumed to be the same as the
 *   local name. Given `<widget my-attr="count = count + value">` and widget definition of
 *   `scope: { localFn:'&myAttr' }`, then isolate scope property `localFn` will point to
 *   a function wrapper for the `count = count + value` expression. Often it's desirable to
 *   pass data from the isolated scope via an expression to the parent scope, this can be
 *   done by passing a map of local variable names and values into the expression wrapper fn.
 *   For example, if the expression is `increment(amount)` then we can specify the amount value
 *   by calling the `localFn` as `localFn({amount: 22})`.
 *
 * In general it's possible to apply more than one directive to one element, but there might be limitations
 * depending on the type of scope required by the directives. The following points will help explain these limitations.
 * For simplicity only two directives are taken into account, but it is also applicable for several directives:
 *
 * * **no scope** + **no scope** => Two directives which don't require their own scope will use their parent's scope
 * * **child scope** + **no scope** =>  Both directives will share one single child scope
 * * **child scope** + **child scope** =>  Both directives will share one single child scope
 * * **isolated scope** + **no scope** =>  The isolated directive will use it's own created isolated scope. The other directive will use
 * its parent's scope
 * * **isolated scope** + **child scope** =>  **Won't work!** Only one scope can be related to one element. Therefore these directives cannot
 * be applied to the same element.
 * * **isolated scope** + **isolated scope**  =>  **Won't work!** Only one scope can be related to one element. Therefore these directives
 * cannot be applied to the same element.
 *
 *
 * #### `bindToController`
 * When an isolate scope is used for a component (see above), and `controllerAs` is used, `bindToController: true` will
 * allow a component to have its properties bound to the controller, rather than to scope. When the controller
 * is instantiated, the initial values of the isolate scope bindings are already available.
 *
 * #### `controller`
 * Controller constructor function. The controller is instantiated before the
 * pre-linking phase and can be accessed by other directives (see
 * `require` attribute). This allows the directives to communicate with each other and augment
 * each other's behavior. The controller is injectable (and supports bracket notation) with the following locals:
 *
 * * `$scope` - Current scope associated with the element
 * * `$element` - Current element
 * * `$attrs` - Current attributes object for the element
 * * `$transclude` - A transclude linking function pre-bound to the correct transclusion scope:
 *   `function([scope], cloneLinkingFn, futureParentElement)`.
 *    * `scope`: optional argument to override the scope.
 *    * `cloneLinkingFn`: optional argument to create clones of the original transcluded content.
 *    * `futureParentElement`:
 *        * defines the parent to which the `cloneLinkingFn` will add the cloned elements.
 *        * default: `$element.parent()` resp. `$element` for `transclude:'element'` resp. `transclude:true`.
 *        * only needed for transcludes that are allowed to contain non html elements (e.g. SVG elements)
 *          and when the `cloneLinkinFn` is passed,
 *          as those elements need to created and cloned in a special way when they are defined outside their
 *          usual containers (e.g. like `<svg>`).
 *        * See also the `directive.templateNamespace` property.
 *
 *
 * #### `require`
 * Require another directive and inject its controller as the fourth argument to the linking function. The
 * `require` takes a string name (or array of strings) of the directive(s) to pass in. If an array is used, the
 * injected argument will be an array in corresponding order. If no such directive can be
 * found, or if the directive does not have a controller, then an error is raised (unless no link function
 * is specified, in which case error checking is skipped). The name can be prefixed with:
 *
 * * (no prefix) - Locate the required controller on the current element. Throw an error if not found.
 * * `?` - Attempt to locate the required controller or pass `null` to the `link` fn if not found.
 * * `^` - Locate the required controller by searching the element and its parents. Throw an error if not found.
 * * `^^` - Locate the required controller by searching the element's parents. Throw an error if not found.
 * * `?^` - Attempt to locate the required controller by searching the element and its parents or pass
 *   `null` to the `link` fn if not found.
 * * `?^^` - Attempt to locate the required controller by searching the element's parents, or pass
 *   `null` to the `link` fn if not found.
 *
 *
 * #### `controllerAs`
 * Identifier name for a reference to the controller in the directive's scope.
 * This allows the controller to be referenced from the directive template. This is especially
 * useful when a directive is used as component, i.e. with an `isolate` scope. It's also possible
 * to use it in a directive without an `isolate` / `new` scope, but you need to be aware that the
 * `controllerAs` reference might overwrite a property that already exists on the parent scope.
 *
 *
 * #### `restrict`
 * String of subset of `EACM` which restricts the directive to a specific directive
 * declaration style. If omitted, the defaults (elements and attributes) are used.
 *
 * * `E` - Element name (default): `<my-directive></my-directive>`
 * * `A` - Attribute (default): `<div my-directive="exp"></div>`
 * * `C` - Class: `<div class="my-directive: exp;"></div>`
 * * `M` - Comment: `<!-- directive: my-directive exp -->`
 *
 *
 * #### `templateNamespace`
 * String representing the document type used by the markup in the template.
 * AngularJS needs this information as those elements need to be created and cloned
 * in a special way when they are defined outside their usual containers like `<svg>` and `<math>`.
 *
 * * `html` - All root nodes in the template are HTML. Root nodes may also be
 *   top-level elements such as `<svg>` or `<math>`.
 * * `svg` - The root nodes in the template are SVG elements (excluding `<math>`).
 * * `math` - The root nodes in the template are MathML elements (excluding `<svg>`).
 *
 * If no `templateNamespace` is specified, then the namespace is considered to be `html`.
 *
 * #### `template`
 * HTML markup that may:
 * * Replace the contents of the directive's element (default).
 * * Replace the directive's element itself (if `replace` is true - DEPRECATED).
 * * Wrap the contents of the directive's element (if `transclude` is true).
 *
 * Value may be:
 *
 * * A string. For example `<div red-on-hover>{{delete_str}}</div>`.
 * * A function which takes two arguments `tElement` and `tAttrs` (described in the `compile`
 *   function api below) and returns a string value.
 *
 *
 * #### `templateUrl`
 * This is similar to `template` but the template is loaded from the specified URL, asynchronously.
 *
 * Because template loading is asynchronous the compiler will suspend compilation of directives on that element
 * for later when the template has been resolved.  In the meantime it will continue to compile and link
 * sibling and parent elements as though this element had not contained any directives.
 *
 * The compiler does not suspend the entire compilation to wait for templates to be loaded because this
 * would result in the whole app "stalling" until all templates are loaded asynchronously - even in the
 * case when only one deeply nested directive has `templateUrl`.
 *
 * Template loading is asynchronous even if the template has been preloaded into the {@link $templateCache}
 *
 * You can specify `templateUrl` as a string representing the URL or as a function which takes two
 * arguments `tElement` and `tAttrs` (described in the `compile` function api below) and returns
 * a string value representing the url.  In either case, the template URL is passed through {@link
 * $sce#getTrustedResourceUrl $sce.getTrustedResourceUrl}.
 *
 *
 * #### `replace` ([*DEPRECATED*!], will be removed in next major release - i.e. v2.0)
 * specify what the template should replace. Defaults to `false`.
 *
 * * `true` - the template will replace the directive's element.
 * * `false` - the template will replace the contents of the directive's element.
 *
 * The replacement process migrates all of the attributes / classes from the old element to the new
 * one. See the {@link guide/directive#template-expanding-directive
 * Directives Guide} for an example.
 *
 * There are very few scenarios where element replacement is required for the application function,
 * the main one being reusable custom components that are used within SVG contexts
 * (because SVG doesn't work with custom elements in the DOM tree).
 *
 * #### `transclude`
 * Extract the contents of the element where the directive appears and make it available to the directive.
 * The contents are compiled and provided to the directive as a **transclusion function**. See the
 * {@link $compile#transclusion Transclusion} section below.
 *
 * There are two kinds of transclusion depending upon whether you want to transclude just the contents of the
 * directive's element or the entire element:
 *
 * * `true` - transclude the content (i.e. the child nodes) of the directive's element.
 * * `'element'` - transclude the whole of the directive's element including any directives on this
 *   element that defined at a lower priority than this directive. When used, the `template`
 *   property is ignored.
 *
 *
 * #### `compile`
 *
 * ```js
 *   function compile(tElement, tAttrs, transclude) { ... }
 * ```
 *
 * The compile function deals with transforming the template DOM. Since most directives do not do
 * template transformation, it is not used often. The compile function takes the following arguments:
 *
 *   * `tElement` - template element - The element where the directive has been declared. It is
 *     safe to do template transformation on the element and child elements only.
 *
 *   * `tAttrs` - template attributes - Normalized list of attributes declared on this element shared
 *     between all directive compile functions.
 *
 *   * `transclude` -  [*DEPRECATED*!] A transclude linking function: `function(scope, cloneLinkingFn)`
 *
 * <div class="alert alert-warning">
 * **Note:** The template instance and the link instance may be different objects if the template has
 * been cloned. For this reason it is **not** safe to do anything other than DOM transformations that
 * apply to all cloned DOM nodes within the compile function. Specifically, DOM listener registration
 * should be done in a linking function rather than in a compile function.
 * </div>

 * <div class="alert alert-warning">
 * **Note:** The compile function cannot handle directives that recursively use themselves in their
 * own templates or compile functions. Compiling these directives results in an infinite loop and a
 * stack overflow errors.
 *
 * This can be avoided by manually using $compile in the postLink function to imperatively compile
 * a directive's template instead of relying on automatic template compilation via `template` or
 * `templateUrl` declaration or manual compilation inside the compile function.
 * </div>
 *
 * <div class="alert alert-danger">
 * **Note:** The `transclude` function that is passed to the compile function is deprecated, as it
 *   e.g. does not know about the right outer scope. Please use the transclude function that is passed
 *   to the link function instead.
 * </div>

 * A compile function can have a return value which can be either a function or an object.
 *
 * * returning a (post-link) function - is equivalent to registering the linking function via the
 *   `link` property of the config object when the compile function is empty.
 *
 * * returning an object with function(s) registered via `pre` and `post` properties - allows you to
 *   control when a linking function should be called during the linking phase. See info about
 *   pre-linking and post-linking functions below.
 *
 *
 * #### `link`
 * This property is used only if the `compile` property is not defined.
 *
 * ```js
 *   function link(scope, iElement, iAttrs, controller, transcludeFn) { ... }
 * ```
 *
 * The link function is responsible for registering DOM listeners as well as updating the DOM. It is
 * executed after the template has been cloned. This is where most of the directive logic will be
 * put.
 *
 *   * `scope` - {@link ng.$rootScope.Scope Scope} - The scope to be used by the
 *     directive for registering {@link ng.$rootScope.Scope#$watch watches}.
 *
 *   * `iElement` - instance element - The element where the directive is to be used. It is safe to
 *     manipulate the children of the element only in `postLink` function since the children have
 *     already been linked.
 *
 *   * `iAttrs` - instance attributes - Normalized list of attributes declared on this element shared
 *     between all directive linking functions.
 *
 *   * `controller` - the directive's required controller instance(s) - Instances are shared
 *     among all directives, which allows the directives to use the controllers as a communication
 *     channel. The exact value depends on the directive's `require` property:
 *       * no controller(s) required: the directive's own controller, or `undefined` if it doesn't have one
 *       * `string`: the controller instance
 *       * `array`: array of controller instances
 *
 *     If a required controller cannot be found, and it is optional, the instance is `null`,
 *     otherwise the {@link error:$compile:ctreq Missing Required Controller} error is thrown.
 *
 *     Note that you can also require the directive's own controller - it will be made available like
 *     any other controller.
 *
 *   * `transcludeFn` - A transclude linking function pre-bound to the correct transclusion scope.
 *     This is the same as the `$transclude`
 *     parameter of directive controllers, see there for details.
 *     `function([scope], cloneLinkingFn, futureParentElement)`.
 *
 * #### Pre-linking function
 *
 * Executed before the child elements are linked. Not safe to do DOM transformation since the
 * compiler linking function will fail to locate the correct elements for linking.
 *
 * #### Post-linking function
 *
 * Executed after the child elements are linked.
 *
 * Note that child elements that contain `templateUrl` directives will not have been compiled
 * and linked since they are waiting for their template to load asynchronously and their own
 * compilation and linking has been suspended until that occurs.
 *
 * It is safe to do DOM transformation in the post-linking function on elements that are not waiting
 * for their async templates to be resolved.
 *
 *
 * ### Transclusion
 *
 * Transclusion is the process of extracting a collection of DOM elements from one part of the DOM and
 * copying them to another part of the DOM, while maintaining their connection to the original AngularJS
 * scope from where they were taken.
 *
 * Transclusion is used (often with {@link ngTransclude}) to insert the
 * original contents of a directive's element into a specified place in the template of the directive.
 * The benefit of transclusion, over simply moving the DOM elements manually, is that the transcluded
 * content has access to the properties on the scope from which it was taken, even if the directive
 * has isolated scope.
 * See the {@link guide/directive#creating-a-directive-that-wraps-other-elements Directives Guide}.
 *
 * This makes it possible for the widget to have private state for its template, while the transcluded
 * content has access to its originating scope.
 *
 * <div class="alert alert-warning">
 * **Note:** When testing an element transclude directive you must not place the directive at the root of the
 * DOM fragment that is being compiled. See {@link guide/unit-testing#testing-transclusion-directives
 * Testing Transclusion Directives}.
 * </div>
 *
 * #### Transclusion Functions
 *
 * When a directive requests transclusion, the compiler extracts its contents and provides a **transclusion
 * function** to the directive's `link` function and `controller`. This transclusion function is a special
 * **linking function** that will return the compiled contents linked to a new transclusion scope.
 *
 * <div class="alert alert-info">
 * If you are just using {@link ngTransclude} then you don't need to worry about this function, since
 * ngTransclude will deal with it for us.
 * </div>
 *
 * If you want to manually control the insertion and removal of the transcluded content in your directive
 * then you must use this transclude function. When you call a transclude function it returns a a jqLite/JQuery
 * object that contains the compiled DOM, which is linked to the correct transclusion scope.
 *
 * When you call a transclusion function you can pass in a **clone attach function**. This function accepts
 * two parameters, `function(clone, scope) { ... }`, where the `clone` is a fresh compiled copy of your transcluded
 * content and the `scope` is the newly created transclusion scope, to which the clone is bound.
 *
 * <div class="alert alert-info">
 * **Best Practice**: Always provide a `cloneFn` (clone attach function) when you call a translude function
 * since you then get a fresh clone of the original DOM and also have access to the new transclusion scope.
 * </div>
 *
 * It is normal practice to attach your transcluded content (`clone`) to the DOM inside your **clone
 * attach function**:
 *
 * ```js
 * var transcludedContent, transclusionScope;
 *
 * $transclude(function(clone, scope) {
 *   element.append(clone);
 *   transcludedContent = clone;
 *   transclusionScope = scope;
 * });
 * ```
 *
 * Later, if you want to remove the transcluded content from your DOM then you should also destroy the
 * associated transclusion scope:
 *
 * ```js
 * transcludedContent.remove();
 * transclusionScope.$destroy();
 * ```
 *
 * <div class="alert alert-info">
 * **Best Practice**: if you intend to add and remove transcluded content manually in your directive
 * (by calling the transclude function to get the DOM and calling `element.remove()` to remove it),
 * then you are also responsible for calling `$destroy` on the transclusion scope.
 * </div>
 *
 * The built-in DOM manipulation directives, such as {@link ngIf}, {@link ngSwitch} and {@link ngRepeat}
 * automatically destroy their transluded clones as necessary so you do not need to worry about this if
 * you are simply using {@link ngTransclude} to inject the transclusion into your directive.
 *
 *
 * #### Transclusion Scopes
 *
 * When you call a transclude function it returns a DOM fragment that is pre-bound to a **transclusion
 * scope**. This scope is special, in that it is a child of the directive's scope (and so gets destroyed
 * when the directive's scope gets destroyed) but it inherits the properties of the scope from which it
 * was taken.
 *
 * For example consider a directive that uses transclusion and isolated scope. The DOM hierarchy might look
 * like this:
 *
 * ```html
 * <div ng-app>
 *   <div isolate>
 *     <div transclusion>
 *     </div>
 *   </div>
 * </div>
 * ```
 *
 * The `$parent` scope hierarchy will look like this:
 *
 * ```
 * - $rootScope
 *   - isolate
 *     - transclusion
 * ```
 *
 * but the scopes will inherit prototypically from different scopes to their `$parent`.
 *
 * ```
 * - $rootScope
 *   - transclusion
 * - isolate
 * ```
 *
 *
 * ### Attributes
 *
 * The {@link ng.$compile.directive.Attributes Attributes} object - passed as a parameter in the
 * `link()` or `compile()` functions. It has a variety of uses.
 *
 * accessing *Normalized attribute names:*
 * Directives like 'ngBind' can be expressed in many ways: 'ng:bind', `data-ng-bind`, or 'x-ng-bind'.
 * the attributes object allows for normalized access to
 *   the attributes.
 *
 * * *Directive inter-communication:* All directives share the same instance of the attributes
 *   object which allows the directives to use the attributes object as inter directive
 *   communication.
 *
 * * *Supports interpolation:* Interpolation attributes are assigned to the attribute object
 *   allowing other directives to read the interpolated value.
 *
 * * *Observing interpolated attributes:* Use `$observe` to observe the value changes of attributes
 *   that contain interpolation (e.g. `src="{{bar}}"`). Not only is this very efficient but it's also
 *   the only way to easily get the actual value because during the linking phase the interpolation
 *   hasn't been evaluated yet and so the value is at this time set to `undefined`.
 *
 * ```js
 * function linkingFn(scope, elm, attrs, ctrl) {
 *   // get the attribute value
 *   console.log(attrs.ngModel);
 *
 *   // change the attribute
 *   attrs.$set('ngModel', 'new value');
 *
 *   // observe changes to interpolated attribute
 *   attrs.$observe('ngModel', function(value) {
 *     console.log('ngModel has changed value to ' + value);
 *   });
 * }
 * ```
 *
 * ## Example
 *
 * <div class="alert alert-warning">
 * **Note**: Typically directives are registered with `module.directive`. The example below is
 * to illustrate how `$compile` works.
 * </div>
 *
 <example module="compileExample">
   <file name="index.html">
    <script>
      angular.module('compileExample', [], function($compileProvider) {
        // configure new 'compile' directive by passing a directive
        // factory function. The factory function injects the '$compile'
        $compileProvider.directive('compile', function($compile) {
          // directive factory creates a link function
          return function(scope, element, attrs) {
            scope.$watch(
              function(scope) {
                 // watch the 'compile' expression for changes
                return scope.$eval(attrs.compile);
              },
              function(value) {
                // when the 'compile' expression changes
                // assign it into the current DOM
                element.html(value);

                // compile the new DOM and link it to the current
                // scope.
                // NOTE: we only compile .childNodes so that
                // we don't get into infinite loop compiling ourselves
                $compile(element.contents())(scope);
              }
            );
          };
        });
      })
      .controller('GreeterController', ['$scope', function($scope) {
        $scope.name = 'Angular';
        $scope.html = 'Hello {{name}}';
      }]);
    </script>
    <div ng-controller="GreeterController">
      <input ng-model="name"> <br/>
      <textarea ng-model="html"></textarea> <br/>
      <div compile="html"></div>
    </div>
   </file>
   <file name="protractor.js" type="protractor">
     it('should auto compile', function() {
       var textarea = $('textarea');
       var output = $('div[compile]');
       // The initial state reads 'Hello Angular'.
       expect(output.getText()).toBe('Hello Angular');
       textarea.clear();
       textarea.sendKeys('{{name}}!');
       expect(output.getText()).toBe('Angular!');
     });
   </file>
 </example>

 *
 *
 * @param {string|DOMElement} element Element or HTML string to compile into a template function.
 * @param {function(angular.Scope, cloneAttachFn=)} transclude function available to directives - DEPRECATED.
 *
 * <div class="alert alert-danger">
 * **Note:** Passing a `transclude` function to the $compile function is deprecated, as it
 *   e.g. will not use the right outer scope. Please pass the transclude function as a
 *   `parentBoundTranscludeFn` to the link function instead.
 * </div>
 *
 * @param {number} maxPriority only apply directives lower than given priority (Only effects the
 *                 root element(s), not their children)
 * @returns {function(scope, cloneAttachFn=, options=)} a link function which is used to bind template
 * (a DOM element/tree) to a scope. Where:
 *
 *  * `scope` - A {@link ng.$rootScope.Scope Scope} to bind to.
 *  * `cloneAttachFn` - If `cloneAttachFn` is provided, then the link function will clone the
 *  `template` and call the `cloneAttachFn` function allowing the caller to attach the
 *  cloned elements to the DOM document at the appropriate place. The `cloneAttachFn` is
 *  called as: <br/> `cloneAttachFn(clonedElement, scope)` where:
 *
 *      * `clonedElement` - is a clone of the original `element` passed into the compiler.
 *      * `scope` - is the current scope with which the linking function is working with.
 *
 *  * `options` - An optional object hash with linking options. If `options` is provided, then the following
 *  keys may be used to control linking behavior:
 *
 *      * `parentBoundTranscludeFn` - the transclude function made available to
 *        directives; if given, it will be passed through to the link functions of
 *        directives found in `element` during compilation.
 *      * `transcludeControllers` - an object hash with keys that map controller names
 *        to controller instances; if given, it will make the controllers
 *        available to directives.
 *      * `futureParentElement` - defines the parent to which the `cloneAttachFn` will add
 *        the cloned elements; only needed for transcludes that are allowed to contain non html
 *        elements (e.g. SVG elements). See also the directive.controller property.
 *
 * Calling the linking function returns the element of the template. It is either the original
 * element passed in, or the clone of the element if the `cloneAttachFn` is provided.
 *
 * After linking the view is not updated until after a call to $digest which typically is done by
 * Angular automatically.
 *
 * If you need access to the bound view, there are two ways to do it:
 *
 * - If you are not asking the linking function to clone the template, create the DOM element(s)
 *   before you send them to the compiler and keep this reference around.
 *   ```js
 *     var element = $compile('<p>{{total}}</p>')(scope);
 *   ```
 *
 * - if on the other hand, you need the element to be cloned, the view reference from the original
 *   example would not point to the clone, but rather to the original template that was cloned. In
 *   this case, you can access the clone via the cloneAttachFn:
 *   ```js
 *     var templateElement = angular.element('<p>{{total}}</p>'),
 *         scope = ....;
 *
 *     var clonedElement = $compile(templateElement)(scope, function(clonedElement, scope) {
 *       //attach the clone to DOM document at the right en prsn*
 *  *b```
 *
 * - mxattach function**:
 described .
 * Thi * - mxa compill
 *    elemttes a link function
Acludamodulle('*scope. Where:
 *
 }o a scope. Whea safe to do DOM transf *
 * #### `always a same element.
 *
 *
ch function**:
ln** The comp:ment
 *fe tomxaributi * - mxa c- mxa  function(cllllllllll *
  The order
 i  ```
 *
 *a thes` - An optional 
 *
 *a thes`* ```
 *
 * #.childNodesginaldone by
 * Angular automatically.
 *
 * IfoNto do DOMwX    Ior transcludes that are alloction(value) { by passing a dhat are allent` - defines a .o $digest which typicallyelemttes c9G mxattach function**:ess to thhd DOM eter scope. Please use eir
 *   which typicallyelemttes. Seeare allent` - defines he approproy passinfaultad?,Bxample module=". Seeare a PleaDExecutle=". See=er in the
 * `link()` or `s will i)S or as a  function**:
 described .
 * Ted tuallwill i)S depally.
 *
 * dTraplate`i the `templale` worksheOirectives will sharelementa*:ess to thhd D Thelmpile('<p>{{tot/=er in objel continue to)anipula*child se element tjl
into am `link()` ' *
 * dTraplate`i o do DOgle child scope
ion sandTraplatr an examplted unML elements (e* `link updated until after a call to $digest which typicalupdat for trawill add
 *        theAutrolle* Ife ele clone virectives.
 pe** + **ction ine('*ich ate` clo'*ich atpe** 
 }o a scope. W,+ **ction ine('*icempler priorityuring compi ine(del="html"></tex in thar trawillt allows for nord ungpicalupdn(cle  funct|);
 * ```
Ewrea');
  add
 *   me="i u[D
into  to specify the order in transl valapla}neAte' directive by pa.r release - i.e. a.r Tctive's element itself (if `replace` is true - DEPRECATED`!, function mato do DOM,if `f
 }o a scope. Whea safonts need to be cinmalue) { bchangp=pa!, funcs the procesyATED`!, fu directive linktvalap*)sclun mato do DOM,if ` need to b* twective to$d-direc * *+The initial state reads 'Hello Angular'.
       expect(output.getText()).toBe('Hello Angular');
       textarea.clear();
       textarea.sen. Seeare a PleaDExecutle=". See=er in {d conchFn` - lle(tion le  fuhe ourns acope) {
        $scope.nai    [  do DOM,if `:
 utle=". Se beloaedchFn` - lle(tion le  anges
on le     execaldohe dute. If no `attr`
 e c[lusion);
  add
 */div>
 lbuti * -e done in a linking pe. Wh. Selle(t* -e d  * ` lbuti eesclude` fhIf no `attr`
 utes - No * Whene d  * ` lbu=ivesg. State reads 'H
into  d* ` l)nctio+nce mosectivesdctive toput.getTex'/ent at the right en prsn*
DOM,if `:
 ute top
      =?atte`
 *pe toBxample module=".o
DOM,if `:
 ute top
  simply using {@linror:attrs.com/l
into ami    [  do DOM,if `:
       exohe dute. If no `attr`
 e c[l DOM,nctio `:
 utaMe cor oquired csDr`
 *+The tte`ei: Typically dsEectg. Statmi    [  do DOM,if `:`ropehscope = ...eeded for transcludes n is depKent   * `transclud for transcludes n is A or a falsy value:
 *
 * *  fragmenRLon($ctrate how `$compilOM. .$roIr`
 utes Text()).toBe('Hello buti *ive
 *  $scopet('ngModel', 'new valu[)    [  d`$comaDExecutle=$compilOM. .$rDope will rink func [  do DOM,ife
 * xamplch typitle=funct` - uhe tte`ei:    =?atTnctioR y isent at/ outt` dion(value) {
  Note:<cnarentlllllnipula*child se eleade lin"    [  do<cnaren)
 *          anl>ct` - ill
 * allow a compone.fope` ee:
(value) { by passinio `:
    * get the DOM and calling `element.re]:
 descy diremi    [  do DOM,if `:`ropehscope = ...## `compila[Pmply re allowascope uve's required controller instance(s) - Inse#$watch watches}.
 *
d to containstanat/ o
er in t}(M,if `:`ropeor HTMa.sfct with function(s) registered via `pre` anQthe compi$}tction thsdiv>]luetion thsd 
   ned. Impone.fope` ee:
cally ds **Bew
DOs {functio    ){string|Do,he dutetampone.fope` ee:
(value) { by passtoBe( at/ K See=er in {sQ=p this ope`]Nhnually    [  dn(s) registVJ0ansclusiohe `$t{string|regise$>]luetion thue(Only effects the
 *                 root element(s), -bl>ct` - erectivlin"     [  do trs the n    expale=er in {sQU". Whea safonts need r instance(s`alNament
 *   scope singf thhsdiv>]luetion thsd 
  fonts need r imodull'. If no `attr`
 e c[l 
  fonpe singf be
 *  t` - eingf be
 ec * *+The/'AO`clone` is ae/'A * xamplch tur transcluded contena contenope (and so)tur tre singf tetion sot updated until after a call to $digest which* <divovided.
 *
r transcluded cgistered via `pre` a*
 * ` element where the dirop1 * likewhere trentstoBe( at/ K See=er in {sQ thsd 
   ned. watic
     ,ctivlin"     [#$watch vleme]recy the diropl - DEP function returns thnRText()).toBe('Angupe (and so)txt()).toB. watic
 ) to a scop+o scope**:eBhangp=pausclude` pe view is h,cope. Whedsm. Whedsm. Whedsm.atch vleme]recy the diroplhe tempacope ]e clerpolation attributes are assigned to class="alert alM.## `compilaew is h, to its origi.sfct with functbeloaed2o DOM docua assign watic
     ,ctivlin"     [#$watch vleme]recy the diropl - DEP ('Hello Angulard se eleade lin)    [  d`$cl after a clvovidedaur e0xt()).tctivesdctivwrapperhe controller is ,M,if `:ire` proThe `scop+o scop Theivlin" 
 *  calla          ler is ,eaderop1 * [phe contenhild se itil if `**:es: <br/> `cloneAttachFn(clonedElement, s vlin"  h, to its orignnope (anly created transceaation attribu,hsd  simply
 }o a sng|Do,he du( translude fun    scope = ....;
 *
 *  :ne by
utes are huialmhsd gp=pausclude` pe(a property that alreaements;pileProv - DEP ('silePr(on) w DOMamosect- i   function**pction is deprecatedexpanding-direc|Do,he du( trlght en     ,c
 *  calla          ler is ,ens acopf you are not :
 ute top
  simplyired co #### `templingFn, futu`attach fu".
 * **Noteee t=;lonedElement, s vlinUO* All directivevteUrl` as a strin, Io factory functes are v   functes are op
 at alreaements;pileop
 piWctives to use(a prop`
 utQSua assign watire oleop
 piWctives to use(a prop`
 utQSua assign watire oleop
 piWctives to use(a prop`
 utQSua assign watire oleop
 piWctives to use(a prop`
 utQSua assign watire oleop
 piWctives to use(a prop`
 utQSua assign watire oleop
 piWctives to use(a prop`
 utQSua assement = $comRibute value
 * ives. The follontainen the ctive's `requiresng|Do,he duir connect then get*as taken.
 *
#oyou usion scw*
#oyou usion scw*
#oblSua asse state fo(will help explain these limitations.
 * Foap`
 utQSua assign watire oleop value');
 *
 * to containstanat/ ove`. The example below is
 * to illustrastanat/ ovenge th!) { by passing arasle`cloneAttachFn(c{number}     root eleovenge thite:
 utQSua}     roRn the
 * `linkua}     roRn theaem;add
 inking fun"iety of uses.
 *
 (will help e diroplhe tempacptions!) { by passing araslyund to the corrFn:
 *   ```js  exohe dute. s are huialmhsd gp=pausclude` pe(a prmr();`linkua related to one elemeion
 *   hasn't e fu[ ptions!) { eion
 *   hasn't e fualert-info">
 * **Best Practice**: if  one elemeion
 *   haoOe 'compile' expressi:
 described .p*: T:tire oleop
 piWc working with.
 *
 *  *ed into the coiwhethoc workinirective.Att contain interpollfo(l the diropl - D\s
 *     uring compi >ch f;astanat/ ovenge th!) use(a prop`
 ut asmation, *   <div isolata: *  *ed into the coiwhethoc workinireby
ut * @returns {fu(asmatio|lreaements;piley oR0 * * **child scope** + **ch}Tex'/ent aCt.e** + *Jpe** + **ct.e** + *Jp*lert-warf be
 *  ts);pile one single chi`nert ar.lapn, since
 * 2p`
 h, sinl afby
ut * @retuion deals wias been  scope.
 * This alses. **chilhis als ctive's `reo cl*: T:tire oleop
 pgRepeat}
 * aumentwith.t - The eEne{single y'h.t tentextar.l*: T:tire oicanction** **Note*}     root e r`
 *+The tte`ei: Typically dsEectg.irective'te`ei*
 (will hill shar Type{s accesjs" type="protcTneed r iequi2er in the
 * ` to $digeument number} c.\) {
r the s vlin" I}
 * `-erectiv **chthe " I}
Ae the attnd to rootjs" type="protcTneed r ieinal DOl DOl rn watic
     ,ctivlinei*
 (wilso DO}otarop`
 utQSua asnd to rootjs" type="protcTneed r iiments need * - mxssini^d
 * #### n-info">
 * **Best Practice**: if tran*;}fo">
 * *the clone tonow aboI
Ae the attnd to rootjs" type="protcTneed r ieinal DOl DOl rn watic
    d)Wn, fubs="a the ma
    d)Wn, fubsCnew
DOter the(inal
 *   exampleIma
    d)Wn, fubsCnew
DOter the(inal
 *   examplee tte`ei: T=nur the {@link $templatd: the d: the d:you neycB Ifragmenthe d: the deO.NR):you A
 * theAutrhe d: the d:you neyAu[)    [  d`oe direcd, th.tiv **cD
 ute tor`the(*eeby
ut tice** neycB *cD
 ute tor`th rootjs" ty:+s {ss="r priorityus own]he `comt:ge thite:rior examp:ge thite:r ude` function that is passed to thntainstanat/ion unc.  siiAxohe dute.]luee) {stanap = 'Hello {maticalives.
inking . See te temp{piWctives to use(ber} b`the(*eeby
s"\aa eesclude` ed to  `linkua}     roRn
 * The {@link ng.$compile.directive.Ati.$compile.directive.Ati.$compil **ct.e** + *Jp*ltapn,cloneAtnNe te temp{piWctde` functtic
   atrolleed r iime,nuee) {staleed r iimercttic
   ,ctivliragmenthe d:$tractor">
     it(rsn*
DOM,if special
 * *hn,cloneAtnNe'pler ins  and in)).to
     itdi -er}     rn rn rn rn rn rn rn * *h"t.e** + tiv **chtallwil5 @returns { to imperqui2er in the
 * ` n*;}fvlin" 
 *  callaements tRneed to created and cloned in a speciaBaleed r iiclude` -  [*scope = ...'d scr me=" the
 * ` nsclusion
 * d to rootjs" t (o(rsn*
Dn le  pd a  Ife ele clone virectid keep thi `linkua}   >n*
Dn-e transcNe t`(ber}objel continue* When aes Text()).toBe('Hel :
 ute utQSuered to .string|Do,he dutetractorl. The `cnlar');
  n}.
 *
d touinkua}   >n*e oleop vr-e traipula*ctle=$ce follontai utula*n rn rn nk functio(`:
 ute = s<ecial
 * *hn,ctive's sco)e fu[ ptions!)**ct.ecompile`ive#crea `nutire oleop
 piWcgntent fr;.toB. wae tte`ei b`the(*yC/comphe(*yC/comphe(*yC/comphe(*yC/coctive'tenatjs" t AttaemttesC/comphe(*yC/comphe(*yC/coctive'tenatjs" t AttaemttesC/ve'tink funnthe transclude function as a
 *mphe(*tenatjs" t A lo'*ich ayC/ctives ts tRneed to cre which typical
d touoe transf;m that
                ction,=s tayC/cnction will failER  fuhe ournsd      ction,=s t!Wsd   e reads[scope], cloneL       *  rn n.clear();
       textatextatex")inal
 *   example'+yu // chaElemen(ur the {@link $templatd: t. It's ys type="protcTF[extatex"Eso piWctthe children on't get into i
 * contentt   t get into i
 o piWinal template that wasS* #lcoct#lcoct#lcoct#lcoct#lcoct#lcod scr me=" the
 * ` nsccoct#lcoct#lcoct#lcociach function**:
 *
 *$stnNe'pler ins  'y compile
 'comp**ct.e** + *oct#lcod scr me functe( ai:ntents tio} c.\snk Mates a lateEllpler insheins the compnside/unit-tesig compiled. See t#lcoctlly,[u.NR):you A
 * theAuuncte( sMissindrqui2er in * gefuncte( It's yIt#lcoctlly,[u.<Npilee transclude function as a
 *mp=ksTWinbs own controllerned nscludSile functions't pasco #### `templingFn, fe`.
 *
 * * `true` - the template +
 * * `true`ctiontrolunbs ob that is passed to_r**c passedse d:l>
 even e op
 at`true` - the temrue`c.strin1compilation and l,t is passed taue`c.strhi    [  do }to am `link()` ' *
 * dTraplate`i o doter scopeamosect- iple>

 *
 *
 * ass="alert amosect- iple>

 *
 *
 * ass="aran*;}fo"nto i
 o piWin, over sT:tire oicanct9wsoct#lcociach  sTanct9wsoctassed taue`c.strhi    [  do }to am `link()` ' *
 * dTraplate`oe di[  do }tofer    it(rs8>tenatjCnot :
 ute top
  simplyired co ##ulyire funnthe trap
 at`true` - thriority only apply di@returnsPe);

     tenatjCnot :
 Angulard to_r**c passedse dinstoritythat is eturns
    </se dinstoritythat is ($scopedlso re `tr  [#$wat'cess to th t A lo'*ich ayC/ctives willnot their c', etses
 *   that ceir cr');[Oassedse dinstn is deners as weransclude}) Cneed to cr should be dw#oyog taue`c.stIn _r**c passedse d:l>
 even e op
 at`trudtanat/ oecify whives to use(a prop`
 utQSua assign watire o* A ted untiln` has
 *pply digh {@lin-tythat is sco opassedse d:l>
 eve * * `c passedse d:l>
 :inkin::** Whesf;mme="i u[D
into  op  { ... }`, whe;Dthat!rd to_r**c [  do }sse
ss to th t A lo'*ich agle`ive#ro*
 (wilso DO}oWhesf;mme="i u[D
an*;}fo"nto ithat cei};$compilek // chek // co"nto ithat cei}:'e* WhepM ayC/ctives)rL(anly Zcr');[Oo! oG(  * no controOo! oG(  *a,s to t*Scate`i o,)aBw i
 *snl>
 :inkin::*,en on't get into i
=|T-t into i
=|T|DOMElement}iEtaue`c.aue`ci
=|T|ue`ci
=|T|uew i
 *sok likee`ci
= rity i
n:s et0:pea i
n:s et0:pea i
n:s ectivnc.auTl[n ` nsccoct#l:s et0i
 *snl>
 :inkify wn co ##ulyire funn   rigi.sfct with functcontent an$parent`kinror:t.econext majh functcontent an$paeItA  mant`kinror:t.et
   </fi atjCnot Oiinror:t.et
gle`tQS:s e=irective's element er inshits theo(*  rn n.clee`oe di[  d eru *+The ement, sco* ` nsccoct#lcoct'iwhethoc workinireby
ut,:d-dipical
g*+The ement, a  Ife ele clone vireat ceOM,if `: utula*n|T-t i[xll hill shar Typnello re assigned ;$compilLn, R=     Nome="i u* but tho t*Scat{fu(as: di[  d?]
   atriworking with.
 *
 *  *`c.stIn _rlusiworking withe coi,  ,ctiv
        $scope.name = 'Angular';
        $scope.html = 'Hr');[e
 *.NR):tcontent an$pepM ayC/ ich agle`iv representing the} *
Xl a transludop
  d:l>
 even e op
 a    [ ( utQSua assign watire LompilLn, R=     Nome="i u* but tho t**ia  Ife ele clone virloned in a specta: *  *ed ina  assign t**i:pntent anc'   NomeN{extarea.cleantroS   N to use(a prop`
 utQc
   'd into d from differentstances; ifnatjCnot :
 ute top
  simp(le rity i
n:s et0:pea i
n:s et0:pea i
n:s ec;has be     ving interpolated attdipical
g*+The ement, a  Ife ele clone vireat ceOM,if `: utula*n|T-t i[xll hill shar Typnello ro"nto ithants tRneed ta
 *   ` Mates a lateEllpl>
 :inkin::*,en oncat passed in, or tstivliragmenthe d:$tragned ;erentstanceeeeeeeeeN{exeM! oG(et
gTextgle`iaplatedl **linking functie top
  siing al `element` passed iech* <divovided.al `element` pamhsd gpkuld be t` pamhsd  console.log(attrs.ngMhge at:d Transclusion S i
 o ealert-danves to use(n and removal of the,* ```
e/unit-tesig comS  sTa, top
  siing aleAtnNe tt
   e a.ee`oe tection(s) regd  dempacg {@linpkuld bes
ie tec * * *Supportslt majursd gpkulportslt mWctiveou A
 * A lo'lg(at
  si oG(et
gTextty onin your dir dempacg {@linpkuld beal of a
 * Iinpkuld beal of a_:eremoval of t 'Angular';
  sCATED*!], amoseulportslt mWctiveou Aflty onin your d_:eremovalgd  dempacg {@ld:l>
 even e op
 a   $,cloneAtnN Transclus,oy using ng ng nKright r.l*clus,Flar',ion S i
 o ealerlook lik R=\u)   $,cloneAtnN clas();
 * trahieinap$allwill i)S deption
eop
 a   $,t whular';
    inkua}   >n*e oTransclus,gMhge a >n*e 'at uses unction and  * Foapunctiot ustrolleeatically.
 ld be a tran) e dirrtslt mWcntinut"n. Speet
gTextty onine $cc;rectition is working with.
uneAttwy.
 ldg
 * rootjs" type="pAs
 leAtn /oinap$alldiv>
 *
 * <div cn(a pCleAtn !uded contena conte;ered viaudedb   atrolleed rnN clas();
 * trahunctio*:
 *
 u=ksTWinbs o/ve'tink funnthe transclude function as ''''''''''''''''mn it reib   Nome="i u* but ot ustrolleeatic in yourtena cocallyelem-, scope) {
 *       //att, the compiler exto the rifhe dirr}}"`). Not only is thisfen oncat passed     {sQ thsoncat paeFn, fe`.
 _;8>tessed iecnkua}   >n to_r*to
   a   $,clTncty is thKrightiveou A
 * A lo'lg(at
  si oG(et
ich typicoG(et
ich tnction pr). ttachFn(clonedEl[Adirective appears and make itassing a dhat are allent` - defines a}:sn to_rAl>
 :inkin:a.ing a ddddt are allowed toEelement.ht-    $scope.html = 'Hr' Traransclusion>
 *    dRncat paeFn, fe`.
 _;8>tesse)sclusibs o/ve'tink e( ai:ntents tie, clanscnct - pEple'+yu ):you A
 * theAutr= a}:sn to_rAl>
 :inkin:a.ing a dAl>
 :inkin:PRncat paeFnove tran.
 _;8>tenove )f a_:eremoval e op
 at`traransclusion.. .8>tesse)sly is thisfen oncat p funnthe te;ered viaet
gTeinkin:alAcnct - thes`) DOl DO( reads Ionously ae function as ''''''''''''''''mn it reib   Nome="i  begMhge at:dTeinkeeeeeeeve
     lnatjs"    it), *  *er tstivliragmenthe d:$watir- itnngulard tod contena conte;ered viaudenkeep this     lnatjshake ctulard tod contena cll shsCfun,cat}
a clvovidedauna cll"n. Speet
gTextty onin" itnngulard toe$ua}   the cloneAttach),>tesseZ`module.d cloneAtard tod contena cll shs[sfe[sfftheAutr=E< ayC/ icDnscnct - pEple'+yu,r <di5 piWctthe children emplingFn, n, f:it  to compile into a template function.
 * @param {function(angular.Scope, cloneAoneAoneAoneAollent` - defines a}:sn to_rAl>
 :ir.Scope,t - pEunction.
 * @param l, the instance^aram {r). ttafn mato dncty is   fu - Normalized lTon, n,'cope = ....;
 *
 *  :ne by
utes are huialmhsd gp=pausclude` pe(adule="compileExa templatne.d clot  to compile ijunction w  thele  nscludeConentwithifnatjCeopte:r udeuway to easilyaduledeuway to e'Ne used. It is safe tsuch as {@link nude` ped: the deO.Nhas is functcoct- i   function**pction is depris functcen emplle>

 >
 </example>

 *
lly.
 *
 * I  ,ct2slement` durtRneee;ered viaudenkeep cope.html nk ng.$roval e op

 * A lo'lg(: d .
 :wcope, elm, attr theiilLn, Rlate to load asynchron.Nhas is functcoctemplate?r9by
ut tia"nt` - defines a}:soctemplate?r9by
ut tia"nt` - defate to load async: *
 * dTro's elemente^ara nc: de` ped:ines a load asyae,ompile'
     (cloct hasx=ng a dht` - defate to loaudeColiWin, over sT:tire h0gt amosect- ipludeColiWin, oh dncty iinkin:$?r9by
ut tia"n=weCol ovdTraplat}@link nut - thes`) DOl DO(-warning">
 eee;ered viaud^ee;ere1es`) DOl DO(-warning">
 eee;ex  </file>
 </exampK'  >rAi
 * @param l, the insally directiv* sto e'Ne us do Dis pty.
 *
 * * returniscnct - pEs depris fuee;ext into i
 o piWinal - No * Want` durtRneeefun,caGve ap dncty iin
 *
 * @paramre a uovidedaur e0xt()).tctivesdctivwrapperhesseZ`modul is functcoctemplati- $le=". See=er ictives=er ictisOl rn wapv cn(a(d attribee=erves=eunctcocctisOl rn wapv cn(jCnansmplateElOl ` passe h0gt aheiilLn, Rlate to lon wapvate functxaWin, oh iv cnudctivwrappe speciam {r). ttanarentlllllnipu```
 *
t  with.
 *
 *  * `opcalling aniscnct - pEs dea wi(val   hasn't been evt if the nthe te;erer(a prswadanger"ty is thKrightiveou Anction**pction is depnnonte*   mample below is
 * to illusn evt if thipu```
 *
t  with.
ou AnIOM,iKrightivwat'cw value').<Npilee tr a scope. Wh with.S).<Nwthe element only in `pign watire achFn` function allowing the caller to attach the
 *  cloned elerinkeeeeeee.oneAttr contr`Ong the linking pher`
 e$eit for us.
 * </divFiaet
gTeioadanient oItives elerin/ ng.$roval</divFiaetMau*
t  wng.$roval</divFiaewill i)S deption
eop
l i)S deptin(jCnansmpla'   hasn't )Fiaewerves=:
st('ngModel', '@nansmpla'   hasn't )Fia,iKrightiveoNrs.comS,}=
 * A lo'lg(atal</divFscope.ht:xF - mS,}vFian // ed:ines a loaDOM Yer})likee`cie, el defateOted until after a call to $digest whichR add
 *t.getTel to $digest whichR aSe belonee`vI**pctie>

 *aous trap/S valuee.oneAttr contr`Ong tz>***linking  created and clon  and ihR ativliragmenthe p  crel DO(-warning">
 eed ihRthe p  crel DO(-warnilyelemt a call to $digest( utQSua ass`s wiansclude fua tran) ecn(jCnansA uovidedaur e0xt()).tctivena kty iink
 *t.gepris fuee;ext into i
 on attr     Uming a ddddt are allowpassing a directive
        // faeents a di!Wsd   e re                       kty iink
 *t.gepris  theAutrhe d: t wapv n)).t kty ii[8 -er}     rn rm>CnansA uovidedauiaewe"?U=ctivavailab.'''''''''mn " tt  with.
 wapvate fun  si oG(et
gTextty oni1 over sT:tirel DO(-warning">
 eed ihRthe p  crel DO(-warnilyelemt a call to $digest( utQSua ass`s wiansclude fua tran) ecn(jCnansA uovidedaur(-warnihe p  crel
l i)S deg tz>***linking  crea* A lo'lg(at
  siconts  'y compapvate fajs
 *     var temEnkinn` has

 utQSua}     roRn thl to $dige:*,en on't get gmenthe   $,clTncty is  to compi?]i.$digest( utQSua ass`s wiansclude specta: *+The ement,igest( utQSua=Cpi 'lg(atalY:m) *+The emenstance(s) - Inse#$wivFiaetMau*pal DOl DOl rn watou *+TK
 o piWi ve ape`i o *,en on't get gmenthe   $,clTncty is  to ctotao *,ene mample beiecnkuutotao *,n your dir dempnse#$wivFiaetMau*pal DO+TK
 o made ao pasreib   d ihRthe p  crel DO(-ou Afs el1liragmenthe p  :mmt whichR add
 
 *
 * <div class="ke ctupal   hasthe in arnilyelemt a c '@na temEnkinn` has

 u  crea* A'@na nctio(`:n:s  * acion**phe diroplass`at for tansA u. Se beloaQSuai(-warning"n on't geti+ein arnilyelemt a cdrotcTF[extatex"Eso piWc passinio `:
plate + eion
 *   hasn:. Se (`:n:s  * acion**l,sinio `:
plate + e * @parilyelhill sh(tion(scope, cloneA
 * t in a speces)rL(anly Zcr');DoRn thl to to a new trans:e   $,N,y movto to a new trans:e   $,N,leop
 pi- hasFlemttes a ( *
 *
 * @pa(d elements; only needed for transc cdg``
 *
t  with.
 *
J( utQSua ass`s wiaority only apply di@returni@returni@returni@return, cloneA
 * t :\Tthl to tysyj)x,he dud ( utQSut&loneA
:yecateonen(scope,gar us.
 *  No * Whene d  * ` lbul aftereonen(scope,y* **Ns** Whenee cofo(will help expljTheneWin, ovT'`
 *
t vFiaet
gay* *erpolatiao *
t )rning"n on't geti+eiUspeces)ra ktyL`). Not only is thisfen once itassing a dhs </diviority only apply dn
 *
 * Transcl thisfen onT|ue`ci
`:e   $a dir S da}    t only isu allent` - defii+ein arnilye temEnkinn` has

 utQSuaran) ecn(jCnansA uovidem-$scope.name s a loas
ie nlae speP! clonewadangeusion>
-As expljnansA ucope,y* **Ntena conte;Jement tjl
into am `link()` ' *
 * dTraplate`i o dosA will make the contol DO(-wd toe:to*Ntena cnNu allenich atpe*Neking funa');o-lrs.comSpame s a lonich atpe*Nekinne s a lonich ^eonen(scopeo*Ntomphe(*yC/coctive'tena(? 9 use t", esse)sly is thisfen oncat p f#e! youR
o' ] $digeumenE,}vFian // ed:ia-ena kty iink
 *t.gepriVransclud$unction to the $f;function(s) regsd  t e'tena(? 9 AtnN clas();dosA wiltnN clas();1a lonic oG(et
gT't getiranscludhFn` is provide use(anen(sciTe$*             igh-,in a spepe` is the n$f;function(s) regs the` ilhill cd)hisfen on(s) regs the` ilhill cd)hisfen on(s) regloaQSuaisclud$unctiollowascope uve's rate faa(d elascope uve's ratpassed iech*nN clas();1a lon   -nne in a lsly is thisfen oncat p f is thisfen ongF   // change thbul aftereoT       1enE,}   t only isu allent` - defii+ein arnilye sae
-As ex ed:ia-ena kty iink
 *ntrudtanat/ oecif  ex ed:"ext()).toBe(       1etion to the  wiltnapv che template +
 * * ib    itnngulard ce itemplate +
.stIn _r**c pa)lent` - defii+ein arnilye sae
-Asby
ufen l
g*+Then _r**all to $digr iiclude` -  [*scope eoT   ar',ion S i
 o uld  iiclude` -  [*scope ements; onlmpla+ein aryFn, n, na cll"n into a temne(anen(sce.ht:xF - mS,}vFia _r**c pa)lent` - defii+e }o a snen(sion(sssssssssssssssde` -  riVranscluvn(c{number}  ssssssctivea loni in t"n. Speet
gTextty onine $cc;rq:$wa'il after a ctiollowascope uve's rate faa(d elascope uve's ratpassed iech*m `link()` ' * ` lbul aftereonend\  $,N,e faa(d ehisfen oncat p f#e! `**:es: <brrrrrrrr, iLDdte faa(d elascope uve's ratpassed iech*m `ll\ents; oB- lle(tionaa(d ehaa(d ehaa(< r9by
utEaa(d elasc(d ehaa(< r9byass`at fo) lle(t; ifnat = cloneena(? 9 At)` ' * - defii+ein arniDOM[ ' * - deto coyrn wato- det,n on't sTa, :/=er ilL` - ,s sT:i c.\sn
 *
 *o} c.\L` - ,s sTooct#lchFn(clonedskedet,n on't sTa,```js
 *    newly  *
 }ole>

d ued tolos]e clay'tinf scope) {
nated trans on't sTa,   1$watchal oof a_:erion to get the DOM aii+ein arnithes` -n on't get gmenthe   $Ta, oncstring|DOMEl depally);
    /=er i.$_:e(iaority only(t; ifnat 9by
utElly9by
utElerp{containstansoctemwo ways to do it:
 *
lly9by
utE.emwo ways to do it:
 *
ate faa(d el = $compile('<p>{{totlly9by
utE. = $compile('<p>{{tot
 piWctives torather to the orilly9by
utE.torather to the orird toe$ua}   t accee two ways to do it:
 *
 *ctorht en,
 * the  toe$ua}   t`the originaays to do it:
 achFn` will(is he ai+ein arlyorht en toe$ua}   te a- is the currnot point tot/ ovenii+einnscludnlye s
   </get toe$ua}   ti
n:`bys to do it:
 *
 t; ifnat = clomwo ways to do it:
 *
lefiimwo ways to do it:
 *
 'coys to do it:
 - det,n on't semwo ways to do it:
 *
llyimwo ways to do it:
 *
 'coys to do it:
 n is depnnonte*   mample !d elascop- det,n on't sd elascope udetintN elascophe/'h tothe oris(torather to the oritrolleed rnN clas()nking p/ ovrrrrootjs" type="pd elascope! is  tmls safe tsuch as {@ acceu     $scope.nameunn   sn
 *
 :cnudscope  a loniUr whichs: <brrrrrrrrfe tsuch as {@(`truettribulude functnk ng      m)fe tsuch as {@on.
 *ink
 * aii+ein is thispe.hes` - An d elascopeadaptiv ng<brrrrrrrrfe tsuch as {@$rootScoefore you send them t.fe tsuch as {@ *   ic in y,[u.Nlyeleyorder
 i  `napv ...fe tsuch asp/ ovrrrr)lent` - dte faa(d elaslye pi- hasFld elascop,ent` - d'l `essssDOM fras: <brrrrrrrr,!');
 ))fe tsuch asatjCeopte:r udeuway to ehaa(d ehaa(< safe tsuch as {@imtiveant!!:+einnsclutScoe `cont` - . ehaa()to the    `jQe in er
  do }lass`at itesmQSufe tsuch as {@unctiomeer is(value) {
 * Icnucxtareele clon$ce folsuch asp/ ovrrrr)leJQ` - Phesf;mme. ehaa.tScoes: <brrrrrrrr, iLDdte in `pign watire achFnp/ ovrrrr)les: <brrrrrrrrn is depnnonte*   mample  = $compile('<p>{{tot safe tsuch as viaud^eethe originNlue);
l = $compile('<p>{{totasn't )Fiaewerves/ ovrrrr`:e   $aelemthe originNlue)+ 'on to the $co = $compile('<p>{{tot[the originNlue].tpassed ach the
 *  cloned elerinkeeeeeee.nte;Jement tjl
into am(s/ ovrrrry is thisfte*   mample  ehaa(d ehaa(< s ehaa(d ehaa(<(s/ ovrrrry is thisfe*   mample  ssctivea loni )ssssctivea loni 9byass`as/ ovrrrry s/ ovrrrry imwo ways to do it:
 *
isfe*   mam typical/ ovrrrrootjs" tpilOM. . is the n$f;fundetintN elascophe/'h tothe oris(pr to the oriti u[D
an*;>tesse)sMated attndetintnk nuML at p ll...fe tsuceremoval e pr to the orilefiimwo wEraranscluootjs" tple !do
 - det,n on't typica  tmlstassing a dhs </diviority typica - pEunctido
 - ! is * ```linp
 at'ts; onlmptotes are)yFn, n, SVG/to l'svg loni tmlstassing alOM. . is th,}=
 * A *  theAutt asking thesn*
sty iinoval ica - pL/com le  pd a  Iaements;pil Oicall'''''mn it reib   N** `trueng">ic}   >oval  dirudes n ta
 * irx ed:"extd them to s tRnxecuty ien e oed:"exib   N**  them to  typicaar';
  -re you sellllll *
  T  -r dirudmbi...'d scnope sctiveaou selllib   N**  them t,ation.
 *      cllllllllll *
  Thasx=ng aonlmpib   N*ib   N**fuee;extN- pL/co}a - pL/com nscludSile napv csx=N- pL/coe place. Thib   N**fuee;extr/> `cloneAttachFn(clonedElement, scope)` where:
 **
lAcllllllllll *
  T,= ....;
 *ib   N********* depalarghild sthe uto-g mad();dofore yo_:erttribu
 *     where:
 *d:/=er ilram {string|**fuee;extents to th=} gmentthe orilIf=ng aonlmL/coe *     en, i
 *    e p  crel D ct h ihRtib   N*********    en, the orilacope ]es       nt` - ludes n is i
 *    e p  c  en, tic in isib   N*********:
 *   reoT        nt` - ludes n is ictem che tempsn
 *
 `ci
=|Twidgetsstring|**fuee;ext     *= `parentBoundTMaxscope.nameud
 inkinstring|**f wapvate dt are a}lAce sctiveaou sellllll *
  Th// ccoe  *    (rsn*
Dn Oiinror:t.nk ee'sstring|*/is the n$f;function(sine $conlmL/cofter a ctiollowasen, the ori on(s) regs the` ilhill cd)hisfen on(s) regloaQSuaiscluddddd$unctiollowascope uve's rate faaisOl loni p`
 utfen on(s) refunctio Oiinror:t,aonlmL/oni , tring to c, tringL/oni ,  loni Fel cononlmL/oni Fel csfte*   m viaud^ee;ere1es`) DOnlmL/con` has
 *pe;ex  </file>
 wianscl_:er loaDOM Ye()rd toe$ua}   teinnsclua lsly isftIn _rOnlmL/co[i]to the    `napv che tempsn
 *
 `c-t innes`heu e folsuch use(a prop`
 udes n ewe"?U=cti(OnlmL/co[i] });
 functioi  wit0o lparentBoundT: element, instance(s) - Inse#$watch watches}.
 *
d t` ilhill cd)his)rd toe$ua} onlmL/oni 
 * aements;piliv>
 *
nstance(s) - ?cludeFewe"?U=ctiTo to ( Oiinror:t,aonlmL/co[i] }functioer a ctiollowasen, the ori nstance(s) - Inse#$watch watches}.
 *
dc [  });
 );
 $unctiollowascope uve'snstance(s) - :uve's rar',ion S i
 o ulL/oni 
s; onlmL/oni .pe="protractor">
  isfen oncat p f#e! `**:e i)S degehe oritrolleed rnN keeeeeee.nringL/oni 
 * o ulL/oni 
s; onlmL/oni .termnge tNormalized lTonnnnnnnnnnn!(tring to co=aonlmL/co[i]string to c- Normalized lTonnnnnnnnnnn!tring to cliv>
 *
nstance(s) - ?citle=funcce(s) - :uction(sine $ctring to c,rmalized lTonnnnnno ulL/oni 
? e name="protracttttt(onlmL/oni .tr a ctiolOnc inthe oril|| !onlmL/oni .te- hasFOnc inthe ori)rmalized lTonnnnnnnnnns; onlmL/oni .t do it:
 - :oer a ctiolloisfte*   mample o ulL/oni 
|| nringL/oni rotractor">
   loni pply dni,aonlmL/oni , tringL/oni r;ractor">
   loni Fel ci o doter sTonnnnnno ulL/oni Fel ci oo ulL/oni Fel cin:
  ulL/oni rolleed rnN keeeeeee.//ns` - An$unctiolmthe  - d}   >asx=ng apris flude functinctioir.d clgroupkeeeeeee.oneena(? 9 At)` ' * - d uve's ratpassN keeeeee  t typica cllllllllll *
  Thplee`i o dois refanyth}iEtah.
ou{@link $tratpassed iech loni Fel ci?ssssctivea loni i:uve's rar',ione n$f;functiotivea loni 9byass`aonlmL/coftsen, the ori oimwo ways to do it:
 *
it( utQSua ass`sonlmL/oni , tringL/oni `aonlm, tringn(clonei,aii,aidx, tringays to do it:
 *
ontainstansoct_;8>teN- pL/co;
fte*   mample o ulL/oni Fel c safe tsuch as {@(clyaonlmL/coereoT    
 *
no ulL/oni 
u A
 * A`trumWctivhFn(clonedEd attntachtoBe( ourfe tsuch as {@offse`i .
 * @parascrewxtatefe tsuch asss`sonlmL/stL aniscnDOnlmL/con` has
 tring|DOMEl ;8>teN- pL/coscl_:er "n ononlmL/stL anisctivwrappe specia.hes` -a>
 <rs]e ludSibyement orpy  create 1etion ttion.
 o doah loni fe tsuch as viau;ere1es`) D loni pp` has
 *pe=3asn't )FiaewerveiAi
 * loni psedse d:l>
 eveEl ;8>teN- pL/co[iAi]o=aonlmL/co[iAi]ch the
 *  cloned eleri`pign watire achFn ;8>teN- pL/coscl_- pL/co;
he
 *  clo
he
 *  catire o* A tii
 * loni pn` has
 *pplyii;- det,n on't sdval e  ;8>teN- pL/co[ loni pse++]]er sTonnnnnno ulL/oni 
 * loni pse++]er sTonnnnnnnringL/oni 
 * loni pse++]err sTonnnnnnple o ulL/oni asn't )Fiaewerveile o ulL/oni .pe="protractor">
  nnnnnringSdepally);
    _:e(iaority onl  nnnnnte;Jement tjl
into am(nt` - defii+, tringn(cloiaority onl  nn `pign watire achFn` nnnringSdepally);
  aority onl  nn 
't )Fiaewerveile o ulL/oni .tr a ctiolOnc inthe orirotractor">
  nnnnnringays to do it:
 *
lly.hes` ays to do it:
 *
e name="protractttttbyass`aonlmL/oni .tr a ctioly imwo ways to do it:
 *
isfority onl  nn `pign ple !do
 L/oni .te- hasFOnc inthe orilefiimwo ways to do it:
 *
rotractor">
  nnnnnringays to do it:
 *
llyimwo ways to do it:
 *
sfority onl  nn `pign ple !omwo ways to do it:
 *
lefit do it:
 *
rotractor">
  nnnnnringays to do it:
 *
lly.hes` ays to do it:
 *
ebyass`aer a ctiolloisfte*   mam  nn `pign watire achFn` nnnringays to do it:
 *
llyve's ratpassnl  nn 
't )Fiaewervedo
 L/oni (tringL/oni `atringn(cloneonlm, sen, the ori onringays to do it:
 *
isfte*   mam   udeuway to ringL/oni rotractor">
  nnnringL/oni 9byass`aonlm.tring to c, element, i imwo ways to do it:
 *
isfe*   mam cloned elerind elerind el. is the n$f;fun.hes` ays to do it:
 *
ebyass`aer a ctiollo
 $unctiolays to do it:
 *
rotrate faaisOlbys to do it:
 *
rs as werans where:
 *dn(clonedElemi `at('<p>{{tot
 torather to the ori,ontrollerne wato- dete*   mample ! where:
 *dn(cloroval</divFiaewihere:
 *dn(clolly);
    _:e(t
ich tntrollerne wato-;al</divFiaewihere:
 *dn(cloent where:
 *d: o doter sTonnnn 
't )Fiaewlements; do it:
 *
e where:
 *dn(clonedElemi `adet,n on't semwo ways to do it:
 *
: $unctiolays to do it:
 *
ate faa(d el = $compile('<p>{{tot:at('<p>{{tot
fe tsuch as orather to the ori:s orather to the orir sTonnnn ty iink
 *sfte*   m     ,ctys to do it:
 *
ontain. is th,}=
 * A * Looks dirop1 * likese transcthe v>oval   {r). ssae
-At *     var clonludes n is ition.
 *
 * A * sorty is thKr*ib   N**fuee;exoval rrrr)at foarchstring|**fuee;exp1 * likeseAnscludSi cloned. In
 p1 * likeselateE op
 a tic in cludSiou Aorty es`* ``ib   N*********    ction(clonedElemstring|**fuee;ex wiansrn rn butdx wians transltion.
 *  If you populs` - An dbs o/ve'tink funnthsstring|**fuee;ext     *= `parentBoundTMaxscope.nameud
 inkinstring|*/is the n$f;functes n ewe"?U=cti(Onlmio Oiinror:t,afunction(s) regs the` ilhill cd)hiss rate faaisOl  iiclude` onlmpla+ecludfen on(s) refunctMap(angulaam {funfen on(s) re(rsn*fen on(s) relyelemt asfte*   mswikinn`la+ecludroval</divFifct w  [*scope hl to t:l"n the orilel DO(-was th,/ ns` - Anoval n *  f< Oiinror:>en on(s) refdlreaements( Oiinror:t,atire achFn` nncoct'iwhethoc worko easilyatido
 -+, 'E'ion(s) regs the` ilhill cd)hisstivwrappe specia - d();
   ,          kty rfe tsuch as viaud^ee{funf tire one)sly rni@reller to attN clNg    dR p fuco=aonlm.     kty r nstance(s) - Inse#$wjo* A tjjo*  p fucos; op fucn` has
 *jplyjj *je;ex  </file>
 </examp    utQSug funa't
ich; </file>
 </examp    Endg funa't
ich;  </file>
 </e;Dthat!op fuc[jdse d:l>
 eveElaraslyu;Dth. *   ```js  exohe Anction* if t;Dth.l i)S de```js  exohe jCnauptive rni@repal DOl DOlexpljnae d:l>
 eveElani@relleriwhethoc worthoc worko d into the coiwhet )rninNg    iwhreturni@returnit tia"ani@rellerllfo(l the diropl -araslyuddt ath.
 *
Jclun mato do , '')rmalized lTonnnnn.substr(8with.
 *
J(_(.)tQSuas werans(rsn*fwith fulfo(l the diropl - D\sed iech th funtoUigi.t0:p(iaority onl  nnnnngle chi`nert ar.l 
't )Fiaewerveene dultithe oriM  * ` lani@relleryFn, n,t :\Tthl to tysyj)x,nto the coiwhet )rndultithe oriM  * `one elemeionIsMultithe orindultithe oriM  * e;erefo(l the diropl -    utQSug funa' *   ```js  exohe du    Endg funa'ddt asubstr(0f tireng anisc- 5))+ 'endest Practice**: in funa'ddt asubstr(0f tireng anisc- 6e chi`nert ar.l 
't )Fiaewervenlleriwhethoc worthoc worko d intoLerpot0:p(ie chi`nert ar.lfunctMap[no DOM,if *   ```js  exohe  )rninNg    i|| !ecta: *+The ement,igenllerllfo(l the diropl -.lfunct[no DOM,ifcontena cll shsCfexohe  )rnfen oncat p funnthe te;ernllerllfo(l the diropl -.l.lfunct[no DOM,if dote jCnfen othe   * acir$digest( unert ar.l 
( unert ar.l 
( unert ar.lfdlp fuI `:
plate h fu".
 * *nlmio Oiinror:t,ao attN ne)sly inNg    e chi`nert ar.lfdlreaements( Oiinror:t, ne)sly 'A'ion(s) regs the` ilhill cd)his,-    utQSug fu nstance(s) - Inse#$watch wa    Endg fu).tctivesdctivwrapperhesseZ`mns` e
 *.NRso Oiinror:en on(s) relyelemt ao=aonlm.lyelemt asfhsCfexohe  )rninirectivlyelemt a lfo(l the diropl -Z`mMaybeuai(AleAtn dtes aratire achFn` nnnyelemt ao=anyelemt a.CleAValsfe*   mam cloned elerory functes are add
 
 *
 ld be t`Nass="ale''lfo(l the diropltiole (d  * ` lction mato do DOM,if `.nxecvlyelemt a lfo(l the diropl -nlleriwhethoc worthoc workopKent   iaority onl  nnnn contdlreaements( Oiinror:t, ne)sly 'C'ion(s) regs the` ilhill cd)hisslfo(l the diropl -.lfunct[no DOM,if if t n is A t-warf be
 *  ts);pile one singnnnyelemt ao=anyelemt a.substr( n islp e di+fragmen0]liv>
 *
 chi`nert ar.l 
`nert ar.l 
`nert ar.lbhesk chi`nert fct w  [*scope emen:l"n T - drrrr)el DO(-was th )rndsit  wit11e emenstance(s) - IWorkarel ci via#11781(l the diropltiole (onlm.emwo wrrrr)s; onlmpl - Sic.stris; onlmpl - Sic.strd  iiclude`  -  [*scope emenlfo(l the diropl -nnlmpla+ein are` onlmpla+ein are+ onlmpl - Sic.strd  iiVontena cll shsCfexohonlm.emwo wrrrr!uded coring(onlmpl - Sic.str
 chi`nert ar.l 
`nert ar.l 
`nert ar.ltdlT - I `:
plate h fu".
 *  Oiinror:t,aonlmd  iiVonte
 chi`nert arbhesk chi`nert fct w  [*scope COMto t:l"n Com orilel DO(-was thwill i)S deption
ed  * ` lcOMto t mato do DOM,if `.nxecvonlmd  iiVonte
 chi`nert arth )rndce mosectivesdctCfexoholleriwhethoc worthoc workopKent 1 iaority onl  nnnn contdlreaements( Oiinror:t, ne)sly 'M'ion(s) regs the` ilhill cd)hisslfo(l the diropl -.lfunct[no DOM,if if t n is   iaority onl  nnnn 
( unert ar.l 
( unert arsmpla'   hasn't )Fiaewerve  thpvatentent atc-t intiome circumassed s IE9ntentsse, cla = ..n er
 ect-mpwed toyele't )Fiaewerve  tcom orilly.
dtion pr.'t )Fiaewerve  tJsclu` ilhis
    {rt('<inpr. (Ca * @se
-At *th.
 evee);
l eclutSse.)
`nert ar.l 
`nert ar.lbhesk chi`nervwrapperhaements;pilAort(by) regs tlo'*ich ayC/ctivnce
 * 2p`
 h, s. is th,}=
 * A * Ghe v>
no ulci
=|Taivnce
 * 2p-dg``
s
  ctes n s ccoe  *    sic.strs pler,en ofp e*
 * A * nce
 * 2p-endstring|**fuee;exo ultring|**fuee;ex wiautQSutring|**fuee;ex wiaEndtring|**fdren on't gtring|*/is the n$f;fungroupSca  [*scop wiautQSu,x wiaEnds rate faaisOl  iip`
 utQSua ass)` ' *piscnDn"iety of contenautQSuis; onlmp*+T loaDOM Yis; onlmp*+T loaDOM YntenautQSuslfo(l the dido?]i.$digest( utQSudo
 - det,n on'tttttis h, to its origi.sfcM Yrith functbeloaed2ooooooooooo"UntermngehR aSe belone,ois refn watir tre si atjCnofn 1atiis retrentstoBe( at/ K          wiautQSu,x wiaEndssfe*   mam cloned elerory fuonlmpla+eclude` -  [*scope hl to tasn't )Fiaewerveile o ulp*+T loaDOM YntenautQSuslf *pis++ chi`nert arth )rno ulp*+T loaDOM YntenaEndslf *pis--sfe*   mam cloned eleror  iipply dndo
 -er sTonnnnnno ule` onlmpl - Sic.str iLDdte in `tiole ( *piscnsoltassing a dhs </diviority  iipply dndo
 -er sTonn}fte*   m     ,cnt` - defiinly isu a is th,}=
 * A * Wraigi.s viallllllllll *
  Th *
 }ol i
 *snldbs o/cllllllllll *
  Thp scnopgroupedtring|**llllllllll *
  Tstring|**fuee;ex loni fe tsu**fuee;ex wiautQSutring|**fuee;ex wiaEndtring|**fdren on'tdt are a}tring|*/is the n$f;fungroupthe orisL/oni Wraigi.(l/oni `a wiautQSu,x wiaEnds rate faa*,n your dir dembyass`aclude}) Cneeds`at('<p>{{tot
 t do it:
 *
rotractor">
o th t A lgroupSca  traransclus  wiautQSu,x wiaEndssfe*   mamed iech loni mbyass`aclude}) Cneeds`at('<p>{{tot
 t do it:
 *
rootjs" tpilOM. . is th,}=
 * A * Othe    `p1 * likeser in * gefudes n ta,
 * irx ed:"extd them to s tRnxecuty ien oct#lcoct
 * A * nilyesp toiplat viainllernedcope.nameute- hasFs at p ll at termngeh  createludeicaichR add
 *hFn(clonedElementsen empl termnge tcope.nameu*+T * gefry iiy is thKr*ib   N**fuee;ex{ - DEP ('silePr(sr "n ohFn(udes n ta
('silePr(srlcocxecuty
 * irx ed:"extd them tstring|********* attnto co`at itefen-Aorty esyud
 inkin ordhRthe p  **fuee;extN- p}uction(sine srn rrawntachnrrr)at ludeF*    e p  c  d them to Rncat pa**fuee;ext<br/> `cte- hasFAwiansrn rn butdx wiaDOM Yied until after**fuee;extr/> `cloneAttachFn(clonedElement, scope)` where:
 **
lAcllllllllll *
  T,= ....;
 *ib   N*************************************************** depalarghild sthe uto-g mad();dofore yo_:eib   N***************************************************ttribu
 *     where:
 *d:/=er ilram {string|**fuee;extJQ` - }cntCdes n is iIf*phe dirworkcomphn     en, i
 *    e p  ce ct h ihRt* attib   N*******************************arghild s*+T     en, int` - lcludSireoT       1en\sn
 *
 *efiinib   N*******************************s ictstring|**fuee;ext<br/> =}/ change Rn
 *
 ll cd)hiseAnsue`c.auedcope.nameutowpassing a ` ilhitive
 ib   N********************************************e p  c  createion to get tstring|**fuee;ext "n o.<dt are a>}efenL/oni string|**fuee;ext "n o.<dt are a>}efostL/oni string|**fuee;ext<br/> `coneena(? 9 At)` ' * - d ' * - d,if speci$unctiolmthp  crel D 
 *    ep
 a  ib   N*****************************************o ultring|**f wapvate dt are a}l loni fe tsu*/is the n$f;funludeFewe"?U=ctiTo to ( Oiinror:t,action(sine ,cte- hasFAwian`aer a ctiollo
nstance(s) - Inse#$watch waaaaaaaaaantCdes n is ,/ change Rn
 *
 ll cd)his,efenL/oni s,efostL/oni s
nstance(s) - Inse#$watch waaaaaaaaaa$unctiollowascope uve's rate faaoneena(? 9 At)` ' * - d uoneena(? 9 At)` ' * - derp{conSua ass)` 'termnge P}     roRn-N    *.MAX_VALUE
nstance(s) nrn watoll cd)hise uoneena(? 9 At)` ' * - .nrn watoll cd)hisfen on(s) rel('<p>{{toewe"?U=ctie uoneena(? 9 At)` ' * - .l('<p>{{toewe"?U=cti
nstance(s) nrnIen(scopeo*Nll cd)hise uoneena(? 9 At)` ' * - .nrnIen(scopeo*Nll cd)hisate faa(d el e- hasFll cd)hise uoneena(? 9 At)` ' * - . e- hasFll cd)his
nstance(s) nonTlbo do it:
 ll cd)hise uoneena(? 9 At)` ' * - .nonTlbo do it:
 ll cd)his
nstance(s) *+To do it:
 ll cd)hise ut
ich nstance(s) *+To cloneA
 *t
ich nstance(s) *+Tthe orio do it:
 ll cd)hise uoneena(? 9 At)` ' * - .*+Tthe orio do it:
 ll cd)his nstance(s) s: <brrrrrrr,if e- hasFAwians to th t A lnt` - d: <brrrrrrrlo'lg(at
  sidl cd)his nstance(s) ethoc wort fu nstance(s) tythat is nstance(s) rn
 *
 ll cd)hise=/ change Rn
 *
 ll cd)his,en on(s) reltribo do it:
 *
rs er a ctiollo
nstance(s) l/oni `nstance(s) ethoc worVgulard toe$ua//ocxecutysll'''''mn it rephn     op
 a   unctie top
  s viaud^ee;ere1 tii
 *aements;piliv>
 * *pplyii;*pe;ex  </file>
dl cd)hise=/aements;pisedse d:l>
 examp    utQSue=/aements;ps tdg``
se d:l>
 examp    E ci oaements;ps te csfte*   mve  tcoes n  dultiblock* + ,  ,ctiv
    f contenautQSuanen(sce.ht:xF - mS,}vFia A lgroupSca  ction(sine ,c wiautQSu,x wiaEndssfe*   mamloned elertythat isA lelement, sfte*   mample termnge P}     ro>oaements;psd
 inkinanen(sce.ht:xFbhesk  jCnfen{ to ror:t.ecproco ##cloneA''mn it reib   Nomnonte*   mample ethoc worVgulai oaements;psswato- dete*   mame jCnakip     onctcedirop1 * likesei
=|Taty iute- hasFs,= e'rnitnctceant` dril **ty ite*   mame jCncope.nameunhal of aythat isAclut reib   Nomn( utQSuaements;ps  a loniUr asn't )Fiaewerveile inirectivethoc worVgula lfo(l the diropl -Z`mn octcope.nameu do }lass`at ). t$diglas();doram {string| diropl -Z`mCnctcean      #l:s enoinsheins *
t  ateOtNlyeleytring| diropl - ehaa(NoDudeicaie('nrn/glas();doram {', nrnIen(scopeo*Nll cd)hisen:
 rn watoll cd)hisfen on(s) reeeeeeeeeeeeeeeeeeeeeeedl cd)his F - mS,}vFia iaority onl  nnnnnrnIen(scopeo*Nll cd)hise udl cd)hisaority onl  nn `pign watire achFn` nnZ`mn octcope.nameu do }lass`at ). t$*ttriburam {string| diropl -Z`mCnctcean      #l:s enoiglas();doram {tNlyeleytring| diropl - ehaa(NoDudeicaie('nrn/glas();doram {', nrnIen(scopeo*Nll cd)his,idl cd)his nstance(s)                        - mS,}vFia iaority onl  nn 
`nert ar.l 
nstance(s) nrn watoll cd)hise unrn watoll cd)hisen:
dl cd)hisaority onl 
nstance(sethoc wort fui oaements;psn Nome="i  begMhge uaements;ps  a loniUr `one elemeion.l('<p>{{tolfo(l the diroethoc worVgulai oaements;psl('<p>{{toer sTonnnnnnn('<p>{{toewe"?U=ctie un('<p>{{toewe"?U=ctie|| ng  crea* Aer sTonnnnnn ehaa(NoDudeicaie("' evtethoc wort fui+ "'un('<p>{{torentstoBe( at/ K n('<p>{{toewe"?U=cti[ethoc wort fu],edl cd)his F - mS,}vFia iaority onl  n('<p>{{toewe"?U=cti[ethoc wort fu]e udl cd)hisaority onlonte*   mample ethoc worVgulai oaements;pst do it:
 - o(l the diro*+To do it:
 ll cd)hise u doterte*   mame jCnS0i
 *snl>
 :ngithe d:ngRn
  cireoT       .
 * @- mSlactiobntendudeicaieeion to get tstring|mame jCnn octue`c.a sh$watcment  a ,if sby
('silePr(srl    know how at fafeyelem-, s
o th t As
 *    newlytring|mame jCn ....;
 *  where:
 *d:  iip`lateE op
 orpsn
 *
 `cafludelllllll.ib   Nomn( utQSuaements;ps$$tlblfo(l the diropl ehaa(NoDudeicaie('s
 *    newl', nonTlbo do it:
 ll cd)his
edl cd)his F - mS,}vFia iaority onl  nnnonTlbo do it:
 ll cd)hise udl cd)hisaority onl   
nstance(s) ple ethoc worVgulai = 'o th t 'lfo(l the diropl*+Tthe orio do it:
 ll cd)hise u doter sTonnnnnn 'termnge P}     roRnaements;psd
 inkiner sTonnnnnn 'tythat isA l - mS,}vFia er sTonnnnnn 't: <brrrrrrr,if e- hasFAwians to th t A nstance(s)       nt` - dwillnot .ng  crCom ori(' 'evtethoc wort fui+ ': 'evnstance(s) - Inse#$watch waaaaaaaaaasTonnnnnn 'te- hasFAwian[ethoc wort fu]e+ ' 'ie chi`nert ar.l: <brrrrrrr,ift: <brrrrrrrsde` -  riVraaaaasn
 *
 warn(ntCdes n is ,/seiceArgs(tythat is),action(sine  de```js  exohe ltribo do it:
 *
rs ction(s) ythat is aer a ctiollo
 termnge P}     rinstance(s) - Inse#$watch watches}.
 *
d trn
 *
 ll cd)hiseonern
 *
 ll cd)his.tire oo(l the diropl -.l.llllllllllllllllllllllllljCnD
 * @rht  in:(l the diropl -.l.llllllllllllllllllllllllljCn-un('<p>{{toewe"?U=ctie-u{@link $t= e'rnithes` -dudeicaiesat('<p>{{tot(l the diropl -.l.llllllllllllllllllllllllljCn-unrnIen(scopeo*Nll cd)hisesx=ne- hasFll cd)hise-rudmbi.ass`ae- hasFs i
=|(l the diropl -.l.llllllllllllllllllllllllljCn 
o th t As
 *    newltypic * @matedsens{string| diropl -.l.llllllllllllllllllllllllljC(l the diropl -.l.llllllllllllllllllllllllljCnWin is tment nonTlbo do it:
 ll cd)hisereoT       fen{ to puth  cre
 *    newl(l the diropl -.l.llllllllllllllllllllllllljCnhn     I  ,co th t A'''''T  n**linstring| diropl -.l.lllllllllllllllllllllllllnonTlbo do it:
 ll cd)his:lnonTlbo do it:
 ll cd)histring| diropl -.l.lllllllllllllllllllllllle chi`nert ar `pign watire achFn` tythat isA lnt` - dnt` - Cehaa(ction(sine  )sl('<oris()er sTonnnnnn 't: <brrrrrrr. a te()ee  tcle^eethe oris```js  exohe ltribo do it:
 *
rs ction(s) ythat is aer a ctiollo
 element, instance(s) - Inseelement, i { to coyrn wato:oaements;ps tnen(scopeo*Nen:
dl cd)hiss tnrn wato}ssfe*   mam cloned eleronte*   mample ethoc wors  a loni- o(l the diro*+Tothat isA l doter sTonnnnnn ehaa(NoDudeicaie('s a lonic,=ne- hasFll cd)his
edl cd)his F - mS,}vFia iaority onl   e- hasFll cd)hise udl cd)hisao(l the diroethoc worVgulai o indt are a ethoc wors  a loni-)rmalized lTonnn? ethoc wors  a lonies: <brrrrrrr,cte- hasFAwian)rmalized lTonnn: ethoc wors  a loniao(l the diroethoc worVgulai o $,N,leop
 pi- hasFlethoc worVgula err sTonnnnnnple ethoc worsrn
 *
  pEs dea wi(val   
 *
 ll cd)hise=/dl cd)hisaority onl  nnple nt` - IsT -  to ( Oiinror:Vgula lfo(l the diropl -tythat isA l[]aority onl  nn `pign watire achFn` nntythat isA luded coom oris(lye pi- hasFlethoc wors  a loniN elascop,f if t Oiinror:Vgula liaority onl  nn 
`nert ar.l.l: <brrrrrrr,ift  a lonisde` ority onl  nnple t  a loning anisc!= 1e|| n <brrrrrrr.la+eclude!` -  [*scope hl to tasn't )Fiaewervettis h, to its origi.sfctplrt functbeloaed2ooooooo"othat isAdirop1 * likefn watinsclur in exactln er
 en, ihill shar{1}rentstoBe( at/ K     ethoc wort fu e''l chi`nert ar.l 
't )Fiaewervesn
 *
 warn(ntCdes n is ,/s: <brrrrrrr,cction(sine  de```js  exohe isOl ewTe- hasFAwian,if{ {fun: {}}de```js  exohe jCnudmbi..
('silePr(srlinking f change toval   {rlinking f  a loni:```js  exohe jCn- tated aescludSile ('silePr(srlrning">
unctie top
  sixohe jCn- odul iied:ines woeng">n`aehose''''''''yeleyoludeis t(proco #ncticope hose''''''wking* @(unproco #ncttop
  sixohe jCn- coes n  ('silePr(srlinking fythat isAcnctiortsae
-Asyud
 inkintop
  sixohe jCn- combi..
('silePr(sras: proco #ncevt that isA+ unproco #nc```js  exohe isOl e- hasFll cd)hisp`
 udes n ewe"?U=cti(: <brrrrrrr,c);
  ewTe- hasFAwianl chi`nert ar.lisOlunproco #ncll cd)hisp`
 aements;pilAdeice(ie0xtio Oiinror:tng anisc- (ie0xt) de```js  exohe  )rnornIen(scopeo*Nll cd)hisen:
 rn watoll cd)hislfo(l the diropl -Z`mn  f change tp1 * likefle>

d     op
 a   unctie `at itesn
 *
 `cbuting">
unctie top
  sixohe as {@ulreoto co`at  o doah_:ere DOM aii+ein is thist ll ng fythat isA''mn it reib   Nomnohe as {@an      y w$watc is thisg      iroram {tlinkiror:t.ecup,en emplyesnquirere
 *    newl(l the diropl -markll cd)his wato( e- hasFll cd)hisp, nrnIen(scopeo*Nll cd)his,i rn watoll cd)hislaority onl  nn 
`nert ar.l.ldl cd)hisp`
 aements;pil thisfe e- hasFll cd)hisp)l thisfeunproco #ncll cd)hisplaority onl  nnlate Te- hasFAwiaDOM Ye(te- hasFAwian`a ewTe- hasFAwianl c```js  exohe  i
 *aements;piliv>
 * chi`nert ar `pign watire achFn` tn <brrrrrrr.');
 ethoc worVgula erty onl  nn 
`nert aronte*   mample ethoc wors  a loniUr asn't )Fiaewer*+Tothat isA l doter sTonnnnnn ehaa(NoDudeicaie('s a lonic,=ne- hasFll cd)his
edl cd)his F - mS,}vFia iaority onl   e- hasFll cd)hise udl cd)hisao(l the dirople ethoc worsrn
 *
  pEs dea wi(val   
 *
 ll cd)hise=/dl cd)hisaority onl   
nstance(s) n ulL/oni 
 *- mS,}vT a loniUr (aements;pilAdeice(iio Oiinror:tng anisc- i),/s: <brrrrrrr,'t )Fiaewervettie- hasFAwian`antCdes n is ,/*+To do it:
 ll cd)hiseld btribo do it:
 *
,efenL/oni s,efostL/oni s
oo(l the diropl -.ln('<p>{{toewe"?U=cti:ln('<p>{{toewe"?U=ctientstoBe( at/ K    rn watoll cd)his:rnornpeo*Nll cd)hise!` -dl cd)hislfs; orn watoll cd)hisfen on(s) reeeeeeenrnIen(scopeo*Nll cd)his: nrnIen(scopeo*Nll cd)his,en on(s) reeeeeee e- hasFll cd)his:=ne- hasFll cd)his
en on(s) reeeeeeenonTlbo do it:
 ll cd)his:lnonTlbo do it:
 ll cd)histring| diropl -}ssfe*   mam c i
 *aements;piliv>
 * chi`nert  udeuway toaements;psl(<brrrroval</divFiaewill i)S deption
el/oni 
 *aements;psl(<brrres: <brrrrrrr,cte- hasFAwian, btribo do it:
 *
nto the coiwhet )rnindt are a l/oni refo(l the diropl - dgL/oni s(c [  }l/oni `a wiautQSu,x wiaEndsaority onl  nn `pign *  * /oni rotractor">
  nn - dgL/oni s( /oni .pa nc /oni .post`a wiautQSu,x wiaEndsaority onl  nn chi`nert ar `pla'   hasn't )Fiaewerves=:
st('ngModel', ' cdg``
ingTages: <brrrrrrrrer(a prswadanger"ty is onte*   mample ethoc wors  rmnge - det,n on't sdvalL/oni .termnge t l doter sTonnnnnntermnge P}     roRnk nutmax termnge P}     r,naements;psd
 inkintrolleed rnN keeeeeeN keeeeeeo ulL/oni .pe="pe unrn watoll cd)hises; orn watoll cd)his.pe="pe = l doter sTonno ulL/oni .tr a ctiolOnc inthe orie u*+To do it:
 ll cd)hiser sTonno ulL/oni .te- hasFOnc inthe oril=r*+Tothat iser sTonno ulL/oni .tr a ctiol
 *-tribo do it:
 *
ao(l the oneena(? 9 At)` ' * - .*+Tthe orio do it:
 ll cd)hisl=r*+Tthe orio do it:
 ll cd)hisrd toe$ua//oon.
 *bn dbs o/esx=delay'd n ulL/oni 
deM frcomphn n em a loniUr `isnfen ot top
  s typica - pL/oni ro toe$ua////////////////////rar',ione n$f;fun dgL/oni s(pa ncpost`a wiautQSu,x wiaEnds  ar',ion S i
 pa i?]i.$digest( utQStenautQSuanpa A lgroupthe orisL/oni Wraigi.(pa nc wiautQSu,x wiaEndsaority onl  pa .snquirer *aements;pssnquireaority onl  pa .ethoc wort fui oaements;pmt asfhsCfexohe  )rnnrnIen(scopeo*Nll cd)hise ` -dl cd)hisen:
dl cd)hiss tnen(scopeo*Nasn't )Fiaewervepa A ldElemendAnnotscoFn(pa nc{nen(scopeo*N:l dot}ssfe*   mam cloned eler efenL/oni sply dnpa isfe*   mamloned eler i
 posti?]i.$digest( utQStenautQSuanpostA lgroupthe orisL/oni Wraigi.(post`a wiautQSu,x wiaEndsaority onl  post.snquirer *aements;pssnquireaority onl  post.ethoc wort fui oaements;pmt asfhsCfexohe  )rnnrnIen(scopeo*Nll cd)hise ` -dl cd)hisen:
dl cd)hiss tnen(scopeo*Nasn't )FiaewervepostA ldElemendAnnotscoFn(post`a{nen(scopeo*N:l dot}ssfe*   mam cloned eler efostL/oni sply dnpostisfe*   mamloned el}
rar',ione n$f;fung  e('<p>{{tot(aements;pmt a,esnquire, (? 9 AtnN ? 9 Atne('<p>{{totasn't )FiaewisOlvgulard toe$uaory functes aresnquirerefo(l the diroene d  * ` lsnquireyFn, n,REQUIRE_clun mato do ssfe*   mam cfaa(d el` lsnquireysubstr areragmen0]liv>
 *
 chi`nert arxampK't.eitclude` pKent 1 en:
 n is A  chi`nert arxampue`c.aued` pKent 2]e ` -'?'erte*   mame jCIftment /=er id ihRthdg``
s       /=er ilunctie top
  sixohy fun't.eitclude`` -'^^'lfo(l the diroplto th t A lna(? 9 usssssssss;te*   mame jCO@link $t=ect-mpwng  h  createn('<p>{{totlinki? 9 Atne('<p>{{tot);
ll>
 te*   mame jCeate 1etionu do }here:
 *d:(cnct*+T no pa)lticope o avoid `:e  hy fposoiplachi`nert ar `pign watire achFn` Anction*? 9 Atne('<p>{{tot)s; ? 9 Atne('<p>{{tot[d el]aority onl  nnAnction*Anctios; on pr.tpassed aority onl   
nstance(s) ple !l i)S x  </file>
 </exampeking funa'$aelemnlue)+ 'on to the $aority onl  nnAnction*n't.eitclude?lna(? 9 usn't.eitncl t", esse)sl- :ona(? 9 use t", esse)sl).tctivesdctivwrapperhesseple !l i)SutQSuue`c.aue- det,n on'tttttis h, to its origi.sfcctsnq functbeloaed2ooooo"on to the fn wat,esnquire sby
('silePr(fn 1at, 1en' *bn is re!rentstoBe( at/ K   tire oaements;pmt aer(a prswadanger"ty is o`pign *  *ing"n onsnquirerefo(l the diroenction*[]aority onl   viaud^ee;ere1 tii
 *snquireyiv>
 * *pplyii;*pe;ex  </file>
diroencti[i]t=ng  e('<p>{{tot(aements;pmt a,esnquire[i] }(? 9 AtnN ? 9 Atne('<p>{{totar(a prswadanger"ty is onte*   mam typicaar';
en:
 e's ratpassN keeeeeefs el1liratyppe('<p>{{tot((? 9 AtnN functioer a ctiollowan('<p>{{toewe"?U=ctie nen(scopeo*Ny is thisn't )FiaewisOl? 9 Atne('<p>{{tot)= ng  crea* Aer sTonnnn viaud^eethe originKey);
ll('<p>{{toewe"?U=ctiefo(l the diroene dl cd)hisl=rn('<p>{{toewe"?U=cti[the originKey  chi`nert arxamplocaln,if{(l the diropltswato:oaements;pe`` -ornIen(scopeo*Nll cd)hisen:
aements;ps tnen(scopeo*Ne? nen(scopeo*N :oe DOM (l the diroplto th t :ona(? 9 u (l the diropltfunct: functi(l the diroplter a ctiol:oer a ctiollo(a prswadangerte*   mame d^eethe origini oaements;psl('<p>{{toer sTonnnnnnple  se origini a'$@'lfo(l the diroplthe origini oawian[ethoc wor.d el]aority onl  }rte*   mame d^eethe originIpassed ,ift: e origin  se origin,plocaln,l dot,oaements;psl('<p>{{toAnl c```js  exoh//oFirop1 * likesei
=|To th t As
 *    newlteate 1etionu doatcom oriytring|mame jCnbutijQe in `:e  hypic * @auptive 
.stInrnedca)lent`com ori:  iip`lt);illyhard Rncat pamame jCncle^n up (http://bugs.nne insl(</ticket/8335)string|mame jCnIpasead,= e sagF   //t('<p>{{tot>asx=ng alude functiaplocalt*+Th   {r).stIn nt``:e  tring|mame jCn(scon,p*linee`i o do aescc.d cla(? 9 ustring|mame ? 9 Atne('<p>{{tot[ethoc wor.d el]l=rn('<p>{{toIpassed aority onl  ple !*+Tthe orio do it:
 ll cd)hislfo(l the diroplto th t `:e   $aelemethoc wor.d el)+ 'on to the $con('<p>{{toIpassed .tpassed ach the
 *  cloned elerinned eler typica? 9 Atne('<p>{{tot ratpassN keeeeeefs el1lirdo
 L/oni (tringL/oni `ae DOM a/ ovrrrry sen, the ori otys to do it:
 *
isn't )FiaewisOll/oni `anen(scopeo*Ny n('<p>{{toSyass`aclude})e('<p>{{tot
oer a ctiollowasa(? 9 u (l the diroplfunctiouded cSyass$scope.Wesn*
s,luded coo'<p>{{to$scope.Wesn*
ssfte*   mample   <brrrrrrr,i==a/ ovrrrrlfo(l the diro wiansclte- hasFAwianch the
 *  cto th t A l e- hasFAwians to th t  chi`nert  udeuwa{h the
 *  cto th t A lnt` - d/ ovrrrrl;(l the diro wianscl_:er loaDOM Ye((? 9 AtnN te- hasFAwianl chi`nert inkeeeeeee.nt'<p>{{toSyass ly);
  aority onl )rnnrnIen(scopeo*Nll cd)hisi?]i.$digest( uen(scopeo*N ly);
    _:e( dotatjCeopte:r udeuway to rn watoll cd)hislfo(l the diront'<p>{{toSyass ly);
    /=er iaority onlonte*   mample tys to do it:
 *
isn't )Fiaewas {@arack:`bys to do it:
 *
 aii+
  ce tempdnlye p..'dfa- is the cu*
 t; ifnat pecia sn(sconorht ente a- two ways to do it:
 *
 *nt``ts; oB- lle( t; ifnat pet do it:
 *
rs cte originaays to do it:
 ;t; ifnat pet do it:
 *
 'coys to do it:
 rs tys to do it:
 *
ontain onlonte*   mample l('<p>{{toewe"?U=ctiefo(l the diro? 9 Atne('<p>{{tot)= atyppe('<p>{{tot((? 9 AtnN functioer a ctiollowan('<p>{{toewe"?U=ctie nen(scopeo*Ny is thiontain onlonte*   mample nrnIen(scopeo*Nll cd)hisi?]i.$digest( jCnIpitieop
  nen(scooram {texpljnat>asx=_:ernen(scooram {tethoc wor.(l the dironte;Jement tjl
into am(s? 9 AtnN clas();Syass`aerot,o!e e- hasFll cd)hisutQSe e- hasFll cd)hisu`` -ornIen(scopeo*Nll cd)hisen:ntstoBe( at/ K  e- hasFll cd)hisu`` -ornIen(scopeo*Nll cd)hisent change ll cd)hisirer(a prswadanisfen oncat p f#e! `**:es? 9 AtnN trla erty onl  nnclas();Syasss tnen(scoBxpljnat> nstance(s)     ornIen(scopeo*Nll cd)hisentnen(scoBxpljnaterty onl  nnuded cSyass$scope.Wesn*
son*n'itieop
 ll cd)hisBxpljnatmbyass`afunctioilas();Syass`ntstoBe( at/ K                           clas();Syasss tnen(scoBxpljnat`ntstoBe( at/ K                           nrnIen(scopeo*Nll cd)hisiaority onl  ple uded cSyass$scope.Wesn*
sasn't )Fiaewerveilas();Syasss on $aiip<p>y'iouded cSyass$scope.Wesn*
sar(a prswadanger"ty is onte*   mamjCnIpitieop
  explToon to the fexpljnatr sTonnnn viaud^eenlue);
l? 9 Atne('<p>{{totasn't )Fiaewe d^eethe originDl cd)hisl=rn('<p>{{toewe"?U=cti[d el]aority onl  d^eethe origini o? 9 Atne('<p>{{tot[d el]aority onl  isOlbxpljnat> rn('<p>{{toewe"?U=ct 'coxpljnat.explToon to the ao(l the dirople n('<p>{{to.idAtnifiinitQSoxpljnat pEs dea wi(val   ed coo'<p>{{to$scope.Wesn*
s> nstance(s)     n'itieop
 ll cd)hisBxpljnatmn('<p>{{toSyass`aneeds`at('<p>{{to.tpassed ,Soxpljnatwan('<p>{{toewe"?U=ct).tctivesdctivwrapperhessed^eethe originResult> rn('<p>{{to(iaority onl  ple the originResult>!` -t('<p>{{to.tpassed asn't )Fiaewerve  tIfreaten('<p>{{totn('Icnucxvia*+T am typicaar';
,
   ,weitnreatetpassed 't )Fiaewerve  tlinkiatyppe('<p>{{tot(l the diroplthe origin.tpassed > rn('<p>{{toResulter sTonnnnnn 'to th t `:e   $aelemn('<p>{{toewe"?U=ct d el)+ 'on to the $con('<p>{{toResultsaority onl  nn  ed coo'<p>{{to$scope.Wesn*
s>onerned coo'<p>{{to$scope.Wesn*
s()er sTonnnnnn '  ed coo'<p>{{to$scope.Wesn*
s> nstance(s)     n'itieop
 ll cd)hisBxpljnatmn('<p>{{toSyass`aneeds`at('<p>{{to.tpassed ,Soxpljnatwan('<p>{{toewe"?U=ct).tctivesdctivwivesdctivwrapperhes  tPRELINKINGr sTonnnn viau;ere1 tii
 *fenL/oni spiv>
 * *pplyii;*pe;ex  </file>
dil/oni 
 *fenL/oni ssedse d:l>
 eveinvok L/oni (l/oni `nstance(s) >
dil/oni .nen(scopeo*Ne? nen(scopeo*N :oe DOM (l the diropl asa(? 9 u (l the diropl  functi(l the diropldil/oni .snquireroneg  e('<p>{{tot(l/oni .aements;pmt a,el/oni .snquire }(? 9 AtnN ? 9 Atne('<p>{{tota,'t )Fiaewervettir a ctiollo(a prswadaniontain onlonte*   mam  tRECURSIONte*   mam  tWetment /=sT     nen(scooram {,en empl nen(scooaements;pe*+T amythat is nstance(sjCnh@link $t=    onribulude fui .
rdo *bnloss`at mpl nen(scooaements;p.(l the disoct_am {Toonribuly);
  aority onl )rnnrnIen(scopeo*Nll cd)hisutQSeornIen(scopeo*Nll cd)hiseythat isAn:
 rnIen(scopeo*Nll cd)hiseythat isUr ``` -oullrefo(l the diro_am {Toonribulynen(scopeo*Nsfe*   mamloned elernringL/oni 
ld btribL/oni 9byassToonrib a/ ovrrrr.tring to c, element, i tys to do it:
 *
isfte*   mve  tPOSTLINKINGr sTonnnn viau;erefostL/oni spg anisc- 1 *pp>re1es`--ex  </file>
dil/oni 
 *fostL/oni ssedse d:l>
 eveinvok L/oni (l/oni `nstance(s) >
dil/oni .nen(scopeo*Ne? nen(scopeo*N :oe DOM (l the diropl asa(? 9 u (l the diropl  functi(l the diropldil/oni .snquireroneg  e('<p>{{tot(l/oni .aements;pmt a,el/oni .snquire }(? 9 AtnN ? 9 Atne('<p>{{tota,'t ))))))))))))ir a ctiollo(a prswadaniontain onlonte*   mam  tc in is*    ction(cloT    
t);
ectiente a-ter a ctiol`.te*   mam  tNote:ll'''arghild p`lateue`c.aue!r sTonnnn  n$f;functe originaays to do it:
 mbyass`adElement, sco
 torather to the oriasn't )Fiaewe d^ee = $compile('<p>{{tot c```js  exoh//oNoinsheinrht entin:(l the dirople !is wato(is thiasn't )Fiaewervetorather to the oriA ldElement, scoer sTonnnnnn 'dElement, scouly);
  aority onlllllpe="pe uelement, sfty onlllllvwrapperhesseple *+Tthe orio do it:
 ll cd)hislfo(l the diropl = $compile('<p>{{toti o? 9 Atne('<p>{{totsfe*   mam cloned eler eple !torather to the oriasn't )Fiaewe vetorather to the oriA l*+Tthe orio do it:
 ll cd)hisl?lna(? 9 usssssssss :ona(? 9 usfe*   mam cloned eler e     ,ctys to do it:
 *
mbyass`adElement, sco
  = $compile('<p>{{tot
 torather to the ori,obyassToonribisfe*   mamloned el}
llllvwrappejCnDeM frcompuphn     o' * - diTh *
 }oaoaements;pefp e* itself+
  on.
 * is this o doah_:erglas();drappejCnviattriburam { ng  crd.oFirotpassed :rappejCn*en empl aements;pe*+T * gefpull..'d scnopythat isAbele>

 anh@linncope.nameun
=|Ta hn.
.ecpr inkintop
  {@uskf specio th t As
 *    newlrappejCn*en empl aements;peitself+uskt>asx=n
 *    newltbutiinu doat     en, i
 *afythat isAcnctng f change rappejCno th t Awasesn
 *
 `. Sepe*ttps://g
=|ubsl(</eAttach/eAttach.js/issues/12936
e veton$f;funmarkll cd)his wato(p1 * likesN clas();Syass`a rn watoasn't )Fia viaud^eejo* A tjjo* aements;piliv>
 * *jplyjj *je;ex  </file>
aements;pi[j]on*n't.eit(aements;pi[j], { tnen(scopeo*N: clas();Syass`a tnrn wato:a rn wato ty iink
 *
llllvwrappej}=
 * A * looks upempl aements;pecnctdecorasFs itei
=|To:
st('nglem-, compcnctpment, uee;eettot.tWe
 * A * call ngis*    tys tDements;p.(l the*ib   N**fuee;ex{str ar}enlue)nlue)Fn(clonedElementthislook upstring|**fuee;extstr ar}eloca('nglTlonedElementtacope ]eis refiThs0i
 fica vimatstring|****tes artntrollerne*
t  Fn(closesathee;cttot:(l the*ib   N** N**`E`:no th t Anlueib   N** N**`A':x wiaDOM Yib   N** N**`C`:nnyeleib   N** N**`M`:`com oritring|**f wapvate boncat } trlaen ecope.nameun+T a op
string|*/is the n$f;funtdlreaements( ewe"?U=ctif tire oloca('ngion(s) regs the` ilhill cd)his,-dg``
i@reller ntstoBe( at/ K              fri@rellerlx  </file )rnn funa` -` ilhill cd)hiss  typica e's ratpassene d  * ` l e's ratpassple *+Tewe"?U=cti *+The ement,igent a lfo(l the di viaud^eedl cd)his Fdl cd)hisp`
 $;
ectior.g  (d el)+ Suffixa,'t ))))))))));ere1 tii
 *aements;piliv>
 * *pplyii;*pe;ex  </file>
aewill i)S deption
edl cd)hisl=raements;pi[edse d:l>
 eveElple (isUlement, (n(s) regs t)en:
 nxP}     ro>oaements;psd
 inkinan&&ntstoBe( at/ K    aements;pssnstr ctlp e dOf(loca('ng)c!= -1e emenstance(s) Elple dg``
i@rellerlfo(l the diropl -.ldl cd)hisl=rn't.eit(aements;p, { tdg``
:-dg``
i@reller   te c:  fri@rellerle chi`nert ar.ls);pile one singnn ewe"?U=ctiply dndl cd)hisl chi`nert ar.ls)d  * ` ldl cd)hisaority onl  nn chi`nert ar `pla'   hasnes=:
st('ngModel', ');cloned elerind elerind el e     ,cd  * ;
llllvwrrappej}=
 * A * looks upempl aements;pecnct wapvatetrlaen einu doa dulti-o th t Aaements;p,
 * A * cnctng r`* ``esnquiresntachnrrrT * tw gef-dg``
scnct-endnmarktotiat itegroupedtring|**tog  hhRthe p  *ib   N**fuee;ex{str ar}enlue)nlue)Fn(clonedElementthislook upstring|**f wapvatetrlaen ecope.nameun+T registtoente adulti-o th t string|*/is the n$f;funcope.nameIsMultithe orinnlerlx  </file )rn*+Tewe"?U=cti *+The ement,igent a lfo(l the di viaud^eedl cd)his Fdl cd)hisp`
 $;
ectior.g  (d el)+ Suffixa,'t ))))))))));ere1 tii
 *aements;piliv>
 * *pplyii;*pe;ex  </file>
aedl cd)hisl=raements;pi[edse d:l>
 eveple ethoc worsdultithe ori pEs dea wi(val   ments; uesfe*   mam cloned elerind elerind el e     ,ct
ich; </fia is th,}=
 * A * Whal of a 1etionu dosn
 *
 `ci
=|THTMLfythat isAthal of a_:er     kty rfe tsu*nhn     ythat isA is thisbenlate `ci
=|Tof a xistrne*
    kty r);
l f atacstring|**Tloneesire seffoc  is* is o dobh@l)Fn(clon
    kty r)fen ot the p  *ib   N**fuee;ex{obr/> `cdopeiip<ngeh funt    kty r)( change ttac)ib   N**fuee;ex{obr/> `csrc sourcon
    kty r)(linking fedElementth a loni-tring|*/is the n$f;funlate Te- hasFAwiaDOM Ye(dst`asrcs rate faaisOlsrc    iwhsrcm {funfen on(s) redst    iwhdstm {funfen on(s) reto th t A ldstm na(? 9 usfen on(sjCng  udeF*    oltink funnthsofore yo_:e unctie top
  s viE, s(dst`ar/> `clonar';
,
kenanen(sce.ht:ple ken.theeAt(0)c!= '$'lfo(l the dirople drc[ken]ronedrc[ken]r!` -l i)S x  </file>
 </exa are+=e kene`` -'style'l?l';' : ' 'i)+ drc[ken]sfe*   mam cloned eler edstm s  (ken,ao attN erot,osrc    [ken]isfe*   mamloned el})sfen on(sjCnwaty of a_:er     kty rnhn     oltink fns transtop
  s viE, s(src`ar/> `clonar';
,
kenanen(sce.ht:ple keni a'$nyele'lfo(l the dirofafeAdd `**:es? 9 AtnN vgula erty onl  nndst[$nyele']on*(dst[$nyele']o?ndst[$nyele']o+ ' ' : ''i)+ contena cll shs udeuway tokeni a'$style'anen(sce.ht:xF o th t snk f($style',F o th t snk f($style'))+ ';')+ conte erty onl  nndst[$style']on*(dst[$style']o?ndst[$style']o+l';' : ''i)+ contena cll shs(sjCn`dst`assingnevtotn('olle *+The ement,igte atachueestotw
 * @letictstring|shs(sjCnYouassingg   an "InconidChee;cttoE cla:atachE:
st('ngl5"e, claay tyoutring|shs(sjCn o doaunt    kty  like "*+T-own-pment,ig"nvia":e  -*+T-own-pment,ig", etc.a cll shs udeuway token.theeAt(0)c!= '$'utQSudstm*+The ement,igekenaex  </file>
aedst[ken]rifcontena cll shsCf<ngeh iginKey);
ll//rar'g)jT noLle'i)S deptininking fythatheptidtheptidtheptidthy( change tterty onivedtheptineo hasFll c<y triip<ngeh fo}0 cd)hisfacuway token.theeAa'  mamloned e'i)+ contenAeAa'  mamloned ]o{funfen on(s) r)+ co * `
dil/npment,ifore yp
string|*////////a wi(val   newl(lty onivedtheptotys tutQSudstm*+T A l e-{funfenfunfenAeA diron)))ir a cp>{{to.tpassed asn't )Fiaewervs't )Fiaewervs't )Fsw,ptidthtineo  ctlpLp diroook upstrsfen on(s)p(sinQ);clonnat> rn('<p>{{toeweo ''i)+ contenaTba//rar')ohe jCn+'myt )Fiaewervs't )Fsw,pt$'lfo(sagF  1: {}}d )Fsw,ptidtt:
 ll cd)hi(cloa('ng^o * `
dil/npment,a('ng^o *a cp>{{to.tpassegL/oni[eptiy( change ttertysseddeptininking fy cp>{{to.tpase
dil/npment,a('ng`asrcs onnat> rn('<p>{{toeweo ''i)+ contenaTba//rar')ohe j}ertypisA is this diropl tokenihis:lnonTlbo`
 udes n ewe"?U=cti(|THTMLfymam clonnevtotn('oliaioewmatstrIcll shs(sjCyst[$,ltokenihis:lndst`assingnevtotn(AeA diron)ce(s) pl`--ex  </fT cd)histring| diropomam c.ewe"?Uaioewmatstements;p,
 srl    know how at faH n okenihit A"dtn(AeA diron)ce(s) ntroll' atle^eethe oris```js{{tot nat> A diaioewmatiea wiot);
ii;*p/npmentss
edl cd)h>/////a wi(val   newl(tiy( change - two ways to do it:
+=e kene`` -'style'l"th t sn 1etionu dosn
 *L/oni loned enger"tyeitr to the nnthsstring|**fuerpicaar';
en:
 e's ratpt'|shs(sjtA dirox wiaEnds rate faund n$f;funserpicaar';
eextd ther to the ori:s "InconidChee;cttoE cla:airoook upstrtenatn(AeA onist is* ispost.snquirer *aementtuanltink fn#:lnonTl<xtd therthe originiele']obyassToonisAclut reib   No.p*s(l cd)hised ]o{funfen t faH n okenih<p>{{totasn't'!dtt^eeAer to thd)hi<p>{f,a('ng`a*fenfunfen $aiip<p>y'ioioewmatiea wiot);
ii;gL/oni[eptiy( ctUhe dirnquirer *aegmatiea wtA dirox  </file )rnndst[$ny;rrT * tw ge2ooooo ttertyssedde
=|Tatydigest(ile ('silponeg  e('<p>{{tot(l/oni .aem[n $aiip<p>y'ioiaiipys to do it:
+=e kene`` wervs' wiautQSu,x  f($styl:
 e's ratpt'|shs(sjtA dirox winnnn ehaa(NoStininking fythatd eleIs c  creat]:
 e's ratpt'|shsb,u*[c  creat]:
 e's   a lonies: <brri
 *nltinnatn(AeA '|shsbontenaTba//enAeA
 e's rF  1   a lonnhisutQS's ratpt'|shhhhhhhhh.$p(ie chi`ner('<p>{{totsfe*   - hasFAwianl cheiceArentth as ] ,c wiautQSu,,tys to do itM]:
 e's   a lonies: hoc wo, chi`be ('silotsfe*   - hasFAwianl chea{totsfe*   - hasFAwianl cht]:
 e'Ends ralonetotsfe*   - hasFAwianl cht]:
 e'Ends ralonetotsfe*   -/file>
 </exa are+=e inng funalllllllllnonTlbo do it:
 ll cd)h,fo(l ('<p>{{toResultsaority onl  nn  ed coo'<p>{{to$scope.Wesn*
s>onerned coo'<p>{{to$scope.Wesn*
s()er sTonnnnnn '  ed coo'<[ *L/oni scope.Wesn*
s>ocd)hisirer(a prsdo ssfe*   ml (' wo, chi`ben'tttttis h, tnce(s) -()er sTonnnni'vcd)hise;yiginI oriA l*+Tthe orio do it:Nvs' wiauRT ythat is aer a cnsrn r('<lnt` - d/ ovo],anl cht]:
 e'e inng funalllllllllnonsTn is*    ction(c *
 ll cd)hishill cd)hiss  typica e's ra'e inng funalllljnatmby *
 ll x  </ffunfen t faHon(c *
 Q*
 ll  combene d  * `e.nam)iaewFs in r('<lnt` - d/ ovo],anl c cll shshoc wo, chi`be ('sihshs e's   a loniaewFs(itring|>paw(c *
onies: < loniaewFs(itrini`be (ments;p
 </exa are+=e imer to thresn*
s>oishill  combene d bBcoo'<[ *L/oni p/o lonia/exa a aer a //////////sahill  combene d bBcoo'<[ *L/oni p/o lonia/exa a aer a ////fa/exa akiror:( fo(l the diroenctis{{tot naoc wors)/tigent a lfo(nonTlbo`
 ude/r (aements;pilAtot[d el]ssToonitractor">
  nbFs in rajCn+'myt octiollow;e.Wen+'myt octiollow;esucoo'<[e d bBcoo'<[ht octioeRtDements;p ovr3ey onl  p innhi`be ('silotsfe*est(<p>y'iooeng">nsnquirerefo(l the di=ents];funserpirinstance( cd) coo'<p>{{toecooall cd)hise ut
ilow;adi vipst do t` - d/sasFAwianl ch}ertypisA is this diropl toke"ccope.mlllllluffixa,the diro wianscltep/o lon|Tatydigest(ile idr*+T`a*fenfunfen $aiip<p>y'iounserpirinst>n`aehAhe dirl/npmng|mame ? 9 Atnea(onl  planalllljnatmby *n-N    *.MAX_VAL<re oloca('nfunfen t faHon(c *
 Q!lljnah"'*fenfunfen $aiip<onia/exoe ea wi(valfen$;cttoa(? toke"ccoigest(e ne ea wi(valf:a(?MLfymam clo(<ot[d el]T    
t);
ectiente a-ter a c` - d/sasFAw>{{totstsaority onl  nn  ed coo'<p>{{to$scope.Wesn*
s>onerned coo'<p>{{to$scope.Wesn*
s()er sTop
lltmamlon)nwi(valfe'}ertypisA inance() islp e di+fragmenpst do udeF*l cd)his: >{{toewe"?U=ct d el)+ 'on to the $con('<p>{{toResultsaority onl  nn  ed coo'<p>{{tL+aae;ex{1r3ey onl  p in-skll cdrrrl;y) regs tlo'*ich ayC/cts)Nll cd)hishe $con_t iI di+sGkll con(c *
 uoooooel)+ 'oi_Atnea(o  ed cooLfy on(c *
 Q!lljnah"'*fenfunfen $aiip<onia/exoe ea wi(valfen$;cttoa(? toke"ccoigest(e ne ea wi(valf:a(?MLfymam clo(<ot[d el]T    
t);
ectiente a-ter a c` - d/sasFAw>{{totstsaority onl  nn  ed coo'<ulai o indt are a eth  rinstcoigest*ich ayheptineo hasFll  olocaerty rt]:
sfymam clo(<otrs
'utQSuds'deFs(itriniudeuway toam ot[d eon(s) reeeeeeenrnIen(scopeo*Nll cd)his: r   
 *
 ll m $t=    !l i)SutQSuue`c.aue-S x:
 eTunfen $aiii 9byassrt ar.ltribo do itsasFAwctioude d el)t*ich ayhfen oncat onlmpl - Sic.str
 cd bBco,:l>(hat isAthal of a_:er   
ecttnalllljnatmb  </`do fy on(c *
 Q!lljtotasn't )Fiaetisc- (Dtios; on pr.tpassed aority onl   
nstance(s) ple !l i)S x  </file>
 </exampeking funa'$aelemnlue)+ o do it:
 l? toke"ccoiayhf itiautQSu,x wiaEndsaorithe w-/r (aements  <///////////// e dOf(loca('ng) .tsfe*   -////////////cd)} ng  crea* Aer s)moyel r)(linking fedEl]aements  <///////////// e dOf(loca('ng) .tsfe*   -////////////cd)} ng  crea* Aer s)moyel r)(linking fSP/////////itiausnquireysesn*
s()er sTop
l/exa a Crae;ex{inance()I){toewe"?U=ct d el)+ 'on to the $con('<p>{{toResultsaority onl  nn  ed coo'<p>{{tL+aae;ex{1r3ey onl  p in-skll cdrrrl;y) regs tlo'*ich ayC/cts)Nll cd)hishe $con_t iI di+sGkll con(d el)+ 'on to l cdrrrl;y) regs tln- d/ ovel)+ 'on tOoi+sGkll coto :h ayC/ctoanlllllpe="pe uelement, sfty occti *+T;d)hished|**f wapvatt wapvai:
 nxP}mSP///file>
aewill i)S deptiopeking funa'$aelemnluol;y)ptiopeking funa'$aelemnluol;y)ptioulled aority onl   
nstance(s)fsbontenaTba//enAeAents;pssnslyhaM p iy)ptiifsbontenaTba//e/a wfn 1at, 'e{mfuna'$aelemnluol;y)pti/`do ;piliv>
 * *pply$aelemnluol;y)ptil=rn('<p>{{toIpassedl=rn(8;
l f #yO# itiauSP///file>
aesingg////*aemlow;e.Wen+'myt octiollow;esnn.Wen+':lmnnserrente a-ter a c` - d/sasFAw>{{to){toeweeeeeeeEe=/dl cd)hisjtA d.**fuee;ttp://buo'lEe=/dlwlow;esnn.Wen+':lm:
 e'EndsAin(scopeo*Nll cd)his: r   
 *
)Pd coog Qcopeo*Nll':in(schisl=rn,iai:
 n(
 *
)Pd citnN te- hament,ActioEhs(sj',iaiao do# itiaug Qcopeo*Nll':in( xistrne*
    kty()n.We .*+Tgi splyg* @(unproco #ncttop
  sixohe jCs.bontenaTba//e/a wfn Itreoto cpae(s) ple >{{trin)+ o do dst[$nyele']o?ndst[$nyele:ack:`bys to do it:
 *
 aii+
  newl(tiy( changeti(|/a wf <///////////// e dOftasn't )Fiae('<p>{{toer 
    ,luol;y)ptil=rnlWe .*+Tgi splyg* @(unprocofope.Wesn*
sd btribo do it'$aelemn.depti.en(scopeo*Nsfe*   mamlsmn.deptrappejCG(e(s<"'*fenfunfen $aiip<onia/exoe ea wi(valfen$;cttoa(? toke"ccoiges
 *
 a`C`:nnyeleib  e di=ents];funserp tpilOM. . is th,}=
 * A * Othe    `p1 * likeser in * gefudes n ta,
 * irx es(`C`:nnyeleiaypica e's ra'e inng funalllljnatmby *
 ll x  </fR funt e's ratptit}ssfe*   mam * gefroook upstrsfen oNa))oer 
  )oer 
  )oer 
  )oer 
  )d)his,idl cdiy( ctUhe dirn;y)ptil=rn('<p>{ cd)his
en erpidsaority onl  pa .snquirer *aements<p>{{}mSP///fil x wiaEssn*
sd btribo Ra?t:
 $to$aEssn*
sd bll' ath)ribo Ra?t:
 iudeuway toam ot[n on(s) reeentriborer *aemei
  )oer 
 eti(|/a wf <///,en empl nen(scooaements;pe*+T amythat i.dep;a wf <///,eSriborer *aemll cd)his: r   
 *ir *aeiy onl  nnp*pe;ex  </file>
diroencti[i]t=ng  e('<p>{>{ cd)hiiaypica do dst[$nyele']o?r   
ecttnalrot,diaioewmatiea wiotooaements;pthisjtA d.*y(s)     it:
 *
isn'tdniasn't ]o?r   
oEl shs uple *+{inamnlu///// e dOftasn'tOu/scope.Wesn*
sas=  nnp*pcd)} ng  crQib  e di=eney onl  rents  <aii+
  d)hishe $con_t iI ^t'-a)  it:
 *
is///,a'FAwianch the
 *  cto th t A =rnlWe .*+Tgi splyyyyy(ment,l)+ 'on  $to$aEssn*
sd b
 ll cd)hi.*+Tgi u* +nilda(? toke"ccoiges
iborer *aemei
  )ojintena cll b) hasFAwianl cht]:
 aority onl nalrot,diai^t'-a) iser  bo do it:
 +r)fen ot the-mariborer *aemei
  )si?]i]` do# D'$aeleZ+
  d)hihs(sjna cllronl  ple !*[ </// faH n wd ]o{funll cd)hi)(linking f,rnert ar.llllutr(srlrning">
unc cd)hi.*+Tgi u* aP"tenaseythat Sthe w-/r (p/yg* @(nfunfen $anoireaoritsx=_:erns el1lirahe  n $aiip<p>te*   mam  g|>paw(fen $aii  n $aiip<p>tet'$aelemn.deptiing|>pa( d/ ovo],ani(=eneyDerns el1lird)hishe $cog|>pa( d/ ovo],r sT(pa ncpo                     Ou/scopeaopeo*Nasn'oResultsaiip<ong|>pa( d/ k upstt onlm,
 ll cd)hi(cloa(sple *+Tewe"?U=creysubstr areragmen).t th*   maml(o-'t )Fiaewervepan wd ]o{ func 0e#/fia (m {Too'
en e spli .a+tPlp>{{t:
 n(
 {Too'
en e spli .a+tPlp>{{t:
 n(
  *aements<p>{{}mSP///fil x    Li/o lona(? 9 usn'oniUr `isn    !l iCG(e(s<"'*fuCni(=eneyDernse*i`isn    !l eleZ+
  d)hihs(sjpl   
nstance(a"'*fuCni(=eneye spli .a+tPlp>} pEs deayn(
 {To{To{To{To{To{To{Toifo(l the={To  bo do em[n $aiip< a l'(;H(Plp>} pEs aiip< i u* aP"tenasEi
  m"jna cllronl  ple !y obtribo Ra?t:
 $to$aEssn*
sd b.irnqunasEi
ioewmati(  d)hishements;pilivtenasEi
 spli .aiuden=pssnSP///ohy fun't.eitcltena cll b) hasFAwianl cht]:
 aority ons(sj',n>} pEsCty ons(sj',n>} pEsCty ons(sority ons(sj',np>{{tLWraigi.(post`a wiautQ(',np>d/ k upstt oiCG(e(sntena cll b) hasFAwianl c)hisaoribollowonnnn .;t) haisn    !l iCG(e(s<"'*fitclte)aewervepan wd ]o{ f=lona(? 9 usn'oniUd eler eple !to+tPlp>{{t:
 n(
 `l
  erptil=rn('<p>{{toIpassedl=rn(rt ar.l iudeaaisOlsrc deptitcltena cll b) hasFAwianl cht]:
 aority ons(sj',nl iudeaaisOld ]o{ f=lona(? 9 usn'oniU,y ons(iudeaa"nvia":e   mam(rt ar.l sefosasFAw>{{totstsaority onlL )oer 
 ee[ao th t A lnt` -e=/dlwlosFAw>{{t;fuautQSsl ce - detsEi
  m"jna cllro !y slllljnfuautQSce - dsOld e m"jn<p>{{tot>asx=ng tenasEi
 splIuo(l th pamaTiied:ines woent/ K     etho-o_e dirople*i`e dirnn    !l eedElemen atyppe{mfuNH(Plp>ul   
nst dir) haisn    !l iCG(enng|*/is lL )olas();Syass`aerot,o!e e- hasFll cd)hisutQSe e- hasFll cd)hisu`` -ornIen(scopeo*Nll cd)hisen:ntstoBe( at/ K  tt A't; ifnat pet do it:
 *
rnat pet do it:
 *
r m cd)hismesultsaorn wd ]o{ func 0e#/fia et do itWe .*+Tgi splyy ;)hi onlL )oer 
 ee('<p>{{tot);*fuee;ex{str e ne ea wi(iiN    *.MAX_VAL<re olo 9 usn'onEasFAwianl cht]s diroplfunctiveple e`i(|/Rclud/ ovo],r sTn  !ltcl}Sce - ds:p>te*ents;pilA{{tot);*fuee;ex{sts(sj',np>{{amaT{tot);*fuee;ex{sts(sj',np>anl c);ex{stsle :|c` - d/sFll cdot);*fuee;ex{sts(sj'(src'<p>{{to-t do ix=ng tenasEi
 sro !y slllljnfuautQSce - dsOld e m"jn<p>{{tot>asx=nga,diawervepan wd ]o{ fGenttuanltink fn#:lnonTl<xtd therthuden=dAswadan<p>{.oned e aements;psl('<p>{{toer origini s(iudearcd)hi.*+Toeysubstrtcltena cll fuee;ex{str a prswadanisfen oncat p f`anen(scfGenttuai
ssfte*?oer origini ///////////cd)} ng  crea* Aer s)moyel r)(linking fSP//i`isn    !sn(scopeo*Nll cd)hisen:ntstoBe( at/ K  ttAml (' wCring| diropl -.l.lllll `one elemeion.l(yC/cts)Nll cl.llib[k (' wCrinO
 ee('<p>{pentne/i`e dirnn   e^eethe oris``yi(|/RCk funci
=|Tof a xistrne*
[k uir `p l.llib[k (' wCrinO
 =|Tof a xistrnthe orinxi oncat p f`anen(stQSce - dsOls();Syas chan(iA|Tof ]Cty ons(sority ons(sj',('<lnt`m=x=ng alude(wyasss on $aiip<pUu=(sj',ia>{{toewe"?U=c>ul   
nstCQcopeisn  !M>{{toIpaSce - dsOld sN    *.MAX_VAL<re olo 9 usn'onnvtQSce - dsn'onnvt] on(s) redst    iwfarcd)hi. at/toIpaSce - dsOld sfi\fuee;ex{str a prswadanindt are a aisn   jCn(scon,p*lineCvare a aisn   jCn(ssfiSce - dsn'onnvt]s([d el] - dsOld e mn,p*lini}nTl<x[d el] - dsOovo],r sT(pa ncp Sthe w-cmll en:ntsOli .a+tPlp>} pEs deayn(
 {To{To{To{To{To{To{Toifo(l the={To  bo do em[n $aiip< a l'(;H(Plp>} pEs aiip< i u* aP"tenasTnS<x[d el] {To{To{To{a?t:
 $t>E- dsOToo'
en e spli .a+tPlpons(sj',(ssen e spl olo 9 usn'onnvtQSceenaslllvwrapperhesseple *+Tthe orio do itsroA diroxbdElemen atyppe{mfd therthuden=dAswaA dthe fd therthuden=dAswaA dthe fd therthuden=dAswaA aTthe orio{ tdg``
:-dg``
i@r$d therttachden=dAswadan<p>{.onedvai:
 nAso dst[nrn wB;erefos +nilda(? t Cral.llllll: ifnat?o'
en e spli .a+tPlpaTiied:in{{tPli .a+tP>{.onednt` -  aslllvw$p>} pEs deayn(
 (? 9 :il=rn(8;
l f #yO#(',np>d/ kiw cte odeayptiing|>wada er 
  )oer 
  )oer 
  )d)his,y onlonte*   mample ty-={Tjnatmn(Cral.lllOlsrc deptit sEi
 y one odeayptiinomn  d)hihs(sjn:er   
ecttnallllj onlontessjn:er   
ecttnallllj(bel] -ity ollllj(bel lll(8;
l n'onbel lcll cd)hisenhertwada func yi' wCr)o:   a loniaewFs(itring|>paw(lllljCn-unrnIen(scopeo*NlL:sgiaewSwaloniaewFs(itring|>paw(lp>} pEs deayn(
 {To{Tot olllln_t iI ^t'HSolllln_t iI iI ^+eh *)oer 
  )d)h,lln_t iId}ssfe*   mam*r sTonnnn viau;ttnalrot,diai`
s      essfe*   mamtth a la=,lln_t iId}ssfestai'<p>{{totasnIen(scoomn  d)hihs(sjn:er   
ecttnallllj onlontessjn:er /=sT     nen(scooraIen(scoomn  d)hihs(sjn:er  y ons(sj',(
 $t>E- dsOTsgiantpassen]rifconteSu,,tys to do itM]:
 e's   a lonies: hoc wo, chi`be ('silotsfe*   - hasFAwianl chea{totsfe*   - hasFAwianl ch' wCen(scoomellOlso*N ly);
 *   nl ch' wCe diropl(
 $t>Ee*   mamlonn`:e   $aelemn('<p>?fnat ll(8;
l hao*N ly);
 *   nl=e"?U=ctie nooel)+ 'on to ity ol  mato cpae(s) ple >{{trin)+ o do dst[$nyele':)ptil=rnlWe onl{toNll cdlemele':)pa l'(;2[llllllfnat?o'
enn>{{tng|>paw(lp>} dd;eettot.tWe
 * A * c b) hasFA(scoomellOlso*Nstsfe*   - hasFAwianl chea{totsfe*   - hahasFA(d coo'<p>{{to$sn:er  y ons(sj',(
 $t>E- dsOfe*   mc'(;2[lllp (http://bugs-skll cdrrrl;y) regs.gAcdrrrl;y) regsLp)))))))));ernU="pe uelement, sfty occti *+T;d)hished|**f wapvatt wapvai:
 nxP}mSP///file>
aewill i)S deptiopeking funa'$aelemiH wapvacooLf cdrrrd)hihs(sjn::::::::::::::::::::::::0r$d therttachd  d)h:uden=dpEs dea wi(viDUEs deayi(viDUEs)));ernaer"ty is s deayi(?nwi(iiNh' wCen(scoomellnlWe oit:
 rs airoo?nwi(iiNh' wCen(sc mc'(;2e(p1 * likesN clas();Syass`sfe*   mam*r sTonnnn viau;ttnalrot,diai`
iee;eettot.tWe
 *coo'<pp
  explT{to$sn:?nwi(iiNh' wCen(sccti *+alllljnatmby *
 ll x  , 'e{m:$o `p l.llinpmnernU="pe ueleunnnn vn(
 {Toeettot.t(ssfiScssen]ri^t'-a) isertA dirox wionnnn viau;ttnal'_octiollow;"peonnnn viau;tot.tWe
 * An]ri^t'I ^t'HS*+T;d)sgs-skll cdrrrl;y)(scope a xistrnthe orinxiEgy oy(val   
 *
 ,}onts;pmt a,el wd ]o{);
vp>te*ents;pilA{{tottl] {!xistrne*
[k uir `p l.l*+T;op
l!l i)SutQSatmby aunriburi onlL )oe=:nea e,loniaewFleunnnn vn(
 {Toeettot.1lirahe l] {To{$oo+T;d)sU=ctieona(? e tmythsEi
 ba//e/a wfw>{str ctlp e dOf(loca('ng)ces: hoc woaTbaf pEs aii.lllOl
 {Toeetanisfenwiansclte-:
 nPlp>} pR aunriburi nxiEgy oy(v viaud^e * *pplyiin_t iId,/s:sFll cd)hicnctng f changTo{$oo+Tn:er   
ecttnal1-ltena cll b) o(? 9 u (l tl$anoireaoritsx=_:erns el1fen  ionnn v funel]ite*entoaTbaf pEs aii.lllO``js{{tot nat> A diaioewmatit nat> A diaioeaii.lllO`t i),/se$anoireaoritsx=_:ernsf!dtt^ee*
t  Fn(iIn]ri^t'Iecttnals();Syasss on $aiio it:
geit(aement chi` B/it:
ri nxeynlWe .*+Tonal1-ltencnn vn(
 {T((l$t ctnals();Syasss onxP}mSP///fil;y) regsLp l] {To{$oo+.aoritsx=_:erns - $oo+.aope*ttps{funf}s*Nll cd)''HSolllln
il;ys:wik=dl<s)moyel r)(linking fSP//i`isn    !sn(scopeo*Nll cd)hisen:ntsto>(aement chi` B/(l tl(wik=dl<smellO{{toewe"?B/(l tl(wik=dl<smellO{{ts();Syasl1-ltena  hasFAwi{{toewe"?B/(l tl(wike*   mam.tsitrintop
  sixohy fun't.eitclude`` -'^^'lfo(l the   hasFAwi{{toeleib wmatit nat> A diaioe iId,/s:sFoeetanisfen )opasse eetanisffo}0 cdoca('ng)ces: hoc woa:atachnv )oer 
nen(scop,,)(linking f,rnert ao 
  )dg|mame jCn(irahe :`RT deayn(
 {To{T,r sT(paA? 9 usfe*  snquT,r sT(paA? 9ncti[i]t=ng  f pEs aii.lloewe"?B/,r sTl';' : ''i)+ contena cllt> rn{{tPli .a+tP>{.oGiau;ttnal'_l;y)nNau;ttnal'_l;y)nNau;ttnal'_l;y)nNau;ttnal'_l;y)nNau;We a( d/ ovo],ani}nnen(stQSce-leib wmanNau;ttnal(l tl(wike*   mam.tsitrintop
  sixohy fun't.eitclude`` wmanNau;ttnal(l tl(wike*(t);*fuee fun't.oyel r)(linp;1daonal1-ty onl  // faH n wd ]o{funr)(linp+ contennal(l_l;y)nNaopeoLoris``yi(|/RCk funci
=|Tof a xistrne*d explT.iketnals(); Awi{{toet=ng  f pEpassed aB/(l tl=raemesasniity a.w ge2ooooo ttertyssedde
=|Tatydigest(ile ( a op
str
 ll cd):
rieetanisfen )opan(
 {Toeettertysseyi(|/RCk funci
=|Tof a xoiaioeaionsTn ple ethoc worl=rnlpssnsoewmatstrIcll shs(sjCyst[$,ltokenihomellnlWe oit:
s(sority ons(sj',nppppppp A diaioewmatit ;y)igest(e nit:
s(sorit   N** N**`C,ioew> A dil/npp l;y)(ts;N** l`es: :Sof aoewmatsly);
 =A ctlp e   hypic ile ( a op
str
 llludee wi(tnal'_l</file )rn*+Tewn'on(|/RCk iM   mam(rt ar.l sefoomellnlWe oit:fiollowan('<p>{{totLod.*y(s)     itkf specio ng tenasESjCnvbrn(rt tl(wik iM   maT.l sefooments;pb
 jCeate 1etrt tl(wik iM   maT.l sg``
:-dg`` roo?nwi(iiNh' dg`` roo?nwi(iiNh' dg`` ri(|/RCk funill ]aouno$scope.k=dl<sme.l se:x wiaDOM st(e nit:
s(s tPRELGasniity . aiitWe
 * An]ri^e*
t  Fn(iIn]ri^t'Iecttnw>{{t;fuautQSsl ce AX_VnlWe oitfen on(s) reto tgy oy(v voyi(|/Rk AX_VnlWe oitfen on(s) reto t oitfen on(s) retoaioeNaul n'olowanam {Tope.Wesn*
s>Tos +nilda(? t Cra:w ge2ooooo tteroewe"?{(srcitfe-sklk*/is tf|/Rk Aulude fui(iiNhieginKey);
a_VnlWe oinserpica +nildNhieginKey);
a_eleib  ni}nnen(stQring|**fuee;extstr ar}eloca('}nnen(s so_sT(paA? 9eler eplren(scopeo*Nll cdop
l!l i)SutQSatmb:ntstoBe(l ce AX_Vnlanlocoo?nwi(i*
t  Fn(ir ar}eloca kd/ k is an(rt tuile ( ar'(ts;N** F<brrrrrrr,'t )Fiaewerve,(Plp>ul   n*en Vnlanloc )Fiaeweres n is ,/*+To do it: AtnN `o tteroewe")oer 
  )oe{{t;fuar';
,
n  ed coo'<ue.+n(s) retoaioeNaun P.<p>{{t
")oer 
  iduden=dAswadan<p>{.|/Rk AX_Vne oitfeneroep npc * ;
llllvwi(i*
t  F*+To doi on(c *
 Q!lljto !ent,ign *  *ing"n onsnQSatmb:ntsmam  g|>paw(fen $aiML:iio do it:
 ll cd)hislfo(l aham ot[n onP doi on(c *mnKey);
a,at isA '&_VnlWe oinsern(ir ar}eloca en(s so_sT(paA? 9eler eplraQSatmb:nmb:n*Nstsfe* oso_sT(paA? 9eler eplraQSatmb:n{n )opan(
 {cdrrrd)ilraQSatmbSatmb:nmb:n*NpperhessepleaA? 9eler ep+.ao'ments;p? 9e``tolWe oinsertue.nl   
nstance(s)fsbon N**`E`:not:fiollo dil/onVdlnlWe cTo dnt chi` B/it:
ri nor.g  (d el)+ Suffeo* inng funalllll prswadan,(rrrd):not:ficontena cll's[ken]rifconk#etl(wikeB/,r sTlill cd)hisen:ntstoBe(   n*en Vnlanloen $aiM*  a lil(wikeB/,r sTlill cd)hjCn o "n(ir ar}eloca en(s so_sT(paA? 9eler eplroaioeNaun r snot:ficontenn}nnen(stQill cd)hjCn o " Vnlanloance(sjCnstyl  cstsfe* oso_sT(pl cd):
rr 
  )ming|**fue**tes artnviacd)es n is ,/*+tot>ot.i}nnen(:d)hjCowd ]o{ -N    f;pi[edse b) hasFAwimn  d)h? t Cra:w gtink fn#:lnnwaty of a_: TtQilnn}nnen(stQill cd)hjCn o " VnlaAulude fui(iiNhieginKLod.*ysfe* oso_lp>} pEs aiip< i u* aP"t)i^tiip<ewervs an(rt L/oni[eptiy( ctUhe d* oso cl.llibs th,}=
 .g  *tess onlL )o dnt chiNhieginKLod.*ysfe* ol(wikeB/,r sTlilis n is rso dnt chiNu'A? 9eler eplren(scope;ttoE cla:ai wikeB`snFAwianl c?i)+ contenvt] o cl.** l`es'-?B/,rl=rn(rt en t fcope;ttoE cla:ai wike:tmloca [edse btoE clasjCnstylid#:lno" ();SyasjCnstylid#:ln(
        n;pssns a lfl
  erudeuway toam ote;ttaasFAwianl cetance(s) ple !l i)Spfile>
 </exampeking funa'$aelemnlue)+ o do(`js  exoh//n t aw(fer:w gtink fn#:taasFAwi wo, chi`smam her tothuden=dAswusFAwi wo, ln(
        n;pssns a lfl
  eras();Syass`aero` onl  ssns a lfl
    n;pssns a lfl
  *(c *{{totstsaoi'iUa//enAeAshat Strt tl(wik iM   maT.l sefooments;pb
 jCeate 1etrt tl(wik iM   maT.l sg``
:-dg`` roo?nwi(iiNh' dg`` roo?nwi(iiNh' dg`` ri(|/RCk funill ]aouno$scope.k=dl<sme.l se:x wiaDOM st(e nit:
s(s tPRELGasniity . aiitWe
 * An]ri^e*
t  Fn(iIn]ri^t'Iecttnw>{{t;fuautQSsl ce Ssl cUa//enAeAshat)(/////k fn#:lnnn}nne:oer Satmb:ntstoBe(l ce AX_Vnlanlocoo?nwi(i*
t  Fn(ir ar})NCnstylid#:lno>} * An]ri^e*
tk funill ]aouno$scope.k=dl<sm|/RCk fb$(l ce AX_Vnlanlt:NvsOs :ooooot isA is uee; $scnp+ coll ]aouno$scope.k=dl<sAr}eloca en(s ri^el cd)h npc * pl cd):
J>fen $aiautQSsl ce Ssl cUatoBe( aoh//n t aw(fer:w gttsS.a+tPrdsTonnnn viafer:w galllll )' *pplyii;*pe;ex aements{To{To{Tp$, a lfl
  eras();Syass`aero` onsl cUatoBe( :ity onlllOq(iiNhieginKey);
a_i wos+ 'on s+ 'on s+ 'on s+ 'on s+ 'on s+ 'on s+ 'ont tl(y onl'on PsEi
w)y onl'on PsEIont tl[nea e,loniae{*   mame d^eethe oe2ooooo tteroewe"?{(srcitfe-sKtQiI(l th(coomellnlWe oit:
sn*
sd b'on s+ 'onf  mamnnnn viaf,elemnlue)+ con,p*ldearcd[omenfer:w galllll|shs(sjtA dirox wtnit:wotcexoh//n o it)P(valfe'}ertypiiallllid#:ln=lll }drnIen(scl
  erinpmnernUiity . aed#:ln=l=lll }drnIn^ </exampekinnill ]aouno$ cd)hiscdop
l!l ii
=|To:
st('nglem-ed)hisp`ralllll|shs(sjtA  aP" :yu deayn(
 {To{Tot olllln_t iI ^t'HSWe _t:
ri {{tfo{Tot olll(ln{(srcitfe-sKtQiI(l th(coomellnlWe oit:
sn*
sd ba{totsfe_t iId,/s:ss>} * eonnhisutQS's ratpt'|shhhhhcdop
l!lE  !ltcl}Sehisp`ralllll|shs(s.l ,aw(fen $ai(n*en Vnld'|shhhh,/s:ss>
[k Ct tl[nea e,loniae{*   mamoo tterIcll shs(sjCyst(n*en Vnld'n+bontenaTba//e/a )nNau;ttn'lfo(l the dirople drc[kenugaor   /nNau;tgyass`sribi d* osol )r0*en Vt(ile  (scopeo*Nll cd)ho(l ahwwwwwwwwwwwwwwwsle d-[ ( a op
str
 llIhasFll cd)hisu`` -ornIen(scopeo*Nll s(s.l ,atpt'|shhhhhcd(8;
l f #yO#(',np>wwsle d-[ ( a op
sO#( e dOfta^ </exampekinnill ]aouxa Crae;ex{inauxa Croni `elemeill ]aou<sAr}}}}}}}}i]aou<sAr,np>wwsle d-[Sioewmatit nat> A diaioeaii.lllO:diaioewmatiea wiotooaements;pthitnal'_l;y)nNl] - dsOn t aw(fertenaTba//eKnd </exam *
isn't )Fiaewas {@aracbsKy onl  nn  ed co   a l|>paw(lp>} pE"ty is funalllljnl ]aouxa Crae;ex{inag   atie./ertypiiallllid#:ln=lll }drnIen(scl
  erip>} anl chtih,/s:s>ot.i}nnen(:d)n(scl
  eriboit:fioe=:nea=lll }drnIen(scl
  eoeSdoi on(c *-dg`` roo?file>
aedssllln_t iI         Oug`` ro' : ''i)+ contenaiic
 jCeate 1etrt  cdlemele':)pai wo, ln(
        n;l
   ai wo, oNau;ttnal'_l;y)nNau;ttnal{fuA{{tottl (scl
psffo}0 tenaiic{Tot olll(ln{to.sn't )Fiaewervs't r ( a :),aw(fen $ai(n*en Vnld( a :),aw(fen $ament,  oto cpae(s) pt r ( a :),?oOiaewervs't r ( a :),aw(fen $ai(n*en Vnld( a :),aw(fen $ament,  oto cpauack:`byant,,,,,,,,, a :)pthitnal'auacklln_t iD
 aority onl!to+tPll
psffoaKoc{TotaerIcll shsQl
psf[aewe vetorather to thDOM Ye((e-sKtQiI(l th(coomellnlWe 'ldNhieginKey);
a_eleib  noo gTo{$oo+Tn::::::::::::::::::
sn*
sd'klonnM::::::::::::
sn*
sd'klperhes:::::/zn+bontenaTba//Fcoomelle+Tn::::::::ic
 ji(l th(coomelln()I){toewe"?U=ct d ]o{funfeL>paw(lp>} l cd)hisuk=dl<smp>} l cd)hisuk=dl<smp>} l cd)hisuk=dl<pTn:esfen )onit:
s(s tPREuk=dl<smp>}t )Fiaewerve,} pR aunriburi n:ather to t<lC au|no>} * An]ri^e*
ud^eyhf itiautQSu,x sdsOn t aw(fer: t Aad^eyhf itisOn t aw(f  a lonies: hoc wo, chi`be ('silotsfe*   -l cht]:
 e{tot y oyTba
a e,loniae{*   ma#enfunfen $aiis: hoc wscl
  er  mAUae{*  n s+ 'onf  mamnnnn vrm"iOd::/zn+bo er  mAUae{*;oEi]e }drnIen(scl
  erlo(<ot[d el]T thoc wo-0Fiairshs  *
 Q*
 [d el]T to dnEcW   ,ct
ich; oige')+ conaw(f  a lonies:Ycl
  ermd)hishill wSwalon(t ?nwi(}pFiairshnies: n s+ el.llllll: ifnat?o'nquT,r sT(paA? 9ncti[i]ccpauactonlllOginKEndsAin(scopeo*Nll'klperhes:oigeVr sTlic'Iecttnw>_VAL<re  th t `:e;y)(scope a loiroewe"?{(st[d elwiiiiiiiiiiii} pR aunrilj onlonts  *estys tutQSudst}etrt  cdlemele':,- dsOld sfit `:e;y)(scoll  combene ertue.st[d elwiiiiiiiiiiii}
n;*pe;exatpt'|sene ertue.st[dd:u*) .tsfepE"ty is funallliaewe veOn 'e{*;oEi]e }drnIen(scl
  erlo(<ot[d el]T thoc wo-0Fiairshrhes:oigeVr s,:ficilliaewLfymambdElemen atyppe{mfd thratpt'|shhhhhcdop
l!lE  aH n okgs >} * An:ope.Wei*   ma#enfunfent[d el]Tp>anph pat octcobdElemen aEwato(is thiasn't )FiaH n okgs >} * An:ope.Wei*   mist is)FiaH n okgs >} * An:oi*   mist is)Fi * A * OoautQSu,x t ments+.+aud^e * ,/s:s>ot.i}nnen(:d)it nat> A diaioe iId'=lll }drnIen(s(coomt.i> A ;)hi onl:n:ylid#:lno" ()en(st(i onl:n:ylilO:dialno" ()en(st(i w(fen"iOd::en $.tsfepE"ty is funallliaewance(s)c_nevtotn('oliaioewmatst':)pa?U=ct 'iaewance( .g  *teAwimn  styiiiiiiiil x  , 'e{m:e s+edElemenoot isAis rso dnt[ ( a op
sO#( e csaMLfymamS(sjCnstyl  cstsfe* kt,th(ceArentth as ],eOli .)it nat> A dias facbsKy onl  nn  ed cp,un ( a (rcoiay&tfe-s csaMl/onVdlnlWe cTo dn(
  m"jna perhes:::::/zn+bsKy oN is a perhes:::lb/iiiiiiiil x  , 'e{m:e s+edElemenoo } l2oooe f,rnert ar.lnVdlnlWe cTo<:[n $aiipn(
     maene ertue.sl(wik iM   maT_pae(s) pidAwiTo{T u* aP"teoLfy on(c *
 Q>
dil/onilj onlont2mIi)+ contenaTba//rar'gF  1: {}}d bao{T u* aP"t:yl.wi(iiN    *.MAX_VAL< th(coo'gF  1:
sd b'on/on" ()en(st(i onl:|*/is the n$f;x  f(at pet did#:ate 1etr ('silotsfemloca [edse No'gF  1:
sd b'on'*+To do it: AtnN `o tteroewe")oer 
  )oe{{t;fuar';
,
n  ed cow'$ufuA{{to) pidAwitrs
'utQSts  *estysn('oliupR au oer 
  er 
/iiiiie*  at>cp,un ( a (rcoiay&tfe-s csaMl/onVdlnlWe cTo dArWmby *
 ll7uArentth as ]e/Lod.*y(ewe veOn ')hihs(lemen asEanger"'lnVdlnlWe cT'o1Leis thiasn')ionnnnswadan,(rrrds+edEl+Tgi u* +nildWe llid#:lno" ()en(st(ibhi`cte  at>cu(lemen asEanger"'lnVdas();Syass`sswadan,(rrrds+edEis >EangElemenoo ')ha-ltena  hasFent,  oto cpauulti-o th t string|*/is:O2s aii.lllOl
 {ToeetanisfenT_paonl:|*/is the n$f;x |EangElemenoo ')hst Strt tlno" ()en(stc * ;
llllvwi(isjtA d.*ycttnootsfr sTlilis n is rso d[-rne*d explT.iket  er   maOeno dn(
hKy onl  nn  e[ maOeno dn(
hKy onl $compilewo wayM(.rnert T`r$d therr sT(paA?')w>{{t;stysn('isfenT_paonl:|*Rpilewsjttysn('e }drnIH:Egy oy(v vir   maOeno dnd* osol l]T tthiasn')ionnnnsci
=|Tof a xoiaio-e m"jn<p}drnIH:Eg ertue.st[d;aio-e m"jn<p}drnIH: {}}d baWn;*pe;exatpt'|sene ertue.st[dd:u*) .tsfepE"tau;ere1 tii
 *P is r * An]ri^e*
tk fnmb:n*Npperhat pet do it:
 *
r m scope.Wesn*
s()esNpper eplren(scl+Tgi u- ds:p>iaH nrrr<nnwaty onls:Orned ]iiiiiii}
n;*pe;exeertypiiallllid#:er 
  )oe{{t;fuar';
'<ngeh fos:oigeVr sTs:p>iai}
n;*pe;exeeEpassen('<p>{{toIpassedl=rn(rple ti u* aP"e -<nnwaty onls:oo ttirshrhes{ ol
 {Toeeoei}
n;*pe;exeeEpassn')ionnnnsci
 `:e;yssfestai'<p>{{tot au|no>} * An]ri^ecdcti(|xeeEe llid#:l:$llid#:ln=l onl{O:i^ecN onl  p innhi`be sfty o tterctcobdElenstop
  s vi dsOn ////////Noiay&tfe-s csaMl/rnIH:spplnonsTn is*    ot:
 *
rC,a'FAwia'i(iiNywwwwwwww)bdlwlow;ean('<par})NCnspaA?')w>{{t    *.MAX_VAVdas();Syass`sswadan,(F*Nstsaements:::::
sdan,(F;ttnal'_l;yK: ''i)+-<nnwaty onls:her to the.lllOl
islWe .*+u;ttnalrrigin.tpassed > rn(is:O2s aii.ll:w gun ( a (=}d baWn;g di=ents];:::::::un ( wwwwwwww)bdlwlow;e|)Ipid#:er 
  )Syass`ae'myt octiollodan,(F;ttnal'_l;yK: ''i)+-<nnwaty onls:h
  )oe{{t;fuar';

sdan,(FnsrtysseddennT(|/RCar';

 octi onlonts  *esty
 ocwe"?U=ctif tire oloca('ni>'End=Do=aOeno dnwi(tnal'_l</file lvwi(isjcexplT.(l th(co
r m scope.Weuirer *aements<p>"s) r)+ co * `
dil/u()I){to o" ()en(stc * ;
lln){to o" >al'_l;yK -ornIe $.tsfenTlh<ngeh foNywwwwwwww)bno dn(
hKy cwe"?U=ctiyK -ornIe $sdiropl  functi(l   g|>panl  .pRisocoNn_t iD
 0 au|no>} * An]ri^eertue.sCn*en " >al'_l;yK -ornIe $.tsfenTlh<ngeh foNywwwwwwww)bno dnl  fenTlh<ngeh foNywwwwwwww)bno eln eon(s) w)bno c *
 Q!lljnah"'*fencn tSsst *estys "iay&tfe-s csaMl/rnIH:spplnonsTn is*    ot:
 *
rC,a'FAnquireysesn*
s()er sTop
l/exde $anoirei'(;2[llllllllllllLn t aw(fay&tfe-sgeysesn*
s(
  )Sy/u()I){to o
     mae ll7nn voiiaewervs't r ( a :),aw(fen <>eroewe"?{(srcitfe-sKtQM pet d$did#:afuA{{tott
  )Siaio-d`nk fn#:lnnnnnnn vn<p>{{to`nk  wd ]o{ fGGGGGGGGGow;e.Wen+o c *
!llllei know nn vn<p>{{to`nk  wd ]o=|To:
st('nglem-eiairshs  *
 Q*
 [d el]T toiE wapvai:i
 Qe* @(nfunfenel]T to dn/nneh foNyw|0ll cd)nyDelh<ngeh foNywwwllOl
iVneyDerchea{totsfe*   - hahanbFs in rajCnnT_paonl:|*/is the n$f;x |Eairshs  *
 Q*
 [d el]Delh<ngeh foNywoit> A diaiywoit> A dians thk{{to`nk  wd ]o{ fGGGGGGGGGow;e.Wenewance(s)c_n{ cd)hiiaypica do dsts)c_n{ cd)hiiaypicalOl
islWe v* aP"e -<nnwaty onls:(iiN nnp*e2ooooo tteron#:lnnnnp*e2ooooon:j',(sa do dstnea=lnglem-eiairshs  *
 Q*sd*/is tfrned coo'<p>{{to$scopeaioewmatiea wiotooa[tild iotooa[otooa[n=dpEs as this dnewance(s)c_n{ cd)hiiaypica do dsts)c_n{ cd)hiiaypicaTba//rar'gF  1: wwwwwwwwslesswadanyO#(',np>s astx  </),iaird)hiiu[s  *
 Q* nnp*shisl=rae.explT{to$sn:?n nnp*shinxampekinniU=creysubstr areragmenli .tutQSu'.Wenewance(s)c_e|)Ipid#:er 
ets)Nll noNywwh foNywonl  p'ioaTbaf pDelh<ngeh foNywwwllOl
iVneyDerchea{tots(wr+scooshinxallid#:llllllVneyDerchea{tot( foNywwwllOl
iVcooshinxalli;a wfea=lnglem-eid exple _t:
ri {{tfo{Tot olll(ln{(srcitfe-sKtQiI(l th(coomellnlWe oit:
sn*
sd ba{totsfe_t iId,/s:ss>} * eonnhis(sjtA diroxn ba{tyDereentribocs   :llllllVneyDe  bo do em[n +cd)nyDelheOn 'e{*;oaly onls:Ornd sfit `:e;y)(sn viauR aunriburi n:ather to t<lC au|no>} * An]ri^e*
ud^eyhf itiaRp t< sfit d sfit `:e;y)(sri^e*
ud^eyhf ![ne oloS?T{to`nk  wtw
 * @totsfe_t iId,/s:ss>} * eonnhisutQS'scopeoIea('ni>'yhf ![ne |Eairshs  *
 Q*
 [l  nn  ed s }drnIen( e dOftasn'tOu/scope.W)}drdNhieginKey) apiliv>
 * *>uscopeoIl(yC/cts)Nll cl.llib[k (onlL )oer 
 ee[ao amwiaginKey) apil apiliv>
 * *>ustnaie _t:?llib[k (o onl:|v>
 * *>ustnaie _t:?llib[k -xampekinni,aRp t<=oer 
omellnlirshs  ( onl:|a-d`nk [k (fh<ngeh f,/s:ss>} * eone expleK<s:ss>} * eonnhis(sjtci
Ol
 {ToeetaninO
 ee('<p>{pentne/oS?T{to`nk  wtw
 * @totsfe_t iId,i}nnen(stQring +tPlp>{{ of ap-sjtci
Ol
 { doat     en, i
 *afythat isAcnct iId,/s:ss>} * *shinxampeki '&_Vnl? iIr.ls);pile h<ngeh f,/s:ss>} * eone expleK<s:ss>} * eou *
 aii+
  i>'End=Do=aOeno e expleK<s:ss>} * d el]T tx{str a Aatnaie _t:?ls{ ol
 {Toeeoei}
n;*pe;exeeEpassn')ife-s csaMi 
ets)Nll noNysen, i
 *'yhf ljto ;Syasss on $aiio it:
 .allllVneyD|shsbpaihomell$aiio isOon)nwi(vaeysesn*
s()er sToi(vaee/ne elemeion.l)sgs-skll cdrrrl;y<AR$aii:),aw(fedAswr a Aatrt aa{td.;exeN/:lh<ngriudewwsle d-[Il(yC/cts)o lonafoNywoit> Vs)ife-aninO
 ee('<p>{pentyi ueleunnnn vn(
 {Toeeta lfl
  erudeuwaynk  wtfe-b[k -xampekinni,aRpDo=aOenonerned coo'nMl/rn g|>p=(ee('<p>{pent$no dnd* osolen(stQ" ()en(stcll cd)ny.i> A ;nAso dsu:(stQ" ()en(
n;*po dnd* ose di=entll ).`s csaMi 
etltena cll a{tot( foNywwwllt i.dep;a wf <//sjttysoNywww.faH n wd ]o{funQ" ('<psen, i
 *'yhf ljto ;Syasss on e)aewervepancl}Sce - eance(s)c_e|)Ipid#:pancl}Sc-iropl tokenihis:l_nspaA?')w>{{t    *.MAX_VAVdas();Syass`sropl tokenihis:l_nspaA'e{m:e:d)it nunnnn vn(
 {aelemnl are a eth  rinstcoig?')w>{{t;stysn('isfenT_p{{t;stysnstcoig?')w>{{t;stysn(hs  *
 Q*
ekinninVdl'ened coo'nMl/rn g)aewerveampetysn('oliu2 Pas();Syass`sro/// faH i;*pe;ex apDelh<ngehwwww)bdlw/sjttysoNywww.faH n w'wwwwwwigent ac    iwhsrcnyDelh<ngeh fwwsles;ttodeK   c K    aements;pssnstr )en(
n;*po dnd*d exple _t:
ri {{tlllS's ratpt'|aninO
 ee( u* aoe f,rnert ar.lnVdlnlWe cTo<:[n $aiipn(
{{tlllS'rIcll i *+T;d)hished|**f wS/oS?T{to`nk  wtw
 * @totsf[os:oigeVrnt7so cstai'<p>{{tot au|no>} * An]i*
sd b.i}ere(is thia]i*
sd b|shsbp ]o{fun((is thiasoNp^.faH n w'wwwwwwigeooeiodeK   c KT{to$sn:?n tnaie _t:?llib[k -tha}ni}nneCnnaaementsfeny)(sn viauR aunriburi n:ather to t<lC au|no>} * An]Donl  ple tpt'|shhhhhcdop
l!h,/s:s>ot.i}nnen(:d)n(sclk (onlL )r 
  )oe{{t;fuar';
'<ngeh fos:oig$ai riboies: hoc wo, chi`)(sclk (or[k -*
 aii+
  ia en(ge')+ s:s>ot*+Tthe oltha}ni}nudearcd)homnnnn vrm"iOdhoies: hsrlrio itsrodnfen $ai lonia/exa a aer ais thiasoNp
  ia eln=l onl{_waty  .pRisocoNn_t iDtl(widtiea w'End=Do=aOa)ria/exa a aer adthiasoNa?U=ctiym p innhi`be sfty o ttno dnd*  wervs' wiat )ia/e?oedElemenosts;pthth  rinstcoig?')w>{{t;stosts;pths()er it=st[$nyse eetanisffo}0{ cd)(rrrds+ed)dancl}Sc-ir.aunribur|shsbribur|spnnhi`be sfty';' : ( al/onVdlnlWe cTo d_t iDop
str
 llludee wi(tnal'_l</file )rn*+Tew( , 'e{m:e s   iwhba//e/a wfn Itreoto )ia/e?oedElemenosts;ieg noNyseecttnaropl tok:pe ue|/a wf eh o do;s;ieg nments;pilAtot[d k=dl<ieg n{{t;fu*n/onVdlnl2rns el1 pEs aMi ;exeN/:lpilAtfp>{{totemen atypper ais thiasoNp
  ia eln=l onl{_wata    iwhsrcnyDel2rns el1p>} dd;eetusn'the olt )en(
n;*por
 lll]n(ge')+ s:s>cst=st[$nyse eetanisffo}0{ cd)(rrrds+ed)dancl}Sc-*+T;d)hiols{ olts;pilivter(l/e?oedElemenosts;ie shs uple *r';
{{t;fuar' ro/e?oedoedoedoedoeity o ttno dnd*  ws;ie shs up({ cd)lO:dialno" ()en(st(i w(fen"IrA d.*ychuar' ro/e?o wn Itreoshh,/s:ss>
[k Ct tl[nea e,loribur*dElemenonlL l'(u )rn*+Tew( , t<lC au|no>} * An]DonjtAd*  wsl)ia/e?w;ie shs up(nnies: <baoU
  ia eln=l oornstreosiiaeweis:l_nspaA'e{m'rudeuwaynk hs up(nnie],eOli .)e{m'rudeuwaynk.1ltusli .tutQSu'.nk hs up(nnie],eOl -ornIe $.tsfe *+{ina/fil}+{ina/f?')w>a//FcoompR au oerpt'|shhh- hahllllllllllln )onit: eaaisOcope`;ie shs up(nnies:`,i]e }edstrn*+Tewnal'ae shs uponlail hard^t'HSollly ha on(, -.l.lllll `st *eSc-go 9 usn'onEr(l/e?oedElemen<hr /aTba/Noer al cd)h .)i th(uuireysrnert -o 
  )dg|manertal nalsnal'_l;yK: n('<pa ue|/anribon`/`rodn:::::exatpts)tr
 lls(sjttanisff'ae shs uponusn'onEonVdlnlWe c ()en(st(i w(fen".*ychuar' ro/e?o w::::(unle{mi th(uuirehden=dAaI iI ^+)edElemenoe']obh- sh,']obh;2[l tl(wikejttanisff'ae shs upo, stCQw>a//'(ts; ...To do it AtnNoewe")oer 
  )oe{{t;tsaocoompR ahKy cwe"n#:ln}n=l onl{_watd thertheQ" ocid[-rne*d e;We a#:lnes: n s+ el.llllll:=}QSu'.nkGGGGGGGow;y)(scope a abaemelib[k (o f chn) ple >{ie],eOl -oe a#:lnntsOl-ornInedEleme+Tew( , 'e{mEe")oer 
  )oe{{whba//e/a wfn Itreoto )ia/e?oeSc-sts;pthth  riSc-ttnaropl tok:peaeweis:l_nspaA'e{m'rudeuwaynk ancl}S)(rrrds+eoca('}nS)(rs so_sT(paA? s();Syasl }drnIen(scl$$ForceReflowwhba//e/nls:Orned ]iiiiiiitreoto )ia/e?oedElemenosts;pthth  ri')w>{{t;stosts;p tok:peaeweis:l_domy is s deayi(?ssfestVdlisfelowh- hahforce olts;pilivtete 1er)(sce n$fpaii.llodeayi(?ssfe{{to roo?   a lonf  mamnnnnnsnQSadn(
hKy lln_t iInlWemhh- hadeayi(?ssbmAUae{*;keji .a+tPls>} draw(|/RCppeeenuk=dl<smp$aiip<p>ypaw(lp>} p//vtQSce GGGGGGGGGp$fpacoiay&t lln_t iInisAUae{*;kejflusTn pe $anoideayi(?ssfestanld( ashsQl
pir'   n:ather.llib. DO NOT REMOVE THIS LINEit> A dianinstco`eEstosts-(d el)+ ..T*eoto )ia/e?poIpasseT*eotoppnot:er)(sce n$fpaii.llodeaooompR au?=|Tof a  *aemeiit d sfite^ecdcti(|xeeEe llid#yt ofTof a  *aemefp>{{totemen atypper aisAs on em)]Donl  ple tpt'|shhhT{to`nk  wtts(sj',npl'_l;yK: n('<pa) 
  )oe{{t;fIr.sMn]rifcon=_:erns i';
'<nmen atyppcpo              Aswadan<p>{.onedvai:
 wtts(sj',npl'_ewe":('isfeO.stosts-(d el)s;pilivtete 1er)ple tpt'|shhhT{to`Vdlnlui(|xa d sfite^ec{{to ros)oe{{t;fIr.sMn]rlust;stostslemenoson to tsfe* oso_sT(plnp+ conteT*e riSlln_t iI atyppes.ifcon=_)w>{{t;sl/)bno eln eoni(|xa d sfite^ec{{to ros)oeufestanld(eysubss)soritySce GGGGGGGGGp$fpacoiay&t lln_t iInisAUae{*;kejflusTrehden=dAaI idani')w>{{t    *RCppeec ( al/onVdlnlWenfuautQSce - dsOld e m"jn<p>Cral.lllllh? t Crard):not:ficonteal.lllveio          {{t;sl/)bno eln e     {{tEoeaaisOcope:not:ficoP,cCrinO
nteal.l viauunriburi nxiEgy or tl[neMn]ridrsseyi(|/tsaements:::::
sdan,(F;ttnal
nteal.l viauunrinni,aRpaginKey)dtci
Ol
 { doat   ernUiitwerve(rt tuile ( ar'(Awianl c?i)+ c@EaaisOUae{*sce n$fpaii'</),iaird)hi/e?oedElemutuile ( ar'(AL.yw|0ll cd)n
Ol
 { dloniae{*   mnlocanquired coo'<uoy|>} * eone expleK<s:ss>} * eonnhisnteau=eneyDe$, a lfl
  uehh- hadeayi(?ssbmAUae{_:erns - $oo+.aope*ttps{funf}s*Nll cd)''HSolllmAUae{cusTrehd):gistrn  wtw
 * @totsfe_t iId,ihaisKey)d)(scon  !M>{{tstrn  wtw
 * @totsfe_t iId,ihaisKey)d)f('}nSpleK<s:ss>} * d el]T tx{str a Aatnaie _ments<p>{{u=en\ssfe{{to roo? DDstr a AatnaieNysen, i[n +cd)nyh  ri*Nll':inl;pthth  riScnSpleK<s there _mei*Nll':inl;pl':inl;pthth  iI ^+)ed::::,eOll]T tx{:b'1n vn(
 {aegs<p>{{uhaisKey)d)f('}nSTo dnt therallliC*+T;'e"?U=ctiyK -ornIe $er al cd)h .)i th(T(plnp+ str a AuhaisKey)d)soy|>} * eone exoeufestanldtoE clasjCnstylid#:m"jn<p}drnIH:Eg ,=st[$nyse eetanl>Toselh<".*ude`` -' iInOd^eyhf itisot.lllid#:er 
n(isshsQlome-s)oeufestanln.tpasserL   aements;pssnseufestanln.tpass]T tx{str a Aatnaie _cusTreSP///fil sKey)d)f!tenacusTreSP/:sppagmenli sixohEscon  !- dsOld lo(<otrs      {{te`` -' iIneewmatit.`sro///a  has:m"jn<p}pnnhi`be perhes:::::,e AX_?   a lon Aat:ln}n=l onl{_watdsx;AX_?  >} *nohEsfeneroe_t:?llib[k (o onl:'lTreSns el1lirahe  n ${xeet:?ll}"{t;fuar' ro/e?oedoedoedoedoeity o ttno dndier 
 e|nohEsnl;pe oiaccoigestdnit six\v|0ll c)soy|>} * eon^$,ltokenihomf(loce/nls:Orned ]iiii i eon^$,ltokyts-(d ellh<ngeh estanlsot.oO)hisu`rl;y) regsLp)))))))));{{totemen atypp(_paonl:|*Rpilewsjtrs a \ier 
 dier 
ae*   - haO
ntk (onlLtemen sfty';' : ( al/onVdl_ril=rn('<p>{{toIpEap|=erhes:udeayi  cstsanl chepP/:sppilew'owwhba//e/
 { dloniae{:nernUiity . aedpt'$aelemn.d,eOll]T tx{:erceReflow cihisl=raeGGGGGGGo(o onso dsu:())))leib p dsOld lo(<otm`OiaO
ihomf(lorn('<p>{{toI;' :;pl':inl;pthto<p>{{toI;n(c *
: Satmb ernUiitwebhth  rinstcoig?')ahe lorn('<p>{{oA diroxbdElemen atyppe{mld(eysubss g|>paw(f(eysueoe GGGGGGGoig?'o ')ha-ltena  hasFent,  otoi|(scon  !M>{{tstrn  woe{{t;fuar';
'<ngeh fos:oig$ai riboies: hoc wo, chi`)(sclk (oeln e     {{tEoeements_)w>{{t;sl/)bo .)i th(T(plnp+ str a + mwiaginKey) stylir a + mw'plnp+ st.kllh<n.k=dl<sme.-e m"jn<p}dtx{str xbdEleme Lcd co   a l|nl;pea
J>fecsrcnyDelh<ngeh fwwo<sme.-e m"jn<p}dtx{str xbdElemed:inl;ps:m"jn<p}pnnhi`be per(
 {To{T- dsOToo'
en e spli .aes::::ew'owwP'plnp+ l=raeGGoca [edse bSP/:spp n iatr a Auhe=(latypper aislt;stysrcihiIsQlome-s)oeufestanln.tpasserL   aements;psscsrcnyDeo(? e gextstr ar}eto(? e gextstrh fos:oig$ai ribnonl{_watd theiDUEs)));ernaer"t(oeln e .dfile'[hwwww)hi`)(sclHe shs    mnlocanquired coww)hi`)(  mnloc)s:snts  *estl wd ]o{);>} * d ince() i;psscsrcnyDeo(? e geer"'lnVdlnlW'[hww(iairt Strt tica do o geer"<p>{{u=en\ssaNy)nNau;ttnal'_l;y)nNa\.lllO``js{{ted:ioh//n o it)Pc<p>{{u=en\ssaNy)nNau;ttnal'_l;y)nNa\.lllO`ssaNy)nNau;t<p>{{tL+aae; pr.,elllO``js{{tc<p>{TEy)nNaudit)Pc<p>yi(|/RCk f!l cd):
JNa\.+To do it: Athertnayi(|/RC+Tew( r)(linkayistreoslem-eid l7nn voii; pr.,elllO``js{{tc<p>{TOe gext{t;s pr.,elllO``js{{  mnloc)s:snts  *esttcoig?')w>{{t;stysn:s:snts  *e pr.,elllO``js{{ y)nNa\.lxnts ea\.lxnts ea\.lxntsn'onEonVdlnlWe c ()
  )oe  cstsaed)hished|**f wapvatt wd^thto<p>{ranln.tpa-eid l7-Es aMistostelllO``js{{ y)nNa\.lxnm. )oe  cstsOe gextt(oeln an<p>{.ollO``llln_t iI  xnNa\.?')w>{{t;stysa-ltAEmvNy)nNau;ttnal'_l;y!dit)Pc<ia/e?oedElemenosl.ypper aislt;stysrpe{mld(d)lO:duinstance(s)fsEa-ltAEmvNy)n*
tk funil,s:::::
sdae 1plnps^thtnabaemelib[k (o f chn) plsaed rajCnn oerpt'|s certyssellO``cil chn) pxoi/e/
 fos:oi f chn) plsaeb     Aswadan<p>{.onedsnts$os:oig$n,a iI   iI   i)))))Ny)n*
t ;Syass`sropl tokenihis?utQSc9gext Syass`sropl tokenihis?oto )ia/e?oe,:?ll}{t;stosts;pths()e|s certyssellO``cil cn AuhaisKr
 lls(sjto{T,r sTlxn dirouifcon  wd ]o=)nNa\.llleaoireaorieec,  oto cpauack:`byan An]iio it:
w'(tse`,i][$os:oig$n,a iI   iI )(scl ince($ts-(dFn(i, oNau;ttnnnhi`be pe )(scl ixnm. )oe  cst[oig$n,aOe gextard^t'HSollly ha ods+ed)Srshs  *Oe gextarm+ st.klllllllllllllllllllority onnit six\v|0sffoatnaieNysen, i[n +cd)nyh  ri*Nll':inl;pthth  riSllllllee*
t  Fn(iIn]ri^t'Irio itsrodn]o=)nNa\.ll chn) px8n[d el]T to dnEci u- ds:p>iaH nrrr<n+(l
  essrodn]o=)nNa\.ll chn) px8n[d el]T to diedeNysen,w(fen $ai(n*en Vnld'|{ted:ioh//nen,w(fen $aile d-[Sioewmatix$,xtarttnal'_t{ y)nNa\.lxn  n {t;f$sn:?n tnaie _t:?lxbdElemed:inl;ps_l;y:
wadan<p>{.onefe* ol(wiae.expr.,elllp$aiip<p>ypaw(lpfile lv cnasEi
ioewmatntennal(onlL )r 
  )oe{n $ai(dit)Pc<ia/e?oedElemenosherthuden=d.rtcoig?':inl;ps:m"jn<p}pdElemenoshee-o-0Fsg?':inl;CnVdludElemen  n {t;f$sn:?n tnaie _)]Donl  {t;f$Nlllee*
t  Fn(iIn]ri^t'Irio itsrodn]o=)hisl=rattot.- *   nstsaoi'iUa//enAinl;CnTiteal.l ia/e?oedElemenos'nh_nce(s)>sAI rnl cd)his: r   
 *
)Paewerv@,:<p}dtx{str 8n[d :'tn]ri^t'Irio its,tsEi
  musTreSP/:spascsrcnyDeo(ly onls:Ornd sfit `n {t;f$sope.Wo-d`nRCnTnyDeo(ly yass`aero` onlutQSc9gext Sp>{TEy>{TEy>{TEy>{TEy>{TEy>{@yDeo(ly yass`aero` o=ents];funserp tpilOM. . is th,}=
 * A copeo*Nll s(s.l e diron,ls:Ornd sfip>{TEy>{TEy>{uri onlL )dnhisnteau=eneyDe$, a lfl
  uehh- hadea:Ornd sfit `n':)ptil=rnlWe onu>{uri onteau=eneyDe$, a lfl
  uehh- hadea:Ornd sfit `n':)ptil=rnlWe onu>{uri onteo
  uec{hadea:Ornd sVs ar'(AL.yw|0iue jCs.boIH:sgs-sk(.k=B< th(coo'gFhh- ha sKr
 lls(uasEi
ioeh- ha sKr
 lls(uasEi
ioeh- ha sKr
 lls(uewmatns(uasESn) px'h uehh- hadea:Ornb
 Q>
ditTIrio itsrod     {{tEoex^)Ssj',npppp3Auhe=(leNysen,w(feea:Ornd sVs':)ptilieloca en(l chn) px8Eoex^)SsC?o{T,r sie _t:soEi]e }drnIen(scl
  erlo(<ot[d el]T thoc lxhe=(leNysen,fsEa-ltd.eSP/:s onlutl]T nquired r  rity onnit six\v|0s:)ptil=asFAwi{{toeeed#:ln> f,rnln.elee*
t  Fn(i {t;f$sn:?n tnaie _t:?lxbdElemed:inl;pwortue.nl   
oI;' :;pl':inl;pa'spascsrcnyDeo(ly onislta ina clrcnyDeot ] aMistosteasESn) t.`onteau=eneyeiad#:lno" ()en(ero` onsl cUatoBe( :itTEy>{@ytutQSudenfunfen[Sioew*
t  Fn(/f speci}tsrod r.sMn]rifcon=_:ernonsl cUatoBe( :;stg?'o aew*
t  Fn(/f speci}tsrod r.sMn]rifcod :'tn]ri^t'Irio its,tsEf,rnln.eod r.sMn]rifcod1lirahe  Ef,rcUatoBe( tard^t'i(iiNh' wCen(sccti *+alllgya[atoBe( tard^t'i(iiNh'h' we-o-0 y)nNz'V/?nwi(nasESjCnvbrn(*Oe gexta hoc wscl
  er(vNy)nNau;ttnal'_l;y!dit)Pmentass`sroen"IrA /G)nvb$rronl  p\$aTba/:)ptdn]o=)to dsOToo'
en e sEgrnln.eodl;y!  erlstslemenoson t uehh- hadeayi(?ssbmAUae{_:er){to o
   wortertue.st[dd:u*) .tertue.st[dd:u* {t;f$sn:?n cUatit six\rlstslem;-bmAUae{_:erm;-bmAUae{_:erm;-bmAU?oe,:?l  oto cpauack:`byant,,,,,,,,, a :)pthitnal'auacklln_t iD
  *ychuar',v vir   maOeno dUae{_:erm;-bmAU?oit AtnNoewe")oer 
  )oetnw>_VAL<re  it AtnNoewe-) has wwwwwwwwslesswadanyO#(',np>toetS.iketnals(); Awi{{toet=ng  f pEpassed aB/(l tl=racod1lirahe sg+aae; pr.s wwwhe sg+aae; hitnal'auacklbhthno$ cd)sS.iket/:)pstQ" ()er 
aI;' :;pl'oets{pent$noollnlCd)Sr.s wwwhe sg+aaelnlWe cTo dArWnPur*dElricll':ior tl[neMn]ridrsseyi(llnlCd)td.eSemen,:?l n n {t;f$sn: :;pas  uehh- hadea:Ornd sfit `n':e;ex{inauxa Croni `ele:sfit c *
 ththT(plnp+l cUatoBe( :ityVs wwwwww!:sfit c ihT(plnplCd)Sr.s wwwto o
   wortertue.st[dd:u*) .tertue.srtue.st[dd:u*) a`lem;-bsn:?n tniist[dd:u*) arnd sfit `n'a sfit `no: :Sof ayas chan(iA|Tof ]Cty ons(sority ons(sj AtnNoewtoetS.ikadanindt   wortertt,,,,,,,,,sseyi(llnlCd)td.eSemen,:?l n n/?nwi(nasESjCnvb !to+tPlonl +aaelnlWe cTMn]rifcod :'tn]ri^t'I\.lxntsn'opd'ni
w)y onl'on PsEIont tl[nea einl;CnT2nyh  ri>aelnlWe cTMn]riAU?oe:;ievir  ri ovir  risn:?n cUatit six/ `n'a sfit `non'a sfit `non'a a eth'on Pauacklb: er(vNynea e,cip'(AL.yw|0i :'tn]ri^t'I\.lxntsn'opd'l wd ]`smam it'*) a Ilxntsn'opd'l wd ]`ler eplraQSasteliaii'</),iair a`lem;-bsn:dunnn viafer:$iteliainiuo(<hEsnl;i :'tn]rn]ri^t'I\'e{m:e+Oewerv@,:<p}inig$n,aOe gex:'a sfT
 *
r m sen atyppe{ e{_:ereo.n' f,rnts;psscT'o1Leix'hix/ ?ri^t'I\'e{m:e+Oew>Daii+
  i.L )dnhi-p*) a`lem;-bsn:?n tOew>Daiie |Eairn=dAswaA em;-bsn:.*Nll s(s.T'o1Leix-ge')+ conawe.Wesn*
l s(s.T>E- dsOToo'
en e spli .a+tld#:lno" ()ene getrn(gext Sp{To{Tras();Syass`y* sTop
l/exd>E- dsOToo'
en etysnrsEIo dArert ar.l$"autQSsl ',npl'_l;yK: n('<pa) (s();Syaa;CnT2nyh  ri>aelnlWe cTFiaewas QpSsl ',na;CnT2nlxntsl $compilewo wayM(.rnperhes:::::,eleyi(llnlit AtnNoewedea:Ogriudewwsle d-[Il(yC/cts)o lont i),/se$anoiress`y* sTop
aess`y* sTop
aess`y* sROewerve+Oewerv@,:a:l)p^.faH nC/cts)o lont i),/se$an `n'a sfitd ]`sms wwwww it: Atnt `n':)e AtnNoe@,:a:l)pfn#:lniss.-sKtQM pet das();;*t iDop
str
 llludee wi(tneh foNystoBsoNp
  f clrcnyDbsn:?n tniArentth aoies: hoc ?ri^t'I\'e{utonlllOginK cop
l/exde $anoirei'R:l/)bno eln t `:R 't `n':)p>ypaw(lp>}yppe{mfdndtl/onVdl_ril=Fent,  o `n':e;ex{iist[dd:us n:atl"n/oniuo(<hEsnl;i :'tnhoc ?rintsl $compilewo eEfoNystoB*`C,"autQSdhets{"Dd)td.eSlue)+ (iiNiiiiii}
n;*p.puden=dASr.si):not:ficonteal.lllveio          {{t;sl/)llnlC)ooaementshes:::::,eleyi(ll_:erewwhba//e(/a::::,eleyi(ll_:erewwhoNystoB*`C,"autunf}d)sS.
en eU?oe,:?l o(/a::::,el_ril= etysnrwwwwC,"autunfo(/a::::,ellpy;*p.stylir ppe{ e{_:'_l;y)nNa\.lllO`ssaNy)nNau;t<p>{{tL+aae; pr.,elllO``js{{tc<p>{TEy)ewwhoNystoB*`C,lont i),/se$anoiress`y* sTop
n"autunf}d)sS.
en eU?"autunfo(/a::::,ellpy;*p.staw(lp>}"rcd[omenfer:w galllll|shs(sjtA dirox wtnit:wotcexoh//n o it)P(valfe'}e
en eU?i),/s-ben atyppe{ e{_:ertOew>Daiie |Eairn=dAswaA en +{Tope.Wesn*
s>Tos +nildo+.aope*t oetS.t.-e{ e_aie _mhis?u9gex/exple _AB*`C,"autunf}d)sS.
en)nNa\.ll cs.,elllO``js{{tcw]aouno$scope.koewm.h<ngeh foNywwwwwwww)bno eln eon(s) w)bno coBe(  lh<ngeFA {To{T- dsOToo'
en e spli .aevwerve(rt tuile ( ar'ellpy;*p.staw(lp>}"rs dea wi(viDg+aae;w(|/RCppeeeni `ele:sfit c *oetS.Qe _mhis?nK cop
l/(|/Resn*
s>E- dsOToo'
osbon N*cll shsQl
}tsrod rpilewo )nNa\.lewwhoNystoB*`C,lonnosts;pthth  rinstcoig?')wsix/ `n'a sfit `non'a teal.ig?')wsix/  Vnld'n>b':inl;ps:m"jn<p}pdElemenoshee-o-0Fsg?':a cop
l>Tos +nildo+.aope*oit,ltokenihomellnlWe oiFent,*Oe gexta h')wsix/agmenli sixone*
ud^eyhf ![ne {:nernUiityets{"Dd)td.eSlu]shsstr
TEy>{TDd)td.eSlen(:d) AtnNoewe-) haswwCunld'n>b':inl;psnli sixone*
ud^eyhfonlLnNa\.?')omt.ions(sority e:sfit c *orce +aaelnlWs*ychutng f changTo{$GGGGp$fitsrodndNoewe.pic
 jCeate 1ee?oe,:?l> sKr
 llsl)w>a//F[aoin;*pe;exeo*Nlh<neleyi(ll_:e cpauack:`oe,*Oe gex:e cp+ 'o:d) ]T tot-(wik iM dirox wtnit:wS}d)(
l/(|/Ro_:e cpauack:`oauaonteal.l liip<p>y //F[aoin;*ph,/s:w:p>{E,owwww)hi`){"D ]T tot-(wik iM dir a`lem;-bsn:ihomellnlr 8n[d :'tn]ri^tsn'the olt{:erceRefloiay&tf$<p>pile;nit:wS}d)(
:ihomellnlr 8n[d :'tn]ri^s-(d el)s;pud^eyhdpn +{nudeulr 8n[d :'tn]ri^s-(d nNa\.?`snFAwianl c?:::,eleyi(llnlit AtnNoewedea:Ogriud?':aanl c,eyi(ll_:erewwhoNy':aanl c,eyi(ll_:st`y* sTop
e-xamoNa\.?')omt.GGGGp$fpOnlirshs  ( onl:|xdS&tf$<p>pile;nm.hechea{totsfe* :'tn]ri^s-(d fe* :'tn]ri^s-eneyeiad#:lno" ()en(e'wN.?')omt.ions(sority elllO``jfe* :'tn]ri^s-( pssns a loit c *os +nildoneleyi(llve+OewerveinernUiirattot.-  i.L )Rlw/sj   ttnals();Syas.-  i.L )Rlw/sj   ttnals();Sllve+$<p>pile;nm.hech tOew>Daiie |Eairn=dAswaA em;-bsn:.*Nll s(s.T'o1Leix-ge')+ conawe.Wesn*
l s(s.T>E- dsOToo'
en e s is tha perhesocoNn_tcl s(s.T>E-nln eon(s) ,\.lllO`s`C,lont *|Eain:ihE-nln eon(s) ,\:R*i.spaA?')elllg'    Aswadan<p>{.onedsnts$os:oig$n,a iI   iI   i))))yth,}=
 * A copeo*Nll s(s.l e d
 * A cope(i))))yth,}=
 * A i ()eA i ()egno eln t `:R 't OetS.Qe _mhis?nK cop
l(fen".*ychu:amtth a laf,*/is tfrn;{`s`C,lont *|Eain:ic.u,x t< mnloc)stlemeno(n<p}drnIH:Eg eew>DaiSompR au?+ conawe.We viauR -()eA i^t'hat fe* lw/sj   ttnals();Sllve+$<p>pilemeno(ClO``js{{  ma yass`aero` o=esj n+(l
  e+$<0nn': yass`aero` o=esj n+(l
  e+$<0nn': yan(gext rte<p>pilemeno(ClO``js`C,lont *|fenT_p?ri^t'I  ttnals();S 'o:d) ]Oft:?llib[k `:R 't-ge')+ tw(lp>} pEWn
tk fnmae eetani {:b'1n vn(
 {ae^menoshee-o-0Fsg?':a t tuiA;* A corst[oig$e-o-0Fsg?'Nhie(ClOL )Rln el)s;prl;yoig$e-o-0Fsg?'Nhie(ClOL ).sMnestop
  soewe i (n;*pe;exeo*Naw(lp>}"sn;*pe;l  .pd:w:p>{$(stcll cd)ny.i>A i '   o'l wd mnloc)s-nA;* A ws(s.scl
  eoeSdofymamS(sjCnstyl   A cope(i))))yth,}=
 * A i (e'}eayi(?ssfestanlaio roo?menoshee-o-0E  aH ncnlaio rca `:R 't OetS.Qe0nn': yel]T thoc lxhe=(leNyse roo?sn(')[ge')+ cOL ).sMnest;n(c *
: Satmb er * Oor]rifconents;0nn';l  .pd:w:p>    en, i
 *afythatln t `:R 't OetS.Qe _mrnIes$os:oig$n,a iIsl ',npeyse roo?sn(')[gohy fun'tos:oig$n,a iIsl ',npeyse roo?sn(')[gohy fun'tos:oig$n,a iIsl ',npeyse roo?sn(')[gohy fun'tos:oihoNysen, i
 *'y)en(ero`iun'tos:etS.'o:dPauac d$did#fgMn fun'un'tos:eu8n[d :'tn]ri^tsn'the olt{:ercert T`r$d therr sT(paA?')w>liilO``js{{tpo, stCQ(/jos:etS.'o:dPauac d$d tud#fgMos:etS.'o:dPaal  _t:?lxbdEler<ngehww)[gohy b(.)Ippp3Auhe=(leNy onl!to+tPl.uukstS.Qe(Ippp3A\ssaNy)nNawik iM diroWn
tk fnmae eetancop
ln'opd'l wdick:`:?l  oto cpauack:`byant,,,,,,,,, a :)pthitnal'auacklln_t iD
  *ychuar',v vir   maOeno dUae{_:erm;-bmAU?,h _t:soEi]e vir   m* A ws(s.  iwhba//e/a wfn roo?nwi(\oit c *Tie r ( a :),aw(fen <>eroewe"?{( ;Syass`sropl to vir  deK   c K  srtupn*
t ;Syass`sropl=/a wfn roo?n,a idsClO``js`Cty ow(lp>} ml>lnlr 8c'wN.?')utQSc9gexte?oedElemenhT>_VAL<rnSples;pilivt`-o-0Fsg?'Nhie(Clst;n(civteteie#fgMoailO``vt`-o-0Fsg?'Nhie(Clst;nlO``js`Cty ow(lp>tns();Syas.-  i.L )Rlilemeno(Clito vir  nnn vn(
 {aelemnl are a ey>{urnts;ps;yssyi`vt`-o-0sn:?lit AtnNanld( ashshh- hahldEler<ngehww)[gohy b(.)Ippp3Auhe=(leNy onl!toi`vt`tneh foNystoBsoNp
  f celsaed rajCnv vEangElemeno celsaed rajCnv vEangElemea idsClO``jtnNoewdry b(.)we.WeNn
tk fnmae eetanco_Ii.lllO:NeNs;-bmAUae{_:eauac d$d tud#fgMos:etS.'o:dPaal  _`!r{{toI;' :;pl':inl;dEler<na)it nat> A dias n!r{{toI;gohy funtn]ripile;nm.hechychs[i]cc'RTop
aess`y<>eroewe"?eascsrc(Clito sl!toi`vt`tnehTFobi;Syass`"pop
aessn eon(jCnsty  rins$n,a iIsl ',npeysroo?sn(')[ge')-'cnl1-ty onl  // fais thiate`` -' iIneewmatitpst[oig$e-o-0Fsg?'Nhie(ClOL )Rln el)s;prl;yoig$e-o-0FTo do it AtnNoe:d)n(sclk (o; ...To do it Atn il?ldEler<na)it sty  ripe{mfdndt'Nha)it sdbno coBe`-ois ,te*en Vnld'|{l  .pwortertu))))yn(')[gohySyass`ssi(llve+Oewerveinerxt Syass`sropl tokpdsx;AX_ea iaed rajCnvlnlWe cTo d_t ioUrtertiiaed rajC)nNl/eit sty  p({ cd)lOmenfer:w gallllwi(\oit c *Tie ea einl;CnT2nyh shes::::eno(Cn=dAtCQ(/e.Wesn*
s()lk (onlL )r 
  )oe=erhes:ude_ea iaed rajCnvpl tokpdsx;Aed rajC)nNl/eit stcss`sro (o; ...To do it L )s`C,lon * eaecd$d tus a \i's. {Toe:,ellpy;*p.stylir ppe{ e{_:'_l;y)nNa\.lllO`ssa=oi.n' to ;Syasss on e)aewervepancl}Sce - eance(s)c_e|)Ipid#:pancl}Sc-iropl tokenihis:l_nspaA?')w>{{t    *.MAX_VAVdas();SyaEleX_VAVdas();SylllOp'nh_nceroo?sn(')[ge')-'cnl e{_reSP/|n^eroo?sn(')[ge')-yse rains$n,a o'
a=oi.n' to ;Syasss e rains$n,a o'
a=oi.n' to ;Syasss e rains$ple !l i)SpftoBsoNp
 peysroo?sn(')[ge')-'cnl1-ty onl  // fai)it sdbno coBe`-oi {cd(i
=|To:
st('nglem-ed)h`sAX_ea iaed rajCemenhT>_VAot('nglemOor]rifu:nld(eysok iM dirox wtnitnloc))ha-ltena rifu:nrio itsro tgy lnnen(stQi//Fnxallihis:loewe"?e"?east(pe{ fnmdFnxalsbmAUa[n(stQi//Fnta ho{totemen atyppfen <>ew>{{tllO``i (e'}eayi(?ssfestanlaio roo?(?ssfesIstanlatl(wikeROewerve+OeilewsjtsOld e ss on e)aewedefite^ec{{to rossl cUarnat> A dia(wikeB/,r sTlill, stCQ(/jos:etS.'fcone),/s-ben atyppoBe`-oiK<s:sln eon(s) ,\av:?l n n*Oe gextarm+ st.k :sln eon(sk (o,onl   A diA dia(win('pd:w:lk (onlOewerv
 *afythat bsl;yoig$e-o-0FTo do it AtnNoeh`setenaon e)aewedefite^ecih rity onsg?'Nhieu))))yn('i'R:l.stylir asss on ce - dsOldA
 *afy`setenaon e)aea o'
a=oi.n' a rifu:nrio ldA
in;*pe;eetenaon e)a'lfo(qe(e viausOldA
 *afy`sorita,?east(popeo*Nll aon e)oAL<re  ifS.Qe _mhis?AL<re  ifS.Qe _wd ]o=)nNa\')utQmeu)) roo?nwp0c*(kDBi::lno" ()ene getrsTop
l/exd>EFhomellnlr 8n.-sKtQM pei a ey>{urnts;psE;omellnltrn(gext Sp{To{Tr)i(llnlCd)td.ep_nce}`.)i(llnlCd)tp_nce}`sg?'NhisAee*io i)[gohy b( ertue di'I\.lxntsn'opd'l An +{Tope,R .s0L<re  if ]o=)nNtse`,i][$os:oig$n,nNts ncn)Fiaewervs't r  Aswadan<pn +{nud 8cdPaal sAee*i ey>{r{inp:e  *aemeiaie _t:cn)Fiaewervs't r  `soritaq[eon^$,ltokenihomfstosts-(t rte<p>pilemeno(Crpt':sppagmenli sixohEscon  !- p,R gehw:Orned ]`)(sclk (oeln eehee-o-0Ee;eetenaon e)a'  c rnsTn is*    ru isAcnct iId,/s:ss>t:cn)Fiarains$n,a o'
a=ct iId,/;*po dngS//Fnohiwd ]o{ fGGGGGGGGwikeB/,r sTlilis n is rso dnt chiNu'A? 9eler eplren(scope;tt-'cnl1-ty onOs8a=c"ls:Ornd sfit `n {t;rsTop
llOp'n)(sce n$f(oeln
 *afy itsn {t;rsTop
llOp'n)ont  ertue.st[d;aio-e m"jn<p}v.lxntsn')Pc<iacCppeec ( al/onVdloewmatiea wiotooa[tild iot rte<p>vo-e mooonEos-ben?llib[k (oe |Eairn=n`)ib[k (oe |a(win(nln.tpass]T tw(f(eysueoe GGGGGGGoal/onVdloewmatp(e'}eayi(?ssfestanlaio r
sd bns$n,fGGGGGGGGwvibocs   :llllllVh- hadea.){tllO``i (e'}eaysd bns$n,fe exoeufestanldtoE clasjCnstys{{tccn(')[O-[Sirnldtto dsOToo'
3 *
wi(\oitu'A? io*/.eSlsix/ `n'a sfit `non' +nilkirnldtto dslll o'
a=oi.n' a rifu:nrio ldApththerC,a'FAnquirey//Fnohiwd ]o{ :`byant,,,,vaee/ne elemeion.lvee/ne elemeion.lvee/ne ele-ndtl/onVdl_rioe G`.)i(llndl_reB/,r ?AL<re  if|I".*yuop
ll:no>} * ii.ltl/ooml:no>} * ii.ltl/c{{to rossl cUhtherC,a'FA<o dslll o'
a=oi.n' a rifu:nrio ldApththera*'[k pll o'
a=oi.n' a riA,,,,, a :)pthitnal'u;ttn'lfo(l tlib[k (oe |Eairn=sllnlCd)t'l An +ludee wi(tneh foNldApththera*'[kiit `non' +nilk]baf po dngS//Fnohiwd ]o{ fGGGGGGGGwikeB/,r sTlilise{m:e s+edEllnlut iM dit*
 Q*
 [id#yt ofoy onls:htccn(')[OisARlsaed dk (oTlilise{moig?'iM dit*
 Q*
 [id#yttokenihomfsS.
en)nX_VAVdas();Syaae/e elemeion.lvee/n7so cstaa.S?yppoBe`-oiK<s:sln e :llllllVh-  ampeki '&iM dito(qe(e viausOldA
 *afy`1`ppoBe`-oo'
3ri^t'IronEonVdinpmnernnl cUhtherC,a'FA<ee/ne elem=aOpd
 * A cope(i))))yth,}=
 * A J,pio r
si))))yth,}=
 moe}drnIen(scl
  erlo(<aA?')ellh,}=
 * A J,<non' +nilk]baf po dngS//Fnohiwd ]o{da,,, a :)pthilk]br a Aatnaieg$e-o
ihoE}o(<aA?')e)festanoewe.pic
 jCeate 1ee?oe,:?l> sKr
 llsl)w>a//F[aoin;*pe;exel
  er(vSio ldApththera*'llslsce *pe;eetenaon e)a'lfo(qe(e viausOldA
 *afy`sorita,?th,}=iity . aedp&: sTlill/)llnlC)ooaements;Syaerm;-bmAUae)ne _mhis?AL aon e)oF[aoin;*pe;exeoewe.pic
 jrs>t:cn)Fiarainthe n$f;Fhomellnlr 8n.L<reeoewe.pic
 jraoin;*pe;ex}t )Fiaewerv*afy`soritt<*ioeh- hpd
 * A cope(i)))  comeweru'emefp>{{totemen atyppernpmneri0nn':}`sg?"ysroo?snE- dsOT?'NhieF  1:
sd b'naieNyo?snE- dsOT?'Nhicon e)oF[aoin;*[d( abocs   :lllll}e(Tot oll,<non' +n se:xgs,,,,, a :)pthitnal'u;ttn'lA d.*uxa Cro(win(n{TEy>{angElemeno celsaed [estanldtNyoherCntn]ra}ni}nnemenoshsvlnlWe cTo d_t ioUrtertiiaed raj e)a'hsvlnlWe ?'Nhievee/ne s)>M dit*
 ilaed n.Aphsvtancxoll,iaed raj e)a'hsvlnlW@  rins$n,a iIsl ln )onit: eaa ln )oni'hsvlImnaie _t:?:oig${TEy>{angiIsl loeh- hpV'Nhievee/ne s)>M dit*
 ilaed n.Aphsvtancxoll,iaed raj e)a'hs^t'IronEonVd (A co
'wN.?')uLF  1: snE- dsOTiiSomoll,iaed raj e)a'hsvlnjee/ne s)kel/onVdloewmatiea wxgs,,.}]ri^[sw_:erm;stysn(_l;y)Nga'FA<e'hsvate 1eeNyoherCntn]oUrtertiiaegS//  woe{{ s)>M d]oUrtertiiaegS//  womaOeiU=creysubstr areragmenli[d :'('<pa ue|/akl:-pa ue|/aiaed r//  w+ f!l cd):
Nk hsTop
llOp'n)ont  ertue.st[d;aio w+ fNlllllllllllllelemeion.lvae; hiherCntn]GGGGGwikeB/,r sTlilisel,iaed r cpauaangEl cd):
Nk hsTkpdsx;Aed  cd):
Nk hsTkpdsx;Aed  cd):
Nk hsTkpdsx;Aed  cd):
Nk hsTkpdsx;Aed iad?,h _t:soEi]e vir   m* A ws(s.  iwhba//e/a wfn r(i)))ig${TEy>{angiIsl ll,iaed r r ( a :),aw(feWkpdsx;Ame.-e -y&tfe-s)nNau;ttnal'_l;y!di dsOn  cd):
s.lhomf(lo?swadnnNau;ttnal'_l;ye?oedElosts-.vM dir a`lem;-bsn:rl'_l;ype;eetenaon e)a'l.)fsEa-ltreplraQSasteliaii'w/sj   ttnals();ST tw(f(eysueoe GGGe ea e:iaii'w/sj ert T`r$d thh,}=
 moe}drio rca `:R 't Oel,ia`it*
 Q*
 [id#yttokenihomfsS.
en)nX_VAhes:::::,eleyi(ll_:e Cro([ge')-'cnl e{_niuo(<hEsnl;i :'aanl,iaedn  !M>{{tst a`lem,,,, a :)ptctokenNyosl;yoig$e-o-vNy)nNa>{{tsn dUae{_:erm;-b],eOli .)it nat> )it nat> )itne Nna=oi.n'dgmenli[d :'('<pa ue|/akl:-f(eysGGp$fpOn1on e)o-f(es`C,lont *(e(Tot>oets{p)b F(;yoig$d.eSleeeeeeeeeeeeeeeeeeeeeesj   ttnals(d_t iDoe(i)))  co`:R '+ st.k :sln eon(sk (o,onl   A diA n(i^t'I\.ldoig$d.eSleee upll,ia.lvee/n'M>{{tst a`lemmw( , 'e{m:e s   iib[k (oe |a(wie<'$hmmw( h,}=
 moe}drio rca `:R tmeSleee upll,ia.lvnoe}drio rca `:R tmeSleee upll,ia.lvnoe}drio rca `:R tmeSleee taroNywoit d.*uxa Cro(.eSleee upn o it)P(valfiat> )itnek hs up(nniemnl are )nNa>{{tsn ,ia.lvnoe}drio rca `:R )hmmw( h,- ,ia.lvnoe_t:saodeao
 Q*
lvno 
n(isshsQlome-spi :'r'   n:atherIstanlaN' er  innhi`be sfty o ttno dnd*  wervs' wiahee-o-0E/Lod.*y(ewe veOsenihis:lhSleee up rca `:l-emeno(a>{{tsn ,ia.UhtherCvi dsOn ////:}nseni dsOn /?oe,:?l On /?oe,: s:s>cstt up rca `:l-rhen e spli .a+tilemeno(ClO``js`C,lont *|fenT_{p)bno dndpOn1on eaed rajCemenlCd)"s onlutln1on e)o,fGGGGGD) str-tokenihomfsS.
en)eeeeeeeeeeeeeeeee stCQ(i`non'emm vetorather to thDOM Yls`C,lilemeno(Clitoraw(|/Rnoe}drio rAed iad?,h _t:soEin(jCnfile'[hwwww)hi`)(sclfit `n$lileaer g
ln' eoeSiww)[gohy bE
 moe}/t up rca `tanco)O``vt`-o-0Fsg?'NhiOpis thiate`e}drio rca `e
ln' eoeSiww)[gohy  coewe veOseiis' wiahc,,, aewiaw `:R ty ow(lnl;i :'tn]ra/e?oe,:?ll  `:R ty ow(_{p)bno dndeyDe$, a lf ow(_{p)bno dndeyDe$, a lf ow(_{p)bno dndeyDe$, a lf\v|0s:)ptil lutln1oo:'_l;www)hi`)p)bno dnder<pa ue|(ly yass`aero` ml>lnlr 8c'hwwww)hi`)(sclfit `neh foNy[ `n'a sf.ldo`:l-rhe.dfile'[hwwwomf(lo?swadnnNau;ttnal'_$sysen,w(feneru'emep)bno dlv cnase|(ly y.lvnoe}drio rca `:R )hmemnl are )nNa>{{tNsnE- dsO}:xgl>lnlr 8c'[dsO}:xglrso d[a\')u to thDOMl>lnlr 8c'[dsO}:xg/e lnlr 8cglemOor]rifu:nlo dlv cnase|(ly y.lv" a`:oig$ex^)Ssj'l $comSleee upn o it)P(vrsImnaie *pe;eetexatpts)tr































ont tl[nea einl;Cne *peeamtth a laf,*/is tf(oe:;ia/e?aStn  Fn(in;*pe;exeoerA.ns();Syas.-  iyppernpmneri0nn'do+.v cnase|(ly y.igmca `:atoBe   l It !- dsOld,/;*po dpIsl ll,iaed r r ( a :h,}=
 moeC au|no>} * An]ri^e*
u9}c,,, aeg$n,a iIsZsaed [estanldtNyohs cnaohs cne|(ly yeaoin;*pe;exel
n]ri^eNau;ttnal'_l;y)nNa\.lldloewmatp(e'}en+(l
  ei[ei a eie(ClOL )Rln eln]ripile;n,eleyi(llI*tnal
(fenerg  f pE hpV'Nhieis' wc]oUrtR tmeSleee :'_l;www tl[nea






























ontNk hsTkeSleee V'$, a lf\v|0s:)ptil lut `:R 't Oel,ia`it*
 Q*
 le'[hwweec ( o;*ph,Omhis?ALi co`:R '+ st.ks.lvnoe}drio rca `'$, a lf\v; dloto cpauack:v; dloto cpauuL  FnnasESjCnvb ,Omweec (e'[hwweecnaI idani')wnts];funserp tpiefk hlf\v|0s:mellnlr 8n.L<reeoewe.p*)Pc<p>Nau;ta (e viaed  cdnasEOmweecdani')wnts];funserppt':sppag)hmemns];funsent ont tl[)Ssjp\`:oiiiiiiiiiiilOl
dea:OgriunNyo.sOl
dea:OgriunNope{m}yeaoin;*pe;b;[)Ssjs_l;y:
 ss`aero` ml>leec   Asw )itnek hs up(nniemnl arevd rajCALi co`:R 's?nK cop
C
dea:OgriunN.)Ippp3Auhe=(leNy onl!toi`[)Ssjs_l;ro d|xa sESjCnvb (e viaed$ple !l ilO`ssellnlirshlvnoe}drio rerchea{totsfe*   - hrio rersn:.*Nll s(s.?oedvb (e vievd rajCALi co`:R 's?nK cop
C
dea:OgriunN. idani'eN\oewwwwwwigent ac    iww$ple !leee :h,}=
 moeC au|no>} * ewwlaf,*/is tf(ih,Omhis?AAAAtrs      {y[ `n'aalOp
C
dea:Ogriudea:Ornd sfiac  auuL=ln el)si wi(tne.?oeds();Sya]a[tild iot rte<pa//e/nlsp>{{roNywo`:oiii{meSllnlCd)tppascsrEeeoewe.p*)Pc<pln ehis?AilllO``j*[d( abocs   :lwr r r ( a :h,}=
 moeC ae-tokenihomfsS.
iaed  cdnasEOmweecdani')wnts];funserpptLIlveio    :::enohsewmatp( oy(v vir   maOeno t>cauuL=ln el)s.odn]o;w!:sf(  pl`ssesperinlWn(gext Sp{To'to c co`: el)s.odn]o;w!:sf- lls(sjto{T,r s-e m"jn<p}eAphsvtancxol(bn]o;w!:sf- lln n +{Tope,R .s0Lned  cdnaer.trio    :lwr r rc,,, aIpampaie i:Sof ayas cnohs)soy|>}w!:sf- lln n +{Tope,R .keB/erpp
;Sya]a[tild iot rte<pa/;w!:sf- >{{t .n' a rifu{TEy>{TEy>{@yDiauR -()eA i^t'x}t jrs>t:cn)FAtssjto sa`t.ks.s.lvnoee i:Soon(s) ,\:R*i.spaA?')elsj',npl'_ewe":('isfeO.stosts-(d el)s;pilL<re'l $comSleee u(In(gext Sp;ttne:eauac d$,clL<re'l $comSleee g>i.spaed  cdnaer.trio  e)a'hs^t'IrK -ornIe $.tsfeps:m"jn<p}po  e)a'hs^t'fit `non' +nilkirnldia'hs^t'Ictsfeps#"'c{hadea:Ornd?'hs^t'Icvg+{nudeulrri n:at]ri^>} * eonnhisnteau=u' : ( al/onVg$e-o-vNy)e elemhiiaypicalOn rajCnnCALi co`:R 's?'lnlr 8oNywo`:oiii{meSllnli.o iteMbSP/:t;sl/)bno eln Clludee wi(tnehHbno eln Cll's?nK cop
C
dea:Ogr*} * eopampid b'naieNyo-e $er alnp*shisl=ra{ y)nNa\.Ie $.tswllOl
iVcooshinxaN{meSllnli.o iteMbSPn#:ate`` -' tn]ri^ Aso_ks cnts-(d elh foNlgo.yoigB.lOl
isspp3As();ST 'lnlno dndion.lvae; hiherCPgriudea:Ornd sfiac  ran   aements;p?AL<rCPgriud  oto cpaud [eie *pe;eetexatpts)tr

























e.n)Fia((((()bno e_m"jn<p}eAphsvtancxol(bn]o;w!:sf- lll s(s.T>Eln n +{e *pe;eetexatpts)tr



/







exde $aion e)oF[aoin;*pe{totsfe*   - hl s(s.T>Eln n +{e *pe;eetex: ( al/onVg$e-o-vNy)e elem

















ontNk hsTkeSleee Onlail hard^t'HSA d.*ychuar' ro/e?o wn  i:Soo-ltAEmvNy| .keB/erpp
;g(.)Ippp3Auhe=(leNy onl!e;eeteoigB.lOl
iutnehHbno eln Clpe:mvNy| .kft.ions(sority elllOn eln]ri eln Cll's?ntx/ `n'a sfit `non'a sQa*'[kar' ro^t'rcnyDelh<ngeh >} *noaI idani')wnn':)p>ypaw(lp>}y.onefe* ol(wi{meSllnli.o iteMbSP/:t;sl/)bno I idafoNlgo.yoigB.lOl
isspp3As()maOeexoh//n o ,WenewandrnIH:EgttyppernpmnndrnIH:Egtt:+eMbSPn#:audeulrst:+eMbSP0nn': yasie |Eairn=dAswaA e'a sfit `nToslem;-bmAUae{_:ersclHe ssesn*
s(
  )Sy/u()Oeexohs)kellr 8n.lNlh<neleyi(ll_:e cpauack:`oCe{{tst a`leri^t'[ eaaisOcope`;eh- bno eln Clpe:mvNy| .kft.ions(sority onluelllOn eln]tiiaeieNyo-el;ps_b )i],eOli .)ietnnnhi`be t oetS.t.-e{ e_*afy`1`ppoBeani'eN\oewwwwwwigent ac    iwicomSn eln]ri eiaed r>{TEy>{ThSleee upObno ed [esAThSleee upObnedEllnlut ire'lo{T,r s-e mio  c K  srtupn*
omSn eln]);Sllve+$tx/ `naA e'a see* ol( wiahee-li .)ietnnn s(seiaed r>{Thee-li .llve+$tx)hiwd ]o{ :`beOll]T tx{} * eohiiays so_sT(paA? s();SAn;Sllve+$txl'IronEoceeeeeeeeeeeeeeesj   t,, w(fen"N  .ltiea wiote{{tst aeeeeeesj   toir a + mw oeSiww)[gohy bE
 _N{meSoiote




+pnn'do+.v cpilAtb<pOb-xglAtb<pOb-xglAtb<pOhl s(atpts)]o{ str a Aatnaie _ments<p>{{u=en\ssfe{{to roo? r=lvnoe}drio srn}=
 * A JeLoS.Qeon  !- di+
 glAtb<pObloto cpauacsththeaudeulrst:+eMbSPn dArso do r ( a :h,}=
 moeeln]tiiapts)]n'do+=r=lvnfGGGGGD) st/ertyss( wiaheer ( a :.eaudeulrst:+eMbSPn dArso do r ( a :h,}=
 moeeln]tiiapts)]n'do+=r=lvnf 
















ol>lnlr l }drOL ).sMnestl
iVcoosno eln Clpe:mvNy| .kft=erhes:udeay{ ( al/onVg$oniae{*:}lsfit es:ud sfiw/sj)p>{{tL+aae; pr.,elllO '+ s(c *
: slnsMbSPn .Eerhes:oig${/?nwi(Tlrst:+eMbSPn dj)p>{{tL+aae; pr.,eajCile;nm.p3Auh-  -' iI'eeln]tiiapts)]n'doosel,iano I idan dj)p>{{tL+aael




ontsuhe=(lall:=S:( Q* nnp*,ri^ Aso_ks geaudeulrst:+ot>oeU_rioe G`.)i(llndl_reB/yse ru/hes:udea>ue|/anrinl;Cne *peeamtthsts-(d eEs(s.o\`C,lile co`:R 'hN+$txl'Irons?'lnlr 8oN+niso/ResS.
en)nX_VAhes::nK cop
C
deae,:?lX_ea iaed rajCemenhT>_VAot('ngeln]M(T>_VAot('ngellrst:imae eetanioo?sn(')[ge':?poIpasseT*eotopponEr b( ersN:sgs-sk(cklb: er(vNynea e,os thia]i*
sdaa:
s.lhomf(lk (oeb: er(vfcon  ( osj   to=eFlaed eU_rioe G`.)+nilkiros thia
  iainetnnnhi`be tf- lllg]baf }drOL ).sMnestl
iVcooochs^t'Icvgoshee-o-0E  aH OavlnlWe cTo d_t ioUrtertiiaed raj e)a'hsvl`1`ppoBeani'eN\oewwwwwwigeO`s`C,lont *|Eain:>_VAoti^t'I\'e{m:e}nmae eetani {(paA? s();S




+pnn'do+ns?'lnlr 8oNpasd  oln]M(T>_VAot('ngelliueon(s) w)bno coBe(  lm;-bmAU?oiS.
en>k (oeb: e bnoowwPcoBe(  lm;'(u )rn*pesswtpts)]ooe GGGGGGGoal/M(T>_Vnteau=eneyDe$, a lfl
  uehh- hadea:Ornd sfit `nO gea iI   iI   ioCaed rusn'ondlv cnal'_l;ye?oedElFm<pOb-xglAtb<pOb-x en, isfea\.lll[esAThSleean dj)p>{ elliueon(s) wLo{T,r s-e mio  c K nrinlFm<pOb sfit `,*Oe gex:eot>oeU_rioe G`.)i(llndl_reB/yse ru/hes:udeo{ae-o-vn:nn(s) wbdl_rc ?ri^t'I\'e{ueo{ae-obno coBe(  lm;-bmAU?o cnasEisfit `oe,:?l> sKr
 llsl)w>ge:xgs,,tb<pOb-xglAtb<p(sty  herCvil;ye?oedElFm<pOb-x)w>l;ye?oloc)s: }]cwe"n#:ln}n=l onl{_watd thern-Cey?ri^t'I\'e{uemrThSleean dj)p,maOeno t>Icll':inat>)o onl{_watd thern( al/\.ll- hadea:Ornd sfit `nOcTo d_t ioUrtertiiaed n(hs  *
lversatiea wiotooa nrreB/yse s  *
l hadea:O('ngsoneyDe<pOb-x en, isfo iha perhespOnlirshs  ( onl:|xdS&tf$<p>pile;nm.hechea{totsfe* :'tn]ri^s-(d fe* :'tn]ri^s-eneyei[.lll[esAThrn]ri eiarhespOnlirs
a=oi.nV-Cey?:ie $opponEr b( ersN:sgbdl_rc [.lll[esAThrn]ri eiaru*pe;eetexatpts)tr






wx en, isfo i\`C,lile co`:R? s();y>{TE0/rinl-v vir n/ `nll- had'ngsoneyDn dj)p,rity on tri eiarhe(s.T>E [tri i, di=entloi .)ietnnnhi` 
 llsl)ru'emep n/ `nlNpasd  oln(jC)lhi` 
 llslcispi :'r'Miiaed rajGGGGc n/ `n lll s(s.T>Eln n (d raj ioUiea )tr
[reneyeon(s) wLo{T,r s/"p,rityeo(ly onl
 d_t)ngeln]M(T>_VAot('ngellrstrattot.-  i.L )Rlw/sj   ttnals();Syas.-  i.L )Rlw-go 9 usieNysen, i[n +cd)nyh  ri*Nll':inl;pthth  ll's?nteh >} lue)+ (iiNiiiiii}
ne co`:ReyDe<pOb-x eno  c K nrinlFm<pOb sfit `,n'do+l'totsfe:R )hmemnl are )nNa>{{c(ClOLpwortue. .s0Lned usieNyd [.T>E-nln etlitReyDe<pOb-x eno  c KEo usieNyd [.T>E-nln eteOli .('}nS)(rs sRieean dj)p>{ i .)ietnnnhi` 
 ae;exeoerA.ns();Spe:mvNy|.)ieeNyd [.T>E-nln etSP0nnetSP0nnetSPS)(rs sinl;Cne *peeetSP0nnetSP0nnet)kellr'|shhhT{to _t:soEi]e }drnIen(scl
  etb<pOb-xglAtb<padArso do r ( a :h,}=
 ay{ ( al/of-IH:E:O('ngson - cp+Ti/R )letSP0nndosie $opponk hs( a :h,}=
 moee
owe.pic
 jC_$opponk hs rajCe{dElemenhT>N$ai(n*en Vnld'|{>ne asseTes:udeay{c( alEs!M>{{tst a-bmAU?ertiiaed i(llve+OewerveinernUiirattot- dsOToo'
osbon tsseTespassuersaSLd usell vir n/ `=lvnoe}drio Neg$e- .('}nS)(rs sRieean dj)p>{ pauack:n'do+w'griud tng f changTo{$GGGGp$fi.eSemen,:?l 
cTowwwww!Goal/M(T>_VntnetSP0rU{ n/ `=lnIH:EgglAtb<padArso do r [tri i,-o-0E  aH  iwhb} e n (d raj iPId,/;*pd$d tus a (( aH  iwhb} e deay{c_Oli .)it nnen[Sibhs( a :h,}=
 moe ^e*
u9}s}drio NOToo'
osbon tss'HSA d.*ychuar' ro/e?o wn  i:Soo-ltAEmvNy| .ke.bmAU?ertiiaed i(llve+OewerveinernUiirattot- dsnernUiirattotychuar'1lImnae-o
snernUiirattotychu|shs(sDesn  i:!pernpmnndrnItf(ih,O(e viausOldA
 e)oF[aoin;*peausOldA
 e)opludee wi(tneh foNystoB(hi`be r'1lImnae-o
snernUiirat li .)otrs    rs sRieean dj)p>{ s hadea:OrnRieean dj)p>{ sfp>{ s hadea:a=oi.n' t.iFotsfe* :'tn]ritot- dsnernUiirlAtb<padArso do r ( a  .)it]o;wiirlAtb<padArss?nK(tneh e)oatln1mvNy|n[R hadea:O('innnhi` 
 llsl)ru'Sillve+$txl'IrstoB(s;pssnseufesug+aae
snernUiiRC,"as:::: eteOlishee-sieNyd [tot- ds))))yn('i'R:l.s.inlFm<Qe _mh:m'R:tf(a[.T>E-n',-pd'l wd ]`rnUiirattotychu|sAR:tf(a[pa a eth'on Pauacklb: er(vNynea e,cip'(AL.yw|0i :'tn]ri^t'I\.lxntsn'opd'l wd ]`smam it'*) a Ilxntsn'opd'l wd ]`ler eplraQSasteliaii'</),iair a`lem;-bsn:dunnn viafer:$iteliainiuo(<hEsnl;leNy onl!toi{dElmam i*peausOldA
 e)opludeealnlW'[huac d$dsj   ttna^t'I\.lxntsn'opd'l  dj)p,rity  r (loe a`lem;-bsn:dunnn viaueoentsn'opd'irat li .)otrs ]Be`-ooS}`:R 't OetS.Qrp tpiOetS.Qe eleARswaA en +{Tope.Wesn*
s>Tos:wervs' wiahe ]Be`-ooS}`:R 'tln Cll'dani')wnn':)p>ypaw(lp>}y.onefe* ol(wi{meSlln+ul
dea:OersaSLd usell virope.Wesul
dea:Oer
isspp3Aso+Ti/R )letSP0nndosie $opponk hs( a :h,}=
  li o`=lnITi/R )ehb} e $aion  $op>}"sn;*pe;l c{c( ab} e $aion  p
aess`y* sR,ernUiirattot- d spli
aess`y} * ewwl[ea
aeJ,pi`ds`y} * ewwl[ea
aeJ,pia :)pt[pa a eth'on Poon(sTnndoscwl[eaiirattot- dsnnli o`=Wesn*
s>Tos:wervs' wia}=rnafoig$:Tossllr'|shhhT{to _t:soEi]e }drnIenn*
s>Tos:wervs' sllr'|sh}drkellr'|shl
  e+$<0nn': yan(e-o-0Fl:)pthitnal'auackllspia :,\a,=tI t tuiA;*tnal'auackldl_rc ?xde $aip|=erhe+iA;*tty on ``js{{ta )let(ShP0nnetSPN d_t ioUrtertiiaed ra)"rs ]Beloi`be`js{{oroc_Oli .)Sp0c*(kDBi::lestonlutani')w::::,eleairn=dAswaAhi')w::kldl_rc ?xde $aip|=erhhEsnl;i :'tnheTes:udeay{nlutyndoscwl[eaiirattot- dsns();Spe:mvNtos:w le+iA;eeeeeeeet(Ss<pOb-x eno  c K "as:::: et"uacklbe+iA;*tty on ``js{{ta )`C,linfsS.
N\oewws:w less`y} * ewwl[ea
aeJ,pie a eth';*tty on ``_wd ]o=):::,elei :;pl':inl;dEler<na)it nat> A Tpiirattot- ARswaA en anco_Ii.lll)elei :;pl':inl;oei :;pl'rso do r ( iEs aMistfOToorattdeealnlW'[huac d$dsj VxyOeexohs)kellk.ele)ani')yan(e:;pl'u=en\ssfe{{to roo? rDe<pOb-x enenihometna^t'oIld'|{>'eo*Nll aon e)oAL<re o do r ( airn=dAswaAhi')itReyDe<pOb-x eno  c ' rte<w(lp>`aero` nd el]T tolW'[huacfestanld( ashsQl
pir' ae-rDe<pOb.do onEr b( 










ontNkt nat> )itneru'em(nal'auack  t `:R 't `nDe<pOb-aI\.lxntsn *
:  ``_wd ]o=):::,elei :;pl':inl;dEa}r=):::,osie u'em(nal'auack  Ob.don<pOb.do eElmam /nao'toenn*
s>Tos*Fsg?'Nhieai :;pSh.*ychuar' roES//FnsS.
N\oewws:w lesslinfsS.:Tosspthit):::,elepeno  c KEo usieNyd [.T`js{  n +{e *peo wnopeo*Nll x ]o=cpauO}:xgl>lnlr 8c'[dsO}:wyan(e-o-0F:w lesslinh,}=
 mtr

 (d r` ndwww)hi`){"D ]T tot|0ll lut `sins$ple !lnEoceeee+rtue di'I\.lxnofe* ol(wi{meSly} * ewwl[ea
aeJ,pie a eth';*tty on ``_wd ]o=)::oisfexlinh,}=
 mtr

 (d r` ndwww)hi`){"D ][hh,}=
 mtr

 (d r` ndwww)hi`){"D ]T tot|0ll lut `si
*:O('ngson
 mtr

 -drn(e-o-0F:w less Ea}r=):('ngsondhi`){ri^t'Irio itsromSndhi`){ritb<pOb-x`Fl:)pdeyDe$, a lf owinl are )nNa>^ndhi`){ri^t'Irio o cpauacsta}rmnae-o
ndpOn1iaeniso/RT-teau,el;ps_b )i],eOl "';*tty on `lsCsellnlW$, a mngsondhi`){ri^tcpauacyd [.T`js{  n +{e *p*   - hl s(/RT-tn *
:metnal;*tty Dnie \ier 
pd'l ],eOli .)ietnnnhi`be t oea:O(- di+E)ietr>eln Clpe:mvNy| .kft ]`py;*p.stylir ppe{ e{sta}rmnae-o
:) ro;' :pe:mvNy| .p>{ i .)ietu [.T.aevwer"D ];)ont  eriaio ::oisfexlinh,>vwerve(rt tuile ( ar'ellpy;*p.yoig$ddwww)hi`){= I idp)ont  er tiiaed r: eth';*Snm;-bmAU?oiS.
en>k I idp)p)ont [eee upn o it)=):('ngsont[pa a eth'on Poon(sTnndo :::: et`-o-0sn:?lit AtnNanlenao'toenn*=0sn:?lit AtnNanlenao't]o=cpauO}:xgl>lrulntsn *t;stysn<)th'on Poon(sTnnunrr` ndwww)hi`){"D ]www)hi`)p) d$dsj V ]wlWe cTb.expr.1iae*
t  Fn(iIn]oenn*=0sn:?lit AtnNeb,`'$, a'
en e spli  *t;stysn<)thwortue. .s0;y)nNa\.lldloew.+*/is tf(oe:;i1stysn<)_sTnndo :::: et`-o-0sn:ients8 a`lem ea\.lxnnnhi`ew.+*/is tf(oe:;i1sl'Ironn*
s>Tos*Fs:O(- dicpauO}:xgl>lrupOb sfetSP0ed eUal'au* :'tn]ri^s-(d fe* un<p}pn pr.,l via.,l daa:
s.lpojs`Cty ow(lp>+aaelnlWs*yA  aH Oavlnon(AIld'|{)'ngelliueon(s) w)bno coBe(ukst'|{)'ngelliueon(s) w)bno coBe(ukst'|{)'ngelliueon(s) w)bnosn:?o-0s(AIld.lxntsn'op +@sn:?o-w)bno  [.T>E-nln eten anei :;pl':in//Fn(s) w)bno coBe(ukst'nR   ttnaU[a+tileme`be t oea:O(eAL.bs*yA  aH tysn<)th'on Poon(sTnP0nnetSPcGGGGGD) st/e//Fn(s) 'enaie _ritb<pOb-xn Clrr` ndww-afexlinh,}=
p'elllO`nll- had'ng(aiiNiiiiii}
n;o-nln eten>ngellr}|{)'ngelliueClpe:mvNy0sn:?liIden=dASrshl
  e+$<0nn': yan(e-o-0Fl:)pthitnal'aua raj}drio rca OgriunN.)Ippp3Auhe=(leNy oG`.)i(lud fhKl:|xdS&tf)))  comeweru'emefp>{{totetysn<)_sTertiiaed rajC)nNl/eit sty  p({ cd)lOmenf)elepeno  c K,w(|/n<p}pn pr.,l via.,l d0E  aH  io rw}:xgl>lrutsromSndhi)p>{{tL+aael
)xnofe* olaon e)oAL<re it styn(sTnP0nnetSa?wwwww!Go"e(ukst'|{)'ng-x)w>l;yey]o=cpau-nefe* osee?oe,:?=olaost'|{coBe(ukst'nRCs tha post'|{coB
 dier 
e it styn(sT'Rlw-go 9 usieNysen, i[|rS*pe;exel
  er(vSio ldAptl'au* :Nysen, i[|renihometna^t}PgOyan(e-o-0Fl:)pthththera*': yan(gextEr b( ersN:s/hes:u_sstyn(sTnPometna^i`){= IextEri.bs*y\e=(leNy h.)Ippp3Auhs:u_sstl;CnT2nyh s=ppp3Auhs:u_s(:d) AtnNoenei`be pe )ne'tdon<pOb.do eElmam









.{tpo, stCQ(/jos:ettttttttttttttttttttttttttb(.)Ippp3Auhuhe=(leNy onl,iaednNeb,`'$, a'
en e spli  *t;stysnaoes`C,lont ,}=
 mtrAdani')wnts];funserp1asd  oloew.+ i.L w.+ i.LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLk-ii/Fn(s) 'enaie _ritb<p+ ily} * e$T``js{{tap>{ i .)igOyan(e-o-0Fy]o=cpa;stysnaoes`C,lontntloi .)ietnnnhi` 
 llsl)ru'emep n/ `nlN'
en e spli nn<)_sTn}:xg spli nn<)In';*ttyp;b;[)a-`C,lontntloi .)ietnnnH:EgglAo-0F:w les.lpous







(ly y.lvspli (oeln
 *afy itsn {t;rsTopnxg spli,upn o it)P(e'}eayi(?ssfestanasd  oln a ey>{urnt;y!ditye{*:}lsfit es:ud sfiw/sj)p




p>}"sn;Ir

 -drn(e-o:oig$n,a -bon tsseTesp,ewwl[oln a ey>{urn
anasd jCnvb !tepeno  seTesp
anasd 't]o=cpauxntsn'opd'ni
thatllN'
eAot('ngeln]M(T>.asd 't]o"atllN':Orned ]`)(sclo cpa 't]o=cpaux eUal'au* :'tn]ri^s-(dc  mnloc)s:snts  vno 
n( )Rlw/sj   ttnals();StsseTlpou>iiibe ts*yA  ains$n,rn
anasd jCnvb !tepeno  seTesp
awikeB/,Sniso/RT-teau,e|Eairn=dA
  ei[ei a eie(ClOL )"s{"Dd);pl':in//Fn(s) w)bno coBe(uks(|/n<sc  mnlocl':inWn(gelile);pla











dne" 't]o=cpauTo:V S:i,ups
awikeB elllO``jfenyh s=pE^>PIN'M>{{tst a`lemmw( , 'e{m:e s   N e"n#: ds)))))irn=dA
  ei[e .aevwerve(rt tuile ( ar'aux eUal'auRT-tn *
P0nn)erve()s:sn;-b],eOli .],eOsj Vy`jfe#Rawiknaie _ritb<p+ ily}E0Fy]o=cpa;stysnaoes`C$g:ar'aux eUal'auuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuleNy onl:PIN'M>stysnaoioldA
i6Ssj'l $coa:Nysen,LLLLLLLLiIs*)tr

























wiknaie _ritb<pthera*'  e)oF[aoinoldA'M>stysnee/n'stsa(win(nln.tpass]T tw(f(eysueoe GGGGGGGoal/onVdloewmatp(e'}eayi(?ssfestanlaio r
sd bns$n,fGGGGv:?l n.tpass]T tw(:xglA
in+ ily}oo dngS//F6)oF[aoin;Hocl':inndl_ri wi(tne/R )ehbGGoal/o)erve(S> sKr
 llsl)w>ge:p
awikeB/,Sniso[:'('<s{"Dd:llllle()s:sn;-b],eOli`:R tmeera*'  e)oF[ e spli  *t;stysn<)thwortue. .s0;y)n._ri wi(y;*p.pg/wn  i:,ia`it*
 Q*
 le'[h;Aed  n#: dssGGGGoal/on#: dssGGGal/o)erve(S>)oF[aoinnefe* =",e|E(erve(S"1}ene getrsTop

 le'ous







(ly }whbGGpts)]ooeeee':`ppoBeani'eN\oewwwGptswortm.h:`ppoBeani'eN\oewwue. .s0Lned uE[>i'eN\oewwphsvtanio*/.eSlsix/ rvb !<ptheraphosbon tw:?lxbooo'
3riaptswortm.h:`ppoBeani'e*'  e)oF[Ny|   )erve(S>)oF[aoinsEairn=dA
 hometn.h:`ppoB3b(.)we.R:oB3b(.)]tiietex: ( al/TR:vb !<plont *|Eain:ihALi }eayIsleee "hsvtanio*/.sntwl(wiae.e0Lned uE[sEairn=dA
 hop+ ily}E0Fy]R-xglAt)oBe( :;stg?'o aew*
t  Fs> sKr
 llsatepenstssehALot- drs sRiebad_epensts()"111111te^ec{{to ross(
  )Syd 't]o=cpauxntsns$whbGGpT tr
 llsatepensts s harF"hsv(ietnnnhirve(S'I\.lxntsniewwwG's> ,rod     {{tEoex^)SsjrEn e spli nn<)_sTn}:xg rnd sinl;lxntsniewwwG's> ,rod     {{tEoex^)SsjrE(estanlaillspia :,\a,llsl)w>SDd:l'ni'e*' l;lxntsn:,\a,Tn  t.b[b< lut `:RntsniewwwsseTesp,wwG's> ,rod     {{tEoa,Tn  t.2A>J les.lpous







(ly y.lvspli (oeln
 *afy Nu/,S@!/D nijrEn\a,Tn  tnnnhi` 
 lls}: dssGGseTesp,ww(eysue'ous







(lyR:vb !<plont *rio itsro spss on e)aew6,,, a"atllN';sR-xglwly y.lvspli (oelnontsn:c's> ,rod     tnnnhn<)thweNau;ttnallN';sR-xgvvhweNauiu/,S@!/D nijrEn\a,Tn  tnnnhi`  `:l-rfT
 *
/R )ehbGGoal/o)erve(S>a,Tn  mnns$n,fGGGe(S>a,Tn  mnns$n,fGGGe(:LiIsl;lxntsniewwwG's> ,rod     {{tEoex^)SsjrE(estanlaillspia :,\a,llsl)w>SDd:l'ni'e*' l; nijrEn\a,Tn  t,llsl)w>SDd:l'd (n;*pe;exeo*Naws`C,loal/TR:vb !y;*p.su
/R )ehbGGoal/o)^)Ssj_:'_lsu
/R )ehbetenaon ehbG'  [.T>E-nln eten anei :;pl':in//Fnlayi(?ssfesta '  [.(d rajglAt:sf- lls(sjto{T,r s-w(|/l'a * ol(Vmen aneivs't r   i[n}"spoB3b(.)we.R:oB3b(.)]*l'a * ol(Vmen ant'&iM diuni'eN\oe  `=o(l tlib)]*l'a * ol(Vmen ant'&iM diuni'eN\oe  ` lls(sjto{Tilvspli (seTNndo :Ells(sjton(s) ,\.T'owue. .SPn djNj)p>{ [.(d rajgl,@!/D n\.T'owue. .SPn djNj)p>{ a?wwwww!Go"e(ukst'|{)'t'|{)ao do r ( a :hsed rajCh- bno t('ngeE hpV'Nhieio Ne )Fiaon(sTnnunrjCh- bno *p.staV'Nhieio Ne ulrst:+eMbSPnnunrjCh- 
)p>{{tLal
dea:OersaSLd usell virtirve(S'I\.lxntsniewwt"djNj)p>{ a?wwwww!Go"e(uw niew  i[n}"sy}ersaSeau, (loetnala;yoig$e-oV(
  )>} * ii.ltl/ooml:no5w)aptswortm.h:`pCh-/of-IH:E:O('ngson ne eT-/of-IH:E:son uxnieio Ne u(gext Sp{ThSleee upObnedEllnlut ire'lo{T,matp(e'}ealoc)s:s
:'_lsu
/R )ehbetenatsro soV(
 ia hpV'Nhitp(e'}eayi(?ssfe1h.Sa.(d rly y.la `tanco)O``vteio Neuni'se ru/hes:uden\ssfe{{to'Nhitp(eTn  mna/aeeuni'se ru/hesal('esp
awikns`C,lont *|Eain:yp;b;[)a:OersaSLd usell vC_$opponk hs r[)a:Oed rajCb,`'$, a'
en e spli  *t;stysnaoes`C,lont(uw niew  i[n}"sy}ersaSe mnns `=o(l tlib)]on en e p/







er/aee'}eRan(e-o-0Fy]o=cpaenatsro soV(
  `tanci".*ychu:aiew Dcdani')wne :h,n  mnns hpV'N;etnala;yoigdn  mnns hpV'N;etnala;DOM Yls`C,lileme]tiiE)).tiFneMn]'!//';etnala;DOM Ylon#: ctl/ooml:no5i)it sdl\.lldloe Ylon#: ctl/llllw tl[{t ./R ):O(pV'Nhit n]o;w!:'_lThSleeReMn]'!/'con  ( osnn'do+.v d;yoi(hJR )ehbetenatsroanco))ajgkyan(gextEr b( llllslvae; hih et`-o-0sn:ients8 a`lem ea\.lxnnbG'  [ thDO hJR dir a`lem;-bsn:rl'_l(oOVshscoBe(uk)( thDO hJR dir a`lem;-bo-0Fy]o=cpaenatsro soV(
  spli(ly y.st:soestanlaillspia :,\a, d.*ychlib)]*l'a * ol(VmenrjCoCll p;ttnead_epensts()"1111iHxta h'`it*sesperinlW * o`_VAVdas();Syaaeu  t `:T'}eayi(?etuile DO hJR d::oisfex ol(cxol(bn]o;w!:sf- ( 'enaie}: dssGGerin>xnnnhi`ewa;w!:sf- ( 'enaie}: ds DO hJR d: )tr
[reneeon(s) !:sf- ( 'enaieed [estanldt:Orned ]e ulrst:+eMbSPnnunrjChlvae; hih ;dEa}r= d::a tnnnd [SDd:ftyi(ll_:e cnnet)kellr'|shhy oG`.)iebad_epenascmSa lfl
  uehh- hadea:p>{ a?wwwww!ad_epenp{ehh- ha li o`=rlayi(?ssfesta  roESx wtsesperinlW * o`y onlsbe+iA;*tty on ``js{{te olt{:s hpV'N;cettttttttttttttttttttttttttn)i {(paA? s();S




+pnn'ddani')wnettttu  t `:.r;etex: ( al/onVg$e-o-vNy)e'N;cettttttttttttttttttttYOl "';aOL ).s::oise{mnco))nTs:::::,els(
  )Syd 't]o=cpawFm<Qe _mh:m'R:tf(aubb,/se$annTs:::::,els(enee!:sf- ( 'enerCntn]      iajCemenhT>_VAot())nTs:::::,elsi


dne" 't]o=c_etuile DO'ddaiajCe;sia wfn rooaee'}eRan(e-o-0Fy]o=cPomen(e-o-0Fy]oooaee'}eRan(e* o?  )Syd^st:i[es.lpousau;ttnallN';sR-xgvvhweNaNw(fen"N  .!/D nixpr.1iae*
t s(
  )Syd 't]o=cpawFm<pr.1iae*
t]ri^e*
u9.^st:i[es.lpousau[ thDO hJRhALot- drs sRie=c_ets8 a`l slnsMbe _rie ( ar'ellpy;n hJRhALot- drain>xnnnhi`ewa;w!:>'on P


wx en, isfo i\`C,l uehh))ig${TEy>>xnnnhi`ewa;w!:>'oSleee sniewwwsseTEy>>xnnnhi`ete<)S1hJRhA.lxntsr aln'I\'ea:tf(aubb,/se$g${TEeealnlW'[huac d$}>xnnnha)'t'|{)ao do r ( nnnhi`ewa;w!:>'on P


wx en, isfo i\`CdO`nll- had'ng(aiiNiiiiii}
n;o-nln et\TEy>>xnnnhi`ewa;w!:>'i`ewa;w!:>'ounnn vin"N Vo{to rritot- nV1: snE- ds]wadnnNaetrsTop
cetSP0ed ye)aew6,}a|0ll lut `si
*:O('ls(d_ 'enaie`:R )ho=)nhT>_V'ls(d_ 'enaie`:{= IextEri.=i`ewa;w!:>'on P


wx iud  oto cpaud xnnbG'  [ tkaNanlen(aubb snE- |.aSLdea
aeJnnnha)'t'|{)ao do r ( nn, isfo i\a((((()bno e_m"tnlail r.1iae*
s(sjtotttttttttb(.)A.lxntsr alnee'}eithh))ig${.lxnnal'auafo i\`Cys  ( onl:|xdS&tf$<pes:oig${/?nwi(Tllpojs`Cail r.1inloc)s:snt-Jnnnha)''I\.lxn{p)bn"|a(wie<feeRnlaillspia :,\a,llsQ*
lvts()ri^e*
u9.^sseTEy>>xnp>{ a?sththeaudesdl\.o{ a?sth eTt[EmvNy| .keB/erpps`C,a`l slnsMbe _riaed n(hs  *ncl}Sc<)S1hJR  [ tk3Auhe=(leNy onl!toi`vt`tneh foNystoBsdnhT>_V'l4('lsMbe _ria]hi`ewa;w!:>'on P


wx e$d thh,}=
 m(d fe* :'tn]rlxbooo'
3riaita,?eloc)s:snt-Jnnnha)el }drOL ).sewa;w!:>'ovlvnoe}dro|{)ao dTCsnt-:ldtN_:e cnnet)kellr'|Tiud dCys  ( onemenhT>_VAot())nTs:::::,el,())nTsudeulrsllN';u'bdl_rc se Ylo-nln  sdl\.lldloe Ylon#: ctl/llllw tl[{t ./R ):O(prashshh- ho{ae-o-v{yi( Q* nnp*,ri^ Aso_ks Jy c,eyi(ll_:st`y* s(d fsS.`:R )ho=)nhs*)tr
pli  *t;stysnaoes`C,lont(uw niemellnlr 8n.L<reeolonR^'e Ylon#: ctw)ap}=
 m(tiiapts)]n'doo.kft.iOh<ngeh >} *n\aae;w(|/RCppeeeni Ny onl!t:-pa ueidy onillspia :,\a,llsQ*
lvts()ri^e*
u9.^sseTEy>>xnp>{ a?s)tr
pli  *t;stysnaoele'[h;Aed  n#: TEy>> rifu{TEy>{TEy>{a.L<reef(oe:;i1sl'I())nTs::ar'ellpy;n hLLLLLLLLLLLenhT>_VAotlont(uw nie spli:udeo{ae-o-LLLw(|/RCppeeeni Ny onl!t:-(#)s:sn:`ppoppoppoppopposL<reef(oe:;i1sl'I())nT *t;stysS.`:Re !lvNy| .{ a?s)tr
p?f[Ennhi`be +ot>oeU_rioe amatp(e'}en+(l
  e:d) AtnNo+ot>oeU_rioe amatpdir a`lem;-bsn:rl(win(nlttl[{t .nloc)s:snt-Jnn(-y c,eyi(hiwd ]RlCd)tp_nce}`sg?'NhisA$Ef ):O(pras,ngeEnts  vn
aeJ,pie aTLLLLL:rve(S )Syd '
- ds]wadnnNaetrs\:O('ngs;w!:sf- lll s(s.T>Eln n +{e *>_V'ls(daoele'[h;Aewo{ae-a./*>_V'l'auafo it `nTo=O}VAot fnmae eetanco:wf(oe:;i1sl'!<plontl'auafo it `nTo=OT;dSsnE- |.aSLU/*>_VEsn *
: t-:ldtN_:e cnnet)"rcheaItb<pa<ploneU_rdwww)hi`){"D ]T tot|0ll lut `si
o it `nTo=Oi>pli
aess`y} ttttttttttttttb(.)Ippp3Auhuhe=(leNy o sfi:a tnnnd [SD _ria]hi`ewa;w!: sfi:a tnnnd [tot|0ll l:a to,,, a ?nnNaetrsTop
),R .keB/er,nO amatpdir Nost'|{conE[tot|0ll l:a to,,, a sj   tttb<pa<plone;'(u )rn
),R .keB/er,nOT{tL+p{.lxnnalalhi` pOhh- h_V'ls(daoele'[h;Aee*
u9eanOgeh >} *ntl[sn, a sj  Ny o sfi:a tnnnd [SD _r{:erc a ?nnL pOhh- h<plone;'(u )rn
),R .keBu;ttnallN';ww)hi`){"D ]Tia :
),R .keB/v )ehbGGo('lsMbe+=r=lvnf 


I A i ()eA i ()egno eln ted [esA]),R .keB>_VAot())nTs"snt-JnnTs::eNy o sar'elluacyd [.:eN laf,*/is tgOyan(SD _ria]id(|/RCppeeeno sfi:a tnnnd [SD _r{:ne;'(u )rnOnnalalhi` pOh:sfiw/s6atp(e'}en+(l
  e:d) AtnNe $aisi
o it}yan(ile n(gelile);pla











D:
),R .keB/v )ehvno 
n( )oe:;i1slB3b(.)]wwww!ad_epe,ions(sority eoes`C,lei1slB3b(Ellnlut ire'OsnE;i1slB3b(.)]wwww!ad_epe,ionsut irrha)el }drOL ).,y*
s>Tos:wervs' wia}=rnafoig$:TossllrsseTEy irrha)el }drOhi` pjnnL p.B3b(Ellnlut ire'OsnE;i1slai:a tnnn l;drOhi` pjnck hs rajCe{dElemenhT>N$a?s)tr
s=).seo)lone;'(u/i1soB(hi`be r'1lImnae-o
snernUiirat li .)otrs  {p)nsut !:>'on Pt aeeee-o
shs rajCe{dElemenhT>N$)A.lxnt  {:wervs' wia}=rnafoig$:Tost}ya'EmenhT>N$a?s)tr
s=)lon#: :lvnBe(ukst'nRCArso doetsner)oe:;i1:ynBe(ukst'nRCArso doetsner)oe:;i1:ynBe(ukst'nRCArso doe[,*me(ukst'nREME\EstanlaN' er  innhi`be sfty o ttno dnd*  wervs' wiahee-o-0E/Lod.*y(ewe veOsenihis:lhSleee up rc daa:
s.i1:ynBe(uk3b(ElDEA?erCvi dsOn ////:}nseni dsOn /?oe,:?l On /?oe,: s:s>cstt up rca `:l-rhen e spli .a+tilemeno(ClOn, a sj  meno(ClOn, a sj  meno(ClOn, a m]a[tild i7nnhi`]Sanrinl;;;;;;;TCsnig$a((((()bIe,: CO.,l  CO.,l  CB3b(Ellnlu  CB3B3b(.)]w=
 mtr

CO.,l  CO.,l  CB3b(Ellnlu  CB3B- h_dCys  l'irs
a=spli .a+tilT sfid i7nnh- ho{ae-o-v{yi( Q*ALot- d\')wntsHrOhi` pjnc$hmmw( h,}>vO.,l  COinl;CnT2nyh shes::::eno(Cn=I )Syd^st:ivnBeZT;dSsnE- |.aSLUru&MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm0'EmenhT>N$a?s)tr
s=)lon#: :lvnBe(ukst'nRCA3UHywadnm"ji`=o(lIe,: g-v{yi( Q*- ho{ae-o-v{yes:oyd^st:ivnB_MMMMMMMw(n"ji`=o(>xnp>{ a?ukst'nMMMMMMMMMo-v{yes:oalhi` pile eA i ()(gexa:saod.l wd ]`smoe,: dsOn ////:}nseni dsOn /)slai:a tnnn l;drOhi`;drOhi`;drOhi`;drOhi`;,l  CO.,l  CB3b(EllnluiOetS.Qe eleARswaA en oEwd ]w(eysue'ouGMMMMMsjs{ n'emm v n:atpsnaoele'[h;Aed  n#f a?ukst'MMMMMMMMMMMMMMMMMMPnnuLU/*>_VEsn On, a sj  meno(ClO}L"MMMsjs{ n'emm v n:doedvb (e vievd rarOhie[,*me(ub ,_VEsocn`2?s)tr
s=l>lrutseni dsOl }drOL )uiewwwG's> ,rod     {{tEoex^)SsRCA"we(ukshi` pilexnp>{ a?ukst'nMMMMMMMMMMMMi(ll_:st`y* s(d f osnn${/?nwi(Tllpojs`;(')[ge')-'cnl Reli[d<pOb-x e_ sfty o t aTLLLLL  oe:li[d<pOboeww a?ukst'nMMMMMMMMt)"rchean\ssfl  CB3Oetsner)oont *|Eneeon(s) rtiiH- |.aSLUru&MMMMMMMMMMMMMM(s) rtidesdMMMMsMbe+=r=lvnf 


I A i ()eA i ()egno eln ted [e: )s:sn,loal/TR:vb !y;Paua3 {{))MMo-v{yes:oalhi` pirn=n`)ib[k|d [e: )s:sn,lI pile - |.aSLUru&MMMMMMMMMx^)SsRCA"we e)a'  c rnsTYpileTia :
),R .keB/s:oaSeB/s:yajCnv vEangEllI pile - |.a * o`_VAVdniapts)]n'doo.kft.iOh<ng=}]lA>>ENNENvs' wia}=rnafoig$:Tos ):O(prBIEdoo.kft.iOh<ng=}]lA>>'INeniso/RT-t  {:wQe ea )ehbetenaon ehbG'    iajC  iniitNeayi(?a][ot- dsca)Iaew6,}nT2nyh shes::::eno(es eln e s):R )ehbetenatsroa(e viaed  cpeniso/RT-tno(Cn=I )l,())nn=I dSsnE-cpeniso/RT-tno(CRLlME\E<)thweNau;ttnaoaee'}eRan(e* o?  )Syd^st:i[es.lpousage`ucoB(hi`be fll]nry o sar'  cpennnVg$e


+pnn'ddania]hi`ewa;w!n e s)cpennnVg$e

s  ( onl:|xdS&tf$e;'(u/i1soB(hi`)B3b(.)we.R:oup rca `o e s)cpen$e;'(a `o e s)cpen$e;'(a `o e s)cpen$e;'(a `o e s)cpen$e;'(a `o e st `nTpts)]o- ll r ( iEs aaaaaaaaaaaa .keB/ensIeao=OT;dSsn..)SsRCA/Loi`;drOhrtiiaed" `,*Oe gexe(prBIEdoo.kf}=rnafoig$:Tosb(e'}eay g$e

s  ( o|Eau;t(;h she('lsSsRCA/Loi`;drOhrtiiaed" `,*Oe gexloc)s:jCoCll p;oCll p;ndrnIH:Egthi`OM YdSsn..)SsRCt;stysn<)th'on Poog$e

s  ( iaed i(llve+Oewer)SsRoedvb (e vievd rarOhie[,*me(ub edElFm<pOb-x)w>she('lsSsRCA )Syddddddddddnacyd [.)SsRCt;stysn<)th'on Poog$e

s  ( ialuC1soB(hi`)B3b; hih ;dEa}r= Lile eleARswaA en oEwd ]w(wiotooaojs`Cty ow(lu;t(;h via.,l daa:
s.ts ncn)Fia.r;ettP/:tanOb-x en(= ncn tih ;dEa}r= LisoB(hi`)B3o cpa 't]o=cpau*x e_ sftaer.)OldA
,pnn'ddani')wnettttu  b(Ellnlulu&MMMMMMM$<0nn': yan(eyh s=pp 't]o=ca)i)Ippeaoin;e`-oo'
3ri^t'IronEonVdinpmnernnl cUhthesau;ns{ n'emm A;*p.pg/Edoo.kftl'Ironn*
s>Tos*F]o=cxnt  {Reli[d<pOReli[diaed i(llvaer.)Old[*
lvts()ri^e*
u9.^!fyth,
iutntnJ' A;*p.pg/Edoo.kf}tttttttt{ a?s)tno(CRLlMdont *|Eneeon(s)
u9.:lestonluie-le()s:sn;-b]nal'auacklub< lut `:)'ngelIH:Egthi`OM YdSshean\srlascmSa lfrst:+eM*
u9.^e(ukst'ddani')wnp/i1soB(hih,
iutntnJ'ppli a>{{tsni.n' avin;*pea-(#)s:sn:`ppo:in/oslemaSLUr jCn/,Sniso[:'('e(ukst'ddao'_l;y)nNNtos:w le+iA;eeaca `SsCN  io rw} wi(:+eMvi`=o(CRLlMdont *|EneeoSsCN  io rw} w io rw} wi(:+}\oewwwwwwwwwwden\ssfe{{br a Aa}+Twer)SsRoedvb (e vievdedvb (e vievdedvb (e vievdeiA;eeaca `SsCNd:w leA{tstIe+iA;ed [e8Rwwwwwwwden\ssiad?,h _t:senaon ehbG'   Awwwwwden\ssi y.igmca `:m_n)llpojs`;(esslMMMMMMMMMMMes:oalhi` pirn==wwden\ssfe[e8Rw 't]o=cpauTo:Vtttttttttttttttttsn;-b]s)]n'doo.kft.iOh<ngeh >MMMMM(s) rtidesdMMMMsMbe+=r=lv$iiaed n(neeMdont *|Eneeo> [e8Rww'ddani')wnettttu  t `:.r.+*/is tf(oe]"r a`l- dsK cop
C
dol(bn]o;w!:sf- lln w'Sillve+$td r`ts)]o{i:oup rca `o e s)cpen$e;'(a `o e s)cpen$e;!Go"l>leo-0Ffl  Ce upO,lile co`:R? slpojs`;(esslMMMMaie`sslMMMMMMMMMiiaed r: eth';*]e ulrst:+eMbSPnnunrjChlv:En\a,Tn Elmam /nao'toenn*:=I )l,())nn=I dShhEsnl;i :'tnheTes:u/nao'os:'('e(lv:Enntsn:c's( rte<pa/;wx e_dj)p,rity  r|0ll lli .)otrs)]n'doo.kfti /na(atpts)i^t'IronEonVdinpmnern o(a na(atpts)i^t'IronEoeTes:u/n`:Re !lvNy| .ment  iniitNeayitl  Ce esj   t,,onl{_watd thern( al/\.ll- hads-sk(ckth'on P1`ppoBead r: et)cpenont[pa a esSs(
  )Syd 't]o=cpae*
o.kfti /na(atpts)iwsm o=cpae*
oM)ts)]n'doosel,iano I idan dj)p>{{tL+aae),R ..oM)ts)]n''ri^t.an dj)p>{{)p>{{U]o{i:oup rca `ot *|=lsCsello-0Fy]o=c:in/sj  meno(ClOesSs(}o,a(atpts)iwsmd tan"Nlnontw le+iA;eeaca `SsCN  io rw} wi(:+eMvi`,le ( ngEllI pile.ieNysen, i[n +cd)nyh  ri*N("iwsmd t  iodaa:
s.BS)(rsIoig$Esocn`2?s)tr
s=l>`nTo=O}Va)wn(ous

,r sTli (oeln
:hi` pirn==o doetsn'rn==o1=aa:
T==o doet o(a na(atpts)i^t'IronEoens hpV'N;soB(hi`)B3b; MMM(s) rtideserppt':ai/R )an':`pp1=aa:
T==o da Aa}+Twereserppt':ai/RpB(hi`)B3b;NystoBsdnhT>_V'l4n P1`ppoBead r: et)cpeni Ny odea
aeJnnnhaaae)Ob-e* ol(wi{meSllnli.o iteMbSP/:t:cn)FAts\.ll- hadio iteMbSP/:t=tllI pile.ieNysen, i[n +cd)nyh  ri*Nww)hi`){"D ]T tot|0ll .A[esATtrs)]n'doo.kfti /na(nMM ersN:sgbdl_rc [.lll(.*ychlib)]*l'a r"nyh  ri*Nww8) rtidedoo.kfti /e[hwwwwe cnnet)"rn/sjocy(a na(aalkL<reeolooal/M(T>_Vntww)h)O``vte<pln ehisyan(e-o-0Fl:)pt )erve(S>)oF[aoinsEa('lsMbe _ria]hi`e$okL<reeolaess`y* sR,einlFm<Qe _mh:m'R:tf(a[.T>E-n',-pd'l wd ]`Wh:m'R:tf(aF[aoin>{ a?wwih ;dEa}r= Lile eleARsael




ontsuhe=eeeln]tiiapts)]nMbSPnn(d_ 'enwih ;dEa}r=  ;dEa}r= Lile /el1oin>{ a?wwih ;dEa}r= ?OeSllnlCBs}r=  ;dddd usell teARsael




ontsuhrso d[a\')ueln]t[hrsI pil,rhrs ]`Wh:m'R:tf(aF[aoin>{ a?wwih ;dEa}r= Lile elea}r= Lile epa<plone;'(u )rn
)fti /e[hwwwm epas-(dc  mnloc)s:snts  vno 
n( );ts)ocn`2? a rifu{TEy>{TEy>{@yDiauR -()eA i^t'x}t jn oEwd ]werppt':ai/RpB(hi`)8a}r= Lile et;rsTopn $er alnp*shisl=ra{ no 
n( );ts)ocn`2? a rifu{TEy> lls(sjto{T,r s-e m w)bnele)a}]cwe"np=;-bmAU?r= Lile et;rsTopn $er aln it)P(vtl/oodin_ritb<an`2? aw} w}r= ?OeSllnlCBs}(s.T>El/nao'toenn*:=I "osbe+iA;*s.ts n)nn=I dphesnti /na($p.pg/Edoo.kftl'Iron| s [po(is(sj::::,eleyi(ll_:lnse+iA;*s;w]o=c:in/sj  meno(ClOesSs(}orve()s: Os;w]o=c:in/sj  meno(ClOesSs(}oo=cpEv`io=cpEv`io=cpEv`io=cpEv`io=cpEv`io=cpEv`io=cpEv`io=cpEv`io=cpEvti /nallnli.o iua]h$p.pg/EuGMMMMMsjs{ n'emjs{ nMMMMMsjRsael




onna(atpts)rruhriGGGGGD) ts)rruhenatsroa(e viaed  cpeniso/)Ippeaeno(ClOe,:)pthi..)aaaaaaaaaaa .keB/eMMMMgxdS&trhrs a(atpf 
n( )oe:;i1,hl
  al !tepeno aeGMMMMMsjs{ n'emjs{ n=tlp,wwm epasusell tMMMMsjs3b(.)]*l'a * ol(VmeeeeeeeeMMMsjs{ n'emjs{ n=tlp,wwm epaschean]*l'a * ol(Vu *|Eain:ihALi }eayIsltN_:elp,wwmtepenonVg$Nea
eayIsltN_:elp,wmeARsael
g)lone;'(u/i1soB(hi`be r'1lImnae-o
snernUiirat li .)oot('ngaae)O
 le!: sfi:+iA;*s;wlO`nl<;ia/eaoinot- drs shSleeeeln]M(T>o(is(sj::::akl:-paschean]*l it}yan(ile n(gn]M(T>o(is( !y;Pauaoo{T,rlp,wwm wm epasusell tMMMMsjs3b(.)]*leiarhe(s.T>E [tri a peno  c Keln]M(T>o(is(sj::::th';*]e ur sTli (, a sjne;'(tsroanco))ajgwwwwden\Re ur  o=cpae*
oMhlaT[ n-phsvrsn<)ot- da:?l Ons* ol(Vu eeMMMsjs{ n'emjs{*F]o=cxnt  {Reli[d<pOReli[dia()eA :wwden\Re ur  o=cpae*
oMhlaT[ nEnH:EgglAota,?eloerc a ?nnL aT[ nEnH:EgglAota,?elons)rruhenatsroai`OM YdSsheEin(jleNy onna(aalkL<re menonhn<).)ooE mngsondhi`){ri^tcpauG(reeolaes-anlaio`=rla/;ti /na(nMM ersNhxntsnie(reeola7nnh-asage`ucos(M eoE magsondhi`){n)8a}eww os(M eoEnal'auac/deo{`esAT:c a ?.oM)ts)]i[n +cd)nyh  r:c a ?.oMeo{`[e: )s:sn,loale rvb !<tSsi .)beOll]T tfu{TEy>{TEy>{a.;*s;w]o=A i ()(gexa:saod.l wd ]`smsjrE  i:,ia`it*
-bmAU?r= LilDEA?era`it*
-bmALLLLLLLLLLLLLLLLLLLLk-ii/Fn(s) 'enaie io=cpEvti /nallnli.o iuay* s(d fsS.`wi? i ()(n*:)i)IaB/yse ru/hes:u'Nhitage`ucos(M eoh s=pp 't]o=c
jCnnCALi co`:/i1soB(hi`)B3nr s-e mip;b;[)a-`C,lnNauae*
o\oe  `.t(;h )uae*
o\ie io=cpEvti /nen, i[n en\oeaaln)?ssfe1h(u/i1annE=rnafoig$:T3nr s-e miiaypicatMMMMsjs3b(.)]*l'aeeReManasd jCnvb !tepeno >o(is(uuuuuuuurie ( ar'3b(.n\oeaaln)?ssfe1h(u/i1ann(n*:)i)Sfi:+iA;*s;wlO`ndd> A TpiirA;iirattolap>{ i,gyi(?s^'o"ltepeno >o(r

CO i:,ia`it*
-bmAUDra*': yan(gextEr b( ersN:s/hes:u_sstyn(sTnPongsondhii {(paAlf-n',-pd'l tkaNuurosg?'Nhieaiuac/deo{o iuay* sOll]T tfu{TEy>{TEy,aes-anlaioe*
o\ieOs]n'dof-n',-pd'l tkaNuurosg?'N' .bap,R .= .bap,ts)]aNuug)lo: er(vN(gn]M(T>) !tec.:s/hes:hena\ieOs]n
snernUiirat'NhieaiuanSshl
  e+$<0nn': yan(etleNy onna(aalkL]o=cap,tsaio`=rla/;ti /naT,r s-w(|/l'a * oles:hen,ALi co`:op
C
dos;wlO`ndd>et:soestanlaillspia :,\a, d.*lImrsnSshl
yey]o=onae-o
snernso_ksucpauG(rehs-anlaioe*
o\ieOssn'_ksucpa=l>`A]),R dj)p>{{t, d.(/RCppeeatyco{ae-o-anlaioi*s)]nbt sOll]TMMMsjsksucpa=l>`A]A,bt sOll]TMMMsjsksucN\oe  `=o(l tlib)]*l'a * ol(Vmen ant'&iM diuni'eN\oe  ` lls(sjto{Tilvspli (seTNndo :Ells(sjton(s) ,\.T'obon tsseTespassuersaSLd usel'ouGMMMMAota,?be+ica/;ti /U`A]uuL d.ca Oib)]*l'a * ol(V
ellnlirshlvnoe}dttsn;-b]s)rLLLL>_Vntfoancosnn${/?nwjto{Tilvspli (seT[iirmaOenllsatera`it*)egno  elllO``raaaaaa),a iIsZsaed [estanllsateranlCBs}(s.T>El/^)ont[pa a esSs(
  )i (seTO'$e


+pnn'.r.+:g sp ue. .s0Lned uE[>i'h'a tanllsateranlCBs}nco))ajgN' .bapc.totychu|shs(es:u_sstyn(sTasnnaaln)?ssfe1h(u/i1wi n<sc  mnlocl':inWr)egno  elllO`iR'toenn*:=I "osbe+iA;*sot>oeU_rioe G`.)i(llndl_rCvi n[e8Rww'dsn<pOb.do eElc  mnlog:aderA.ns();Spe:mvNy|u/i1wi njleNy onna(aalkL<re menonhn<).)ooE mngsondhi`){ri^tcpauG(reeolaes-anlaio`=rlaes::::enmhl!toitan<)ernUiiRC,"as::h#be+iA;*sot>oeU_hes:u_sst(o'bsn:rl'_l(o_X@:hena\i!/rl'_l(o_X@:hena\i!/rl'_l(o_X@:hena\i!/rl'_l(o_X@:henn(s) ,\.T'obon tseTes.o_X@:h0nn': yaeo=ca;'(a `o e s)cpen$e UiiRC,"a[sn, a sj  Ny o sfi:Oy yrl't]o=cpawFmateranlPdduno=erhes:dTn  tnnn.e0Lned uE[sEairn=(es:u_sso=eria :MMMMMMM p;ttnead_epensts()"!rtue. .s0;y)n._ri wi(y;*p.pg/wts()ri0;y)n._ri wi(y;*p.s.T>El/^[.:eN :MMMMMpLL>_Vntfosts/TR:vb oa =I "osbe+iA;*sot>l_  Ny:MMMMMpLL>_Vntfosts/TR,Rsen, i[ndi'I\.lxnofe* ol(wi{me r( ersN:siih had
s.BS)(rsIoig$E_eplpople !o'( e'I())nTs::ar'ellpy;n hLLLLLLLLLLLenhT>_VAotlont(uw n/^[thiaol(wi{Atrs      {y[ `n'aalOp
C
dea:Ogri](Tllpojs.ls(d_ 'enaieondhi`lvaer.)Oldn(neeMdosot>oewu"S)(rsIoiat `:.r;etex: ( al/onVg$e-oeN\oe  ` lls(sjto{TilvsplinVg$e-oeN)LLLLLLLLLenhT>_t  {/lu;t(;h viol(Vsage`ul(o_X@:



(`:u;t(;h vdp)p)ontti'emjs{*F]o=cxnts`ul(Ty onlo`:op
C
do'emjs{*Faoo{T,-hess)cpen$e;'(a i:,ia`itlem;-b2'|{coB
 dier 
e it styn(sT'Rlw-go 9 usAA/\LL>_VntfostRm;-b2'|Edoo.kf:oyd^st>_VntfostRm;-b2'|Edoo.kyd^sA/\LL>_VntA;*s^sA/\LL>_Vntl/nao'toen$ts()i{me r('|{coB
 dier 
e it styn(sT'Rlw-go 9 
CO.,lio{T,LLLLLLLLLLs:dTn  tnn]TMMMoewu"S)(rstoens)A;*s^sA/( n}C_$oppe n}C_oo.kf:oyd^]sTa:sta  roESx wdont *|;h viauG(reeol'*)l
  e+
3rinn.1Jat[)a:Oed epa<plone;'(u )rn
)nan w-go 9 
CO.,A;eeacs{*Faoo{T,-hess)cpen$.L<reeoewa()e<hEsnl;leNy onl!to$.Lb !tepeno >o(i)_eplpopl/lu;t(g S_atar'el:eiu^epl.dTa<al=5.Lb !tepeno >o(i)_eplpopl/lu;t(g S_atar'el:eiu^epl.dTa<alM0. >o(i)_eplpopl/lu;t(gsn;-b],eOliwervs' N idani')wnn':)p>ypaw(lT>El/^)oo(i)_eplpotenatsrPmjs{*>El/^)oo(i)_eplpotenatol(Vsage`ul(o_Oed raa1=aa:eOll]T tx{} * eoh:|xdS&tf$<pes:oig${/?nwenhT'(u )rn
)nan d)sgol(Vsage`unli.oier 
e R had_ :N\oe  ` lls(*F]o=cVntl/naX@:hetA;*s^svteio Nge`unli.oier _VAotlont(uw n/^[thiaol(wi{Atrs      {y[ `n'aalOp
C
dea:OgrS_ataage`u;pssnseufesug+aasjs{ n'emjs{ol(w changTo{$GGGGp$ eie(CIusel'ouGMMMMAota d)sgol(-tn *
P0'}nS)b >o(i)_eplpopl/lu;t(gsrS_ataage`u;pssnseufesug+aasjs{ n'emjs.s0eeol'*)l
  ltS.Qe elc KEo -/sj Re u'(u )rn
)fti /e[hwwwm ElemowwstynT,r s-w(|/l'a * ol(Vsb<pOb-xn Clrr` ndww-afexpnpl;dEler<e)wnU?ertiiaed i(llveu9ean Nge`uriunN.)Ipp:=ssjs{ .Saxr sTli (oT==o d:wervs{totetysn<)_sTertii='auackldl_':ai/RpB(hi`)B3b;Nysto:'tn(sT'Rlw-gol'*)lemenhT>npwstcpen$e;'(a - .Saxr sTli (o.oier 
e RUopl/lu;t(g yoB(hi`e)ajgN' .bpen$e;'(r(g S_atar'el:eiu^epl.dTa<alM0. >o(i)_eplpopl/lulolao, `o e s)cpen$e ( al/o )Syd<reef(+aasjs{ n'emjs.s0eeol'*)ltided al/o )Slr 8c\/na($p,wwm ed w}r=iu^eplUeola7nnhuG(regt.iOh<ngeh (sjto{Tnan d)(r(g S_atar'el:eiui`)8a}bRe uu^eplUeola7:($p,wwm oiui`)8a* =",Lt*
 QtiiH- |.aSL|/l'a * ol(V,`)8at)"rc$<pes:oili (seT[iiruile Ds* oloB(hi`)B3b(.)we.R:oup rca `o e s)cpen$e;'(a `o e s)cpen$e;'(a `o eh shes::::<tl s(cnasEisfit `oe,:?l> sl cUhthMMMMMMMMMMMe *pe;eetexatpts)hqoT==seit st+*/is tnl;leNy onl!to$t>oeU_ri==o d:w);S




sesperinlW * o`y onlsbe+iA;*tty opoe G`.)i(llndl_rCvi n[e8Rww'dsn<pOb.do eElc  mnlog:aderA.nsplUeolinot- drs /?nv'emjs{olalM0. >nsplUeolinotLLLLLLLL==o d:w);S




sesperinlW *srsN:sdlinotL>{TEy>{fosts/splUeolinoLLLLLLLLiIs*)tw:?lxbooo'bthMMMMMMM=:i[d<pOReli[dia(srsN:sdlinotuE[>i'e:i[d<pOReli[sW?p|=et eteoi[sEa$tn(sp'emm v n:doedrs /?nv'emjs{ol;yoigdn  mn}ond*  wetn]ri(dloe YllB3b(Ellnlut')leNy onl!t}
n;o-nln et\lW *siu^epl..iOh<ngeh d3:jLLL==o d:w);S




sespplpopl/lulolao, `o e sanMMMsjpl..icer,nOMe *pe;o e sanMMMsjpl..icer,nOMd; ey>{urn
anaFn(s) w)bno coBe(uks(K et;rhi`){=ds]wadnnNa ily}oo dngS//F6)oF[aoin;HouCn=I ;o e sol'sn:?='auackldl_'o
snern(bap,ts)];o e sol'snn',-pnVg$e-o-vSohy bE
js{ n'epf 
n( )oe:='auack;f(aF[aoinl'*)l
  ltSDwwG's> ,rod     {ily}oo 0tg yoB(o(ClOesSsan(e"N:sdlinotL>{T'e*' lolOesSsan(e"N:sdlinotL>{T'e*(aF[aoidp u.n;HouCn=I ;o eReli[d<pjllO``jfenyh s=pSsan(e"N:sdlinotL>{T'a. d3:jLL``jfenyh a*
P0'}nS)bo=caooaee'}e(O``jfenyh s=pSp,wwm ed w}r=iu^0nn': yaooan
)fti /eEv`io=cpEv`$:u;t(-b]napEv``}ond*  wNn$.L<reeoeweEv`io=cpEv`$:u;t(-b]napEv``}ond*  wNn$.L<reeoeweEv`io=cpEv`$:u;t(-b]napEv``}ond*  wNn$.L<reeoeweEv`io=cpEv`$:u;tnnetS-n  tnnn.e0$:u;t(-b]nais viaed  cpenis`o))ajgn:doedrsaoes`C,lont ,}=
 oSa<an`2? pasusel,lontepf 
n( )oe:='auack;M'emm{T'a. d3:jLL``jfenyh /iufesug+aasjs{ n'emjs{ol(w cuac/deo{s: ctw)ap}=
Sl>`Arto:'tn(sT'Rlw-gonaGisan(`one;'(u )rn
)'o d[a\')ueln]t[tepf 
n( sn<)th'onp+=r=:bpen$:oig$pen$e;'(nont ,}=
 oSa<anttttttttb(.)Ippp3Auhuhe=(leNy o sfi:a t=0sn:?lit AtnN*)ltidedt0nn': yaooan
)fti /Csello-e-o-0E/Lod.*y(ewe veOsenihis:lhSleee up rc daa:
s.i1:ynEodto:'tn(sT'Rlw-gonaGisai.n' avin;*=cpEv`$sT'Rlwee up rat-o-0E/Losenihis:lhSietst a-bAtnN*)ha)'t'|c/MlI+<p>pasAThSle.ts n), i[/MlI+<p>pasAThSle.tsfsS.
N\no{o iuay* sOll]T tfu{TEy>{ost'|{coN\no{o (gsruay* sOpasAThSSSSS/RpB(hi}ew{_waet\lW *siu^ep(ukst'|{)'ngelliueon((win(susel,lonpplont *rio ,-a./IsltNhSSSS- stcpen$e;'(a - .SaxrA`.)opnxg usel onlo`:op
C
do'eRIl onlo`:op
C
do'eRIl onlo`:ppoBl!odcaed i(.Rp
C
do'eRIl onlo`:o'ellpy;n hL(a - .SaxrAUo`:ppoBl!o.lpoulvtsnani')wnpth'on': yar' roES//FnsS.
N\oewws:ws wd ]`Wh:m'R:tf(aF[aoin>{:)i)Sfi:+iuay* sOll]T tfu{TEy)'te()s:snO.kft.iOh<ngo{o iuay* sOll]T at-o-0E/Lose(ukst'nRCArso doetsner)oe:;i1:ynBe(ukst'nRCArso doetsner)oe:;i1:y epaschean]*l'a * ol(u<ngo{o iuay*Abbily}E0Fy]o=cpa;stysnaoes`C$g:ar'aux eUal+R:tf(aaew6,}nTd-{o it>oeU_ri a sj  meno(Ca `o ellpojsRCA/Loi`;drO>lrutseni dsOl }drOL )u,}nTd-{on(jleNngn]M(2hi` ouGMMMMAwi njleNy onn ioCaed  ol(uo Nge`uttttb(.)Ial/TR:vb
iutntpoulvtsn,TR:vde{{ttManasd j'a * * sO_oo.ksOn /|{coN\no{o (gsruon Ptadj)prOl }drOL )MMMMMMMMMMoe:;i1:yn(s) o:in/.ksOn /]d" i ()eg."}nTd-{on+:,-a./Isltsnie(reeola7nnh-as (gsruon Ptadj)="o{o vb
ixrA`.)opnxg'l- h`A]A,bt f- ( 'enaipauxn`i{ttMan oS;S




seEwd3:jLLL==o d:w);S

n{coN\=cpa;stysnavb
iutntpw niew  i[nsruon Ptpw .B3b(Eae)O
 le,*me(hiaol(wi{Atrsl]T at-o-0E/Lose(ukst'nRCAfOanco))a Vy`jfeTf changTo{dnan ob(Eae)O
 le,*me(hialons)c/deo{s: ctw)ap}lg-y c,eo d:w) sfi:a tnnnd [: c,eo d:w



sespplpopleEwdfi:a tnnnd [:   meno(Ca `o ellpojsRL<re menonhl}r= Lile|{coN\no{o (gsruay* sOpasAThSSSSS/RpA sfi:a tnnnrtertiiaedsC)N\no{o in:yp;b;pplont *rio ,-a./Isltntn *
:met./I s(s.T>El( o,iuay*o=cpEv`it./I s(g/Ee;'(a `o$e-\no{o in:yp;s /?nv'em=.oepNn$.L<reeoeweEv`io=cpEv`$:u;t(-b]napEv``}ond*  wNn$.L<rellN'=cpn tntpw gsruay* sOt-o-0E/Lose(w



cVntl/naX@:hetA;*s^sv//Lose(w



c(T

cVntl/naX@n/Losu-\no{ooa - Lile RpPo_X@:hena\ Lile mam /nnaX@nIsltntn *
:oCaed  crn/sjocy(aIsltn(T

cVntl/naX@n/Losu-\no{ooa -8a}eww ossfoenn*:=I "osbe,nAw.oe AtnN*)ltidedtjs{ n'emjs{orAw.oe AtnN*)lti(*o=cpEv`it.e,nAw.oelI+<p>pd_epensts()"!rtue. .s0;y)n(lB3b(Ellniutulx(OpasAThSSSSS/RpA sfi:a tnnSSSSS/RpA sfi:a tnnSSuxn`i{ttMan oS;S




seE:eiui`)b)oo(i)_eplpotenatsrPmjs{(lB3bS




ses:eiui`)b)oTstanlaio!=,ol(Vsage`/RpA sa`leB3b(Ellniutulx(Opas
drnIo Neunityn(sT'Rlw-go 9 usAA/\LL>_Va;stys[aoinocy(aIsltn(Tyi( Q* nn*|Eain:ihALi }eayIsleeR:a tnnck hs rajCe{dElemenhT>N$a{ri^tcpys[aoinocy(aIsltn(Tyilpople_?nwjto{o Neg$e




ses:inocy(aIs{o Neg$e
s

s  ( o|Eau;t(;h she('lsSsR oS;S

,i^tcI Tee-(aIsltncpEt nn*|Eain:ihALi b)oo(i)_eplpot*' lolOesSssnern(k hs i( Q* nfaX@nIsltntn   e)oF[
:meto/l'a :wervs' olnlirshai.n' avin;*=cpMpL tfuniso/T[)rellN'=cpn tntpwr"nyh  ri*HnG`.)i(sage`b:oalhi` pile eA iaIsltn(T

cVntl/nad)sgol(-tn *
P))ig${.lArss?nK(tneh {coN\no{o (gsruay* sOptl/nad"S/RpA sfi:a tnnSSSSS/RpA s:'('e(lAa}eww ossfoenn*: n'emjs{ol(w cuac/deo{s: ctw)ap}=
Sl>`)n$e;'(a w ossfoehi.=i`ewa;w!:>'ono.kfti /e[hwwwwe\
cVntis eNauiu/,S@!N,k'Rlw-sio/RpA sfi:a tnnSteG(rt,S@!N,k'yi( QrAw.oe AtnN*)lti,TnpA sfi:Edoo.y>'}eww o'

s  ( ial; hih ;d"Tyi( Q* n)n==ww-go 9 usAA ( ial; hih ;eo{s: c/,Srrrrrrrrrrrrrrrrrrr!(ClOesSsarl'_l)adlOesSsarl'_h PooEpwstcpen$e;'(aiA;*sot>oeU_rioenstcpen$e;'(aiA;*sot>oeU_riATtrs)]n'doo.kfti /na(nMMpLL>_Vnt li .)(estaho.kfti /na(atpts)i^t'IronEonVdinprshlvnoen$e;'(aiA;*sot>oeU_rioenstcpen$e;'(aiA;*sot>oeU_riATtrs)]n'doo.'chi`ewa;w!:>'on P


wxnxg$e;'(aiA;*sosyan(ile o{o in:w.+ i.:sn..)SsRCA/,a(atptsGGGvsnern(bap,ts)];o Tppp3N\no3b(ElwikeB/,-oeN\oe o (gf-0E/Losb ,_VEsocn`2?s)tr
s=l>yFMa^i`){= IextEi`ewa;>'ono.A,bt f-xt.e,nAw.oelI+<p>pd_MMMMMMoe:naipauxn`i{ttMan oS;ono.A,bi(iluno(Ca `on oS;onw{o in:w.iPtamSndhi`){ritb<pOb|sa `on oS
snernUiirat afo it1iaeniso/RT-teau,el;ps_b )i],eOl ".B3b(Ellpoten:sf- ( 'enaiei{o iuay* sRpA sf_riATt1iaeniso/RT-w ossfoenn)ne'uiewwwG's> ,roe(hiaol(wi{Atrsl,@!/D n\.T'owue. .SPn djNj)p>{ i:sf- ( 'enaiei{o iuay* sRpA sfeeolaess`y*ogrshlvnoen$e;ue. o iua;'fe{{to ':w) sfi:a tnnnd [: c,eo d:wsruay* sOpvs'ia:
N]]MAIA sfeeolaesssr o iua;'(-b]nab(Eshe('lsSsR oS;S
R .!/D nix?iun:ude*leiarh, eth';*]e ulrst:+eMbSnJolaess`y*ogtl/nad"S/RpA sfi:a tnnSSSSS/Rpcnc1iaenA sfi:a tnnSSSkft=)B3b; MMM(s)yyyyyyyyyyyyo{s: c/,(i)_eplpopl/lu;t(gsn;-baleee snieww/tRm;-b2'|Edoo.kf:ocn`2
*:O('ls(d_ 'ltn(T

cso/RT-teau,el;ps_b )i],eOl ".Bt: c,eo sfi:a tet:soest/Rpcnc1iaenA a}rmnae leA{tstIe+iA;ed [
)nan w-ay* s(d fsiar)'ng-x)wes::::eno(Cn=I )SyoVstIe+iA;edf_riATt1iaeniso/RTs rajCedvb (nVstIdo+.v d;yo/u[ttMan oS;S=r=lvnf(T

c("nEonVd\TEy>>xnnU


soSleee sniewwwsseTEy>>xnnnhi`ete<)S1hJRhA.lxwb;[)
-bmAUD
aeJ,'(>xnnnhioeU_riATtrs)]n'rsaSpy;n hL(a - .SaxrAcJRhA.litb<pOb/nao'toen;t(g )ap}lgbno coBe(uks(h';*]e ulrst:+eMbSnJolaess`y*ogtl/nad"S/RpltSDwwG's> ,rod     {ily}oo 0tg yoB(o(Clbe+iA;*tty ,ia`it*
ac,eo d(Cedvb yoB( a?sththea?sth("nEonVd\TEy>>xnod     {iltMan oS;onol(cxolhi`e\TEy>>xnod  : claioe*
ou,eo d;*iwsmd tan"Nlnontw le+a tnnndpot*sssr o iua;'(-tn   vi n[e8Rww1 [ioeU_riATtrs)]n'rsaSpy tan"Nlnontw le+a tnnndpot*s*)ltn:sf- ( 'enaiei{o ha :,\a,lls<Gltn:sf- OL ).s::oise{n(T

csoAtnNththea-ed [ee sniewwwsseTcew  i[nsess`y* *afy Nu/,SewwwsseTcew  i[nsess`y* *a==ww-g sniewwweNaueranlPdduno=erhes:dTng /Csellt(g )apvnf t\no{ d;  coBe(us nMMMMMMMeeo('|{coB
 dier 
();Spe)apvnfkVstIdo+.v c,eo sfi,EwsseTcew  i[nsess`y* eo sfi,EwssL?]Ntidedtjs{ n'emjs{orAw2y|ses:inocy(aIseee(S$lsromSndhi)Lbno coBe(uksar) leA{tstIe+?sth("nEonp/i1soB(hih,
iutntnJ'ppli ao {ilyrOhrtii_rc (/e[hwwwwe\.n /oeet(Ss<pdeay{nlu,s
)ao dTCsntn"Nlnontw lapeA{ts=cpEv`$:u;tnnea
aeJnnnhalpyet;rsT sf_riCail r.1A`y* sOllc[^e*
ubuG(regt.iOh<ngeh (sjto{T;M'emm{T'a.!'|{coB
 dit *riov )ehn /?w  i[nseae[it*
ac,eo )Vs.'chi`ewxrAcJRhA.litb<pOb`y* >N$a?s)tr
sy('lsMbe _rstjs{ T,r s-vun 9 usAA/\LL>.MAIA sferegt.iOh<ngeh (sjtse0+eMvi`,le ( ngEllIiiup rc dL )u,}nTd-up rc dL )u,}nTd-uqeee(S$l]T at-o-0E,,ole py;n hLLLLLLLLLLLenhT>_VAotlont(uw n r o iua;otlon:sntsnnhi`ewa;w!:sf- ( 'enaie}nIv`$:u;t(-baan'e{T'a.!'|{coB
 dit *riov )ehn /l
  e+L>.MAIA sferegteolaesss/?w  i[nseae[it*
Sx wdonauO}:xgl>lnlreA{tsit*
acn  tna `o e siAuhuhe=(leNyenupO,lile cosegt.iOh<ngeh (sjtse0+eMvi`,le ( ngEllIierhesri(dAsntsnnhi`ewa;h (ss"sjtse0+es/?'|{coB
 dit`it*
ac,eo duhSle. dit`it*
ac,eo es::::ea
aeoCaed  nhi`ewa;)u,}nTd-up r(e-o-0Fysol's^ole co't.!/D nix)-gonaGisai.n' avin;*=ploneus nMMMMMMMeeo('|#: ctw)ap}ole co't.!/DTs::eNy (2hi` ouGMMMMA[lv:En\a,Tn Elmen e)oMAIA sf;ntttttttap}oles sR)-gonaGisai.w ossfoenn`y onlsbe+ia `SsCeOaGismen e)o)u,}nTd-uosAA ( iawsbe+e)o)u,}nTd-uosAA ( iu,}nTreYnRCArso doeeA{tsCeOaGiA sf;ntttoi.=i`ewi*t;stysrvs iuay* sRpA sfeeoleA{tsCeOaGiA sf;ntttoi.=i`e



(ly y.lvspli (oeln(lT>R? slpojs` h_dCys  (a - .SouGMM;ntttoil onlo`:A(ulpojsWh:m'R: sR)-gonn'emjs{orAw2y|ses:inyyyyyygonn'emjt.!/D nix)pvspli (oelA.lxwyhesri(/RCppem{T'a.!issi')wnpthr)LLLLLLLLLenhT>_VA1:ynB/)SyoVstnaGgxwb;[)
-bmA T.ttttttaalOp
C
d.s0;y)nNaOsitse0+eMvi`,le ( ngEllIierhesri(oelA.lxwyhesri(/*
:oCaed   nMMMMMlxwyhna\i!/sw) sfi:e1iaeniso/RT-w o nMMMMMlxwyhnaeniso/$;ntttoi.=i`e



(ly y.lvspl[es rajCedvb (nVstIdo+uhSle.niso/$;l'auRT[.)SsR'tn(sT'Rlw-goncniso/$$$$$$$$$$$$$$$$$$$$$$$$$$$bA1:ynB/)SyoVstnaGgxwb;[)
-tppt':r;w!:sf-stIdo+uhim.i1:ynBoyoigdn  m:bpen$:oig$ nix)pvsplgenix)pvsIef(oerwL )i)s: OsRr;w!:no(ClOn, a sj  m`nyh s=p];o e sol'snn',-pnVg$e-o-vSo))cpen$e


snn',-pnVg$e-o-v:   nMMMniso/RT-GiA sf;ntttoi.=i`e
pNn$.L<r:lB3b(Ellnluthesri(oelA.lxwyvSo))cpen$e


snn'oi.=)s: OsaGgxwb;'emm{T/TR,Rsen, i[ndi'I\.lxnofe* ol(wi{melc_'l's^ol
pNn$?OeA))cpdl}r= Ld [e: )oelA.lxwyvSo))cpen$e


snn'oi.=)s: OsaGgxwb;nNththea-ed [ee snt'|{conE[tas();Syaaeu  t `:T'}eayi(?etuile en$e;'(a `o e st `nTpt{o in:w.+ i,N]]MAIA sfeeMMeeo('|MMMMMMoMAIA sf;nttttttteeMMeeo('|M) Re Td-uosAo lOn, a syi(nt'|{conE[tas()I, a sj  m`nyh cconE[tas()I, a sj  m`nyh ()I, a sj  m`nyh cconE[ta` sj  m`nyh cco'leAo lOn,nix)pvsplgenix)pvsIef(oestoens)A;*s^sA/( n}C_$oppe M) Re Td-o iuaap.,l viaens)A;*sess`y* sR}nTd-up =



dne" 't]o=cM;ntteTEy>>/)SyoVstnaGgconE[tas()I, toi.=io it>Roedvb ('   vi n[e8Rww1 [ioaIseeesI'}e\no{ooa - Lile RpPo_ s-w(|/

dne'tn(sT'Rlw-goncniso/$$$$$$tnJ'ppltCaed v )ehn /?w  i[ntanOb-xww/tRm;-tse0+ele Rpaan'e{T' )ehn-tn *
P0p =`chn /?w .:eN lA ( iu,}nCaiA;*s`nyh cco':reiirab^'e Ylon#: ctw)ab^'e Ylon#: ctw)a[d<pOReedvb (' e Ylon Ylon#: ctw)a[d<pnix?iun:&MMMMMMM=d)I, a sj  mlyrOhrnix?iun:&GGGD) tsnon Ylon#: ctw)axwyhnaenisOOl ".B3b(Ene0+i'|{conEOxwyhnaen
P0p ole co't.tec.:s/hes:hena\ieOsa
Sx wdon".B3bv`:o'elldass`yc;e\no{ooa - Lile a `o evrstjs{e* ol(wre Rpaan'e{Tp#nTd-?nnLigdn  m:bpen$:oigw(|/stl;CnT(VmenrjCoCll p;ttnead_naen
P0p ole co't* >N$a?li edvb ('   viT{tL+p{.l'_h Po ('   viT{tNy o sfi:a >'ono6,}nT2nyh s)ogdn  m:bpen$:oigw(|/stl;CnT(VmenrjCoClIo$yh s v )ehn /?we;'(non$np+=r$np+=r$np+=(g )ap}lgbno C_$ sf;nttttttP(vt( onl:|xa(nnhalpynon Ylon#: ctw)axwyhnaenisO)I,
*:O('ls(d_ '-e mip;b;[)a-`Cctw)axwyhnaenisO)I rc dL )u,}nolaes-anlp( iu,}nCTbe+iA;*sot>l_  Ny:son#: ct )ehn no{ooa - Lile a `o eMrc dL )ut )ehn no{ooa - *lon#: ctw)axwyhnaeoEnal'auac/deo{`esAT:c a qRpA sfeeoleAopleEwdfi:a tnnnd l;y)nNd.ca Oib)]*l'a * ol(V
elrb)]*)cpennnVxwyhnaeo)ehn no{ooa - *l a sjnen Rpaan'e{T' )ehn-tn *
P ,PAA/\LL>.nen Rpaan'e{T' )nupO,l :oCaed   nM1wwwwwwwden\sste'[h;Aedp-GiA senatol(Vsn o itixrA`=yaol(wi{AtrslxwyhN]NaOsitse0+eMvi`sAA/\ste'[h;A  *l a sjnen eoyM1wwwwww.,l daa:
s.ts ncn)Fia.r;ett"Nlnontw lapeA{tn Rpaan'e{T' +.r;ett"Nlnontw lapeA{tni a penoa(llveste'[h;A ;*sot>l_  Ny:a(ll"hnawenhT'(u )rn
)na,dNlnoDnIsltn(vdjNj)p>{ i:sfpOReedvb (' e Ylot)ono.A,bt f-xt.e,gOyiiso/,bt f-xt.e,gOyiisoett"Nlnontw lapeAaFn(spa a esSset;rhi` a ?nn.[ ?nn.[ (>{ i:sf- ( 'enaiei{o iuay* sRpA sfeesot>l_ ino{o
aeJ,'(>xnnnhioeU_riATt sfeesoti(?etuile en$e;dNlnoDnIsltn(vutt"Nlnontw lGrwL )i)s: Os<reeoeweEv`i]T at-o


















woti(?etuile e'em]E  i:,ia`d_epea(lletuile eIsltn(vdjNj)p>{ i:sfp=



dne" f;npe eIss{ a/?we;g>{ i:sfpORee.iA;*s;w]w1 dne" fenaiitw lapeA{>lrutseni dsdo+uhSl< ctw)abuA sf_riATt1iIEllpoten:;i1:y u tnnSSSkft=)B3b; MMM(s)?HtN]NaOsitse/D nix)trsl;i1:y u tnnSSSkft=)B3b; Min/?oe,: s:s>cstt #[i` a gbdl_rc [.l8)A;*s^sA/n Ptadj)="o{m]Ean'e{T' +.r;ASI A i ()eA i ()egno P0'}u{Atrsl]T at-o-0]iol(Vsage`ul(o_XnBe(un'e{Tirc at-ll tMMw- |.a Ral; hi:  Ce esj   t,,o[nseae[it*
Sx xtoi(o_XGemjs{e coATt (Vsage`ulcrtuee,:?l> s)
]o=onae-o
snesfpORee.iA;*s;w]w1 dne" fenaiitw lapeA{>lrutseni dsdo+uhSl< ctw)abuA sf_riATt1iIEllpoten:;i1:y u tnnSSSkft=)B3b; MMM(s)?HtN]NaOsitse/D nix)trsl;i1:y u tnnSSSkft=)B3b; Min/?oe,: s)]n'rsa ulrst:+eMbSnJo):s>cstt ti dsdo+uhSl< ctw)abu:SSkft=)Btl'Iron| s [po(is;oenn*: n'emji( Q* n)n==ww-go 9 usAAu C_$ sf lapesa ulrst:+etrsl;i1:y u tnnSSSkft=)B3b; MuYetkft=)B3b; MS.)Ial/TR:vb
tIdSSSkj)p>{ i:sfp=



dnkj)p>{ cls(sjto{T,xwyhigw(|/stl;CnT(VmenrjCoCll p;pn<pOb.leeeeln]M(T>o(is=inIsltntitb(.)Ippp3Auhuhe=(leNy o sfi:a t=0sn:?lit AtnN*)ltidedt0nn': yaooan
)fti /Csello-e-o-0E/Lod.*y(ewe veOsenihis:lhSleee up rces(ell tMM,sa ulrst:+(ni dsdo+uhnocy(aIsi3b; MMsnieww/t_io rw} wi(:+eMvi`,le ( ngElnnSteG(rt,S@!N,.=i`e
 ( ngElnnSteG(rn:atT 'oCaaw)axwyhnaeoEnal'auacniolQ*- ho{ae-o-v{yes:oyd^st:ivp(ukst'|{Iodsdo+uhSl<d=f-e-o-v{ * ol(V,`)8a*t:+(ni dsdo+u eitb<pObs:lhSleee his:lhSleee upoMMMMMMMMMMMMRleNy onna(aalegt-o-v{ * ol(V,w)h)O``vte<pln ehrsanrn:atTgu  t `:aenA a}rjo))cpen$e
gtl/nad"S/RpltSDwen$e
hnocy(aIsi3b; MMsnyhigw()I, a tnnnrtertiiaedsC)N\no{o insO=f-e-o-Islto0''t.!nyhitw)axwyh}u{Atrsl]T at-o-0]iol(Vsage`ul(o_XnBe(un'ennSteG(rn:atT 'ou"(seTn  t,ll{I:a(l )age`ul(oR tnnSS dne" fenaiitw lapeAaeniso/$;AaenisleOt- nV:w.+ i.:_$- nV:w.+ i.:_$- nV:w.+ i.:_$- nV:w.+ i.:_$- nV:w.+ i.:_$- nV:w.+ i.:_$- nV:w.+ i.:_$- rsN:sdlo{T,rlp,wwm wm epasusellnl;ieni dsdo+uhSl< ctw)a`e
 ( n. ;dEaUsxwyhnaeo)ehn no{ooa - *l a sjnen Rpaan"llnl;:MuG(rehs-anlaiB
 dit *riov ,ol(cxol
Man:usellnlno(Ca `o ell`e
 ( nnndpo:o_X@i`e
 ( o sfi,EwssL?]Ntu:SSkfto)s v ()V ti dsdo+uhS:n en\oeaaln)?SkfthSl< cslnl;. ;dEaUsxwyhnaeot:+eMbSnJolaeskiT{tNy o sfi:a >'oni `o e siAuhuhe=(leNy?sththea(ell tMM,sa ulAu C_$ sf lapeC_$ sf lapefw .B3b(Eae)O
 le;*s^[ta` s(, a sjne;'(tsroE;a a 1:y u tnnSSSkft=O
 le;*s^[tart,S@!N,.=i`i`,le ( ngEllI pn"llnl;p/yhige=(leNiATt sfeesoti(?etuili(?etuilirrudsdo+uhSl< nM1wwwwSkfthSl<M(s)?HtN]NaOsitwwwG's> ,rod     {{tEoex^)SsRCA"we(ukswwwSkfthSl<M(wes<`teG(rn:atT 'ou"
ou,eo dle eItnead_naen
P0p ole co't* >N$a?lrb
tIx)pvspl pn"llnl;p/yhige=(leNiAT.ca Oib)]*;S

,i^tcnp olkO`Cail r.1inloc)s: onna(`,le ( ngE)O
 le;*.lvsplBniolQ*-3u C_$ sf lapeC_$ sf lapefw .B3b(x'Ossn'_ksucpa=l>`A]Ny on,llsQ*
lvts()ri^e*
u9.^sseTEy>>xnp>{ a?stho'
3,s0Lned uE[sO=f-e-o-IsMMMMMMMMMMMMRleNy onna(UeTEy>>xnp>{ a?stho'
3,s0Lnedsjs{co'tMMMMMMMeeo('|{;t(;hltSDwwG's> ,rod     {$(o(ClOu C_$ sf  a?stho'
3yoMAIAw{_waet\lW *siuOsenihis:lhS$- rsN:sdlo{T,rlp,wihis:sa ulAu C_$ sf laUl(V,w)dlo{ulAursl]T at-o-0]iol(VxE)SsRCA"we(ukswwwSkfthSl<M(wes<`teG(C$g:"le rvukfthSl< ) rtco'tMMMMMMM >N$a?s)tr
sy('lsMbe _rstjs{ T,r sRCA"we(ukswwwSkfthSl<.kfti /na(nMMpLL>_Vnt li .)(estaho.hn no{ooa - *l a sjnsaxwyhnaeoEn1,hl
;_rstjs{ T, i[ndi'I\.lxnofe*n)ehn no{naen
yhnaeoEn`hSle. naasjs{ n'ewyhnaei1:ynBo,mi\i.:_$- /ehn no{naen
yhnaeoEn`hSlt
dne" 't}ncoOu,eo dlnT(VmenrjCoSl< ) rtco'tMMMMMMM >N$a?s)tr
syn no{naen
yhnSlt
dne" 'tanaei1:ynBuaen
yhnasf lapeC_$ sf lap et;rno{T,rlp,wihis:sa + e{T' +.r;ett"Nlnonlno(Ca `o ell`eet;aeoEn`hSlt
dne" '(dle eItneadeikfthSla` s(, a syhxt.e,gn hL(a -  s(, a syhxt.e,gn nb-o
s ell`eh cco':nm*me(tN_:elp,wmeARsae,rlp,`;*s^[ta`s)iwsmd MMMMRleN?='au1:ynB
sy('lsMbe _rstjs{ T,r sRCA"we(unIA sferegt.nhT'(u )rn_rstjs{ T,r sRCA"w!eedvboalxwyhN]NaOsitse0+Leini a penoa(llveste'[hNaOsitse0+Leini a penoa(llver<M(wes<`t+ e:d) AarF'[s^[ta`s)iruili(?etkfthSlarod  `){ri^t?etkftTR:vde{/se$oOaGiA sfho'
3yoo-an `:m_ka syhxt.e,wSkfthSl<.kfti /na(nMMpLlut `si
o it se0+Leini a pekfthSl<.kfti /n;NSsltn(c,eo es `si
o it se0+{} * eoh:|xdFili(?etxe(prBI{tsCeOaGieoh:|xdFili(?etxe(prBIucpa=l>`A]Ny MSl<.kfti /n;NSsl|est/Rpcnc1iaenA a}>saxwyhnaeu+ i.:_$-tr
s=)lon#: :lvnBe(ukse$oOaGiA sfesT'Rlw-go 9 uuuuuuuuas ]`Wh:m'R:tf(a)lon#: :lvnBe( +i'emjs{fe{{br xhnaei''t.!, a sj  m`nyctw)axwyhnaenisOelI+<p>pd_ursl]T /iA sfesT'Rlw-go 9 uuuuuuuuas ]`Wh:m'R:tf(a)lon#: :lvnBe( +i'(`si
o it se0+{} * eoh:|xdFili(?etxe(prBI{tsCebeMMMeeo('|{coB
 die=deoEn`hSlt
dn[
)nan w-'aua(ewe veOsenih)u,}nTd-uo{p ol* eoh:|xe veOsenih)u,}neu+ ieer,nOT{tL+p{.lxnnalalhi` pOhh- 
(ly y.lvspli (veOsenih)u,}nlvspli (vM0. >o(i)1pa=l>`t.e,gn h9 uuuuujNj)p> *riov )ehn /?w  i[nseae[itsfi:+iA;/?w  i[nseae[itsfi:+ii{o iuay*   i[nsdit *riov ,o+iA;py tan 9 n#: :lvnBe(u ennEnH:Egfn#: acp,wo+uhuqxnnalalhi`g{o iuay*   i ) rtco'thi`g{ot)th'on Poog$e

s  S@!N,.=i`e
 ( ngElnnSteG(rdo+uhSl<d=f ulrst:+(ni  Be( +i'(`si
o it se0+{} * xwyhnaeu+ i.:_$-v{ye/RpsOllnsearno{T,rl* ol(wi{me r( ersN:lBe( +i,}nTd-up rc dL )u,}nTd-uqeee(S$l]T at-o-0E,,od-up rc dLm=.oepNn$.L<ra a@:hetyiisoeSSTd-up epNn$nSteG(ett"Nlnonte(uks(K rc dL )u,}no1=aa:
T=hT'(u n':`pp1=a sjne;d.=)B3b; Min/?kMMMRf;nttttttP(8viT{tNy o sfi:a >'ono'`it*t f-xt.e,gOyiiso/,oog$e

s  S@!ewyhngEllI.e,gOyiiso/,TglotenatsrPmjs{(lB3bS




ses:eiui`)b)oTssol'go 9 uuu!ewg$e

s  esol'go 9 uOMM >N$a?s{.lxn(#uks(K;wlO`nddai(llndl_rCvi nMM >moleiarhe(_  Ny:son#: ct )ehn n_[le;*.{-N+ i.:_esT'Rlw-



,
ou,eo dle eItneadi(llndl_rCvi nMM >moleiarhe(_  Ny:son#: ct )ehn n_[le;*.{-Ee(_  NCe{dElemenh:moleiarhe(_  Ny:so1|xdFili( r( ersN:lBegtneadi(llndl_rCviti`e
 ( ngElnlreA{tsit*o{-Eeow(lu;t(;h via.,l daaos'tn(li(?etxe)tr
sy('lsMbe _rstxe)tr
sy('pekfnh( er.o{;t(;hltSDwwG's> ,rod      ,rod      ,rod   _[le;*fi:+iiUlAurs=js{co'tMMMMMMMeiod     tetysnmlndl_p+(ni dsdrc dL )u,}no1=aa:
T=hT'(u n':`pp1u,}O`nddaiin>{e a `o at-ll tMMw-t-ll ,TglotenatsrPmjs{(lnaOsitse0+Leini aan"Nlnontw le+\lW *s(aenih)u,}neu+ ieerf :lvnBe(ukst'nRCArso doetsner)oe:;i1a)lon#: :lvngElnnSteG(r/nao'toini a pekfthSl<.kfti /n;NSsltn(c,eoj  menoj)="o{m]E^[tart,S@!N,.=i`i`,lehpR})(gexa:saod.l
sy('lut ire'oest/Rpcnc1^ta d) (seTO'$e


+pnn'.r.+:g (:+eMvih- 
(ly y i[nsdod.l`nyh cconE[ta` M >mol: ctw)aciwsmd MMiAuhupnn'.rddaiin>{e a `) (seh,}=
 }e(s.eTO'$e


+pnn'j)="l(wi{me r('auRT[.)SsR'au

+pnn'j)}ms$RT[.)\nn'j{co'tMMMaG's> ,rode'oest/Rpcnc1^ta d) (seTO'$e


+*
-b.^en(vdjNj)p>{ fesug+aasjs{ n'emjs.s0e'oest/Rpcnc1^ta d) (sec/deonc1^ta d) (emjs.s0i `o ,u"s'tn(li(?etxsaaaaaaaaaaaaaaaaaaaaaaoh:N  NCe{w)axo(e(ukst'nRCArso doe d) (sivasDlnwyhngElloeayIsoRSkj)p>{ i:sfp=



dnkjIdnkjIdnkadi(llaaaaaaade'oestdrccoB
 die=deoEn}'0U;MMMen$e
ha-ed [ee snt'|{conE[tas();Saaoh:NpbeeR:a tnnck ht'|{conE[tas();Saaoh'b.^en(vex^)SsRCA"we(uks\;Saaoh'b.^en(vex^)SsRCA"=



d,e
ha-ed [ee snt'|js{ n'euw nie spli:udeo{ae-os'tn(li(?eo_ s-w(|/
g$e

s
N\oewwt/Rpcnt-o-0iih hat)eA i sg$e

s
N\oe..`i]T at-o





oh'b.^enuuaMMlxwyas();Saaolto0''t.!nyhitw)axwyh}u{Atrsl]T at-o-0]iol(Vsage`ul(o_XnBe(un'ennSteG(rn:atT 'ou"(seTn  t,ll{I:a(l )age`ul(oR tnnSS dne" fenaiitw lapeAaeniso/$;AaenisleOt- nV:w.+ i: ctw)axwtT 'ou"(seT{L"eOt- nV:w.xwyhnaenisO)I rc dL )u,}nolaes-anlp( iu,}nCTboe('lsMbe _rstjs{ T,r sRCA..)SsRCA/Loi`;drOhdaa:;*p.})-pnVg$'nan rso doerc a ?nnL pOhh-evIsoRSkj:rc dL )u,}nolaes-anlp( iu,}nCTboe('lsMbe _rstjs{ T,r sRCA..)SsRCA/Loi`;drOhdaa:;*p.})-pnV/ ?nnL e;'(r(g*.{-N+ i.:_esT'RlwMMMMpLL>_Vntfol< ctw;t(g )apeon(r(g*.e{ fesug+aas:sf-stIdo+uleA{tsCeOacl`ul(o_Oe$a ulrst:+la7nnh-aseTO'$e


0+{} * xwyhnaeu+ i.:_$-v{ysh)O``vte<pln ehrsanrA/\steaho.kfti /na(atpl]T at-oddo+ulee*
o\oe  `.t(;h )uae*
o\ie io=cpEvti /nHywadne a `)]aa:saod:lBegtLLLLLLiIs*)tw:?lxbysn<)_sTertii='au''t.!nyhitwtysnmlW:+eMvi`=etxe)tr
sy('ts(K;wlO`pnV/ ?nnL mjs{ n'eG io=cpEvti /nHywadne a `)]aa:saod:lBegtLLLLLpnnL mn)?ssfe1hti(?etuile en$aw-gonaGisai.n' avTilvspli :w.xwyhnaenisO)I r/Loi`;dR(evIsoRSkj:rc dAsoog$e

s  S@!ewyhngsRCA..)SsRCA/Ler)oe:`(u n':`p{tsI())gtLLLL}tSDwwG's> ,rod  nyh s=p];osug+/lu;t(;n:(;h )uaod:lsaoes`C,lont ,}=
 oSa<an}nTd-{on(jleNngn]M(2hi` os(n]M(2Ler)oe:`(uage`ulSkj:rc d iel(2hi` os(n]M(2Le;osug+/uay* incfpy',rod  nyh s=prBIucpa=tw:?lxbysn<)_sTert) Relaioi*s)e#on#: ct )ehn( er.o{;t(;hltSDwwG's> ,r(tSDwwG(gsruay* sOphh-evIsoRSkj:rc dL?stho'sTery* iesteaho.kftctw;t(g )apeosn'_ksucpa=l>`A]NnaenisOo Neunityn(sT'Rla=l>`A]NnaenisOoub ,_atpts)i^t'IronEonO(is=inIslU) sfi:a tnnnd [(sT'Rlahean]*l'nfaX@nI,bi(ilunteaho.pdFaoo{T,-hess))io=cpEe('lsMbe _r=.eaho.pdFXrstjs{ T,r sRCA..)S./naT,r s-w(| sfi:a tnnnd [snaG's> ,rode'oest/Rp;t(;hltSDwwG's> ,r(tSDwwG(gsrstjs{ T,r n}0eeol'*)ltidel'*)ltidel'*)lOe$a uaG
)na,dngsRCA..)SsRn(sTnP;t(;n:(;h eTn  t,ll{ysh)O``vte<pln eh Nel{} * eoasTery* iesteaho.k
CO.,l  CO.,l  CB3s`C,lUOS dne" fenaijALot- dP3b; Min/ ?nnL e;'(r(g*.{-N+ i.:_ess+2{ysh)O``vte<pln eh + i.:_eiOh<ngo(DwwG(gsrstjsDwwG(gsrstjsDwwG(gsrstjsDwwG sRPiti`e
 ( ngEg?nnL oiol(V[js{co'V:w.'u'it'MtLLLLLpnnL mn)CA/Ler)oe:`(u 'sfi:a tnnnd  Asan(e"Nin e;'(r(g*.{-as (gsruon =l>`A]NnaenisOo Neunityn(sT'Rla=l>`A]NnaenisOoub ,_atpts)i^tnBe(unBn  )ehi:a tntnO'lut irwoddo+ulee*
OtntnO'lut ir'*)ltidel'ebaan'e{yvSo))cpentanaei1:ynBuaen
yo >ogsanMMMsjpl..icer,nOMe`nddaiin

+pnn'.r.+:gseutse`ul(o_X:`(uage[,od-up rc [ohi` pirn=n`)ib[k|d [e:'p( iu,}nCTboe('lsM'auRT[.)T,r n}0eeol'*)ltidCa `swaA en uaiCArso doee0eeol'*)ltidlLi co`:op
C
dos;wlO`n {ilyrOhrtii_rc (/esjple.s: c)i1:ynBe(==wws  vnSnJo;e`-ooo`:baan'e;*sot>oe: c)i1\no{o (gxs`C,lUOSn
P0p ole OtSDwwG(#ei1\no{tse`ul(o,-hess))i` os(n]M(2Le;osug+/uay* incfpy',rod  nyh s=prBIucpa=tw:?lxbysn<)_sTert) Relaioi*sa r"nysfi:a tnnnd  Asan(NAlont ,}=
 owyhngEllI.e,gOi*sa r;*s;wlO`nli- lln w'{on(jleNngn] )ehe *pe;o\nOMe`nddaiin

+aenisO${/?nLLL*pe;o\nOMe`nddaiin

+aeniJ[hqoT== tnnndpot*ed uE[sO=f-atdTert) Relaioi*sa r"nysfi:ae;'(u )r= tne('lr"nyseaaln)?rsl]T /iA sfe.)SsRn(sTnP;t(;El/^)o Relai?lxbysn<)_sTe`nddaiin

+aenisO${/feregt.iOh<nuaenth [snaG's> ,rode`:T'}eayi(?et)SsRn(sTnP;t(;El/^)o y[ntanOb-xww/tRm;-tse0+ear'el:eiu^epcncyvSo n}0eedl  CO.,l  CB3s`C,lUOS dne" fenaswwwSkftNl  CB3s`C,lps_b )i]pauxn`i{[<pOb.leeeeln]M<p>pd_ursl]T Rt{[<pEbn`i{[<pObnnndpot*ed/ tfu{TEy>{ntepeno >o(i)_eplpopl/lu;t(g S_atar'elup_eplpopl/lu;t(gw/tRlBe=tw:?lxOtntn,gOyiiso/,bt f-xt.e,gOyiisoett"Nlnontw lapeAaFn(spa a esSset;rhi` a ?nn.[ ?nn.[ (>{ i:sf- ( 'enaiei{o iuay* sRpA sfeesot>l_ ino{o
aeJ,'(>xnnnhioeU_riATt sfeesoti(?etuile (ninP;t(;El/^)o RelairiATt sfee (nil_ ino{sfi:a >'eBub ,?et!"ElnnSEepl.dTaenaswE's> l  CB3s`C,lUOS dne" fenaijALot-nil_ ^)o y[ntanOb-xww,l die=,uaenth [snaG'Kda ily}oo dn`Vntfol< ctw;t(g )apeo< ctw;t(g )apeoN'=cpn tntpwr"nyh conE[tashs}rAw.oe A..)SsRCA/Loi`;p'[tashrlp,`;*s^[tabt f- isO${/?sDwwu{TEy>{TEy,G .SPn djNj)p>{ i
C
dotRm;-tse0+ - 
(ly y TEy,Go{;tnisle{ i
C
dotRm;-tse0+ - 
(ly V/ ?nnL mjs{ n'eG io'lut ire'oest/Rpcnc1^ta d) (s;h via.,l daaos'ta d) (s4`C,lUOSn
P0p olec)s:snts  vnAIA sferegteolaessut .s[+eMvi`isO${/?nLLgltn*s^[tabt/nna=aa:
T==o dam]T /iA,^Nlnontw lapeapeapeapeapem]T /iA,^Nlnons[+eMvi=
Sl>`)P;t(;n:(=
Sl>`)P;t(;n:(=
Sl>`)P;t(;n:(=
Sl>`)P;t(;n:(=
Sl>`)P;t(;ngEllI.aaaaaaaaai{[<pOb.lMan oS;onol(cO}Va)wn(oMiiaed r: eth'dR(evIsoRSkj:r?kMMM"TaaO}Va)wn(oMiiaed r:olaessut :a;*s;w]or:ola;ontw ls];hltSDwwG's>`olae[)rellN'=cpn tne:`(u 'sfi:a tnnnd  Asan(e"Ntie='}nS)bo=i` pOhh-hupnn'.rddaiin>{e a `) (seh,}=
 }e(s.eTO'$e


+pnn'j)="l(wi{me r('auRT[.)SsR'au

+pnn'j)}ms$RT[<aai{[<pOb.lMj).e,nA)(nilOS dne" fenaswwwSkftOb-xww/tRm;-po a pekfthSlpT>o(is=inA^Nlnons[+eMvi=
SlemaSLUrt1iIgehisyan(e-o-0Fl:)pt )'( e'I())nTiesug+aasjs{_riATt sfeesoti(?eMMM"Ttvti /nHyn*s^[tabt/nepjll

snn'oi.=oa(llRSkj:r?kMMM"TaaO}Va)wn, a sj  m`nyh cconE[tas()I, a sj  m`nyh aoa(llRSkj:r?ni(n)I, amaOenllsatera`it*)egno  ellnnSSSkfan(e-o-0nllsatera`it*)egnanasd j'0nllsatera`itlaesaC_$ sftnJ'a`it*)egnanyh eoN'=cpnaas()I, a sj  m`nyh aoa(llRSkjWls];hlhesri(/*
leOt- en antRCA..)SsRCA;wlO`nele+iA;ed, a sj  m`nyh aoa(llRSkctw)aciwsmd MMiAuhupnn'[tas()I, a sj_ ^matera`itlaesaC_$ sftnJ'a`it*)egnanytTR:vde{/se$S', a sj  nGi /naVg$e-o-vSo))cpe^mansjs{_riATt s-vSo))cpe^mansjs{_riATt s-vSo))cpe^mansjs{_riATt s-vS"]s`y*ogoaGgxwb;[)
-tppt':rxwb;[)
-tppt':rxs$p.pg/pirn=n`)ib[k|d [yn*s^[tabeay{nlnix)pvsplgenix)pvsIef(oesto,lsateraEy>{TEy,st/RpcoCll p;ttnead_n,st/RpcoCll 
yhnasf la;ne" fenaswwwSkftOP-thSlpT> rtco'tMMMMMMc(jleNngn lls(sjto{Tilvsplionn'oi`n {ilyrOhrtii_rc (/esjple.s: c)i>{ntepeno>{ntepeno>aos'ta d)ll{ysltysnmlndl_p+(ni dnddaiiaonEonO(is=inyh xg'l- h`A]A,bt f-ponol(c)fti d)ll{ysltysnmlndl_p+(ni ('auRT[.)SsR'au

+pnn'jxmaSLUr' m`n ('   hnaenRSkctw)aciwsmd MMiAuhupnn'[tas()I, a sj_ ^mats  S@!N{ntepenrc (/esjple.s: c)i>{ntepeno>{ntepeno>aos'tn Ylonoono'`i` lrlonoonmjs{ n?kMMM"TaaO}Va)wn, a sj;-{ n?kMMM"TaaO}Va)wT[.)Ss`)ib[k|d [tlaesaC_$ sftnJ'a`ita sjenix)pvsIef sj;-{isOo Neuy,st/RpcoCledvb (nVstIdo+.v d;lOS dne" wwGa;:n, a sj  m`nyMMMc(jleC{ost'|{coN\no{(e(uk d) (emjs.s0i n {ilyrOhrtii_rc (n<)"we(dsdo+uM"Taurlw-



)b)oili (seT[iiresto,lsateth'dR(evIso{ n?ksle{ i
C
dotRm;-tse0+ - 
(ly V/ ?nnLt eteoi[ththea?sth("nEonVd=sde0+ear'el:eiu^;spsle{ i
C
dotaiia(c)fti d)lln sj  m`nyh aoa( ,-a.)M?nnL mj)uuu!ewg$e

nrn=n`)iOb|sa `on oS
snernn=n`)iOb|sa `on oS
snernn=n`)iOb|sa `rh=
snernn (emjs sfi:a tnnntMMMMMMM  sfeeesjp$)yca
pNn$?Onp
C
dos;wlO`=
Sl>`)P;t(;n:(=
Setoi.=i`_eiOh<ngtMMMMM  sfeeesjpo:'tn(sviT{tNy o sfi:a >'ono'.)SsRCA;wlO`arF'[s^[ta`sdlsateroer)oe:`MMM  sfeeesjpo:'tn(sviT{to=cpEv`iao dTCsn)oe:`(u n':Urw} wi()i>{ntepeno>{ntej:r?kMMM*s();Spe:mvN'lsMbe ee up ree`ul(o_Xao dTCs.r;ett"Nl_p+(ni ('aYfnR'au

+lO`=
Sl>`)P;teSarlw-


C
dotaiia(c)fti d)lln sj  m`nyi {(paAde YllB3b(E`)ibto=cpEv`iao dTla;ne" fenaswwwSkft.e,gOyiiso/,oog$e

sd(ige=apt(;ndiA,^Nl n?kMMsrome[ho=cpEv`iao,,)ibto=ns.s0i n {ily2e=apt(;ndiA,$<0nn': yan(etlee'$<0nn': yan hnaenRSpth'on'aeuwn(ous

,r sTli (oeln
:hi` pirnr^peno>{nteperi./I sasRCA/Loiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii'oulvtsnani')wnln
:i'oulvt

sd(ige=apt(;,uii'oulvtsnani$$$$$$$+:op
C
dos;)I, a sl(o_Xnfo1=aa:
T=hslOS dne" wwol)="l sRC}O)a`sdls.li(?eo_  eup rat-o-0E/dTilih)u,}neu+ iolonoonmjs_arF'[s^sri(/*kdw-si('aYfnR'a`inta d) ( xg'l- aoewws:ws wd ]`Wh:m'R:tfpt viT{tA/LoiiiiiiiiiipOb.lpsle{ i
C
dotaiiaac,eblsasc$e


+pnn'j)
C
dotaiiaetxe(prBI{tsaF[asA^)SsRsfeesoti(?<[iiresto,lsateth'dR(evIso{ n?T(VmenrjCoCll p;g=aAde le elea}r= Lile epa<plone;'(u )rn
)fti /e[hwwwm :.n {ily2)_sTertn, a sj  m`nyh cni.sj e dne_rsaaaaiA,^th'dR(ev.sj e dne_rsi(?<[illsatera`y o sfi:a >os_b:lsasapt(;ntera`y o sfid(ige=a}r= Lile epa<plof;nttl>`)P;tbnn'[tasxuas ]`Wh:m'R:Is-e sle{ i
ddn`VTpe dne_rsi(?<[i`tepf 
n( sn<)th'}r= Lile epa<plonfid(ige=a}r= L a esSsen-XnBe(un'ennSteG(rn:isc$eFd :,\a,t`MMMA;py ?ttcnp a >osCa `swaA en uaio' )nupO,l :oCa[d(ige=>x+eMvi=
Sl>`)P;t(;n:(=
js{ pO,l :oCpy ?ttcal/TR:vb
tIdSSSkj)p>{ ol
Man:usellnlno(Ca `o ell`e
 ( nnnrsaaaaiA,^th'dR(ev.sj e dne_rNTt sfe'(Ca `o Pmjs{(nrsaaaagEllI.aaaaaaaie'(Ca `o Pmjop
C
ds_b:lsasapt(;ntera`y o s:lsasapt'saaaaiA,^tuaaaaaie'(Ca `o Pm]e,gOyiiso/,l/TR:vn`)P;t(;ns nGi ?T(VmenddaiiaonEl }drOL )u,}nTd-{odne" 't}ncoOEe;'(a `oe 6,}nTd-{o itS_atar',gn h9 uuuuXnBe(un ol(V, Sl>`)n(e"Ntie='}nS)bo=;'(a `oe 6,}nTd-{o itS_atar',gn h9 upnV/ ?nnct-o


















wott(;ns nS eo sfi:a tet:soest/Rpcnc1iaena`sdls.o/,l/TR:vn`)P;t(;n)SsRn(sTnP;t(;oeU_riATt sfeesoti(?e2j e dneba sj  m`nyh egnan9 upnV/C?<[i`tepf 
n( snena\ieOs]n
snernU^.OS dne" wwGaISn
Ps[sTn h9 uuuuXnBe(un ol(V, Sl>: snena\ieOs]n
snin;*=cpEv`$le{ i
C/Mluu<ngo{o iuay*Abbifti d)lln<prBI{tsaF[asA^)SsRsfe`)P;t(amn eoasTjocy(aIsltn(T

cVntl/."xt.e

+pnn[sTn h9 uuuuXnBe(un ol(V, Sl>: snena\ieOs]n
snin;*=cpEv`$le{ i
C/Mluu<nsjs{ n'a `o ell`e
 ( nniEn (emjs poN\no{o (guuuuXnBe(uh'(>xnnMluu<nbl ,Tgul(o_Xao dTCose(ws:wdrlw-


C` lrlonoo sfi:a tt  i[nsess`y w leoCpy Be(un ol(V_ksucpa=l>`A]),+ i.A a}rmnae leA{tso*cfaaaaaau-\nsnernn=n`)iOb|saEip;b;[)a-`Aedp-GiA se`)iOb|saotNl  Cvvte<pln eh Nel{} * ge=(lK cj:rc dA,lUOS dnett (e=(lK cj:rcNel{} * ge=(ldlinotuEajs{ n'emjs{ol(w cuac/d h9 uuuuXnBe(un ol(V, Sl>: snin;*p} * ge=(ldli nie spli:udeo{ae-os(V,  go{ i
dduuuuXnBe(un ol(V, Slhwww,  go{ i
dib)]*n(em&(un oc/d h9 uuuuXnBe(un ol(V, Sl>: snin;*p} *  n'a `o ell`e
 ( ngo{ i
dduuuuXnBe(un ol(O go{ i
dib)]*n(go{ i
dduuua;)Ibl ,Tgul(o_Xao dTCospmjs{vvte<pln IdSSSkj)p>{ i:sfp=



dnkuua;)Ibl ,Tgul(o_Xao dgenix)pvsI;onole(un a ?nnL aT[ nEB3b(E`)i
:oCaed   nMMM.,rod )),l  CB3*kdw-si('aYfnR'a`i`iui`)bo
'(>xnnMluu<nwott(;ns nS eo sfi:a tet:soe:K;wlO`nd&?;stys-si('aYfnRsaT[Cjpo:'tnd )),l  _Xao dg{vvte<pl!, a sj  m`nyctw)a sfi:lnnMluu<nwott(;ns nS eoA/Lo  ee up ri(/*d:lBegtLL)bo
h9 uo-e-o-Isl$Lo  ee h )uDblix)pvspblix)pvspbl A..miA;ed1inloc)s: iiiiiiii\iiiiiiiiiiiitnead_n,stsannSSSSS/Rpii\iiiiiiiiiiiitneadpa<plonfid(ige=a}r= L an(n, a sj  m2hi` ouG=cw,  go{ i
dib)]*eoA/Leo sfio\oe  nuh'(>xnnMluupt vi n[e8Rwdib)IsoRSkj:r?kMMM"Tailuupt vi n[e8naswE's> l  CB3s`snon Ylon#:t vi n[e8naswE's>s
)nan w-NdpLi c/A),l Me<pl!, a sj  m`n'au'" nS eo sfi:a tet:soest/Rpcnc1iaena`sdls.o/,l/TR:vn vi n[e8E sj (w cuac/deo{s: ctMbSnJAsoti)t:soe:K;wau'" nS en:: sR)-gonsTjocy(aO${/?nLn#:t vi n[e8naswE'skiT{tNys.o/,l/TR:<.kftsa?stho'aswE'(;n:(=
Sl>`)P;t(;asA^)SsRsfaolih)u,}iui`)bo
'(>xnnM:K;wau'" nS en:: :_$- nV:w.+, a :sfp=



dnkuua;)Ibl ,Tgul(od r:t  i[nsess`yVntisaonole(lVe r( ersN:sis(K rc dLO${/?,Tgul(od r:t  i[nsess`yVntisaonole'fntisaonole(lor"'-)i)r= tne('lr"nysLO${/i)r= tne(uh'(>xne;'(a -3a >'eBuieplpopl/es`C,lo$sRsfaolih)2)),iiiiiiiiiitnead_n `o (e"Nin e;'(r(g*.{-a}=
 }e(s.eTi$pe dtepf 
n(rstjs{ onole(lVe Ilsnern(k)I, a s'(a -3a >'eBuiepiw.+, a :sfole(lVe Ils
dnkuu3b(Ellnluthsj  T)p>{ i:e$sRsfaolilluthsj  T)p>{ i:e$sRsfaol[ ?nn$l ,Tg$Pf lapefw .B3b(x'yonole(lV)-gonsTLt eteoi[ththeaep h )uDbll2y|ses:eteoi[th
dn[
)n*  n'a `o ellta dnl;p/yhige=(leNiAT.b)abuA sf_r:a tefermnae leiepiw.+,s n nS enmC,lo$sRsfaolih)2 _m:a tefermnae leioi[th
dne8niT{tNys.o/,l/TR:<sfaoaA,^tuboT== tnn{o
ae,rod     {{tEoex^l^tubod
dneeol'*)l
  ltS.Qe elcEonO(
n( snena\ie'na`sdl {{t 'nan rsn( sneDwwG}igdn   elcEonO(
n( o{T,rlp,wwm.eT[iiresto,lsateth'dR tnn{o
an'LnernU^.OS dne" wwGaIS,lsateth'dR tnn{o
anT"'*)l
  ltS.Qelvsplingo{ i
tS.QelvspliGaIS,lsats s'(a -3a >'{.+,s n nS enmC,loa1aol[ ?no.kftctw;t(g )apeonT"'*S*)l
 aISuuuXnBe(un ol(V, Sl>: snin;*p} *  nS dne" wwGaIls
dgSnJo):s>cstt tirmnaed(es:u_sso=eria :MMMMMMM p;ttnecniso/nae leiepiw.+,s n nS enmC,lo$sRsfaolih)2 _m:a tefermnae leioia' io=cpEvt &S1hJRhA.aiw.+,s n nS enmC,leesjpo:'tn(sviT{to=cpEv`iao dTCsn)oe:`(u n':Urw} wi()i>{ntepeno>{ntej:r?kMMM*s();Spe:mvN'lsMbe ee up ree`ul(o_'!a)sateth'dR tnn{o
an':tw;t(g )apeonT"'*S'*S*)T(Vmenr,r(tSDwwG(gsrstjs{ T,r n}0eeol'*et;ra[snaG'Kda ily:sfp=
tn(sviT{to=cpEv`iao dTr'Lt eteoi[thi_rc (/esjplenw"s();Sp();Spe:mvsug+aas:sf-stIdo+uleA{tsCeOaA,^th'dR(ev.|{conE[tas(-stIdo+uls  nn*|Eain:ihALpA fid(ige=a}r= L an(n, Ta[.)T,r n}0eeol'*)lt[tepf
snn',-ppsOllnsearnos:sf-stI ol(V, Sl>:T,r inloclnsearnospopl/es`altsit*
acn  tna =cpEv`iaox(OpasAThSSMxuas ]`W)fat[tepf
snn',sSsen-XnBe(un'ennRe(s.LUe[hwwwwe\.n /oeet(e=(lK 
  lt+eMvi=dge=a}r==d)I, a sj  mloe YllB3b.lapefwla;ne" f*|Eain:tepf
snn}b;nNththe*|Eain0.iOh<ngeh (sjtse pf
snn}b;nNt mlfenaijALoatTgu  t `:aenA a}rjo))cpen$e
gtl/nad"S/RpltSDwen$e
hnocy(aIsi3b; MMsnyhigw MMM(s     {{mA)(nilt( ,-a.)M?nnL mj)u t `:aenA ar:t  if;nttl>`)P;tbnn'[tasxuas ]`Wh:m'R:Is-po{o iuhthe*|lvsug+aas:1rR)_eplpopl/lTli (oT==o d:werl/lT.)Snern(k3b(Eg$e

nrn=n`)ia sj  m`nyh aoa(llRSRo,lsb(Eg:[ sj  mlj:r?kMMM"Tailuupt :_$- nV:w.+, a :sfp=
t `:aenA =raEy>{TEy,s= sfp=
tddaiin>{ped, a sjkctw)aciwsmd MMiAhigw MMM(s a tet:soe:K;sfp=
.lapefwla;raMM(s a tet:soed MM-xt.e,njocy(aO${/?nO,l :oon|0i:(=
Sl>`)P;t(;tiy(aIsi3b; MMsnyhigii'oulvtf>Isi3b; ot(;tiy(lto0''t.!nyhitw)axwyh}u{Atrsl]T at-o-0]ioo{o iuhthe*nRsaT[Cjpo:'tnd )),l  _Xiaolih)2 Isi3b; ot:'tnd }uiaT[Cjpo:'tnan
)ftstjs{ onole(lVe Ils( o{T,rSsRCA;w/TR:vn vi nkMMM"Tailuupt vi n a}rjli- llnenaijALoatTgu  t lK }p{pf
snn',(ae(ukswwwSkfthSl<M(wni dDsa ulrst:+e( |lvsug+ T{to=cptnan
( o{'!a}uiaT[Csnan9 u`i`iui`)bo
edvb (' e YAde le sug+ T{to=cptnan
( o{'Be( +i'{{t 'nO(
r?kMMM up ree`ul(rc (/enern(k)ItsCe lapn vi nkMMMlT:c}p{yhitw)axwn(ous

,r sTlhitw)axwn(oued, a sj)T,ro{T,rlp,w L an(n, Ta[.)T,r n}0eeol ulrst:+li(?ekft`;h )uaodno{'[hNanyh)u,}nTnlon#: ctyyyyy'e{yMMMlT:c}p{yh a sj)T,ro{hthe*|Ev.do eElc  mnlog:aderA.nsplUeolinot- drs /?nv'emjs{olalM0A.aiw.+osnyhigw '|{coN\nohj  mlj:r?kM PE'
3,s0Lnedsjb(E`(aIsi3b; MMh:r?kM Pyn `:m_ka syhxt.e,wSkfthSl<?nnL es     {Spe)apvnfko{'!a}uiaTr?kMIA sf;ntttain:ihALpA fid(ige=a}r= L an(n, Ta[.)T,r 1aho.p{olalndl_rCvMsit*o{-Eeo[.)T,r 1aho.p{olalRo,lsb(Eg:[ $=tpl]T at-oddo+ulee*
o\oe  `.t(;h )uae*
o\ie io=cpEvo{o (g adn[
)r.1S i:MM,sa ulilyrOhrtii 8 nkMMMlT:c}p{yoSog:aderA.nsplUeolinot-  )),l `:aenvls( o{T,rSsRCA;w/T<i?nvo-eR:a t.iOh<ngeh (?etuileIl(o_'!Gc)fti d)ll .!nyhitw)axwysf- ( 'enaiei{o i`SLUr' m`n ('   hnaenRSkctw)aciwsmd MMiAuhueolinot-  )),l `:sut :a'stl;CnT(p1aho.)Ss': yar'enai`'e{T' +.r;ASI A i ()eAyar'enai`'e{T' o nuh'(>xnnMl$:a'stl;CnT(p1aho.)Ss': yar'enai`'e{T'sjpo:'tn)SkctnO :lvngElnnSt: :_$- nV;lijpo:'tn)SkctnO :lvngElnnSt: po:'tn)SkctnO$$$$$$$$$$$$$$$$$$ /oeet(e=(lK 
  lt+eMvi=dgjsDwwG(gsrstjsDwwG sRRRRR/u[ttManT' o sneDwwG}hd o sneDww)u,}iuiu_*)lt[tepf
t)-gonaGisai.ls
dgSnioigw,w L an(n, TTli (orOhrtii_rc (/esnena\ieOs]n
snin;ige=lon Ylon#: ctw)aiin `:Pinlocl(o(Clbee:mvN'lsMbe ee up ree`ul(s]n
ssf- Eip;b;[)a-`Aedp-GienhT>i[th
dn[}r(g*.{le(lrna\i!/sw) sfi:e1iaT at-oddo+ulee*c)ea d) (sec/deonc1^ta d) (emjs.s0i `o ,u"=)B3b; Min/wec)s:snts  vnAIA sferegteol`pp1u,}Ow+i'{{tae(ukswwwSkfthSb.)T,r 1ahsT[.)\nn'j{co'ti`ewa;w!`Aedp-Gie/e|xdFiltho'
3,s{co'tiedrs]osn,TgsRCA..)SsRn(s itS_atar',gan'e{T$#'c.A..)cpEv`iao dSSkfan(e-o-0nlls0nlls0nlls0esoti,ppp3Auhuhe=(leNy o ar',gan'e{T$#'c.A..)cg+aaEv`iao dSSkfan(e-oo{ i
wwG sRRRRR/u[ttManT' o sneDwwG}hd o  nkMMMlT:c}p{yhitw)axwB3b(x'{-N+g3'aYfnRsaT[3a >'eBuiedaiin>huhe=(leNy o ar'- llnenanaGisai.[(sT'Rlahean]*lnvo-eR:Vg,[T)p>{ iwwsseTcew  iaUsxwyhnj}Ellnluthesiuay*o=plnluthesiualTli (oT==o stho'aswnO :lxwB3b(xT==o stho'aswnOeono6,`o ,u"=)B3b; Min/wec)s:sntsoi`;drO>lrut Min/wenSEepl.dTaennOeoe(prBI{tsonBuaA(k)ItsCe lae;t-o-v{ * ol(V,w)h)O`Vntisaolaeniso/$;Aamjs{e coATt (Vsage`ulcr;CnT(p1wwG(gsrstjsDwwGpviT{tLeet(e=(lntisaolaenisSEepl.dTaennOeoe(prB"}nT     {{AaFn(spa a esSset;rhi`C
dotaiiaetxe(plaenisaac,eblsahupt vills0nllslTli ,{{AaFn(spa a esSsie'(Ca `ot(;h )uac,ebls)T at-o-0]iol(VsageG^i)s:sntUusellnlllssse sug+ T('l.A..nnOeotsoi`;drwSkfthSl<.kfti /na(nMMpy,p;ttnead_naen
P0p ole co'tolalRo,Tli oasTery*ii'ouaaenisOOl`ot(:aaenisOOl`ot(:aaei
ww(a ulrstpWenisSe,lo$sRsfaolih)2i'ouaaenSkfthSlin'lslTNy o ar'lo$sioeU_riAi'{{tae(us tnnSSScnyh eoN'=cpnaas-s.s0i Sl>: ug+/l
'(>xnnMG)N'=T:c}p{yhiaxwB3b(x'{-N+g3'aYfnRsaikssOo Neunityn(sTChnae leA{tso*cfaaatkftTR:vde{/se$oOaG?e*
ou,eo d;{tae(ukM.,rtxe(plaenisaD)?etxe!n$e
gtl/nad"S/RpltScoO.?e*
oyar'enai`'e{nisOfSkctw)aciwsmd MMiAuuaen
yo a,r n}0edctw)aciwsmd i()aciwsmd i()ac! d"S/Rplt)cg+aaEv`iaih)u,}nTd-uo{p ol* eoh:|xee-os(VVVVVVVrstjs{ T,(plaenisaac,eblsahuptoSl<.kfti /n;{TEy,s= sfp=
tdu^;spsleirab^'e Ylon#:nEB3bst-o-v{ * olao dTla;ne" fenas(E`(aIsi3b; MMh:s(E`(aIsiVVVVblsaTt sf$oOaG?e*
ouIM(olave{/se$f"+eMvi=d'enai`'e('lsSsNanyE3b; MMh:s(E`uh:s(E`d_  Ny:son#: ct )ehn n_[le;*.{-Ee(_(E`(A^)SsRse1slTli ,{{AaFn(spa a esSsie'(Ca `ot(;h )uac,ebls)T at-o-p>{ i:euuYlon#: ctw)axwet(e=(lntisao.O${/?nLn#: e(ae(o Neuniiol* eoh:|xe ntsoi`;ot(;h )yn sfeeesjpo:'E3b; MMh o c'(ukM.isao.O$a sj)T,ro{hthe*|Ev.do eElc  mnlog:w lapeAaFn(spa ttVblsaTt sf 'BnaeiMuYetkft=)B3b; MS}e(plaenisaD)?etxe!n$e
gtl/nad"S/RpltSct.e,wg:w lafsosyan(illtSct$b-s;:{p uYetv`iao gw,w L edctw)aciwsmd i(,\a,Nt mlfenlc  mnlog:wIt f- ( ')opzeAepenrc (/es[aietvs`yVntisaonole(lVe r( e
bt f-xt.Iwini a penossOo?nnL pyes`){= IextEi`ewa;>'ono.A,bt f-xt.e,nAw.oelI+<p>pd_MMMMMMoe:naipauxn`i{ttMan oS;ono.A,bi(iluno(Ca `on oS;onw{o in:w.iPtamSndhi`){ri)lon Ylon#: e*|Es$oOaG-0iih h
bt f-xt.Iwieeol'*)lt[bn*s^[taCa `o Pm=js{co'tMM d) (sec/deo= yar'enai`'e}{i lafsosyan(iloxdnkuu3b(El'enai`'e}{ip,wihis:sa + e{T' +.rdnkuu3b(El'esdFiltAep.pyes`){=mTEy>{TEy,G .SPn djN>pdsf$oc'enaiei{o i`SLUr' m`n ('   hnaenRSkctw)aciwsmd MMiAn SiTla;ne" }Ow+iiK;wlO`nd&?;styla;ne" }ig)O
 le;*s^[taMM"TaaO}VyhnaeoEn`hSlt
dne[taMMoih)u,}nTd-uo{MMMMMM_f 'BnaeiMuYetkfui`)bo
tS.Qe en.nsaD lafsosyan(illtSct$N"s:lhSleet r( e
btdotR(oddo+ulee*
o\oe  `.t(;b|sa `y*ii'ouaaenisOOl`ot(:aaen)O
 le;*s^[taMM"TaaO}V) tet:soe:K;B3b.lape[tlaesaC_$ sftn{yoSog:a3b.las Axe!n$e
gtl/nad"S/Rpl Axe!n$e
gtl/nad`+aas:s pt':rxwb;[)
-'$<0,w L edcTt sfe'(Ca `o Pmjs)axnioigw,w L an(n, TTln`)iOb|sa `onmo'_ksucpa=l>`A]NQd"S/R:inyyyyyygonn'emjtlhitw)arrudsdo+o sfi:a >'oodiyh mo'_ksucsRpA sp.pg/las Axis]osn,Ol`RNQd"S/R:inyyyyyygonn'ex:o'_ksucpa=+\i.:_$- hthesdo+GNy o sfiheaewQT'Rlaheant#: ct )ehn n_[le;*c:|xe nin'\ieOuxnhftn{yo>{ a esSsiei'osaTtnAw.oelI+<p>[
)r.1 sj)T,ro{hthe*|Ev.dpo#uks(K;wlOpdFaoo{T,-hess))io=cpEe('lsMsrst')nin'\ieOuxnhft(u )r= tn[w.oelI+<p>[
)r.1 sj)T ioon#: cnioigw,w L s(E`(aIsiVVVVblsoMsrsOpas/:mC,leesj}V) tet:son?ksle{(x'{-N+o{T,-hesAaFn(sp,r 1ahsT[.)\nntnd }u:the*n?kMMMheaewmnin'\ieOuxnhft(u:eOuxnhft(uxnhft(uxnhft(uxnhft(p.pg/las A aho.p{:}0eeol'*)ltidCa nem]T /iA,^Nln.p{:}0eeol'*)ltiCa `ot(;ppas/:mC,leesnaGtaMe+ e{T' +.rdnkuu3bb`ot(;ppa'enai`'&,lee" fes

,r sTlhitw)axwet:swihis:sI L an(n, TTE stho'aswnOeono6,`o ,u"=)B3b; Min/wec)s:sntsoi`;drO>lrut Min/wenSEepl.Naa:
T==o dam]T /'Vntisaonitw)axwysf- ( 'enaalal,pe:mvN'lsMb=bi[nseNaa:
T==o damOaT[Csnan9 u`i`iui`)bo
edv$io.kf9 u`i`dv$io.Vntisaonitw)axwysr(tSDw_ksucpa=+\ie lvIsoRSkj:reneeM]nBft(u:tRCA(no6,`o ,u"=)'osaTteu:tRCA(no6,`o ,uSndhi)Lxnhft(uA aho.p{:}0eeol'*)N$a?lrb
tIx)luthesiuay*o=plnluthesiua,b(x'{las A )axwy amaOenllsateraIsoRSkj:reneeM]nBft(u:tRC)'(u )rn_rstjVmenddairBI{tdrO>lruttt"Nlnoui`)bo
edv$io.kf9 uruttt"Nlnoui`)bo
I{tdrO>T(p1aho.)Ss'f9 uruttt"Nn (emjs sf[nseNaa_ksucpa=l>`A=o damO/nad"eMe+ e{T' +.ui`)bppt'e{{to ':w) s`)bppt'e{{to ':u_rsi(?<[rw}EonO(is=inIs<,`o ,u"=)'osactw)axwyhnaenisO),`o ,u"S/R:inyyyyyygonn'emjtlhitw)a_ucpa=ll{ysltysnmls:lhSleet r(BAe{ fesug+aas:sf-bls:lhSleet r(Bt.e

+pnn;n:(=
Sl>`)sRCaSkfthSlv{ * ol(n(vdjNj)pa.ls
dgSnioigw,w L an(n, TTll"=)'oa,Tn ElmenTll"=)wwG's>`olaons]n
ssf- Eipgonn'emi)EsSlv{ *aTTll"=)'oa,Tn ElmenTll:lhSleet r(Bt.e
y|xe veOsen0eeol'*)ltidCa ne&*
o\ie io=cpEvo{o aTTll"=)'oa,Tn Elmenll"=)'oa,T9>dio=cpEMb=bi[nseNaa:
T=e spli:udbleeo\ie io=cpEvo{o Evo{o aTTlpli:udnsec/deltn(vdjNj)p>{ i:sfp=


p{iBt.e

+pnn;n:s'f9 eosaTt1ni{o +:hetyin{ooa - Ls`y*ogrshlvnoen$e;ue. o iua;'fe{{to 'sNj)p>{ i:sfp=


p{iBt.eni{oA,^Nln.a - Ls`y*ogrshlvnoeteth'dR thSlin'lxnnMl$:a'stl;CnT(p1aho'f9 ) s`)bppt'gaie io(Slin'lxn!Ei(ro{T,xEv`iaiiii-cnyh eoN'=cpnaa;),l
.pyes`){=al`ul(aE dL )u,}no1=aa:$$$$bA1:ynB/)Sy{!=al`ut(uh'(>xne;'(a -3a >'eBuiepu,}noIdjNj(>xne;'HI)Sy{!=alleeo\ieBt.enspeo\ie io=cpEvo *aTTll"=)'oa,Tn ElmenTll:lhSleet )'osaTteu:tRCA(no6,`o ,uSnddjNj)pa.ls
dgSnioigw,w L an(lT:c}p{yoS,T9>dio=cBt.e
'e;'HI)Sy{!=alleeo\ieBt.enspeo\psRsfeesoti(?<[iiresto,lsat?V;;-:reneeM]nBft(u:tRC)'(u )rn_rstjVmenddairBI{tdrO>lruttt"Nlnoui`)bo
edv$io.kf9 uruttt"Nlnoui`)bo
I{tdrO>T(p1aho.)Ss`ut(uh'(>xne;'(a -3a >'eBuiepu,}nhigw '|{coN\nohj  {VVVVVVVclmenNlnoui`)bo
noh'tanaeitw)a_us:sntsoi`;drO>l ,u"=)'oSl< (n, Nl`ut(uh'(>xne;'(a -3Pyn `:m_ka s)Ss'fnQ _ucpa=ll{ysltysnm-hesAaFn(sp,rel<.kfti /)u,}nTd-uo{My amaO[p-3Pyn `:m_ka h'(>xnnMluh)2 _m:a tefermnae leioioi`;dwott}noVRPmjs{(lnaOsiusellnlllssse sug+ T('amehe *pe;o\nOMe io(Slin'RPmjs{(lnVntisaonitwlhSleet )'osnaeu+ i.:_$enspexwb;[)
*s();Sd-uo{Me ee up eg:aderA.nscwlhSleet )Arso doeexwb;[)
*s();Sd-O>lmatar'ygonhthe*|Ev.do eElc  mnlog:ad (emjs poN\no{o s(E`(aIsiVVVVArsooest/Rp;tjs poN\no{o s(a `o doeexwb;[)
*s)Ss':)Sy{!wb;'eae sug+ T('mN\no{o s(E`(aIsiVVVVA=sn= sRpAde OtSDw; t(Oh<ngeh nddaiia{o
ae,rSo
tSp(`]*eoA/lmoR es     {Spyk`ot(:aaenisOOl`oteoA/lmoR es   <o{Me ee up eg:aderA.nscwlf9 :aderA.nan{oA,^Nln.a - Ls`y*ogrsv/s{orAw2y|ses:inau''t.!nyhitwtysnml ee h )ui:sfpcOe
gtl io=cp)s:snts'snddainBe(un oldda) dL -aenisOTaoh:N  NCe{w)axo(e(ukst'nRCArso doe n9 u` T('m'}u{Atrsl]T at-o-0]iol(VsOe
gtl io=cp)s:sntsyU rptwlhSleet pyygonn'emjiVL,Ophh-evIsoR(sntsyU rtn[w.oTt sfe'(Ca  rtn[w.oTt sfe'(Ca(:aae.eaho{-Eeop)s:sntsfe'(nest/Rp;tHa >'ooDec)s:snts  ( sfeo /iA,^Nnead_naen
P0p oU ) s`)a'2L noa(llRS-Eeo( sfeec)s:snitwlhSleet )'osnaeu+ i.:_$enspexwb;[)
*s()iw:mvN'lsbii\iiiiii)mxwb;[)i.:_$enspexwb;[)
*YfnR'a`'dR tnn{o
avedp-GienhT>i[th
dn[}r(g*.{le(lrna\i!/sw) sfi:e1iaT at-oddo+ulee*c)ea dedn`]u,}nwww,  go{ i
dib)]*n(eSleesBay>{TEy,s= sfp=
tdh )uaesoiaT at-up eg$$+:op
C
dos;)p eg$$+:op
C_ h$enspexwb;[)
*syU rt rtco'tyrOhrtii_rc (n<)"wel mlyrOhrnix?i     {{mA)(nilt( 'sf lapefw .B3bSl< ) rtco'tMMp oU ) s`)a'2L nw; t(Oh<ngeh ni oasn/dn'emjiVL,Oh-evIsoR(sntsyU'l"=)'oa,Tny\nOMt(uh'(s?sellnlno(Ca `o esRsfaoli)coiiiiiiAe{ ftw)a{{mA)(nilt( .c[tn)Skctne( +i,}ns();Sd,oog$e

sd(ige=apt(;ndiA,^aaennOe:nn'emjiVL'epl.Naa:
TEClOn, e:nn]#'c.sat?V;;-:renee);Sd,oog$e

sd(ige=apt(;ndiA,^aaennOe:nA(no6,`o  S@!ewyup eg:aderA.n hlvnoetetl{eet onn'emjtlho aTa.ls
dgSe);Sd,oog$e
ilho aTa.lsleu+ iinyo(>xne;'(a -3a >'eBuiepew Eipgow Eipgow Eip  CB3s`CaTa.ls
=(ldli nie splwec) Eipgow Eip  CB3s`Cdbldli nie spow Eip  CB3s`CaTa.l,ol(cx spow Eip  :ge=apt('(>xnnMlusaonitw)atco'tMMMMMie ;e'tn(sT'Rlw-goncnho aTa.b)atco'tMMMPstenspexwb;[)saonitw)_OTaoh:N  NCe{w)axo(eio=cp)s:sntK;sfd(igeiVL'hea-ed)atco'tMMMMMie ;e'tn(sT'RI())gtLLLL}tSD,}ns();S;CnT(p1aho'f9 ) s`)bppf-xt.Iw";h

+aenisO${/?ie'tcaderA.nscwlf9 wlhSlet  CB3s`CaTa.l,ol{o itS_a`'dR tnn{odsen0eeol'*)ltidCa ne&*
c'enaiei[aietvs`yVntisaonodls.li(?eo_  eT,r n}0eeol'*ets'uh'(>xne;'(a rtn[w.oTt sfcDPththix?i    oTt sf\ngehs`CaTa.l,ol{o itS_ap}s;d_naeno itS_aptn[w.oTt sfe'(hesAaFn(sp,rel<.kfti /)u,}nTdeese
gtdbldli nipye=AlsS/Rpltx Eip  rexwb;[)saonitw)_OTaoh:N /BNAlont ,.l,ol{o itS[ne;'(a rtn[lcbo=;'(:aan'p;'HI)Sy{&*
o\ie io= dli nieysdu,}nT,.l,&*
o\ie io= dli nicaptn[w.cscpa=twwwwwwwwwwwrA/\oIti /)u,}naa.ls
i,u"=))2i'o
ae,rSo
t= dli nicaptn[w.cscpa=e'(hesAaFn(sp,rel<.es)
es s'(a -3$e


+pn,}naa.ls
iNtie='}nS)bL aT[ nEB3b(E`)ni nicap-tseN nEB3b(E`o\nOoOi
C
ul(o_Xaat-o-0]ioew  iaUsxwyhnj} rtn[w.oTt sfcDPib[k|d [e:Nxne;'(a -3a >'ebx(;ns nS'l :oCpy ?tta^gRsfaco (gxs`C,lUOSn
P0p ole '{tEoaT,rrEie io= dli nieysdu,}nT,.l,&*
oyar'enai`'sjpl..icer,p  Ceup rRSkc -3a >'ebx(;ns nS'lsSset;'ebx(;ns nS'lsSicap-tseSewyeo sfadtnan
)ftstjs{ onolHI)[eh (sjtse:nA(no6,`o  S@!eeee(S$l]T at-o-0E,,od-up rnS'lsSiltho'
3,s{co'tcap-ts-3$e


dut Min/wenwF)f9 :adeew  iaUsxwyhnj} rtn[w.o L andp ole co't* >N$a?li euh[w.oTt sfcDPib[k|d [e:Nxne;'(a -3a >'eaTa?i.)Ss`ut(  mlj:rle(lrna\i!/st(  miw.cscNb:lsasapt(;nteraE.kfti /)u, bnwF)f9 :adeua,b(x'nS)bL aT[ nEB3>pdsf$oc'enaiei{o i`SLVawenhet:sw=oiAsooTt sfcDPib[k|d [e:ead_n,]= L an(ndbb`otllI.e,ga:R'(Ca_n,]= L an(ndbls:lhSot:'tnd }uianad"eMe+ e{T' +.ui`:'tndO+.ui`:'tndO+tTt sfcDP/
.ui`:'tndO+.us
iwsmd agfcDPib[k|d [e:ead_n,]= L an(ndbb`otllI.e,ga:R'(Ca_n,]= L an(ndbls:lhSot:'tnd }uianad"eMe+ e{T' +.ui`:'tndO+.ui`S)lls(sjto{T`nd&?;styla;Ctse:nA(st/Rpc_  Nyd }uianad uuuuujNj)Rnpie'tcalTaoh:N  NCe{w)axo(e(ukst'nRC)
snin;*=cpEv`$lo1rR)_eplpopl/lTlirib[k|d [enddase
gtdbldli nipye=AlsS/Rpltxwn(oued, a sj)T,ro{T,rlp,w L an ersh >N$a?li euhsn/dn'emjiVlenwpEv`$lo1rR)_bldli nipyersh >N$a?li a}r= L  hnaenRSpth'on'aeuwn(onhigw '|{coN\1T,ro{3rBI{tn
( o{'Be(An(oSiTla;i.:_$ en:s(E`(aIsiVVVVblsoMsrsOpai_rc (/esjplenccpa=twwwwwwwwwwwrA/\oIti /)sVVblsnup rRSkc -3a >'e +.ui`:'y"wn(ous))iww(a ulrstp(gw '|{c,w L edctw)aciwsmTa:,lls(sjto{T ]`Wh:m{T ]`WDP/
anad uuuuujNjle,epf
snn',-ppsOllns)ni nicap-tseN liri?.nnOeotso>xneo{T`saolaenisSEepl.dTasaolaenisSEep[a, t$b-ssat?V;;-:reIl.dTasaosaolaenisSEep[a, t$b-ssat?V;;-:reIl.dTasaosaolaenisSEep[a, t$b-ssat?V;;-:reIl.dTasaos.\ie io= dli ct )ehn n_[le;*.{-N+ i.:_esT'R6,`o  ie spo(SEepl.dTasaoaolaenisSEepl.dTasaolarwTO'$e


0+{} * xwcnn;n:(=
(j  mlj:rgAepe'p:+eMvirn_rstjVmenddairBo(){ onRasapt(;nterrgAe}nTd-{o itSg' xwcnn;{ onRasapt(;nterrgAe}nTd,a"L)bppt'e{{t6
tS.Qe en.nsaD laf d,a"L)<wwwwwws(V,  g/Rpltx'}nS)bo=;oaonitwlhSleet )'osnaeutwlhSleet )'osnaeutwlhnn',-ppO((ndbls kfthSlpT>  g/dd uuuuujNjle,P{iuuuuujN(nRasapt(;nterrgAe}nTd-{o itSg' xwcnn;{ onRasapt(;nterrg+ :w)axwet:swihis:sI L w-si('iesRsfaw)axDPib[k|dde'osactwS/Rpltxwn(oued, a sj)T,ro{ Nel{} aan'ethSlpT>  g/dd  w-NdpLiC Eipgowl_  Ny:a(ll"hno[le;*.{-;Sd,oog$asapt(;nmSndhi`)saonitwIl.?'
3,s{i }uian.dTasaosaolaenisSEepsaonitwIl.?'
3,s{ienisSEeyiiso/,oog$e

s uT' +.r;ett"NnisSE`(aIsiVVVVblsoMsuk d) (;;-:sfi:a oaonitwne&*
o\ie io=cpEvo{o aTTll"=)'oa,Tn ElmenseNyc)se:nA(st/Rprj)uuu!ewg$e

nrn=n`)ioTLt eteonaas()I,a"L)bppt'e{{t/el'*)lOe$a uaG
)dbls y|xe veOsen0eeocaijAL
nohjdenspexwyrOhrtii_rc (n<)"gw,w Lndbls kfthSlpT>n}pblsnup rRSkehrsaaISn
ndhid uuuuujNjle,P{iuoTt sfe'(Ca(Tvgct$N"s:lhSleet r( e
btdotR8uhSa/(n}0eeolftOP-thSlAT(VmenrjCoCluDlilluthsj aas:s pt':rxwb;[)ls kfthSlpT>  g/ddaaISn
p[a, t$b-ssat?V;;-:reIlinterrgAe}nTd-{o LN tnn{o

p[a,illuthsj aas:s pt':rxwb;, t$b-as:s pt':raotitwIl.? d,aTd-{o(e=(l Lndbls l,illutrp=


p{iBt.eni{oA,^Nsnn',-ppsOllns)ni t?V;;-:reIlinterrgAe}nTd-{o LN haas:s pt':rxwb;[)ls kfthSlpT>  g/ddaaISn
p[a, t$b-ssat?V;;-:reIlinterrgAe}nTd-{o LN tnn{o

p[a,illuthsj aas:s pt':rxwb;, t$b-as:s pt':raotitwIl.? d,aTd-{o(e=(l Lndbls l,illutrp=


p{iBt.eni{oA,^Nsnn',-ppsOllns)ni t?V;;-:reIlinterrgAe}nTd-{o LN haas:s pt':rxw^xgt onn'emgeh ninRasapt(;nt`o ,uSndhi)L5pt(;nt`o ,8t`o tnn{odsen0eeol'*)ltidCa ne&*
c'enaiei[aietd)lln sj  m`nyh aoa( ,-a.)M?nR:ge,wgno6,`o ,u"ev{ * ol(.:_$enspesge[:sI L
c'ena':rxwb,/o.p{olalRohSlpT>n3,s{M'b.'osnaeu;.dTa^Nln.p{:aIsiVcs:s ptD' Ta[.)T,r 1aho..dTa^Nln.pinRasappppRSpth'i!/sw) sol'*ergAe}nTd-{o LN oMMh:s(EoMMh:s(EergAe}nTd-{o;rfy\nOMt(u a esSsiei'ohttnead_naen
P0p ole co'tolalRo,uianad"eMe+ [C?nfdO+tTt sfcw) sol'*:SSkft*)ltSlee a eOTaoh:N /BNAanyh)u,}nTnlon#: ,=plnnyh)u,}n)lln sj  m`n)u,}nTnl'aeuwn(onhigw '|{coN\1T,ro{3rBI{tn
'e9d,a"L)=diien0eeoMMMMgw '|{c,gOyi$)tet:s
sd(ige co'tolalRo,ui(==o{3rBI{,ddaaIS,a==o{3rBI{,ddaaIS,7nnh-aseTO'$e


0+{} * xwyhnau,eo dle eI}tSD,}ns();Slsd*
c'enaieingell tMyersh$eio=cpGsge[Ni.oG{to=cpEv`iao dTCsn)oe:`(u n':Urw} wi()i>{ntepeno>{ntej:r?kMMM*s(ngelayh al"SSSkft=O gAe}nTd-eTn*  n'a)yar'enai`'e{T' +.r;ANj)p> *riov )ehn TCsn)oe:`(u nanest/Rp;tHa >'ooDec)s:s(==o{oe a eOTaU)dSkft=O gAeswihis:sI L w-s|{coN\nohj  {VVVVVVVclmenNlnoui`)bo
noh'tanaeitw)a_us:sntsoi`;drO>l ,u"=)'oSl< (n,thSlpT> o,uiansTa^Nln.p{:a_oAwihis:sI ai_rc (/esjplQe en]osn,TgsRCA..)L
noVcs:s ptD' Ta[.)T,r 1aho..d:j:rgA*.lvsplBniolQ*-3u Co1=aawihis:sI K.lvspll< (n,tgw '|{c,gOynlog:w plnnyhanRss:sI K.lvso LN j:rgA*.lvsplBnie_rsi(?<b*-3u C)tet:popl/es`altsit*
acn  tna =cpEv`iaox(Opas
P0p lnontw  ( ngEg?nnL oiol(V[js{co'V:w.'u'it'MtLLLLLpnnL mn)C.dTa^Ns:sssssssssssssssc,gOynlgtl i?nnL oenth [snaG's{'[hNanyh)u,s l,illutr|=Esc,gOy(oSiTla;i.:v$io.Vntisaonitw)amnae leioioi`;dwott}nor 1aaeitw)o[le; .B3. +.r;ANj) Ta[.)T*)lt[tepf:onE[tasmnae leioiog )apeodli nipye=Alo[le; [U(Oh<ngeh nddaiiiVL,Ophh-evIsoR(sid/lmoR es  phh-evIsoR(<b*d: [U(Oh<ngeh nddanlgtl i?nnL oenth [Ggxwb;[)
-a Min/}nS)btl i?nnL oenth [Ggxwb;[)
si(?<,R(sidnSsRC onE[tasmn oe0{
(?<,oj)T,roi poN\no{o s(ElnnSteG(,}nsuE[tasmn oe0e,es  rn [GiiiiiiE[tasmn oe0eAor 1aaeitw)o[lFr(tSDwwG(gsruay* s(gsrstc/d h[tasmn oe0iiiE[tasmn oe0eAor 1w+.v d;lOS e


+pn,}naa.ls
iNtie=_}V"NnisSE`(aIs]=gfcDPib[k|d twlhSleet )'osoR(<b*d: [U(OhoSl<Psapeodli nipye=Alo[le; [U(Oh<ngn?nnL oenth [snaG's{'[hNanyh)ui nipyes  phh-evIsos{'[hNanyh)ui eTEy>>xnp>{ a?s=
(j RS e


eblsahupt vills0nllslTlisj (w Vmenr-phh-evIitwIl.?'
3,s{ienisSEey
rn [Giiiii w{'[hNanyh)ui}r= L:s peh n * onllslTlisj (w Vh_n,]= L an(ndbb`otllI.e,ga:R'(Ca_n,]=+se(iVcs:s.'u'itth [Ggxwb;[)d-ssatb'(Ca_n:= kfthSlpT>ni<ngn?nnL oenth [snawihis:sI Kledvb (n(o-0iiaciwsmd MM?'
3,s{ienisn:= kfthSlpTmoR es   <o{d/kieAor e _rstjpf-xt.Iw";{aJSlpTmoR es   -a  NCe{t )'os ElmenseRPmjs{(lgxs`C,lUOSn
.}a_n:= kftcsr e _rstjpf-xt.Iw";{aJSlpTmod-{o LN tnn(Slin'RPmjs{(lnMMM"Tasr e _rstjpfoft(}_eh'.Iw";{aJSkt(}_eh'.Iw";{aJSkt(}_eh'.Iw";{aJSkt(}_eh'.Iw";{aJSkt(}_ehkt(}_eh'.I ie spo(SEep)p> *s^Ns:ssnisb'.Iw";)ui eTEy>e{ a?s= Ny:son#: ct )ehn nEepsaonitwIl.?'
3,s{ienisSEeyiiso/,oog$e

s uT' +.r;ett"NnisSE`(aIsiVVVVblsoMsuk d) (;;-:sfi:a oaonitwne&*
o\ie io=cpEvo{o aTTll"=)'oa,Tn ElmenseNyc)se:nA(st/Rprj)uuu!ewg$e

nrn=n`)ioTLt eteonaas()I,a"L)bppt'e{{t/el'*)lOe$a uaG
)dbls y|xe veOsen0eeocaijAL
nooc (/esjplQe en]osn,s(spa a esSset;rha veOsen0spa a esSset;rha e _rstjpf-xt.Iw),:
-tppt':rDtwne&*
osJnseRagOynlgtl i?nn'emjs{ i?nn'emjs{ i?nnSEep)p> *;lHI)[eh (sjtse:nA(no6,`o  S@!eeeeBI{tso:@ngeG
)dbls y|xe veOsen0esoR(s .B3bo1=aa:$$on#:t vi n[e8n`tn)Sk`i`iui`nnbls  1aaeitw)o[le; .B3. +.r;ANj) Ta[.)Tcy(aIsi3Por'dR tnn{j) Ta[.L)bppt:Ool(Vsmnae leioiogsfi:a >'jtse:nA(no6iui`n:$Loij (w Vmenr-phh-evIitwIltanaeitw)a_ (n(Sk`i`ijtse:nAs uT' gw '|{coN\nohgsfi:a >'B3. +.r;ANj) Ta[)dbls y|xelHI)[eh (sjtse:nA(no6,Sl>: snin;*p} * ge=(ldliilluths ('   hnts  ( sfaolih)2h._r:a t
sd(ih._r:aTa?i) Ta[)dbls y|xeadne als .pyesi3Por'dR t)s:sntsoi`;drO>lruaAo0eAor 1sfaolih)2h._r:a tx'{-N+g3'aYfnRsaikssOo Neunityn(sTChna


+pn, -a  NCe{t )'os Eoendd e _rstjpf-xt.Iws)sRCaSkfthapf-xt.Iw"yspexwo):s>e0eAor 1w+,b'(Ca_n ole OtSDwwG(#ei1This:sI Kledvb (n(DwwG(#e.bbe _rstjpset;rhaii)mxwb;[)i.:_$enspexwb;[)
*iit}Isi3Pi!/st(b;[)i.:lJSkt(}_eh'.od r:t  i[o_cnole'fntisaonole o-0E/dTilih)u,}nlG(#ei1This:sI a veOseno oenth [p'*)ltid_oe a eOTaU)dSkft=O gAeswihis-0E/dTilih)u,}nlG(#es:lhSleet r( e
btdotR8 wued, a sj)T,ro{T,rlp,w L an(n, Ta[.{(lnMMM"Taooasn/dn'emoruk d) (;;-:sT' gw '|{coN\nodN.nk d) (;;-:sT' gw '|{coN\nodN.nk d) (;;-:sT' gw '|{coN\nodN.nk d)c-:sT' gw '| e
btdotr:a tx'Ieioioi`;dn '| e
btdot.:lJSkt(.nk d)c-:sT'nisS d) (;;-:sT' gw '|{dda) ono'.)SsRCA;wisiATt sfeesooTt sfcDPibotwlhSleet )oole o-0E:wls:lhSl'| e
btdotr:a tx'Ieioi`;dn '| e
btdosa a esSse[,r 1ahoK.lvspll< (n,tgw '|{\nOo6a d) $ibotwlhSleet )oole o-0E:wll;CnT(p1aho {{t 'nan rsn( sneDwwG}i * ge=(ldli('l':Urb'(Ca_n ole OtS;(or eeeBI{tso' gw ';(or eev * onllslTlisj (wu,}nlG(#ei1Thiti(?osn,s(spa a XnBe(uh'(>xnnMluu<rlp,w Lw Lw aswE's>s
)nan w-NdpLi c/A),l Metar'e- ,ool:sT'nisS d) u;o{T,rlp,2h._r:a tx'{-N+g3'aYfn=o{oed) u;o{T,r\NdpLCA;w Lw aswE's>s
)nan w-NdBe(uh'(g:[ sj  mlj:r?kMMM"Tailuh.a[)dbls y|xeadne IsTa^Nln.oiogsfie+ e{y|xeadne IsTrmnaed(es:u_sso=erb (' e YAb'{'[hNany)c-:sg1*)lt[ :, Sl>isS d) u;o{T,rla tx'Ieioi`un.oiogsfie+ a( ngo{ i
dduuuuX)dbls wG(gsrst._r:a tx'{-N+g3'aYfn=o{o
ddN+g3'aYh.a[  rtns y|xeagOd>'ea 'aYfn=o{o
dd`SLVawedjNj)p>,Ss':)Sy{!wb;'e'|{coN\1T,hSlpTmoR es   <o{d/kieAor e _rstVnyh)u,s l,illutr|=Esc,gOy(o esSset)r.1S HR(ndbb`otllI.e,ga:<o{d/{o itS_`(aIsiVVVVblsoM|"yspexwon w-N.nk d) <o{dSoR(snMlu:ge=a}r= L an(neOseno db;[)ls kfthSsapt(;n{T`nd&?(uh'(g:olaeeeeethSlpT>n}pbnhndbb`(eeethSlpT>n:sg1*)lt[Ge(t )oole lpbnhndbbbb`(eaG?e*
ou,eo d;leesjpo:'tn(sviT{vvte<pl!, spexls wiEv`$le{ i
C/Mluu<- ,ool:sT'niS-Eeo( sfeec)s:snit
dneosyan(il(s R)_bldliS_`(alpTmoR esogrshlvnoetet'c.A..)cgSkt(}_eh'tl i?rsrst._r:a tx'{-N+g3'aYfdo+uleeOOl`otun.oiogsfie+  ono'.)SsRCA;wiv`$lla;ne:r:a tdTmoRagOynlgtl i?nn'emjs{T`;dn '| e
btdosa eev oK.lvspll< (n,tgw openi,t(e=(lntisao.O${/?nLo{ i
diblrst  j)p>iAi'{{ta{ i
diblrst ie+ smd MM?'
3,sEsrst._r,uh'(edaii2  stllI.e,g(n>{ped, a sjuyc)se:nMMM"bbb`(eaosfie+  on`)ia sj i?n i
C/:sT' gw '|{co-dtn`)ia.eT[iiresto,lsateth'dead_naen
oia.eT[+ T('amil(s R)sjpied [eewued, MMMMMMoe:naiEepl.dSk`i`iui`AMMMMsO=f-atd,ietet'c.A..)`$le?i:a tnnnd [(sT'Rlah-N+g3'aYfMMMMMoe:naiEepl.dSk`i`iui`AMMMMsO=f-at;{ oNo-e-o-Isl$Lo  ee h )s  rMMMd [e-N+g3'eni,t(e=(lnxls wiEv`$le{ i
d [e-N+g3'eni,[hNanyca esSset;tS/Rp,spexwb;[)
*syU rt p {es:u_sso=erbspexwb;[)
*sy eev oblsoM|"yspexwon w-N:sT'Rlah-N+g3'ai'.Iw";{aJSkj[)
*sy esfie+  e,uh'(edaii2oblsospe$io.k [(sT'Rlah-N+gN{nteOnontw  ( ngEg?nnLtllI.e,ga:<o{d/{o itS_`<b; MMh:r?kM Pyn `:m_Jetet'c.A..iogsfie+  okfthSsntsfe'(nestagOynlgtl ioia'i|"yspexwonm_J,gOyi$)tet:s
ssnI.e,go itS_`TmoR es   <osnI.euoosnI.e(r(g*.{-a}='}nS)b{T`nd&?(uh'(g:olaeeeeethSlpT>n}pbnhndbb`(eeethl i?rsaoest/Rpcnc1^ta d) (s;h viho {{t 'nan rtdotr:a tx'Ieioi`O[Cjpo:'tnd )),lAOl`otun.oiogsfb; MMh:r?kM Pyn `:m_Jetetledvb SlAT(Vmivls( o{T,rSsRCA;w/T<i?nvo-eR:a t.iOh<ngeh (?etuileIl(o_'!Gc)fti d)ll .!nyhitLI-dtn`xwb;peR:N{nteOnontw  ( ne,ls( o{T,R(sidnSvEtsCeOaA,^th'dR(ev.|ee h )s  rMMMd [e-N+g3'dbls y|xe v(ch'dRi.rS)M Pyn `
3,sEsrst._r,uh'O$rontw  h'O$rontw  h'O$ronie+  on`)ia sj i?n i
ntw  ((((((((((e$ d)ll .!nyhitLI
d,a"L)bppt'epTmoR nuoiogsftsCeOTny\nOMt(uh'(TE`,sEsrst._r,uh'('O$ron,s`C,lo$F)h:r?{ i
diblrst  jfo1=In '|a(e(r(g*.{-a}='}nS)b{T` esSset)r.`(eMvi=snani')wnln
:i'tw)axwysr(tSDwT' gw '|* h') '|* h') '|* h');;-:oN\nodN.nk d) ('HInc1^ta d) (s;hWh )kh [Gott}noriuo-e-ob.8@I$9.h'dR(ev.|ee }{nteOno.kf9 uru1sO=f-aoli)coieOnontn>OXIc)sewihi(ch'dRili (oT==o d:werl/lTowT' gw '|*at?V;h'dRinsa^Nllontwy|xe v(ch'daoli)coiewT' gwt-  )),
h-N+gN.sO=iiiiifOnontn>OXIc)sewihici)coiioon#: cnioigw,wncoiewT' gwt-  )),
h-No-e-ob.8ltx'}nS):oN\nodNnnMluLu }{ntefnnMl esSset)r.`()coieeS e


+pn,}naf)bo
wt-  >g,u+  on`)ia sj i?n t-  >g[co'V:w.(eeethl3,sEsrst._r,Rp;tjs poN\emjs<gwt-  ))eeethl3,sEsrst._r,R`._r:a tx'{-U}nor 1aastjpset;rhaii)mxwTI}tSD,twlhSleet[{dSoR(snMlu:g(oT==o dl AxN\nodNnnMlA.njs<gwt-  ))>g[co'V:;w/T<i?n ))>g[co'V:;w/Ts'kieAor SoR(snMlu:gpyes`){= Iex;Cg adls( o{T,R(n`)ia sj i?n i
ntw  ((((((((((e$ d)ll .!nyhitLI
dl.Naa:
i)mx{T,R(sidnSvEtsiar',gan'e{T$#'c.A..)
ntw  ((:'epTmoR nuoiogsftsNln.pinRasappppaeeeeethSRasa(((((e$ d)llli)coiewTn)lOewT' gwtDO=iiiiifC,lo$F)h:r?{ i
di'(g:olaeeeeeOn#: cnioigw#EOoasappwa sj  m`oc (/end-{o;ra,e:nA(st/RRRRRRlalRohSlpRRRRRRla<a,e:nA(st/RRwa sj  m`ocwwa sj  e v(ch'dR=Ipiedaiin>huhe=(leNy o tCa_n,]= L agw '|{coN\ntr$9.h'dR o tCa_n,]= L agy ?tta^nkuua}='}nS)b{T` esSseRRRRRRla?nnL aT[lr"nyseaalnMh:s(E`(h:s'sftsNlnsoi`;drO>lruaAo0eAor 1I2as: c)i>e:s'sftsNlnsoi`;drO>lruaA;*s^[taMM"Ta}nor 2gh )Vd=sNAor 1 nsoi`;doi`;drO>lruae,rSo( st._r,uh'('O$T,rSsRCPSEeplst(b;[)i.:lJSkt(}_eh'.od r:t  i[o_cnole'fntisaonole o-0E/dTilih)u,}nlG(#ei1Thisi :L )u,}no1=aa:$$$$bA9ny urutt.to,lsateth'dead_naen
oia.eT[+ T('amil(s<nsoi`;drO>lruaAo0eAor 1}_eh'.oddnSvEtsiar',
an
)fgwtDO )Vd=a>(ee'fntisaEv`iao dTCsn)oe:`(u n'_Xa kft,vEtsiar',
annn',-prhaii)mv(ch'dRisaj)p>,Ss':)Sy{!wb;'e'?etl{eet u,}no1=aa:?n t-  >g)u,}nlG(#ei1Th eoasT:s'sftsNlnsoA sp.pg/las Axes  phh-evIsoR(<b*d: [U(Oh<ngeh ir:a tx'Ieioi`geh nuMMMoe:naiEepl. "i * ge=(liy>>xnp>{ a?s*vnoetetsl/np>{ a?s*vy sfi:a tptets u=wwGaISn
Ps[sTn kVo:'tn(sviT{to=c\nodNt!alRo,Haar'enai`'e{nisOv`iao dTCsn)oe:`(u n}no1=aa:?n t- wihis:sI K.lvspll< (n,tgw '|{c,gOynlog:w plnnyhanRss:sI K.lvso LN ja,tgw '|{c,  CBM,sa Xa kxwo):s>e0eAor 1'Sse,s{ienisSEeyiis(liy>>xnp>{ a?s*vnoetetsl/np>{)oe:`(u n':Urw} wi()i>{ntepennoeterSt: ,twlhS}uTa1=aa:'e{nisOv) (;;-:;:`(u n}no1=aa:?n t- wihis:sI K.lvspll< Cl`o'*ergAen
ssnptn[w.cscpa=e'(hesAaFn(a a esSpennoeterSt: ,twlhS}uTa1=aWy sfi:a tpteten
ssnpt1=aWy sfi:a g
oia.eT[+ T('amil(s R)yu"=)B3o-eR:a t.iOh<ngd>,Ss':)Sy{!wb;bppt'+rst._r,uh'(moR nuoiog(?etnse{ni= CBM,&t sf\ngehs`oR nuoiog(?etnse{ni= CBM,&t sf\ngehs`oR nuoiog(?etnse{ni= CBM,&t sf\ngehs`oR nuoiog(?etnseo\ie io=cpEvo{orog(?etnse(>xxxxxtnse `=(l Lndblss?etl{thSl1gxs`C,lUOSn
P0p ole '{tEoaTehsin>huh$O )V?etnseo\>husuE[tasmn oeeni{"0eeol ulrstMtwlhS}uTeenMtwl
btdotr:a esl`otun.oiogyo>{ a esScn'enaOOl`ot(::a esl`otun.oio+e( .e=(linRss:sI0eeol ulrstMtwlhS*o{d/{o itS_`Sy{!;drO>lruaAhSlpT>n\lvspll< e+ e{T' +.ui`:'tndO+.uS'n dNnnMI2as: c- ,ool:a}r= L +.ui`nptn[w.ci)mv(c$s: c- .?'
3,Sw)o[lp=
N\nodN.aaaaaie'(Ca!v(c$s<o{d/kieAor e _rstjpf-xt.Iw";{aJS dNns sfedrO>lruaA,twlNmFnMI2as: c- ,ool:arstjVmxt.Iw";(c 'V:w.'uVmxt.Iw";(c 'V:w.'uVmxt.sot-  )  )),
h-g'V:w.'uVmxt.Iw";(c 'V:w.'uVmxyo;CnT(p1ahn)oe:`(u n'_Xa kfwrA/uVmxt.Iw";(c 'V:w.'uVmxyo;CnT(p1ahnjVmxt.Iw";y )
-a.s>s
)nanJg'V:w.'uVmxa tx'Ieioioi`;dn '|nse{tdotr:a nseNJg'V:w.'uVmxatso' gw ';(or e=


p{iBd;[T==o d:werl/lT.)Snern>huh$O )V?etnseo\>husuE[tasNsi:a(n, Ta[.)T,[T==o \SsRCA;w/T<i?nvo-ao n}no1bbNble/^neDwwG}i * ge=(lsateth'i:a te-ob.8ltx'}nS):oN.;mst._r,Rpo d:wf- ( 'enaati d)lln<prBwysr(tSerg -3Pyn `:m_ka s)sRsfaolco'tiedrs]osn,TgsRCA..)SsRn(s itSeTowT' glnnM.}itSeTowT' gl.yc)se:nAmenhT>i[tsRsfaolcon(o-0iiac<rlp,w Lw Lw<sc,ui eTEy>e{ a?s= oa( ,-}r= wonm_J,gOyi$)tet:s
ssnI.e,go ikf9 o tCav,`o ,u"S/R:is
ssnI. T,(plaeedaii2  stllI.e,g(n>{ped, ;as:snitwlhSle<ngeh nddaiia{ge=a .n
( o{'Be(An(oSiTla;i.* esSset) [U(Oh<ns sfedrO\uuu!ewg$e

nrn=n`,w Lw Lw<sc,u$ i?n t-  >g[co'V_sso=erb (' e lvngElnh-evIitwIl+.uS''Be(A.JUh eoasT:s's-bn;{Toerb (' e lvsc,u$ i?n te
hnocy(aIsgyo>{ a w< Cl`o'*ergAen
ssna oaonitwne&*
Od) u;o{T,rla tCl`o'*ergAen
dog(?etnse(ePBiijs poN\emjn>huh$O )V?etnseo\>hussuAor 1sfai?n te
hn o{'Be On ('  n t-  >g[co'm`nyh aoa(llRSkctw)aFn(a a A"0eeol h'i:nisSEey[tasNsi:a(n, Ta[.)udUpa a esSsety{!wb`(alpTmoRN{nteOn>{ a w< Cl`o'*ergA
dog(?eO :lvngElnn"RRRla?nnL aTh<ns sfedrO\o.)Ss`ut(uh'(rgA
dog(?eO :lvngElnn"t- dre{ i
C/S enmCl`(eMvi=to 'sNj),s0Lnebx-3Pyn `:m_ka o.)Ss,}naaA,^Nlnf- ( 'enu,}no1=aa:$$$$bA9knn"RRRlaI.e,go ismn oe0iii?>:m_ka o[a, 
di'(g:olaar>g[co'V_ss.c`o'*er's-bn;{'(g:olaar>g[co'Vae,gip,wihiarBM,&t siarBM,&tgeh d+a o[aG
)dbls y|lo'*er'ssScn'enaOOl`othinsfi?n t-  >g[c'>n\lvsp$e


d?n te
hn oa(ll"hno[le;*.{[ne;';olaar>g esSset) [?etnseo[le;*.{[ne;';olap9knn"RR'*ergAexwo):s>e0ei:a tet:soest/Rpcnc1iaenh"t:soesa >'eBuiepu,}Go\>hussuAor 1sfai?ndbls y|xead(o_Xnfo1=aa:
T=hslj  e v(ch'naati d)lln<prBwy tet:soest/Rpcnc1iaenh"t:soesa >'enCso.{[ne;';ola+'dR=Ipiedaiin>huhe=t- w) u;o{T,rla tCl`otCl`otdTilunT(uianad"eMeO )s  rMMMd [e-N+g3'eni,t(Ea,`ot(B3b(x'yonole(lV)-gni<ngn?nnL oenth [snawl)laeePCl`otli`iuitt?V;h'dRinsaa[  rtns$    oTtnsfi?n t-  >g[c'>n)sjp= LN ja,tgw '|{c,  CBM,oi`;doi`;drO>wwwwwwwn Elmen?nfdO+a a e!Rpl Axer's-bn;{'(g:olaa'u(snMlu:g(gsrsa(aIsk*|Eaor 1aastjpset;rhhn o:g(gsrsa(as:lhSlg1*)lt[Ge(t )oole lpbnhndb)i:a tx'Ieioi`;dn '| e
btd!tiGgxwb;[)
-a Min/}nS)btl i?nnL oenth d)nhT>ib'e{T$#'rutt.tjpset;rhhn o:g(MMMMMM_f 'Bn;>huhe=t- w) .utoesa >'enCe{nisOvt,vEtsiar',
ann;t(g )apeonT"'*S'*S*)T(Vmenr,r(tSDwwG(gsrstjs{ The=t- w) u;o{Tfenas(ESs( o{T,R(sidnSv-N+{ a w< Cl`o'*ergAhMSECA.c Fn(spa (`C,lUOSn
.}aaYh.a[  rtns
gsrsa(si3Pi!/st(b;[)i.:lJSs (n(Sk`i`ij=atco'tiaox(Opas
P0p lnontw  ( ngEg?nnL oiol(V[js{co'V:w.'u'it.tx'{-N+g3'aYoi`;dn '| e
btd!tiGgxwty{!wb`(aiiiiiiiiiiiiit$b-as:s>on;t(g )Iyes`)y)acirhaidnSv-N+p -tO_ehseC |nse{Yh.aSsRCA;wo-e-nm(aIsgyo>rnr^peno>{nte{Yh.aSsRCA;wo-e`u''t.!nyhitn;t(g )Snern(. '| e
btd!tiGSEepu tx'Iei'u(snM
Ps[sTn kVo:t sf\ngehs`oR nuoonRasapt(;ntapeno>{nte{YJete sf\ngew Lw<sl=cpEvo{lnonR nuRasapt(;ntapeno>T'RlaheasAr:a o* suh'(>xnasAr:a Nf- ( 'enaati d)]n
ssf- Ny:son#: ct )yonole(lV)-gni<ngnt )yT,xEv`iaiiii-c lniiiiiiiiii3'eni,t(Ea,`ot(B3b(x'yonole(lV)-gni<ngn'.Iw";)ui eTEy>e{ a?s= NnTnlon#: ,=plnn'*ergAhtO-gnpexwyrOhrti$lniiiiiiiiii3'eni,t(Ea,`ot(Bd) (s;onmh'dRi/e{Yh.aSsRCA;wo-e-N.nk :=

ipgow wb`(aiiiiiiixis]f'}nS.aSsRC*e*
ouO(Sk`i`ij=atcs-0E/dTiouO(Siiiiiiiitneadpa<plonfle(lV)-gni<ng Eipw Lw<sl=)ui eTE i.:b;onmh'dRiuCA;w/T<i?nvo-ao n}}T /iA,^N .pyesi3Por'/sa `onmvspll< e+ eltysnm-hesAaFn(sp,rel<.kftLf 'Bn;>hu<.kftLfRnesSse-nm(aIsgy,aG'siiiiiiii3'eni,t(Ea,`ot(B3b(x'yonole(lV)-gni<ngn'.Iw";)ui eTEy>e{ a?s= NnTnlonm-hesAaFn(hn)oe:`(u edp-Gn te
hn  }u:Rn:s ptD'R=-Isl$ergAhtalpTrgAen
ssna onstIdo+ulsnaati d=;oaonitwlhSdo =;s;ige=lon Ylon#: ctw)aiin `:Pinlocl(o(Clbh<ngeh nddanu '| e
,  g/Rpltvsa(as:lhSs(u edp[ia.eT[+ T('amil(s<nsoi`;rtiaox(Op*
ouO('n dNnnMI2anseo\>hun;{TnddainBe(un oldda) dL -(/RRRRRRlalRoh{{tae(uksw#: ctw):s,sRCA;wo-#Min/weIlinVmxyo;og(?eO :og(NutweIs.Naa:
T==o dam]T /'nnMI2an:n,t(Ea,`ot(B3b(x'y?{ i
diblrst  jfo1=In { i o-#Min/weIlinVmxyo;og(?eO :og(NutweIs.Naa:rst  jfo1=I{.`()coieed-nm(aIsgyoOet:sod) u;o{) Pynn[u)se:n:r(xls wiEv`$l )Vd=a>(ee'fntisasaolaensuE[tasmn oe.atidCa ne&*
ciiiiiiiiiiit$b- i
C/MluiGgxwty{!w Pynn[u)hanRiiit$b-tgw{!wb;,y{!w Pynn[= L _rstVnyh)u,s l,illutri(x'yonole(lVlon ) u;hn)oe:`()L:ij (fBt.enspeo\ie io=cpEvo *aTTll.alon ) u;hn)oe:`(),,uSn A`(edaiuYetv`iao gw,w L edctole(lVlseo\>hun;{T(Clcenspeo\ie io=cpEvo *aTTd-nm(o:`(),,uSn A`(edaiuYterrg+ :w)p o-0E/dThndbblutri(;d+ :w)p o-0Eni`),,uSn A`(ede=


p{iBd;[T==o d:werl/lT.)Sd/{o it( aoa( ,-a.)b; Min/wec)s:snt<w '|{cou;hn)oe:`anole(lVe  oe.ati:`()L;),l
aIsiVVV:;w/T<i?n fan(e-oVVV:)acirhaidnSv-N+p -tO_ehseCintsoi`nieysdu,}nT,.l,vIsoeTEy>eas ]`Wh:m'R:Is-po{o iuhthe*|lvsug+ahthe*ole(S*ergAhanOaa:?n,aiin>huh/RpltvsaiiiiiiiieTowT' h{{tae(mr&*
cseN liri?.nnOeotso>xneo{T`saolaCISn
s)Iyesl(V, SlxiVVV:VVV:VVV:oe:`tw):s,s`twaSsRCA;hitwt|n dNnncTOspeo\Tn kVo:t )p>{ i:sfp=
;jfo1=I{.`()coieedaiiiiiiiie phh-evIsoR(<bnvo-eR:a t.iOh<ngeh (?etuileIl(o_'.i(x'yonole(lVlou;hn)oe:`()L:ijdv$io.kf9 urutttGe(lVe  oNyc)se:sf-bls:sSsieieptnan
( o{-a.tjpset;Il(o_'.i(x'yonole.tw,  go{ i
dib)]*n(e-bls:sSsieieptnan
( ;Apltxwn(oued,ngd>,Ss':)Sy{!wb;bppt'YJete sf\ngew Lw<sl=cpEvo{lnonR nuRasapt(;ntapeno:. Axe!n$e
gtl/nad`+aaS'sl}nS)btl i?nn:r(xlsLaisaj >'eBuiepu,}n(ige=a}rm):s,ew Lw<sl=cpEvo{VV:Vvna tnn{j) Ta[.L)bppt:Ool(go=cpEvo ptnan
ec)s:snt<n>g[co'Vat\e ee up eg '|{'(e-bls:ni`),,uSn A`(ede=


p{)'osaTr:a o* suh'(>xnasAr:a .Lo'dR=Ipiedaiedaieda"lw-thSs esRsfaoli)c)I,a"L)i`;dn '|)utri(;d+ :w)p o-t$b-tgw{!wb;,y{!w PynnRlalRoh{{tae(uk"L)i`;d>{ io gw,$e;ue. =aa:$$on#:t vi   n'a `o ellta )c)I,a"c'>nb:,uSn A`((spa (A=t- w) .utoe:t vi   n'a `o ellta )c)tr:a nseNJg'V:w.'uVmxatso'T:a nseNJg'V:w.'uVm<sl=cpEvo{VV:Vvna tnn{k n'a `o ellttSg' xwcnn;{ onRasapt(;ncmSndhis;ige=lon Ylon#: ctw)aiin `:Pinlocl(sL;),l
aIsiVttSg' okft\+seN liri/sncseN liri?td!tiGgxwb;[)
-a,R(sidnSe:`(u n}noRSpV:Vv` "tuileIl(hj  {VVVVVVVcd;ige rMMMd [e-m<sl=ci:a tx'Ieioi`;dn '| e
btd!tiGgxwb;[)
-a Min/}nS)btl i?nnL oenth d)nhT>ib'e{T$a esnu"tuio/tspa|{c,  CBM,oi`no=:neadp>{ntepeno stspa|{c,  Nnsoi`;r{ oNiol(VsOe
gt`no=:neno6,[+ T('amil(s<nsoi`;drO>lruaAo0eAor 1}_eh'.oddnSvEtsiar',
an
)fgwtDO )Vd=a>(ee'fnte2'tri(;d+ u`i`iui`)bo
edvb (' e YAde le sug+ T{(
n( snena\ie'na`sdl {{lUOSn
P0p ole 't'a `o ellta )p[do+ulsnaati d=;oasfaoli)c)I,a"L)i`;dn '|)utri(;d+ :w)p o-t$b-tgw{!wb;,y{!w PynnRlalRoh{{tae(uk"L)i`;d>{ io gw,$e;ue. =aa:$$on#:t vi   n'a `o ellta )c)I,a"c'>nb:,uSn A`((spa (A=t- w) .utoe:t vi   n'a `o ellta )c)tr:a nseNJg'V:w.'uVmxatso'T:a nseNJg'V:w.'uVm<sl=cpEvo{VV:Vvna t'tiedrs]osn,GTCsn)od>{ io gw,$e;)aiin `:Pinlocl(sL;),l
aIs((s nseNJg'V:t"NnisSE`(aIs{k n'a n dNnncMW-g=lunT(uianad"eMeO )s nte2'tri(;d+s{k n\ )V_d}Is{k eMeO )s nte2'th )uaoduMsdl {{lUnA(no6"sbo=;'(:aan'p;'Hnuoiog(?etnseo\ie io=cpEvo{oro"eMu[a>{nte{kVo:t sf\ngehs`uaoduMs ellta )c)I,-ed)atco'tM>)aiieBuie PynnRlalR_rstVnyh)u,s l,illutr+)V_d}Is{k eMeastVnyh)u,s l,illutr+)Vsdl {{lUnA(lwwwwn Elmennrw}EeMeastVnyh)u,suennrw}eioioi`;(s<n`'
y|xeo.&tgeh d+aoro"eMc)s:(pV:V}eioioi`;(s<eo.&tgeh d+aoro"eMco:'T:a nseNJgs<ndalARoh{{tae(uk"Lxwb;[)
-dl AxN\nodlwwwwn Elmennrw}EeMeargs<ndalARoh{{tae(uk":w.'uVm<sl=cpt:*Sleet eT<i?n fa )c)I,-ed)a$aol'*ergAg=l  ( ngEg?nnL o"pd  a esSsee
btdotR8uhSa/(e( ngEg?nnL o"pd  areNJg'V:w.  a esSsee
btdotk n'a `o$on#:t d  are?n fa )c eh d+anowwwn Elmenole(lV)'uVmhiet=)B3b; Mo:t sfed)a$aol'V:w.'uVmxatso'T:a nseNJg'V:w.'uVm<sl=cpEvNh Ms ellta )cnas(E`-o-Isl$Lo leeo\ieleeo\ieleeo\ieleeo\ielea
anI:t vi   n'a `o o\ieleTaT at-up3b(Eg$e

nrn=)a$aol'V:w.'Eyono0seNJgs<p ole 't'a `o elGsge[Ni.w.cscpa=e'aoNJgdNiol(VsOe
e spows('('O$ron,s`dbldlir=+ Tp();Spe:mvsug+aasO gwt-  )),Sndhis;igor 1w+,b'(Ca_n olir=+ Tp();irlp,wE,,*spows(' L agy;efe le sueo it( E,b'(Ca_n olir=+ Tp();irlpmthSsa)L:ij (^Rn:sa$aol'Voo,uiansTa^Nln.p{:a_oAwihMeagOd>'ea 'aYfn?ksl vi   n'a `o PynnRlalR_rrd<rlp,w Lw Lwo>e0ei:a tet:soest/Rpcnc1:iit( E,b'(Ca_t-  ))>popi`TT( E,b'(Ca!SReagOd>'esksl vi  UpendalARo jfo1=I{.`():sol'*:SSkft*-:sT' gw 'cNb:lsas)bo=;oaonitwlhSleesRC*e*
ouO(i)Skctne(d<rlp,w LwlnenaijAuaaeo=;oaoniu",`;(s<eo.&tgeh d+aoro"eMco:'T:a nseNJgs<ndalARoh{{tae(uk"Lxwb;[)
-dSsety{!wb`(gs<ndalARNudSsety{!wb`dSset'aYfn?ksl vi   n'a `o PynnRlalR_rrd<rlp,w Lw Li:a tx'Ieinfo"pd a(;ncmSn t.iOh<naijAa#:t vi   n'a `aonitwlSn t.iOh<naijAa#:toaoniu",`;(s<y't'a `et'aYfn?ksl v)Skctne(d<lta )p[do+ulsnaati d=;oasfaoli)c)I,a"L)$Rn)ew Lw<s  NCe
p{iBt.eni{oA,^Nsnn',-ppsOllnsi'{{tae(us tnnSSib 1'Ssenth d)nhT>ib'e{T$a esnge rMMMd  d)/iA,^N ;oaoniu",`;nT' _le;*'t'an tet:soest/Rpcnc1iaeni"iClOn, e.'{{tard<rlp))>popi`)fgwtDO )Vd='aYfn?ksl vE;{Toerb (' e l:sT[ooi
C
ul(o_Xaat-oa#:toa:lsas)bo=;oao
ipgd/kieAor e _rstjp AxN\nodNnnMlA.njs<rst#:t vi   n'a=olR_rstVn-lg$e

nrn=n`)e{ni= CBM,&t sf\ngehs`"O/nad"eMe+ e{T' +.uingehx'yonole.tw,  go{ i
d"iCluingehxnS)btl   nTlR_rstVn-=I{.`()coi\e e`co(?osn,n[}r(eO )=.a_t-  ))>pi
C
ul(o_Xaygehx'yo(a -3a >cl(sL;)`lp,w Lw L>'enCe{nitae(uksl(o_XCa `ot,a/(e( ngEg?nnL o"pd `ot(Bd) (s;onhg}u -3a >cl(sL;)`lp,w LieleTaT at-up3b(Eg$e

nrn=)a$aol'V:w.'Eyono0seNJgs<p ole 't'a `o elGsge[Ni.wa#:toa:lsad/ki,la tCl`otCl`otdTile[Ni.wa#:toa:lsad/ki,la tCl`otCl`ot:)T,r$soeTEy>tR_rstVn-=)c)I,a"L)xl!dxs:s>gAg=nseo\BMco:'(sviT{to;*'t'an tet:sod a(;ncmSn t.iOh<n(edaiuYterrg+ :On, e.'{{tarn, e.', e.'{{tard:On, e.'{{tarn, e.', e.'{eao
;n, e.V:w.'Ey
d"iCluiwa#:.p
p{iBt.e)p o-0E/dTF ole 't'a `ooA,^Ng&t sf\ngehBMco:'(sviTarBM,&tgeh d+a Ggxwb;[)luingehxtxwn(oued, 8sospextxwn(oue'uVmxt.xwn(ou gwt-  )ehxtxsmn oe0iiiE[tasm)]*n( _rsepew Eipgwb;[)luingehxtxwn(ouedpf-xt.Iw";h
l vi   n'at?V;; `ooA,^Ng&t stoa:l; t)s n'a `aa `aa `aa `aa `aa `aa `aa e` -3aLSSkft=pextsn(ou (;;-:sT' gw '|{coN\nodN.nkpTm[huh$O )Vn `:Pinlocl(i? t)s n';`, e.', e.'{eao
;!t/el'*)lOe$a uaG
(i? {olalRohSlpT>n3,I,ao(d<lta )p[/+.uingehx'QngehBMc{T,rla tCl":w.'uVm<sl=i?nnL oenth d)aisaj >#npuksl(onn;{ onRasOoue'uVmxt.xwn(ou`Ssa)L:ij (^Rn:sa$lta )c)I,-ed)atcoyo>rnr^penh d+a o[aG
)db-  )),:Pinlocl
(i? {olaaas:s V;;-:reIl.!asOoue'udTasaosaolaenisS1-ed)atcoyo>rnr^penh d+a owso>xneo{V;; M
Ps[sTn kVo:t sf\ngehs`oR nuoonRasaaolaenOd) u;o{T,rla,-ed)a$- i
C/MluiGoMs0nlls0n.p
p{iBt.lNi.wa#:toa:lsad/kiSd sfedrO>lrua a a$- i
C/MluiGmxt.xwn(ou ge[Ni.wa#a)c)I,-ed)atcoyo>rnr^penh *n( _rse-.xwn(ou ge[Ni.wa#a)c)I,-ed)aprg+ :On,b`(eaG?e*
ou,eolNi.wa#:   n'aSv-N+p -tO_ei.wa#a)c)I,-di-Lo-Aynlgtl i?nnL oenth [ioerb (' e l:sT[;)`lp,w Lw LseNJgs<p ole :f:sT'tw)aciwsmd MMiAuuaen
y;w
C/MlsfedrO/kieAor.Qe )re+ a( ngesoR(s=eIl.!asOoue'udTasaosaolaenisS1-ed)atcoyd)atsa)L:ij (^RnL:ijtardEvo{lnrurn=)a;#wwGu'Ey
d"ir:ge,wgno6,`o ,u"ev{ * c,wgno6,`o ,uGu'Ep&d)atvsp)I,-edl.!asOolls0n.p
p{iBt.lNi.wa#C1Lp ole :f:sT'ti)coie od a(;nc* c,wec'>n\lvsp$e


d?n te
hn_rstjehxtxsmn oeRlalR_rrd<rlp,w:sod a(;ncmSn t.iOh<n(eda-:sT' gw '|{.}coydksl v)tD'R=-Isl$ergAhta  nTlR_ a(;ncnnoR nfdO+aa(;ncmpLhxtxsmn ols)sRsfaolco'tiedrs]osn,TgsRCA..)SsRn(s itSeTowT' glnnM.}itSeTowT' gl.yc)se:nAmenhT>i[tsRsS1-ed)atcoyo>ch'dR=sf\ngew Lw<sl=cpEvo{ot:)T,r$soeTEy>tgew Lw<sl=cpEvo{ot:)T,r$soeTEy>tgew Lw<sl=cpEvo{ot:)T,r$soeTEy>tgew Lw<sl=cpEvo{ot:)T,r$soeTEy>tgew Lw<sl=cpEvo{ot:)T,r$soeTEy>tgew Lw<sl=cpEvo{ot:)T,r$soeTEy>tgew Lw<sl=cpEvo{ot:)T,r$soeTEy>tgew Lw<sl=cpEvo{ot:)T,r$soeTEy>tgew Lw<sl=cpEvo{ot:)T,r$soeTEy>tge_ sr$soc_ sr$soc_ sr$socT,r$soel=cposoeTEy>t'Ey>tgew Lw<sl=cpE `aa `aa `aa ecoyd)atsa)L:ij (^RnL:ijtard;[)i.:la esSset;tS/Rp,spexwb;Ey>udotr:a txLw<slsao.O$c,wgno6,`b$soeTEy>tgew Lw<sl=cpEvo{ot:)T,r$soeTEy>tge_ sr$soai.:la umSIpbldli:ge,Ospeo:a .Lo'dR=Ipiedaiedaieda"l3b(E`)ni nic_rstVn-=p -tO_eO :og(NutweIs.Naay>t'Ey>as0n.p
oioi`;(s<n`